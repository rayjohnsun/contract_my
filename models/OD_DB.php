<?php
namespace app\models;

use app\models\org_diller\ODContract;
use app\models\dil_buyer\DBContract;

class OD_DB extends \app\base\AModel {

  public static function tableName() {
    return 'd_od_db';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'od_id' => 'Organization',
      'db_id' => 'Diller',
    ];
  }

  public function getODContract() {
    return $this->hasOne(ODContract::className(), ['id' => 'od_id']);
  }

  public function getDBContract() {
    return $this->hasOne(DBContract::className(), ['id' => 'db_id']);
  }
  
}
