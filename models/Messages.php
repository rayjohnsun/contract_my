<?php
namespace app\models;

use Yii;
use app\components\Status;

class Messages extends \app\base\AModel {

  public static function tableName() {
    return 'b_messages';
  }

  public function rules() {
    return [
      [['user_id', 'status'], 'integer']
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'user_id' => 'Пользователь',
      'txt' => 'Сообщения',
      'link' => 'Ссылка',
      'status' => 'Статус',
      'created_at' => 'Создан',
      'readed_at' => 'Прочитан',
    ];
  }

  public function getUser() {
    return $this->hasOne(Users::className(), ['id' => 'user_id']);
  }

  public function runBeforeSave() {
    if ($this->isNewRecord) {
      $this->status = Status::NOACTIVE;
    }
  }

}
