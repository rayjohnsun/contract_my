<?php
namespace app\models;

use Yii;
use app\components\Status;
use app\models\org_buyer\OBContractProd;
use app\models\org_diller\ODContractProd;

class Products extends \app\base\AModel {
	public static function tableName() {
		return 'b_products';
	}

	public function rules() {
		return [
			[['title_en', 'title_ru', 'description_en', 'description_ru', 'code', 'unit', 'country_id'], 'required'],
			[['price'], 'number'],
			[['status', 'country_id'], 'integer'],
			[['description_en', 'description_ru'], 'string', 'max' => 250],
			[['title_en', 'title_ru'], 'string', 'max' => 200],
			[['code', 'unit'], 'string', 'max' => 50],
			[['title_en', 'title_ru', 'description_en', 'description_ru', 'code'], 'trim'],
			[['status'], 'default', 'value' => Status::ACTIVE],
			[['status'], 'in', 'range' => Status::list()],
		];
	}

	public function attributeLabels() {
		return [
			'id' => 'ID',
			'title_en' => 'Название VT (en)',
			'title_ru' => 'Название VT (ru)',
			'description_en' => 'Название LTZ (en)',
			'description_ru' => 'Название LTZ (ru)',
			'code' => 'Код ТНВЭД',
			'country_id' => 'Страна',
			'unit' => 'Ед. из.',
			'price' => 'Цена',
			'status' => 'Статус',
			'created_at' => 'Создан',
			'updated_at' => 'Редактирован',
		];
	}

	public function getCountry() {
    return $this->hasOne(Countries::className(),['id'=>'country_id']);
  }
  
	public function getTitleRuEn() {
		return $this->title_ru . ' / ' . $this->title_en;
	}

	public function getDescrRuEn() {
		return $this->description_ru . ' / ' . $this->description_en;
	}

	public function returnBeforeDelete() {
    $cnt1 = ODContractProd::find()->where([
      'product_id' => $this->id,
    ])->count();
    $cnt2 = OBContractProd::find()->where([
      'product_id' => $this->id,
    ])->count();

    if ($cnt1<1&&$cnt2<1) {
      return true;
      Yii::$app->session->setFlash('success', "Successfully deleted");
    } else {
      Yii::$app->session->setFlash('error', "Error to delete. This product is used using for contracts.");
    }
    return false;
  }
  
}
