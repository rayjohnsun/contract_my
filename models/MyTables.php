<?php
namespace app\models;

use Yii;
use app\components\Status;

class MyTables extends \app\base\AModel {

  public static function tableName() {
    return 'i_table_of_users';
  }

  public function rules() {
    return [
      [['user_id', 'column1', 'column2', 'column3', 'column4', 'column5', 'column6', 'column7', 'column8', 'column9', 'column10', 'column11', 'column12', 'column13', 'column14', 'column15', 'column16', 'column17', 'column18', 'column19', 'column20', 'column21', 'column22', 'column23', 'column24', 'column25', 'column26', 'column27', 'column28', 'column29', 'column30', 'column31', 'column32', 'column33', 'column34'], 'integer'],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'user_id' => 'Пользователь',
      'txt' => 'Сообщения',
      'link' => 'Ссылка',
      'status' => 'Статус',
      'created_at' => 'Создан',
      'readed_at' => 'Прочитан',
      'column1' => 'column1',
      'column2' => 'column2',
      'column3' => 'column3',
      'column4' => 'column4',
      'column5' => 'column5',
      'column6' => 'column6',
      'column7' => 'column7',
      'column8' => 'column8',
      'column9' => 'column9',
      'column10' => 'column10',
      'column11' => 'column11',
      'column12' => 'column12',
      'column13' => 'column13',
      'column14' => 'column14',
      'column15' => 'column15',
      'column16' => 'column16',
      'column17' => 'column17',
      'column18' => 'column18',
      'column19' => 'column19',
      'column20' => 'column20',
      'column21' => 'column21',
      'column22' => 'column22',
      'column23' => 'column23',
      'column24' => 'column24',
      'column25' => 'column25',
      'column26' => 'column26',
      'column27' => 'column27',
      'column28' => 'column28',
      'column29' => 'column29',
      'column30' => 'column30',
      'column31' => 'column31',
      'column32' => 'column32',
      'column33' => 'column33',
      'column34' => 'column34'
    ];
  }

  public function getUser() {
    return $this->hasOne(Users::className(), ['id' => 'user_id']);
  }

}
