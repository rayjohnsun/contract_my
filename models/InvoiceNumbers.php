<?php
namespace app\models;

use app\components\NumberShablones;
use app\models\org_diller\ODContract;

class InvoiceNumbers extends \app\base\AModel {

  public static function tableName() {
    return 'aa_invoice_numbers';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'num' => 'Номер',
      'number' => 'Номер',
      'pack_number' => 'Номер',
      'type' => 'Type'
    ];
  }

  public function saveAuto($type, $f_id) {    
    $f_id = (int)$f_id;
    $first = $f_id < 10 ? '-0'.$f_id : '-'.$f_id;
    $number = (int)static::find()->max('num') + 1;
    $this->type = $type;
    $this->num = $number;
    $this->number = NumberShablones::get($number, $type.$first);
    $this->pack_number = $this->number;
    return $this->save();
  }
  
}
