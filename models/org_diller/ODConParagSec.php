<?php
namespace app\models\org_diller;

use app\components\Helper;

class ODConParagSec extends \app\base\AModel {

  public static function tableName() {
    return 'c_od_con_parag_sec';
  }

  public function rules() {
    return [
      [['title_en', 'title_ru'], 'string'],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract ID',
      'parag_id' => 'Parag ID',
      'title_en' => 'Title En',
      'title_ru' => 'Title Ru',
    ];
  }

  public function getNew_title_en() {
    return Helper::decodeQuote($this->title_en);
  }

  public function getNew_title_ru() {
    return Helper::decodeQuote($this->title_ru);
  }

  public function runBeforeSave() {
    $this->title_en = Helper::encodeQuote($this->title_en);
    $this->title_ru = Helper::encodeQuote($this->title_ru);
  }

}
