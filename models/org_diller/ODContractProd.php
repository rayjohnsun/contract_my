<?php
namespace app\models\org_diller;

use app\models\Countries;
use app\models\Products;
use app\models\dil_buyer\DBContractProd;

class ODContractProd extends \app\base\AModel {

  public static function tableName() {
    return 'c_od_contract_prod';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract ID',
      'product_id' => 'Product ID',
      'title_en' => 'Название O&D (en)',
      'title_ru' => 'Название O&D (ru)',
      'description_en' => 'Название D&B (en)',
      'description_ru' => 'Название D&B (ru)',
      'unit' => 'Unit',
      'code' => 'Code',
      'price' => 'Price',
      'quantity' => 'Quantity',
      'total_price' => 'Total Price',
      'qty_db_contract' => 'Qty DB Contract',
      'qty_od_invoice' => 'Qty OD Invoice',
      'qty_od_com_invoice' => 'Qty OD Com. Invoice',
      'summ_db_contract' => 'Summ DB Contract',
      'summ_od_invoice' => 'Summ OD Invoice',
      'summ_od_com_invoice' => 'Summ OD Com. Invoice',
      'podtverjden' => 'Podtverjden',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'country_en' => 'Country',
      'country_ru' => 'Country'
    ];
  }

  /******************************GETTERS******************************/

  public function getContract() {
    return $this->hasOne(ODContract::className(), ['id' => 'contract_id']);
  }

  public function getProduct() {
    return $this->hasOne(Products::className(), ['id' => 'product_id']);
  }

  public function getDBContractProducts() {
    return $this->hasMany(DBContractProd::className(), ['od_contract_product_id' => 'id']);
  }

  public function getODInvoiceProducts() {
    return $this->hasMany(ODInvoiceProd::className(), ['od_contract_product_id' => 'id']);
  }

  public function getODComInvoiceProducts() {
    return $this->hasMany(ODComInvoiceProd::className(), ['od_contract_product_id' => 'id']);
  }

  public function getTitleRuEn() {
    return $this->title_ru . ' / ' . $this->title_en;
  }

  public function getDescrRuEn() {
    return $this->description_ru . ' / ' . $this->description_en;
  }

}
