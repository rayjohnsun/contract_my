<?php
namespace app\models\org_diller;

use Yii;
use app\components\Helper;
use app\components\POInvoice;
use app\components\Status;
use app\models\InvoiceNumbers;
use app\models\companies\Consignees;
use app\models\companies\Dillers;
use app\models\companies\Organizations;
use app\models\dil_buyer\DBInvoice;
use app\models\dil_buyer\DBInvoiceProd;
use yii\helpers\Url;

class ODInvoice extends \app\base\AModel {

  public static function tableName() {
    return 'c_od_invoice';
  }

  public function rules() {
    return [
      [['ndate', 'consignee_id'], 'required'],
      [['contract_id'], 'required', 'on' => static::SC_CREATE],
      [['org_text_ru', 'org_text_en', 'dil_text_ru', 'dil_text_en', 'cons_text_en', 'cons_text_ru', 'notes'], 'string'],
      [['receive_date', 'ndate'], 'string', 'max' => 50],
      [['receive_sum'], 'number'],
      [['receive_sum'], 'default', 'value' => 0],
      [['position', 'director_org_en', 'director_org_ru', 'place'], 'string', 'max' => 250]
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'ndate' => 'Дата',
      'number_id' => 'Number',
      'contract_id' => 'Контракт',
      'organization_id' => 'Продавец',
      'org_text_en' => 'Seller En',
      'org_text_ru' => 'Seller Ru',
      'diller_id' => 'Покупатель',
      'dil_text_en' => 'Buyer En',
      'dil_text_ru' => 'Buyer Ru',
      'consignee_id' => 'Грузополучатель',
      'cons_text_en' => 'Consignee En',
      'cons_text_ru' => 'Consignee Ru',
      'notes' => 'Notes',
      'position' => 'Position',
      'director_org_en' => 'Name',
      'director_org_ru' => 'Name',
      'director_dil_en' => 'Name',
      'director_dil_ru' => 'Name',
      'place' => 'Place',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'status' => 'Status',
      'nprice' => 'Итоговая цена',
      'receive_sum' => 'Сумма поступление',
      'receive_date' => 'Дата поступление',
      'currency' => 'Currency',
      'den' => 'День',
      'mesyats' => 'Месяц',
      'god' => 'Год',
      'po' => 'Purchase Order',
      'contract_no' => 'Номер контракта/PO',
      'primechanie' => 'Примечание',
      'po_db_inv_qty' => 'PO DB Остаток',
      'po_db_inv_nprice' => 'PO DB Баланс',
    ];
  }

  public function getContract() {
    return $this->hasOne(ODContract::className(), ['id' => 'contract_id']);
  }

  public function getOrganization() {
    return $this->hasOne(Organizations::className(), ['id' => 'organization_id']);
  }

  public function getDiller() {
    return $this->hasOne(Dillers::className(), ['id' => 'diller_id']);
  }

  public function getConsignee() {
    return $this->hasOne(Consignees::className(), ['id' => 'consignee_id']);
  }

  public function getNumber() {
    return $this->hasOne(InvoiceNumbers::className(), ['id' => 'number_id']);
  }

  public function getProducts() {
    return $this->hasMany(ODInvoiceProd::className(), ['invoice_id' => 'id']);
  }

  public function getPoChildInvoices() {
    return $this->hasMany(DBInvoice::className(), ['po_id' => 'id']);
  }

  public function getPackinglists() {
    return $this->hasMany(ODPackList::className(), ['invoice_id' => 'id']);
  }

  public function getNew_org_text_en() {
    return Helper::decodeQuote($this->org_text_en);
  }

  public function getNew_org_text_ru() {
    return Helper::decodeQuote($this->org_text_ru);
  }

  public function getNew_dil_text_en() {
    return Helper::decodeQuote($this->dil_text_en);
  }

  public function getNew_dil_text_ru() {
    return Helper::decodeQuote($this->dil_text_ru);
  }

  public function getNew_cons_text_en() {
    return Helper::decodeQuote($this->cons_text_en);
  }

  public function getNew_cons_text_ru() {
    return Helper::decodeQuote($this->cons_text_ru);
  }

  public function getNew_ndate() {
    return date('d.m.Y', strtotime($this->ndate));
  }

  public function getNew_notes() {
    return Helper::decodeQuote($this->notes);
  }

  public function runBeforeSave() {
    $this->org_text_en = Helper::encodeQuote($this->org_text_en);
    $this->org_text_ru = Helper::encodeQuote($this->org_text_ru);
    $this->dil_text_en = Helper::encodeQuote($this->dil_text_en);
    $this->dil_text_ru = Helper::encodeQuote($this->dil_text_ru);
    $this->cons_text_en = Helper::encodeQuote($this->cons_text_en);
    $this->cons_text_ru = Helper::encodeQuote($this->cons_text_ru);
    $this->notes = Helper::encodeQuote($this->notes);
    $this->ndate = date("Y-m-d", strtotime($this->ndate));
    if ($this->isNewRecord) {
      $this->number_id = 0;
    }
    if (!empty($this->receive_date)) {
      if (strtotime($this->receive_date) < strtotime($this->ndate)) {
        $this->receive_date = $this->ndate;
      }
    }
  }

  public function runAfterInsert() {
    if ($this->number_id == 0) {
      $num = new InvoiceNumbers();
      if($num->saveAuto('VT', @$this->contract->number->num)){
        $this->number_id = $num->id;
        $this->update();
      }
    }
    $this->sendMessage('Создали Инвойс VT&D: '.$this->number->number, Url::to(['org_diller/o-d-invoice/update', 'id' => $this->id]));
  }

  public function runBeforeDelete() {
    $packinglists = $this->packinglists;
    $products = $this->products;
    $this->clear($packinglists);
    $this->clear($products);
    $number = $this->number;
    if (!is_null($number)) {
      $number->delete();
    }
    if ($this->po == POInvoice::YES) {
      $po_chil_invoices = $this->poChildInvoices;
      $this->clear($po_chil_invoices);
    }
    Yii::$app->session->setFlash('success', 'Suuccessfully deleted.');
  }

  public function runAfterDelete() {
    $this->sendMessage('Удалили Инвойс VT&D: '.$this->number->number);
  }

  public function sendUpdatedMessage() {
    $this->sendMessage('Обнавили Инвойс VT&D: '.$this->number->number, Url::to(['org_diller/o-d-invoice/update', 'id' => $this->id]));
  }

  public function applyDefaults() {
    $this->director_org_en = 'Mr. Mirkamilov M.T.';
    $this->director_org_ru = 'Миркамилов М.Т.';
    $this->position = 'Директор / Director';
    $this->place = 'г. Ташкент, Узбекистан / Tashkent, Uzbekistan';
    $this->status = Status::ACTIVE;
  }

  public function selfProducts() {
    $self_prods = $this->products;
    $new_self_prods = [];
    if (!empty($self_prods)) {
      foreach ($self_prods as $key => $value) {
        $new_self_prods[$value->od_contract_product_id] = $value;
      }
    }
    return $new_self_prods;
  }

  public function availableProducts() {
    $products = Yii::$app->request->post('Products');
    $a_products = [];
    if (!empty($products)) {
      $prods = [];
      $qtys = [];
      foreach ($products as $key => $value) {
        $p_id = isset($value['product_id']) ? (int) $value['product_id'] : 0;
        if ($p_id > 0) {
          $prods[$p_id] = $p_id;
          if (isset($value['quantity'])) {
            // $qty = (isset($value['quantity']) && (int)$value['quantity'] > 0)?(int)$value['quantity']:1;
            $qty = isset($value['quantity'])?str_replace(' ', '', $value['quantity']):'';
            $qty = ((int)$qty > 0)?(int)$qty:1;
            if (isset($qtys[$p_id])) {
              $qtys[$p_id] += $qty;
            } else {
              $qtys[$p_id] = $qty;
            }
          } else {
            $qtys[$p_id] = -1;
          }
        }
      }
      $new_prods = ODContractProd::find()->where(['in', 'id', $prods])->andWhere(['contract_id' => $this->contract_id])->all();
      if (!empty($new_prods)) {
        foreach ($new_prods as $key2 => $value2) {
          $a_products[$value2->id]=['product' => $value2,'qty' => $qtys[$value2->id]];
        }
      }
    }
    return $a_products;
  }

  public function saveOne($post) {
    $product = $post['product'];
    if ($product->qty_od_invoice > 0) {
      $model = new ODInvoiceProd();
      $model->contract_id = $this->contract_id;
      $model->invoice_id = $this->id;
      $model->od_contract_product_id = $product->id;
      $model->title_en = $product->title_en;
      $model->title_ru = $product->title_ru;
      $model->description_en = $product->description_en;
      $model->description_ru = $product->description_ru;
      $model->unit = $product->unit;
      $model->code = $product->code;
      $model->country_en = $product->country_en;
      $model->country_ru = $product->country_ru;
      $model->price = $product->price;
      $model->quantity = $post['qty'];
      if ($model->quantity > $product->qty_od_invoice) {
        $model->quantity = $product->qty_od_invoice;
        $this->response['messages'][] = ['id' => $product->id, 'text' => "Доступное количество для продукта `{$model->description_en}` -> `{$model->quantity}`, автоматом сохранили максимальное количество."];
      } else {
        $this->response['messages'][] = [
          'id' => $product->id, 
          'text' => "Продукт `{$product->description_en}` успешно добавлен"
        ];
      }
      $model->total_price = ($model->quantity * $model->price);
      $model->net_weight = 0;
      $model->gross_weight = 0;
      $model->save();
    } else {
      $this->response['messages'][] = ['id' => $product->id, 'text' => "Нет доступное количество продукт `{$product->description_en}`."];
    }
  }

  public function deleteOne($sp) {
    $sp->delete();
    $this->response['messages'][] = [
      'id' => $sp->od_contract_product_id,
      'text' => "Продукт `{$sp->description_en}` успешно удален"
    ];
  }

  public function updateOne($sp, $post) {
    $product = $post['product'];
    $o_qty = $sp->quantity;
    if ($post['qty'] != -1) {
      $sp->quantity = $post['qty'];
    }
    $soob = true;
    if ($sp->quantity > ($o_qty+$product->qty_od_invoice)) {
      $sp->quantity = ($o_qty+$product->qty_od_invoice);
      if ($sp->quantity > 0) {
        $this->response['messages'][] = ['id' => $product->id, 'text' => "Доступное количество для продукта `{$sp->description_en}` -> `{$sp->quantity}`, автоматом сохранили максимальное количество."];
        $soob = false;
      }
    }
    if ($sp->quantity > 0) {
      if ($sp->quantity != $o_qty && $soob == true) {
        $this->response['messages'][] = ['id' => $product->id, 'text' => "Продукт `{$sp->description_en}` успешно обнавлен"];
      }
      $sp->total_price = ($sp->quantity * $sp->price);
      $sp->save();
    } else {
      $this->deleteOne($sp);
    }
  }

  public function saveProducts() {

    $contract = $this->contract;
    if ($contract) {
      $invoice_id = $this->id;
      $products = $this->availableProducts();
      $self_prods = $this->selfProducts();
      if (!empty($products)) {
        if (!empty($self_prods)) {
          $ostatok = $self_prods;
          foreach ($self_prods as $kp => $sp) {
            if (isset($products[$kp])) {
              $this->updateOne($sp, $products[$kp]);
              unset($ostatok[$kp]);
              unset($products[$kp]);
            }
          }
          if (!empty($ostatok)) {
            foreach ($ostatok as $sp) {
              $this->deleteOne($sp);
            }
          }
          if (!empty($products)) {
            foreach ($products as $product) {
              $this->saveOne($product);
            }
          }
          $this->response['code'] = 253;
        } else {
          foreach ($products as $product) {
            $this->saveOne($product);
          }
          $this->response['code'] = 252;
        }
      } else {
        if (!empty($self_prods)) {
          foreach ($self_prods as $sp) {
            $this->deleteOne($sp);
          }
          $this->response['code'] = 251;
        }
      }
      $this->refreshBalance();
      $contract->refreshBalance();
    } else {
      $this->response['message'] = 'Контракт не найден';
      $this->response['code'] = 170;
      $this->response['status'] = false;
    }
  }

  public function getPODBInvoiceProdQty($prod_id) {
    return (int)DBInvoiceProd::find()->where([
      'od_po_invoice_product_id' => $prod_id,
      'po_id' => $this->id
    ])->sum('quantity');
  }

  public function refreshBalance() {
    // $new_nprice = (float)ODInvoiceProd::find()->where([
    //   'contract_id' => $this->contract_id, 
    //   'invoice_id' => $this->id
    // ])->sum('total_price');

    $po_db_inv_prod_qty = 0;
    $po_db_inv_prod_nprice = 0;
    $total_nprice = 0;

    $products = ODInvoiceProd::find()->where(['invoice_id' => $this->id])->all();
    if (!empty($products)) {
      foreach ($products as $key => $value) {

        $all_po_db_inv_prod_qty = $this->getPODBInvoiceProdQty($value->id);
        $new_po_db_inv_qty = ($value->quantity-$all_po_db_inv_prod_qty)>=0 ? ($value->quantity-$all_po_db_inv_prod_qty) : 0;
        $po_db_inv_prod_qty+=$new_po_db_inv_qty;
        $value->qty_po_db_invoice = $new_po_db_inv_qty;
        $value->summ_po_db_invoice = ($value->qty_po_db_invoice * $value->price);

        $value->total_price = ($value->quantity * $value->price);

        $po_db_inv_prod_nprice += $value->summ_po_db_invoice;
        $total_nprice += $value->total_price;

        $value->update();

      }
    }

    $this->po_db_inv_qty = $po_db_inv_prod_qty;
    $this->po_db_inv_nprice = $po_db_inv_prod_nprice;
    $this->nprice = $total_nprice;

    $this->update();

  }

  public function getModelName() {
    $name = explode('\\', __CLASS__);
    return array_pop($name);
  }
  
}
