<?php
namespace app\models\org_diller\search;

use app\components\Helper;
use app\components\Podtverjdeno;
use app\components\Zakryto;
use app\components\Zapolnen;
use app\models\ContractNumbers;
use app\models\org_diller\ODContract;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ODContractSearch extends ODContract {

  public $check_dbcon_prod_nprice = 0;
  public $check_inv_prod_nprice = 0;
  public $check_com_inv_prod_nprice = 0;
  public $check_total_lprice = 10;
  public $vt_ids = [];
  public $nnumber='';

  public function rules() {
    return [
      [['id', 'organization_id', 'diller_id', 'status', 'check_dbcon_prod_nprice', 'check_inv_prod_nprice', 'check_com_inv_prod_nprice', 'check_total_lprice', 'number_id', 'zapolnen'], 'integer'],
      [['nnumber', 'date_en', 'date_ru', 'adres_en', 'adres_ru', 'text_en', 'text_ru', 'created_at', 'updated_at', 'vt_ids'], 'safe'],
      [['dbcon_prod_nprice', 'inv_prod_nprice', 'total_nprice', 'inv_prod_lprice', 'total_lprice', 'receive_sum'], 'number'],
    ];
  }

  public function scenarios() {

    return Model::scenarios();
  }

  public function search($params) {
    $query = ODContract::find()->joinWith('number');
    
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'organization_id' => $this->organization_id,
      'diller_id' => $this->diller_id,
      'number_id' => $this->number_id,
      'status' => $this->status,
      'zapolnen' => $this->zapolnen
    ]);

    if ($this->check_dbcon_prod_nprice == Zakryto::DA) {
      $query->andFilterWhere(['dbcon_prod_nprice' => 0]);
    } elseif ($this->check_dbcon_prod_nprice == Zakryto::NET) {
      $query->andFilterWhere(['>', 'dbcon_prod_nprice', 0]);
    }

    if ($this->check_inv_prod_nprice == Zakryto::DA) {
      $query->andFilterWhere(['inv_prod_nprice' => 0]);
    } elseif ($this->check_inv_prod_nprice == Zakryto::NET) {
      $query->andFilterWhere(['>', 'inv_prod_nprice', 0]);
    }

    if ($this->check_com_inv_prod_nprice == Zakryto::DA) {
      $query->andFilterWhere(['com_inv_prod_nprice' => 0]);
    } elseif ($this->check_com_inv_prod_nprice == Zakryto::NET) {
      $query->andFilterWhere(['>', 'com_inv_prod_nprice', 0]);
    }

    if ($this->check_total_lprice != '') {
      if ($this->check_total_lprice == Podtverjdeno::DA) {
        $query->andFilterWhere(['total_lprice' => 0])
          ->andFilterWhere(['zapolnen' => Zapolnen::YES]);
      } elseif ($this->check_total_lprice == Podtverjdeno::NET) {
        $query->andFilterWhere(['>', 'total_lprice', 0])
          ->andFilterWhere(['zapolnen' => Zapolnen::YES]);
      } else if ($this->check_total_lprice == Podtverjdeno::EMPTY) {
        $query->andFilterWhere(['zapolnen' => Zapolnen::NO]);
      }
    }

    $query->andFilterWhere(['like', 'date_en', $this->date_en])
      ->andFilterWhere(['like', ContractNumbers::tableName().'.number', $this->nnumber])
      ->andFilterWhere(['like', 'date_ru', $this->date_ru])
      ->andFilterWhere(['like', 'adres_en', $this->adres_en])
      ->andFilterWhere(['like', 'adres_ru', $this->adres_ru])
      ->andFilterWhere(['like', 'text_en', $this->text_en])
      ->andFilterWhere(['like', 'text_ru', $this->text_ru])
      ->andFilterWhere(['like', 'total_nprice', $this->total_nprice])
      ->andFilterWhere(['like', 'inv_prod_lprice', $this->inv_prod_lprice])
      ->andFilterWhere(['like', 'total_lprice', $this->total_lprice])
      ->andFilterWhere(['like', 'receive_sum', $this->receive_sum])
      ->andFilterWhere(['in', 'id', array_keys($this->vt_ids)])
      ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)]);

    return $dataProvider;
  }
}
