<?php
namespace app\models\org_diller\search;

use app\components\Helper;
use app\components\POInvoice;
use app\models\InvoiceNumbers;
use app\models\org_diller\ODInvoice;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ODPOInvoiceSearch extends ODInvoice {

  public $invoice_ids = [];
  public $nnumber='';

  public function rules() {
    return [
      [['id', 'contract_id', 'organization_id', 'diller_id', 'consignee_id', 'status', 'number_id'], 'integer'],
      [['invoice_ids', 'nnumber', 'ndate', 'org_text_ru', 'org_text_en', 'dil_text_ru', 'dil_text_en', 'cons_text_en', 'cons_text_ru', 'notes', 'position', 'director_org_en', 'director_org_ru', 'place', 'created_at', 'updated_at'], 'safe'],
      [['nprice', 'receive_sum'], 'number'],
    ];
  }

  public function scenarios() {
    return Model::scenarios();
  }

  public function search($params) {
    $query = ODInvoice::find()->joinWith('number');

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'number_id' => $this->number_id,
      'contract_id' => $this->contract_id,
      'organization_id' => $this->organization_id,
      'diller_id' => $this->diller_id,
      'consignee_id' => $this->consignee_id,
      'status' => $this->status,
      'po' => POInvoice::YES
    ]);

    $query->andFilterWhere(['like', 'org_text_ru', $this->org_text_ru])
      ->andFilterWhere(['like', InvoiceNumbers::tableName().'.number', $this->nnumber])
      ->andFilterWhere(['like', 'org_text_en', $this->org_text_en])
      ->andFilterWhere(['like', 'dil_text_ru', $this->dil_text_ru])
      ->andFilterWhere(['like', 'dil_text_en', $this->dil_text_en])
      ->andFilterWhere(['like', 'cons_text_en', $this->cons_text_en])
      ->andFilterWhere(['like', 'cons_text_ru', $this->cons_text_ru])
      ->andFilterWhere(['like', 'notes', $this->notes])
      ->andFilterWhere(['like', 'position', $this->position])
      ->andFilterWhere(['like', 'director_org_en', $this->director_org_en])
      ->andFilterWhere(['like', 'director_org_ru', $this->director_org_ru])
      ->andFilterWhere(['like', 'place', $this->place])
      ->andFilterWhere(['like', 'nprice', $this->nprice])
      ->andFilterWhere(['like', 'receive_sum', $this->receive_sum])
      ->andFilterWhere(['like', 'ndate', Helper::dt($this->ndate)])
      ->andFilterWhere(['in', ODInvoice::tableName().'.id', array_keys($this->invoice_ids)])
      ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)]);

    return $dataProvider;
  }
}
