<?php
namespace app\models\org_diller\search;

use app\models\org_diller\ODParagraph;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ODParagraphSearch extends ODParagraph {

  public function rules() {
    return [
      [['id', 'norder', 'status'], 'integer'],
      [['title_en', 'title_ru'], 'safe'],
    ];
  }

  public function scenarios() {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  public function search($params) {
    $query = ODParagraph::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['norder' => SORT_ASC]],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'norder' => $this->norder,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', 'title_en', $this->title_en])
      ->andFilterWhere(['like', 'title_ru', $this->title_ru]);

    return $dataProvider;

  }
}
