<?php
namespace app\models\org_diller\search;

use app\components\Helper;
use app\models\InvoiceNumbers;
use app\models\org_diller\ODPackList;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ODPackListSearch extends ODPackList {

  public $nnumber='';

  public function rules() {
    return [
      [['id', 'contract_id', 'invoice_id', 'status', 'number_id'], 'integer'],
      [['nnumber', 'ndate', 'notes', 'created_at', 'updated_at', 'container_no', 'poddon'], 'safe'],
    ];
  }

  public function scenarios() {
    return Model::scenarios();
  }

  public function search($params) {
    $query = ODPackList::find()->joinWith('number');

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'invoice_id' => $this->invoice_id,
      'contract_id' => $this->contract_id,
      'number_id' => $this->number_id,
      'ndate' => $this->ndate,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', 'notes', $this->notes])
      ->andFilterWhere(['like', InvoiceNumbers::tableName().'.pack_number', $this->nnumber])
      ->andFilterWhere(['like', 'container_no', $this->container_no])
      ->andFilterWhere(['like', 'poddon', $this->poddon])
      ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)]);

    return $dataProvider;
  }
}
