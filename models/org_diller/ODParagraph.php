<?php
namespace app\models\org_diller;

class ODParagraph extends \app\base\AModel {

  public static function tableName() {
    return 'c_od_paragraph';
  }

  public function rules() {
    return [
      [['title_en', 'title_ru'], 'required'],
      [['norder', 'status'], 'integer'],
      [['title_en', 'title_ru'], 'string', 'max' => 250],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'title_en' => 'Title En',
      'title_ru' => 'Title Ru',
      'norder' => 'Norder',
      'status' => 'Status',
    ];
  }

  public function getSections() {
    return $this->hasMany(ODParagraphSec::className(), ['paragraph_id' => 'id']);
  }

  public function setSections($sections) {
    $this->sections = $sections;
  }

  public function runBeforeDelete() {
    ODParagraphSec::deleteAll(['paragraph_id' => $this->id]);
  }
}
