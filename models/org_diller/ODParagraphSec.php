<?php
namespace app\models\org_diller;

use app\components\Helper;

class ODParagraphSec extends \app\base\AModel {

  public static function tableName() {
    return 'c_od_paragraph_sec';
  }

  public function rules() {
    return [
      [['paragraph_id'], 'required'],
      [['paragraph_id', 'norder', 'status'], 'integer'],
      [['title_en', 'title_ru'], 'string'],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'paragraph_id' => 'Paragraph ID',
      'title_en' => 'Title En',
      'title_ru' => 'Title Ru',
      'norder' => 'Norder',
      'status' => 'Status',
    ];
  }

  public function getParagraph() {
    return $this->hasOne(ODParagraph::className(), ['id' => 'paragraph_id']);
  }

  public function getNTitle() {
    return $this->id;
  }

  public function getNew_Title_En() {
    return Helper::decodeQuote($this->title_en);
  }

  public function getNew_Title_Ru() {
    return Helper::decodeQuote($this->title_ru);
  }

  public function runBeforeSave() {
    $this->title_en = Helper::encodeQuote($this->title_en);
    $this->title_ru = Helper::encodeQuote($this->title_ru);
  }
}
