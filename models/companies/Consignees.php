<?php
namespace app\models\companies;

use Yii;
use app\models\Countries;
use app\models\dil_buyer\DBInvoice;
use app\models\org_buyer\OBInvoice;
use app\models\org_diller\ODInvoice;

class Consignees extends \app\base\AModel {
  public static function tableName() {
    return 'a_consignees';
  }

  public function rules() {
    return [
      [['title_en', 'title_ru', 'country_id'], 'required'],
      [['country_id', 'status'], 'integer'],
      [['created_at', 'updated_at'], 'safe'],
      [['title_en', 'title_ru', 'adres_en', 'adres_ru'], 'string', 'max' => 250],
      [['phone', 'email', 'director_en', 'director_ru'], 'string', 'max' => 100],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'title_en' => 'Названия En',
      'title_ru' => 'Названия Ru',
      'adres_en' => 'Адрес En',
      'adres_ru' => 'Адрес Ru',
      'phone' => 'Номер тел.',
      'email' => 'Email',
      'country_id' => 'Страна',
      'director_en' => 'Директор (En)',
      'director_ru' => 'Директор (Ru)',
      'status' => 'Статус',
      'created_at' => 'Создан',
      'updated_at' => 'Редактирован',
    ];
  }

  public function getCountry() {
    return $this->hasOne(Countries::className(),['id'=>'country_id']);
  }

  public function returnBeforeDelete() {
    $cnt1 = ODInvoice::find()->where([
      'consignee_id' => $this->id,
    ])->count();
    $cnt2 = DBInvoice::find()->where([
      'consignee_id' => $this->id,
    ])->count();
    $cnt3 = OBInvoice::find()->where([
      'consignee_id' => $this->id,
    ])->count();

    if ($cnt1<1&&$cnt2<1&&$cnt3<1) {
      return true;
    } else {
      Yii::$app->session->setFlash('message', "Error to delete: Consignee <strong>{$this->nTitle}</strong> is used.");
    }
    return false;
  }

}
