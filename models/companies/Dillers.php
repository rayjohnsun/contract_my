<?php
namespace app\models\companies;

use app\models\Countries;
use Yii;

class Dillers extends \app\base\AModel {
  public static function tableName() {
    return 'a_dillers';
  }

  public function rules() {
    return [
      [['title_en', 'title_ru', 'country_id', 'director_en', 'director_ru', 'director2_en', 'director2_ru', 'code'], 'required'],
      [['country_id', 'status'], 'integer'],
      [['title_en', 'title_ru', 'adres_en', 'adres_ru'], 'string', 'max' => 250],
      [['phone', 'email', 'director_en', 'director_ru', 'director2_en', 'director2_ru'], 'string', 'max' => 100],
      [['code'], 'string', 'max' => 30],
      [['created_at', 'updated_at'], 'safe'],
      [['corporate_id', 'swift_code', 'cif', 'account_no'], 'string', 'max' => 100],
      [['bank', 'bank_address', 'iban', 'bank_correspondent'], 'string', 'max' => 100],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'title_en' => 'Названия En',
      'title_ru' => 'Названия Ru',
      'adres_en' => 'Адрес En',
      'adres_ru' => 'Адрес Ru',
      'phone' => 'Номер тел.',
      'email' => 'Email',
      'country_id' => 'Страна',
      'director_en' => 'VT&D Директор (En)',
      'director_ru' => 'VT&D Директор (Ru)',
      'director2_en' => 'D&B Директор (En)',
      'director2_ru' => 'D&B Директор (Ru)',
      'status' => 'Статус',
      'created_at' => 'Создан',
      'updated_at' => 'Редактирован',
      'code' => 'Код',
      'corporate_id' => 'Corporate Id',
      'swift_code' => 'Swift Code',
      'cif' => 'Cif',
      'account_no' => 'Account No',
      'bank' => 'Bank',
      'bank_address' => 'Bank Address',
      'iban' => 'Iban',
      'bank_correspondent' => 'Bank Correspondent',
    ];
  }

  public function getCountry() {
    return $this->hasOne(Countries::className(), ['id' => 'country_id']);
  }

  public function returnBeforeDelete() {
    $cnt1 = ODContract::find()->where([
      'diller_id' => $this->id,
    ])->count();
    $cnt2 = ODInvoice::find()->where([
      'diller_id' => $this->id,
    ])->count();
    $cnt3 = DBContract::find()->where([
      'diller_id' => $this->id,
    ])->count();
    $cnt4 = DBInvoice::find()->where([
      'diller_id' => $this->id,
    ])->count();
    $_id = static::find()->orderBy([
      'id' => SORT_ASC,
    ])->one()->id;

    if ($cnt1<1&&$cnt2<1&&$cnt3<1&&$cnt4<1&&$_id!=$this->id){
      return true;
    } else {
      Yii::$app->session->setFlash('message', "Error to delete: Diller <strong>{$this->nTitle}</strong> is used.");
    }
    return false;
  }
  
}
