<?php
namespace app\models;

use app\components\NumberShablones;
use app\models\org_diller\ODContract;

class ContractNumbers extends \app\base\AModel {

  public static function tableName() {
    return 'aa_contract_numbers';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'num' => 'Номер',
      'number' => 'Номер',
      'type' => 'Type'
    ];
  }

  public function saveAuto($type) {    
    $number = (int)static::find()->max('num') + 1;
    $this->type = $type;
    $this->num = $number;
    $this->number = NumberShablones::get($number, $this->type);
    return $this->save();
  }
  
}
