<?php

namespace app\models\search;

use app\components\Helper;
use app\models\Messages;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class MessagesSearch extends Messages {

  public function rules() {
    return [
      [['id', 'user_id', 'status'], 'integer'],
      [['txt', 'link', 'created_at', 'readed_at'], 'safe'],
    ];
  }

  public function scenarios() {
    return Model::scenarios();
  }

  public function search($params) {
    $query = Messages::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'user_id' => $this->user_id,
      'status' => $this->status
    ]);

    $query->andFilterWhere(['like', 'txt', $this->txt])
      ->andFilterWhere(['like', 'link', $this->link])
      ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)])
      ->andFilterWhere(['like', 'readed_at', Helper::dt($this->readed_at)]);

    return $dataProvider;
  }
}
