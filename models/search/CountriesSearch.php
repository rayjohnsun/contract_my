<?php
namespace app\models\search;

use app\components\Helper;
use app\models\Countries;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CountriesSearch extends Countries {

  public function rules() {
    return [
      [['id', 'status'], 'integer'],
      [['title_en', 'title_ru', 'created_at', 'updated_at'], 'safe'],
    ];
  }

  public function scenarios() {
    return Model::scenarios();
  }

  public function search($params) {
    $query = Countries::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', 'title_en', $this->title_en])
      ->andFilterWhere(['like', 'title_ru', $this->title_ru])
      ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)]);

    return $dataProvider;
  }
}
