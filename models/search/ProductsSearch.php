<?php
namespace app\models\search;

use app\components\Helper;
use app\models\Products;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ProductsSearch extends Products {

  public function rules() {
    return [
      [['id', 'status', 'country_id'], 'integer'],
      [['title_en', 'title_ru', 'description_en', 'description_ru', 'code', 'unit', 'created_at'], 'safe'],
      [['price'], 'number'],
    ];
  }

  public function scenarios() {

    return Model::scenarios();
  }

  public function search($params) {
    $query = Products::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'price' => $this->price,
      'unit' => $this->unit,
      'status' => $this->status,
      'country_id' => $this->country_id
    ]);

    $query->andFilterWhere(['like', 'title_en', $this->title_en])
      ->andFilterWhere(['like', 'title_ru', $this->title_ru])
      ->andFilterWhere(['like', 'description_en', $this->description_en])
      ->andFilterWhere(['like', 'description_ru', $this->description_ru])
      ->andFilterWhere(['like', 'code', $this->code])
      ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)]);

    return $dataProvider;
  }
}
