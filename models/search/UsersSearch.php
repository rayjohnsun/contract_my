<?php

namespace app\models\search;

use Yii;
use app\components\Helper;
use app\models\Users;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UsersSearch extends Users
{

    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['username', 'email', 'created_at', 'role_id', 'fio'], 'safe'],
        ];
    }

    public function scenarios()
    {        
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'                => $this->id,
            'status'            => $this->status,
            'role_id'           => $this->role_id,
            // 'organization_id'   => $this->organization_id,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)]);

        return $dataProvider;
    }
}
