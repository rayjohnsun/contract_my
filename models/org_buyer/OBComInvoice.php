<?php
namespace app\models\org_buyer;

use Yii;
use app\components\Helper;
use app\components\Status;
use app\models\ComInvoiceNumbers;
use app\models\companies\Consignees;
use app\models\companies\Buyers;
use app\models\companies\Organizations;
use yii\helpers\Url;

class OBComInvoice extends \app\base\AModel {

  public static function tableName() {
    return 'h_ob_com_invoice';
  }

  public function rules() {
    return [
      [['ndate', 'consignee_id'], 'required'],
      [['contract_id'], 'required', 'on' => static::SC_CREATE],
      [['org_text_ru', 'org_text_en', 'buy_text_ru', 'buy_text_en', 'cons_text_en', 'cons_text_ru', 'notes'], 'string'],
      [['receive_date', 'ndate'], 'string', 'max' => 50],
      [['receive_sum'], 'number'],
      [['receive_sum'], 'default', 'value' => 0],
      [['position', 'director_org_en', 'director_org_ru', 'place'], 'string', 'max' => 250]
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'ndate' => 'Дата',
      'number_id' => 'Number',
      'contract_id' => 'Контракт',
      'organization_id' => 'Продавец',
      'org_text_en' => 'Seller En',
      'org_text_ru' => 'Seller Ru',
      'buyer_id' => 'Покупатель',
      'buy_text_en' => 'Buyer En',
      'buy_text_ru' => 'Buyer Ru',
      'consignee_id' => 'Грузополучатель',
      'cons_text_en' => 'Consignee En',
      'cons_text_ru' => 'Consignee Ru',
      'notes' => 'Notes',
      'position' => 'Position',
      'director_org_en' => 'Name',
      'director_org_ru' => 'Name',
      'place' => 'Place',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'status' => 'Status',
      'nprice' => 'Итоговая цена',
      'receive_sum' => 'Сумма поступление',
      'receive_date' => 'Дата поступление',
      'currency' => 'Currency',
      'den' => 'День',
      'mesyats' => 'Месяц',
      'god' => 'Год'
    ];
  }

  public function getContract() {
    return $this->hasOne(OBContract::className(), ['id' => 'contract_id']);
  }

  public function getOrganization() {
    return $this->hasOne(Organizations::className(), ['id' => 'organization_id']);
  }

  public function getDiller() {
    return $this->hasOne(Buyers::className(), ['id' => 'buyer_id']);
  }

  public function getConsignee() {
    return $this->hasOne(Consignees::className(), ['id' => 'consignee_id']);
  }

  public function getNumber() {
    return $this->hasOne(ComInvoiceNumbers::className(), ['id' => 'number_id']);
  }

  public function getProducts() {
    return $this->hasMany(OBComInvoiceProd::className(), ['invoice_id' => 'id']);
  }

  public function getNew_org_text_en() {
    return Helper::decodeQuote($this->org_text_en);
  }

  public function getNew_org_text_ru() {
    return Helper::decodeQuote($this->org_text_ru);
  }

  public function getNew_buy_text_en() {
    return Helper::decodeQuote($this->buy_text_en);
  }

  public function getNew_buy_text_ru() {
    return Helper::decodeQuote($this->buy_text_ru);
  }

  public function getNew_cons_text_en() {
    return Helper::decodeQuote($this->cons_text_en);
  }

  public function getNew_cons_text_ru() {
    return Helper::decodeQuote($this->cons_text_ru);
  }

  public function getNew_ndate() {
    return date('d.m.Y', strtotime($this->ndate));
  }

  public function getNew_notes() {
    return Helper::decodeQuote($this->notes);
  }

  public function runBeforeSave() {
    $this->org_text_en = Helper::encodeQuote($this->org_text_en);
    $this->org_text_ru = Helper::encodeQuote($this->org_text_ru);
    $this->buy_text_en = Helper::encodeQuote($this->buy_text_en);
    $this->buy_text_ru = Helper::encodeQuote($this->buy_text_ru);
    $this->cons_text_en = Helper::encodeQuote($this->cons_text_en);
    $this->cons_text_ru = Helper::encodeQuote($this->cons_text_ru);
    $this->notes = Helper::encodeQuote($this->notes);
    $this->ndate = date("Y-m-d", strtotime($this->ndate));
    if ($this->isNewRecord) {
      $this->number_id = 0;
    }
    if (!empty($this->receive_date)) {
      if (strtotime($this->receive_date) < strtotime($this->ndate)) {
        $this->receive_date = $this->ndate;
      }
    }
  }

  public function runAfterInsert() {
    if ($this->number_id == 0) {
      $num = new ComInvoiceNumbers();
      if($num->saveAuto('VT', @$this->contract->number->num)){
        $this->number_id = $num->id;
        $this->update();
      }
    }
    $this->sendMessage('Создали Коммерчиский Инвойс VT&B: '.$this->number->number, Url::to(['org_buyer/o-b-com-invoice/update', 'id' => $this->id]));
  }

  public function runBeforeDelete() {
    $products = $this->products;
    $this->clear($products);
    $number = $this->number;
    if (!is_null($number)) {
      $number->delete();
    }
    Yii::$app->session->setFlash('success', 'Suuccessfully deleted.');
  }

  public function runAfterDelete() {
    $this->sendMessage('Удалили Коммерчиский Инвойс VT&B: '.$this->number->number);
  }

  public function sendUpdatedMessage() {
    $this->sendMessage('Обнавили Коммерчиский Инвойс VT&B: '.$this->number->number, Url::to(['org_buyer/o-b-com-invoice/update', 'id' => $this->id]));
  }

  public function applyDefaults() {
    $this->director_org_en = 'Миркамилов М.Т. / Mr. Mirkamilov M.T.';
    $this->position = 'Директор / Director';
    $this->place = 'г. Ташкент, Узбекистан / Tashkent, Uzbekistan';
    $this->status = Status::ACTIVE;
  }

  public function selfProducts() {
    $self_prods = $this->products;
    $new_self_prods = [];
    if (!empty($self_prods)) {
      foreach ($self_prods as $key => $value) {
        $new_self_prods[$value->ob_contract_product_id] = $value;
      }
    }
    return $new_self_prods;
  }

  public function availableProducts() {
    $products = Yii::$app->request->post('Products');
    $a_products = [];
    if (!empty($products)) {
      $prods = [];
      $qtys = [];
      foreach ($products as $key => $value) {
        $p_id = isset($value['product_id']) ? (int) $value['product_id'] : 0;
        if ($p_id > 0) {
          $prods[$p_id] = $p_id;
          if (isset($value['quantity'])) {
            // $qty = (isset($value['quantity']) && (int)$value['quantity'] > 0)?(int)$value['quantity']:1;
            $qty = isset($value['quantity'])?str_replace(' ', '', $value['quantity']):'';
            $qty = ((int)$qty > 0)?(int)$qty:1;
            if (isset($qtys[$p_id])) {
              $qtys[$p_id] += $qty;
            } else {
              $qtys[$p_id] = $qty;
            }
          } else {
            $qtys[$p_id] = -1;
          }
        }
      }
      $new_prods = OBContractProd::find()->where(['in', 'id', $prods])->andWhere(['contract_id' => $this->contract_id])->all();
      if (!empty($new_prods)) {
        foreach ($new_prods as $key2 => $value2) {
          $a_products[$value2->id]=['product' => $value2,'qty' => $qtys[$value2->id]];
        }
      }
    }
    return $a_products;
  }

  public function saveOne($post) {
    $product = $post['product'];
    if ($product->qty_ob_com_invoice > 0) {
      $model = new OBComInvoiceProd();
      $model->contract_id = $this->contract_id;
      $model->invoice_id = $this->id;
      $model->ob_contract_product_id = $product->id;
      $model->title_en = $product->title_en;
      $model->title_ru = $product->title_ru;
      $model->description_en = $product->description_en;
      $model->description_ru = $product->description_ru;
      $model->unit = $product->unit;
      $model->code = $product->code;
      $model->country_en = $product->country_en;
      $model->country_ru = $product->country_ru;
      $model->price = $product->price;
      $model->quantity = $post['qty'];
      if ($model->quantity > $product->qty_ob_com_invoice) {
        $model->quantity = $product->qty_ob_com_invoice;
        $this->response['messages'][] = ['id' => $product->id, 'text' => "Доступное количество для продукта `{$model->description_en}` -> `{$model->quantity}`, автоматом сохранили максимальное количество."];
      } else {
        $this->response['messages'][] = [
          'id' => $product->id, 
          'text' => "Продукт `{$product->description_en}` успешно добавлен"
        ];
      }
      $model->total_price = ($model->quantity * $model->price);
      $model->net_weight = 0;
      $model->gross_weight = 0;
      $model->save();
    } else {
      $this->response['messages'][] = ['id' => $product->id, 'text' => "Нет доступное количество продукт `{$product->description_en}`."];
    }
  }

  public function deleteOne($sp) {
    $sp->delete();
    $this->response['messages'][] = [
      'id' => $sp->ob_contract_product_id,
      'text' => "Продукт `{$sp->description_en}` успешно удален"
    ];
  }

  public function updateOne($sp, $post) {
    $product = $post['product'];
    $o_qty = $sp->quantity;
    if ($post['qty'] != -1) {
      $sp->quantity = $post['qty'];
    }
    $soob = true;
    if ($sp->quantity > ($o_qty+$product->qty_ob_com_invoice)) {
      $sp->quantity = ($o_qty+$product->qty_ob_com_invoice);
      if ($sp->quantity > 0) {
        $this->response['messages'][] = ['id' => $product->id, 'text' => "Доступное количество для продукта `{$sp->description_en}` -> `{$sp->quantity}`, автоматом сохранили максимальное количество."];
        $soob = false;
      }
    }
    if ($sp->quantity > 0) {
      if ($sp->quantity != $o_qty && $soob == true) {
        $this->response['messages'][] = ['id' => $product->id, 'text' => "Продукт `{$sp->description_en}` успешно обнавлен"];
      }
      $sp->total_price = ($sp->quantity * $sp->price);
      $sp->save();
    } else {
      $this->deleteOne($sp);
    }
  }

  public function saveProducts() {

    $contract = $this->contract;
    if ($contract) {
      $invoice_id = $this->id;
      $products = $this->availableProducts();
      $self_prods = $this->selfProducts();
      if (!empty($products)) {
        if (!empty($self_prods)) {
          $ostatok = $self_prods;
          foreach ($self_prods as $kp => $sp) {
            if (isset($products[$kp])) {
              $this->updateOne($sp, $products[$kp]);
              unset($ostatok[$kp]);
              unset($products[$kp]);
            }
          }
          if (!empty($ostatok)) {
            foreach ($ostatok as $sp) {
              $this->deleteOne($sp);
            }
          }
          if (!empty($products)) {
            foreach ($products as $product) {
              $this->saveOne($product);
            }
          }
          $this->response['code'] = 253;
        } else {
          foreach ($products as $product) {
            $this->saveOne($product);
          }
          $this->response['code'] = 252;
        }
      } else {
        if (!empty($self_prods)) {
          foreach ($self_prods as $sp) {
            $this->deleteOne($sp);
          }
          $this->response['code'] = 251;
        }
      }
      $this->refreshBalance();
      $contract->refreshBalance();
    } else {
      $this->response['message'] = 'Контракт не найден';
      $this->response['code'] = 170;
      $this->response['status'] = false;
    }
  }

  public function refreshBalance() {
    $new_nprice = (float)OBComInvoiceProd::find()->where([
      'contract_id' => $this->contract_id, 
      'invoice_id' => $this->id
    ])->sum('total_price');
    if ((float)$this->nprice != $new_nprice) {
      $this->nprice = $new_nprice;
      $this->update();
    }
  }

  public function getModelName() {
    $name = explode('\\', __CLASS__);
    return array_pop($name);
  }
  
}
