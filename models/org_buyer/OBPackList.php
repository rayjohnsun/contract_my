<?php
namespace app\models\org_buyer;

use Yii;
use app\components\Helper;
use app\components\Polojenie;
use app\components\Status;
use app\components\text\NDate;
use app\models\InvoiceNumbers;
use app\models\companies\Buyers;
use app\models\companies\Consignees;
use app\models\companies\Organizations;
use app\models\org_buyer\OBPackListProd;
use yii\helpers\Url;

class OBPackList extends \app\base\AModel {

  public static function tableName() {
    return 'h_ob_pack_list';
  }

  public function rules() {
    return [
      [['ndate'], 'required', 'on' => static::SC_UPDATE],
      [['pstatus', 'show_siller', 'show_buyer', 'show_consignee'], 'integer'],
      [['notes'], 'string'],
      [['director_org_en', 'director_org_ru'], 'string', 'max' => 250],
      [['poddon'], 'string', 'max' => 9],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'ndate' => 'Дата',
      'number_id' => 'Номер',
      'invoice_id' => 'Инвойс (VT&D)',
      'contract_id' => 'Контракт (VT&D)',
      'organization_id' => 'Продавец',
      'buyer_id' => 'Покупатель',
      'consignee_id' => 'Грузополучатель',
      'notes' => 'Notes',
      'container_no' => 'Container No',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'status' => 'Status',
      'pstatus' => 'Положение',
      'director_org_en' => 'Директор',
      'director_org_ru' => 'Директор',
      'poddon' => 'Поддон',
      'show_siller' => 'Показать продавец',
      'show_buyer' => 'Показать покупатель',
      'show_consignee' => 'Показать грузополучатель',
    ];
  }

  public function getInvoice() {
    return $this->hasOne(OBInvoice::className(), ['id' => 'invoice_id']);
  }

  public function getContract() {
    return $this->hasOne(OBContract::className(), ['id' => 'contract_id']);
  }

  public function getOrganization() {
    return $this->hasOne(Organizations::className(), ['id' => 'organization_id']);
  }

  public function getBuyer() {
    return $this->hasOne(Buyers::className(), ['id' => 'buyer_id']);
  }

  public function getConsignee() {
    return $this->hasOne(Consignees::className(), ['id' => 'consignee_id']);
  }

  public function getNumber() {
    return $this->hasOne(InvoiceNumbers::className(), ['id' => 'number_id']);
  }

  public function getProducts() {
    return $this->hasMany(OBPackListProd::className(), ['packlist_id' => 'id']);
  }

  public function getNew_ndate() {
    return NDate::get($this->ndate);
  }

  public function getNew_notes() {
    return Helper::decodeQuote($this->notes);
  }

  public function getContainer_no2() {
    $con_no = $this->container_no;
    if (!empty($con_no)) {
      $no = explode(',', $con_no);
      $new_no = '';
      foreach ($no as $val) {
        $new_no .= "<div>{$val}</div>";
      }
      return $new_no;
    }
    return '';
  }

  public function runBeforeSave() {
    $this->notes = Helper::encodeQuote($this->notes);
    $this->ndate = date("Y-m-d", strtotime($this->ndate));
  }

  public function runBeforeDelete() {
    $products = $this->products;
    $this->clear($products);
    Yii::$app->session->setFlash('success', 'Suuccessfully deleted.');
  }

  public function runAfterInsert() {
    $this->sendMessage('Создали Упаковочный Лист VT&B: '.$this->number->pack_number, Url::to(['org_buyer/o-b-pack-list/update', 'id' => $this->id]));
  }

  public function runAfterDelete() {
    $this->sendMessage('Удалили Упаковочный Лист VT&B: '.$this->number->pack_number);
  }

  public function sendUpdatedMessage() {
    $this->sendMessage('Обнавили Упаковочный Лист VT&B: '.$this->number->pack_number, Url::to(['org_buyer/o-b-pack-list/update', 'id' => $this->id]));
  }

  public function applyDefaults() {
    $this->status = Status::ACTIVE;
    $this->pstatus = Polojenie::HOLD;
  }

  public function saveProducts() {
    $products = Yii::$app->request->post('Products');
    // $self_prods = $this->products;
    $this_id = $this->id;
    $invoice = $this->invoice;
    $inv_prods = $invoice->products;
    $pstatus = Polojenie::HOLD;
    OBPackListProd::deleteAll(['packlist_id' => $this_id]);
    if (!empty($inv_prods)) {
      if (!empty($products)) {
        $approve = true;
        $p_count = 0;
        foreach ($inv_prods as $product) {
          $kk = 0;
          foreach ($products as $key => $value) {
            $kk++;
            $p_id = isset($value['product_id']) ? (int)$value['product_id'] : 0;
            if ($product->id == $p_id) {
              $p_count++;
              // $quantity = isset($value['quantity']) ? (int)$value['quantity'] : 1;
              $quantity = isset($value['quantity'])?str_replace(' ', '', $value['quantity']):'';
              $quantity = ((int)$quantity > 0)?(int)$quantity:1;
              if ($quantity < 1) {
                $quantity = 1;
              }

              // $net_w = isset($value['net_weight']) ? (int)$value['net_weight'] : 1;
              $net_w = isset($value['net_weight'])?str_replace(' ', '', $value['net_weight']):'';
              $net_w = ((int)$net_w > 0)?(int)$net_w:1;
              if ($net_w < 1) {
                $net_w = 1;
              }

              // $gross_w = isset($value['gross_weight']) ? (int)$value['gross_weight'] : 1;
              $gross_w = isset($value['gross_weight'])?str_replace(' ', '', $value['gross_weight']):'';
              $gross_w = ((int)$gross_w > 0)?(int)$gross_w:1;
              if ($gross_w < 1) {
                $gross_w = 1;
              }
              $container = isset($value['container']) ? $value['container'] : '';
              if (empty($container)) {
                $approve = false;
                $this->response['messages'][] = [
                  'id' => $kk, 
                  'text' => "Введите номер контейнер для {$kk} ого строка! "
                ];
              }

              $model = new OBPackListProd();
              $model->contract_id = $invoice->contract_id;
              $model->invoice_id = $invoice->id;
              $model->packlist_id = $this_id;
              $model->ob_invoice_product_id = $product->id;
              $model->title_en = $product->title_en;
              $model->title_ru = $product->title_ru;
              $model->description_en = $product->description_en;
              $model->description_ru = $product->description_ru;
              $model->unit = $product->unit;
              $model->code = $product->code;
              $model->country_en = $product->country_en;
              $model->country_ru = $product->country_ru;
              $model->price = $product->price;
              $model->quantity = $quantity;
              $model->total_price = ($model->quantity * $model->price);
              $model->net_weight = $net_w;
              $model->gross_weight = $gross_w;
              $model->container = $container;
              $model->save();
            }
          }
        }
        if ($p_count == 0) {
          $approve = false;
          $this->response['messages'][] = [
            'id' => 0, 
            'text' => "Продуктов не выбран"
          ];
        }
        $this->response['status'] = $approve;
        $pstatus = $approve == true ? Polojenie::APPROVE : Polojenie::RECALL;
      } else {
        $this->response['messages'][] = [
          'id' => 0, 
          'text' => "Продуктов не найден"
        ];
        $this->response['status'] = false;
      }
    } else {
      $this->response['messages'][] = [
        'id' => 0, 
        'text' => "Продуктов не найден"
      ];
      $this->response['status'] = false;
    }
    $this->pstatus = $pstatus;
    $this->update();
  }

}
