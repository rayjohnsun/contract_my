<?php
namespace app\models\org_buyer;

use app\models\Countries;
use app\models\Tables;
use app\models\org_buyer\OBContractProd;

class OBInvoiceProd extends \app\base\AModel {

  public static function tableName() {
    return 'h_ob_invoice_prod';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract (O&D)',
      'invoice_id' => 'Invoice (O&D)',
      'ob_contract_product_id' => 'Contract product (O&D)',
      'title_en' => 'Название O&D (en)',
      'title_ru' => 'Название O&D (ru)',
      'description_en' => 'Название D&B (en)',
      'description_ru' => 'Название D&B (ru)',
      'unit' => 'Unit',
      'code' => 'Code',
      'price' => 'Price',
      'quantity' => 'Quantity',
      'total_price' => 'Total Price',
      'net_weight' => 'Net Weight',
      'gross_weight' => 'Gross Weight',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'country_en' => 'Country',
      'country_ru' => 'Country'
    ];
  }

  /******************************GETTERS******************************/

  public function getInvoice() {
    return $this->hasOne(OBInvoice::className(), ['id' => 'invoice_id']);
  }

  public function getContract() {
    return $this->hasOne(OBContract::className(), ['id' => 'contract_id']);
  }

  public function getOBContractProduct() {
    return $this->hasOne(OBContractProd::className(), ['id' => 'ob_contract_product_id']);
  }

  public function getTitleRuEn() {
    return $this->title_ru . ' / ' . $this->title_en;
  }

  public function getDescrRuEn() {
    return $this->description_ru . ' / ' . $this->description_en;
  }

  // public function getTables() {
  //   return $this->hasMany(Tables::className(), ['vt_inv_tovar_id' => 'id']);
  // }

}
