<?php
namespace app\models\org_buyer;

class OBConParag extends \app\base\AModel {

  public static function tableName() {
    return 'h_ob_con_parag';
  }

  public function rules() {
    return [
      [['title_en', 'title_ru'], 'string', 'max' => 250],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract ID',
      'title_en' => 'Title En',
      'title_ru' => 'Title Ru',
    ];
  }

  public function getSections() {
    return $this->hasMany(OBConParagSec::className(), ['parag_id' => 'id']);
  }
}
