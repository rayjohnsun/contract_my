<?php
namespace app\models\org_buyer;

use app\models\Countries;
use app\models\Products;
use app\models\dil_buyer\DBContractProd;

class OBContractProd extends \app\base\AModel {

  public static function tableName() {
    return 'h_ob_contract_prod';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract ID',
      'product_id' => 'Product ID',
      'title_en' => 'Название O&D (en)',
      'title_ru' => 'Название O&D (ru)',
      'description_en' => 'Название D&B (en)',
      'description_ru' => 'Название D&B (ru)',
      'unit' => 'Unit',
      'code' => 'Code',
      'price' => 'Price',
      'quantity' => 'Quantity',
      'total_price' => 'Total Price',
      'qty_ob_invoice' => 'Qty OB Invoice',
      'qty_ob_com_invoice' => 'Qty OD Com. Invoice',
      'summ_ob_invoice' => 'Summ OB Invoice',
      'summ_ob_com_invoice' => 'Summ OD Com. Invoice',
      'podtverjden' => 'Podtverjden',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'country_en' => 'Country',
      'country_ru' => 'Country'
    ];
  }

  /******************************GETTERS******************************/

  public function getContract() {
    return $this->hasOne(OBContract::className(), ['id' => 'contract_id']);
  }

  public function getProduct() {
    return $this->hasOne(Products::className(), ['id' => 'product_id']);
  }

  public function getOBInvoiceProducts() {
    return $this->hasMany(OBInvoiceProd::className(), ['ob_contract_product_id' => 'id']);
  }

  public function getTitleRuEn() {
    return $this->title_ru . ' / ' . $this->title_en;
  }

  public function getDescrRuEn() {
    return $this->description_ru . ' / ' . $this->description_en;
  }

  // public function getHproducts() {
  //   return $this->hasMany(ContractProductsLTZ::className(), ['g_product_id' => 'id']);
  // }

  // public function beforeDelete() {

  //   if (parent::beforeDelete()) {

  //     if (!empty($this->hproducts)) {

  //       foreach ($this->hproducts as $key => $value) {

  //         $value->delete();

  //       }

  //     }

  //     return true;

  //   }

  //   return false;

  // }

}
