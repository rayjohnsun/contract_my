<?php
namespace app\models\org_buyer\search;

use app\components\Helper;
use app\models\org_buyer\OBComInv;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class OBComInvSearch extends OBComInv {

  public function rules() {
    return [
      [['id', 'contract_id', 'organization_id', 'buyer_id', 'status'], 'integer'],
      [['number', 'ndate', 'org_text_ru', 'org_text_en', 'buy_text_ru', 'buy_text_en', 'notes', 'signature', 'position', 'name', 'place', 'created_at', 'updated_at'], 'safe'],
    ];
  }

  public function scenarios() {
    return Model::scenarios();
  }

  public function search($params) {
    $query = OBComInv::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'contract_id' => $this->contract_id,
      'ndate' => $this->ndate,
      'organization_id' => $this->organization_id,
      'buyer_id' => $this->buyer_id,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', 'number', $this->number])
      ->andFilterWhere(['like', 'org_text_ru', $this->org_text_ru])
      ->andFilterWhere(['like', 'org_text_en', $this->org_text_en])
      ->andFilterWhere(['like', 'buy_text_ru', $this->buy_text_ru])
      ->andFilterWhere(['like', 'buy_text_en', $this->buy_text_en])
      ->andFilterWhere(['like', 'notes', $this->notes])
      ->andFilterWhere(['like', 'signature', $this->signature])
      ->andFilterWhere(['like', 'position', $this->position])
      ->andFilterWhere(['like', 'name', $this->name])
      ->andFilterWhere(['like', 'place', $this->place])
      ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)]);

    return $dataProvider;
  }
}
