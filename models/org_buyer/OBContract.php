<?php
namespace app\models\org_buyer;

use Yii;
use app\components\Helper;
use app\components\NumberShablones;
use app\components\Podtverjdeno;
use app\components\Status;
use app\components\text\Adres;
use app\components\text\NDate;
use app\components\text\OBTxt;
use app\models\ContractNumbers;
use app\models\Products;
use app\models\companies\Buyers;
use app\models\companies\Organizations;
use app\models\dil_buyer\DBContract;
use app\models\dil_buyer\DBContractProd;
use yii\helpers\Url;

class OBContract extends \app\base\AModel {

  public static function tableName() {
    return 'h_ob_contract';
  }

  public function rules() {
    return [
      [['ndate'], 'required'],
      [['currency', 'organization_id', 'buyer_id'], 'required', 'on' => static::SC_CREATE],
      [['text_en', 'text_ru', 'note_en', 'note_ru'], 'string'],
      [['date_en', 'date_ru', 'ndate', 'res_type'], 'string', 'max' => 50],
      [['adres_en', 'adres_ru', 'region_en', 'region_ru'], 'string', 'max' => 250],
      [['director_org_en', 'director_org_ru', 'director_buy_en', 'director_buy_ru'], 'string', 'max' => 150]
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'ndate' => 'Date',
      'number_id' => 'Номер',
      'organization_id' => 'Продавец',
      'buyer_id' => 'Покупатель',
      'date_en' => 'Date En',
      'date_ru' => 'Дата',
      'adres_en' => 'Adres En',
      'adres_ru' => 'Адрес',
      'text_en' => 'Seller En',
      'text_ru' => 'Seller Ru',
      'status' => 'Status',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'note_en' => 'Note En',
      'note_ru' => 'Note Ru',
      'inv_prod_qty' => 'Инвойс Остаток',
      'inv_prod_nprice' => 'Баланс Инв.',
      'inv_prod_lprice' => 'Баланс Инв.(не подв.)',
      'com_inv_prod_qty' => 'Ком. Инвойс Остаток',
      'com_inv_prod_nprice' => 'Баланс Ком. Инв.',
      'com_inv_prod_lprice' => 'Баланс Ком. Инв.(не подв.)',
      'total_nprice' => 'Итоговая цена',
      'total_lprice' => 'Состояние',
      'receive_sum' => 'Поступление',
      'zapolnen' => 'Статус',
      'currency' => 'Валюта',
      'director_org_en' => 'Director',
      'director_org_ru' => 'Director',
      'director_buy_en' => 'Директор',
      'director_buy_ru' => 'Директор',
      'region_en' => 'Region En',
      'region_ru' => 'Region Ru',
      'res_type' => 'Поступления'
    ];
  }

  public function getOrganization() {
    return $this->hasOne(Organizations::className(), ['id' => 'organization_id']);
  }

  public function getBuyer() {
    return $this->hasOne(Buyers::className(), ['id' => 'buyer_id']);
  }

  public function getNumber() {
    return $this->hasOne(ContractNumbers::className(), ['id' => 'number_id']);
  }

  public function getParagraphs() {
    return $this->hasMany(OBConParag::className(), ['contract_id' => 'id']);
  }

  public function getProducts() {
    return $this->hasMany(OBContractProd::className(), ['contract_id' => 'id']);
  }

  public function getInvoices() {
    return $this->hasMany(OBInvoice::className(), ['contract_id' => 'id']);
  }

  public function getCominvoices() {
    return $this->hasMany(OBComInvoice::className(), ['contract_id' => 'id']);
  }

  public function getNew_text_en() {
    return Helper::decodeQuote($this->text_en);
  }

  public function getNew_text_ru() {
    return Helper::decodeQuote($this->text_ru);
  }

  public function getNew_note_en() {
    return Helper::decodeQuote($this->note_en);
  }

  public function getNew_note_ru() {
    return Helper::decodeQuote($this->note_ru);
  }

  public function getNew_ndate() {
    return date('d.m.Y', strtotime($this->ndate));
  }

  public function getMainParagSection() {
    $mainParagSec = null;
    $mainParag = OBParagraph::find()
      ->where(['status' => Status::ACTIVE, 'norder' => 0])
      ->orderBy(['id' => SORT_ASC])
      ->one();
    if ($mainParag) {
      $p_items = $mainParag->sections;
      if (!empty($p_items)) {
        $mainParagSec = $p_items[0];
      }
    }
    return $mainParagSec;
  }

  public function runBeforeSave() {    
    $this->text_en = Helper::encodeQuote($this->text_en);
    $this->text_ru = Helper::encodeQuote($this->text_ru);
    $this->note_en = Helper::encodeQuote($this->note_en);
    $this->note_ru = Helper::encodeQuote($this->note_ru);
    if ($this->isNewRecord) {
      $this->number_id = 0;
    }
  }

  public function runAfterInsert() {
    if ($this->number_id == 0) {
      $num = new ContractNumbers();
      if($num->saveAuto('VT')){
        $this->number_id = $num->id;
        $this->update();
      }
    }
    $this->sendMessage('Создали контракт VT&B: '.$this->number->number, Url::to(['org_buyer/o-b-contract/update', 'id' => $this->id]));
  }

  public function returnBeforeDelete() {
    $invoices = $this->invoices;
    $cominvoices = $this->cominvoices;
    $products = $this->products;
    $this->clearParagraphs();
    $this->clear($invoices);
    $this->clear($cominvoices);
    $this->clear($products);
    $number = $this->number;
    if (!is_null($number)) {
      $number->delete();
    }
    Yii::$app->session->setFlash('success', 'Suuccessfully deleted.');
    return true;
  }

  public function runAfterDelete() {
    $this->sendMessage('Удалили контракт VT&B: '.$this->number->number);
  }

  public function sendUpdatedMessage() {
    $this->sendMessage('Обнавили контракт VT&B: '.$this->number->number, Url::to(['org_buyer/o-b-contract/update', 'id' => $this->id]));
  }

  public function clearParagraphs() {
    OBConParag::deleteAll(['contract_id' => $this->id]);
    OBConParagSec::deleteAll(['contract_id' => $this->id]);
  }

  public function applyDefaults() {
    $this->adres_en = Adres::TASHKENT_EN;
    $this->adres_ru = Adres::TASHKENT_RU;
    $this->date_en = NDate::en($this->ndate);
    $this->date_ru = NDate::ru($this->ndate);
    $this->status = Status::ACTIVE;
    $this->res_type = 'cominv';
  }

  public static function replEn($value, $seller, $buyer) {
    $search_en = [
      '{{SELLER_EN}}',
      '{{SEL_ADRES_EN}}',
      '{{BUYER_EN}}',
      '{{BUY_ADRES_EN}}',
      '{{CORPORATE_ID}}', 
      '{{BANK}}', 
      '{{BANK_ADDRESS}}', 
      '{{SWIFT_CODE}}', 
      '{{IBAN}}', 
      '{{CIF}}', 
      '{{ACCOUNT_NO}}', 
      '{{BANK_CORRESPONDENT}}'
    ];
    $replace_en= [
      $seller->title_en,
      $seller->adres_en,
      $buyer->title_en,
      $buyer->adres_en,
      !empty($buyer->corporate_id)?'Corporate ID: '.$buyer->corporate_id:'', 
      !empty($buyer->swift_code)?'SWIFT CODE: '.$buyer->swift_code:'', 
      !empty($buyer->cif)?'CIF: '.$buyer->cif:'', 
      !empty($buyer->account_no)?'ACCOUNT NO: '.$buyer->account_no:'', 
      !empty($buyer->bank)?'Bank: '.$buyer->bank:'', 
      !empty($buyer->bank_address)?'Address: '.$buyer->bank_address:'', 
      !empty($buyer->iban)?'IBAN: '.$buyer->iban:'', 
      !empty($buyer->bank_correspondent)?'CORRESPONDENT: '.$buyer->bank_correspondent:''
    ];
    return str_replace($search_en, $replace_en, $value);
  }

  public static function replRu($value, $seller, $buyer) {    
    $search_ru = [
      '{{SELLER_RU}}',
      '{{SEL_ADRES_RU}}',
      '{{BUYER_RU}}',
      '{{BUY_ADRES_RU}}',
      '{{CORPORATE_ID}}', 
      '{{BANK}}', 
      '{{BANK_ADDRESS}}', 
      '{{SWIFT_CODE}}', 
      '{{IBAN}}', 
      '{{CIF}}', 
      '{{ACCOUNT_NO}}', 
      '{{BANK_CORRESPONDENT}}'
    ];
    $replace_ru= [
      $seller->title_ru,
      $seller->adres_ru,
      $buyer->title_ru,
      $buyer->adres_ru,
      !empty($buyer->corporate_id)?'Corporate ID: '.$buyer->corporate_id:'', 
      !empty($buyer->swift_code)?'SWIFT CODE: '.$buyer->swift_code:'', 
      !empty($buyer->cif)?'CIF: '.$buyer->cif:'', 
      !empty($buyer->account_no)?'ACCOUNT NO: '.$buyer->account_no:'', 
      !empty($buyer->bank)?'Банк: '.$buyer->bank:'', 
      !empty($buyer->bank_address)?'Адрес банка: '.$buyer->bank_address:'', 
      !empty($buyer->iban)?'IBAN: '.$buyer->iban:'', 
      !empty($buyer->bank_correspondent)?'CORRESPONDENT: '.$buyer->bank_correspondent:''
    ];
    return str_replace($search_ru, $replace_ru, $value);
  }

  public function saveParagraphs() {
    $id = $this->id;
    $paragraphs = Yii::$app->request->post('Paragraphs');
    $this->clearParagraphs();
    $organization = $this->organization;
    $buyer = $this->buyer;    
    if (!empty($paragraphs)) {
      foreach ($paragraphs as $key => $value) {
        $paragraph = new OBConParag();
        $paragraph->contract_id = $id;
        $paragraph->title_en = @$value['title_en'];
        $paragraph->title_ru = @$value['title_ru'];
        if ($paragraph->save()) {
          if (isset($value['items']) AND is_array($value['items']) AND !empty($value['items'])) {
            foreach ($value['items'] as $key2 => $value2) {
              $item = new OBConParagSec();
              $item->contract_id = $id;
              $item->parag_id = $paragraph->id;
              $item->title_en = static::replEn($value2['title_en'], $organization, $buyer);
              $item->title_ru = static::replRu($value2['title_ru'], $organization, $buyer);
              $item->save();
            }
          }
        }
      }
    }
  }

  public function selfProducts() {
    $self_prods = $this->products;
    $new_self_prods = [];
    if (!empty($self_prods)) {
      foreach ($self_prods as $key => $value) {
        $new_self_prods[$value->product_id] = $value;
      }
    }
    return $new_self_prods;
  }

  public function availableProducts() {
    $products = Yii::$app->request->post('Products');
    $a_products = [];
    if (!empty($products)) {
      $prods = [];
      $qtys = [];
      $prss = [];
      foreach ($products as $key => $value) {
        $p_id = isset($value['product_id']) ? (int) $value['product_id'] : 0;
        if ($p_id > 0) {
          $prods[$p_id] = $p_id;
          if (isset($value['quantity']) && isset($value['price'])) {

            // $qty = (isset($value['quantity']) && (int)$value['quantity'] > 0)?(int)$value['quantity']:1;
            $qty = isset($value['quantity'])?str_replace(' ', '', $value['quantity']):'';
            $qty = ((int)$qty > 0)?(int)$qty:1;
            if (isset($qtys[$p_id])) {$qtys[$p_id] += $qty;} else {$qtys[$p_id] = $qty;}

            // $prs = (isset($value['price']) && (float)$value['price'] > 0)?(float)$value['price']:0;
            $prs = isset($value['price'])?str_replace(' ', '', $value['price']):'';
            $prs = ((float)$prs > 0)?(float)$prs:0;
            if (isset($prss[$p_id])) {$prss[$p_id] += $prs;} else {$prss[$p_id] = $prs;}

          } else {
            $qtys[$p_id] = -1;
            $prss[$p_id] = -1;
          }
        }
      }
      $new_prods = Products::find()->where(['in', 'id', $prods])->all();
      if (!empty($new_prods)) {
        foreach ($new_prods as $key2 => $value2) {
          $a_products[$value2->id]=['product' => $value2,'qty' => $qtys[$value2->id], 'prs' => $prss[$value2->id]];
        }
      }
    }
    return $a_products;
  }

  public function saveOne($post) {
    $product = $post['product'];
    $model = new OBContractProd();
    $model->contract_id = $this->id;
    $model->product_id = $product->id;
    $model->title_en = $product->title_en;
    $model->title_ru = $product->title_ru;
    $model->description_en = $product->description_en;
    $model->description_ru = $product->description_ru;
    $model->unit = $product->unit;
    $model->price = ($post['prs'] > 0) ? $post['prs'] : $product->price;
    $model->quantity = ($post['qty'] > 0) ? $post['qty'] : 1;
    $model->total_price = ($model->quantity * $model->price);
    $model->qty_ob_invoice = $model->quantity;
    $model->qty_ob_com_invoice = $model->quantity;
    $model->summ_ob_invoice = $model->total_price;
    $model->summ_ob_com_invoice = $model->total_price;
    $model->podtverjden = Podtverjdeno::NET;
    $model->code = $product->code;
    $country = $product->country;
    if ($country) {
      $model->country_en = $country->title_en;
      $model->country_ru = $country->title_ru;
    }
    $model->save();
    $this->response['messages'][] = [
      'id' => $product->id, 
      'text' => "Продукт `{$product->description_en}` успешно добавлен"
    ];
  }

  public function deleteOne($sp) {
    if (!empty($sp->dBContractProducts)) {
      $this->response['messages'][] = [
        'id' => $sp->product_id,
        'text' => "Продукту `{$sp->description_en}` не можем убрать из список, так как она используется для другого контракта"
      ];
    } else if(!empty($sp->oDInvoiceProducts)){
      $this->response['messages'][] = [
        'id' => $sp->product_id,
        'text' => "Продукту `{$sp->description_en}` не можем убрать из список, так как она используется для Инвойса"
      ];
    } else {
      $sp->delete();
      $this->response['messages'][] = [
        'id' => $sp->product_id,
        'text' => "Продукт `{$sp->description_en}` успешно удален"
      ];
    }
  }

  public function updateOne($sp, $post) {
    $all_inv_prod_qty = $this->getInvoiceProdQty($sp->id);
    $all_com_inv_prod_qty = $this->getComInvoiceProdQty($sp->id);
    $txt = "";
    $o_qty = (int)$sp->quantity;
    $o_prs = (float)$sp->price;
    if ($post['qty'] != -1) {
      $sp->quantity = ($post['qty'] > 0) ? $post['qty'] : 1;
    }
    if ($post['prs'] != -1 && $post['prs'] > 0) {
      $sp->price = $post['prs'];
      Yii::$app->db->createCommand("UPDATE ".OBInvoiceProd::tableName()." SET price=:price, total_price=quantity*:price WHERE ob_contract_product_id = :prob_id", [':price' => $sp->price, ':prob_id' => $sp->id])->execute();
    }
    $min = $all_inv_prod_qty;
    if ($sp->quantity < $min) {
      $sp->quantity = $min;
      $txt = "Продукт `{$sp->description_en}` используется для инвойса! минимальная количество {$all_inv_prod_qty} автоматом сохранилься";
      $this->response['messages'][$sp->product_id] = ['id' => $sp->product_id, 'text' => $txt];
    }    
    $min2 = $all_com_inv_prod_qty;
    if ($sp->quantity < $min2) {
      $sp->quantity = $min2;
      $txt = "Продукт `{$sp->description_en}` используется для Commercial Invoice! минимальная количество {$all_com_inv_prod_qty} автоматом сохранилься";
      $this->response['messages'][$sp->product_id] = ['id' => $sp->product_id, 'text' => $txt];
    }

    $sp->qty_ob_invoice = ($sp->quantity - $all_inv_prod_qty);
    $sp->qty_ob_com_invoice = ($sp->quantity - $all_com_inv_prod_qty);
    $sp->summ_ob_invoice = ($sp->qty_ob_invoice * $sp->price);
    $sp->summ_ob_com_invoice = ($sp->qty_ob_com_invoice * $sp->price);
    $sp->total_price = ($sp->quantity * $sp->price);
    if ($sp->quantity != $o_qty || $sp->price != $o_prs) {
      $sp->podtverjden = Podtverjdeno::NET;
    }
    $sp->save();
    if (($sp->quantity != $o_qty || $sp->price != $o_prs) && !isset($this->response['messages'][$sp->product_id])) {
      $txt = "Продукт `{$sp->description_en}` успешно обнавлен";
      $this->response['messages'][$sp->product_id] = ['id' => $sp->product_id, 'text' => $txt];
    }
  }

  public function saveProducts() {

    $contract_id = $this->id;
    $products = $this->availableProducts();
    $self_prods = $this->selfProducts();
    if (!empty($products)) {
      if (!empty($self_prods)) {
        $ostatok = $self_prods;
        foreach ($self_prods as $kp => $sp) {
          if (isset($products[$kp])) {
            $this->updateOne($sp, $products[$kp]);
            unset($ostatok[$kp]);
            unset($products[$kp]);
          }
        }
        if (!empty($ostatok)) {
          foreach ($ostatok as $sp) {
            $this->deleteOne($sp);
          }
        }
        if (!empty($products)) {
          foreach ($products as $product) {
            $this->saveOne($product);
          }
        }
        $this->response['code'] = 253;
      } else {
        foreach ($products as $product) {
          $this->saveOne($product);
        }
        $this->response['code'] = 252;
      }
    } else {
      if (!empty($self_prods)) {
        foreach ($self_prods as $sp) {
          $this->deleteOne($sp);
        }
        $this->response['code'] = 251;
      }
    }

    $this->refreshBalance();
    $this->refreshBalanceOfInvoices();

  }

  public function getInvoiceProdQty($prob_id) {
    return (int)OBInvoiceProd::find()->where([
      'ob_contract_product_id' => $prob_id,
      'contract_id' => $this->id
    ])->sum('quantity');
  }

  public function getComInvoiceProdQty($prob_id) {
    return (int)OBComInvoiceProd::find()->where([
      'ob_contract_product_id' => $prob_id,
      'contract_id' => $this->id
    ])->sum('quantity');
  }

  public function refreshBalanceOfInvoices() {
    $invoices = $this->invoices;
    if (!empty($invoices)) {
      foreach ($invoices as $key => $value) {
        $value->refreshBalance();
      }
    }
  }

  public function refreshBalance() {
    $inv_prod_qty = 0;
    $inv_prod_nprice = 0;
    $inv_prod_lprice = 0;
    $com_inv_prod_qty = 0;
    $com_inv_prod_nprice = 0;
    $com_inv_prod_lprice = 0;
    $total_nprice = 0;
    $total_lprice = 0;
    $products = OBContractProd::find()->where(['contract_id' => $this->id])->all();
    if (!empty($products)) {
      foreach ($products as $key => $value) {

        $all_inv_prod_qty = $this->getInvoiceProdQty($value->id);
        $new_inv_qty = ($value->quantity-$all_inv_prod_qty)>=0 ? ($value->quantity-$all_inv_prod_qty) : 0;
        $inv_prod_qty+=$new_inv_qty;
        $value->qty_ob_invoice = $new_inv_qty;
        $value->summ_ob_invoice = ($value->qty_ob_invoice * $value->price);

        $all_com_inv_prod_qty = $this->getComInvoiceProdQty($value->id);
        $new_com_inv_qty = ($value->quantity-$all_com_inv_prod_qty)>=0 ? ($value->quantity-$all_com_inv_prod_qty) : 0;
        $com_inv_prod_qty+=$new_com_inv_qty;
        $value->qty_ob_com_invoice = $new_com_inv_qty;
        $value->summ_ob_com_invoice = ($value->qty_ob_com_invoice * $value->price);

        $value->total_price = ($value->quantity * $value->price);
        $value->update();

        $inv_prod_nprice += $value->summ_ob_invoice;
        $com_inv_prod_nprice += $value->summ_ob_com_invoice;
        $total_nprice += $value->total_price;
        if ($value->podtverjden != Podtverjdeno::DA) {
          $inv_prod_lprice += $value->summ_ob_invoice;
          $com_inv_prod_lprice += $value->summ_ob_com_invoice;
          $total_lprice += $value->total_price;
        }

      }
    }

    $this->inv_prod_qty = $inv_prod_qty;
    $this->inv_prod_nprice = $inv_prod_nprice;
    $this->inv_prod_lprice = $inv_prod_lprice;

    $this->com_inv_prod_qty = $com_inv_prod_qty;
    $this->com_inv_prod_nprice = $com_inv_prod_nprice;
    $this->com_inv_prod_lprice = $com_inv_prod_lprice;

    $this->total_nprice = $total_nprice;
    $this->total_lprice = $total_lprice;

    $this->receive_sum = ($this->res_type == 'inv')?(int)OBInvoice::find()->where(['contract_id' => $this->id])->sum('receive_sum'):(int)OBComInvoice::find()->where(['contract_id' => $this->id])->sum('receive_sum');
    $this->update();
  }
  
  public function getModelName() {
    $name = explode('\\', __CLASS__);
    return array_pop($name);
  }

}
