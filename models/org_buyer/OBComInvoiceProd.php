<?php
namespace app\models\org_buyer;

use app\models\Countries;
use app\models\Tables;
use app\models\org_buyer\OBContractProd;

class OBComInvoiceProd extends \app\base\AModel {

  public static function tableName() {
    return 'h_ob_com_invoice_prod';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract (O&B)',
      'invoice_id' => 'ComInvoice (O&B)',
      'ob_contract_product_id' => 'Contract product (O&B)',
      'title_en' => 'Название O&B (en)',
      'title_ru' => 'Название O&B (ru)',
      'description_en' => 'Название O&B (en)',
      'description_ru' => 'Название O&B (ru)',
      'unit' => 'Unit',
      'code' => 'Code',
      'price' => 'Price',
      'quantity' => 'Quantity',
      'total_price' => 'Total Price',
      'net_weight' => 'Net Weight',
      'gross_weight' => 'Gross Weight',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'country_en' => 'Country',
      'country_ru' => 'Country'
    ];
  }

  public function getComInvoice() {
    return $this->hasOne(OBComInvoice::className(), ['id' => 'invoice_id']);
  }

  public function getContract() {
    return $this->hasOne(OBContract::className(), ['id' => 'contract_id']);
  }

  public function getOBContractProduct() {
    return $this->hasOne(OBContractProd::className(), ['id' => 'ob_contract_product_id']);
  }

  public function getTitleRuEn() {
    return $this->title_ru . ' / ' . $this->title_en;
  }

  public function getDescrRuEn() {
    return $this->description_ru . ' / ' . $this->description_en;
  }

}
