<?php
namespace app\models\dil_buyer;

use Yii;
use app\components\Helper;
use app\components\NumberShablones;
use app\components\Podtverjdeno;
use app\components\Status;
use app\components\text\Adres;
use app\components\text\DBTxt;
use app\components\text\NDate;
use app\models\Contract2Numbers;
use app\models\OD_DB;
use app\models\companies\Buyers;
use app\models\companies\Dillers;
use app\models\dil_buyer\DBContractProd;
use app\models\org_diller\ODContractProd;
use yii\helpers\Url;

class DBContract extends \app\base\AModel {

  public static function tableName() {
    return 'f_db_contract';
  }

  public function rules() {
    return [
      [['ndate'], 'required'],
      [['currency', 'diller_id', 'buyer_id'], 'required', 'on' => static::SC_CREATE],
      [['text_en', 'text_ru', 'note_en', 'note_ru'], 'string'],
      [['date_en', 'date_ru', 'ndate'], 'string', 'max' => 50],
      [['adres_en', 'adres_ru', 'region_en', 'region_ru'], 'string', 'max' => 250],
      [['director_dil_en', 'director_dil_ru', 'director_buy_en', 'director_buy_ru'], 'string', 'max' => 150]
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'ndate' => 'Date',
      'number_id' => 'Номер',
      'diller_id' => 'Продавец',
      'buyer_id' => 'Покупатель',
      'date_en' => 'Date En',
      'date_ru' => 'Дата',
      'adres_en' => 'Adres En',
      'adres_ru' => 'Адрес',
      'text_en' => 'Seller En',
      'text_ru' => 'Seller Ru',
      'status' => 'Status',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'note_en' => 'Note En',
      'note_ru' => 'Note Ru',
      'inv_prod_qty' => 'Инвойс Остаток',
      'inv_prod_nprice' => 'Баланс Инв.',
      'inv_prod_lprice' => 'Баланс Инв.(не подв.)',
      'total_nprice' => 'Итоговая цена',
      'total_lprice' => 'Состояние',
      'receive_sum' => 'Поступление',
      'zapolnen' => 'Статус',
      'currency' => 'Валюта',
      'director_dil_en' => 'Director',
      'director_dil_ru' => 'Director',
      'director_buy_en' => 'Директор',
      'director_buy_ru' => 'Директор',
      'code' => 'Code',
      'region_en' => 'Region En',
      'region_ru' => 'Region Ru'
    ];
  }

  public function getDb_od() {
    return $this->hasMany(OD_DB::className(), ['db_id' => 'id']);
  }

  public function getParentContracts() {
    $contracts = [];
    $parent_contracts = $this->db_od;
    if (!empty($parent_contracts)) {
      foreach ($parent_contracts as $db_od) {
        $contr = $db_od->oDContract;
        if ($contr) {
          $contracts[] = $contr;
        }
      }
    }
    return $contracts;
  }

  public function getDiller() {
    return $this->hasOne(Dillers::className(), ['id' => 'diller_id']);
  }

  public function getBuyer() {
    return $this->hasOne(Buyers::className(), ['id' => 'buyer_id']);
  }

  public function getNumber() {
    return $this->hasOne(Contract2Numbers::className(), ['id' => 'number_id']);
  }

  public function getParagraphs() {
    return $this->hasMany(DBConParag::className(), ['contract_id' => 'id']);
  }

  public function getProducts() {
    return $this->hasMany(DBContractProd::className(), ['contract_id' => 'id']);
  }

  public function getInvoices() {
    return $this->hasMany(DBInvoice::className(), ['contract_id' => 'id']);
  }

  public function getNew_text_en() {
    return Helper::decodeQuote($this->text_en);
  }

  public function getNew_text_ru() {
    return Helper::decodeQuote($this->text_ru);
  }

  public function getNew_note_en() {
    return Helper::decodeQuote($this->note_en);
  }

  public function getNew_note_ru() {
    return Helper::decodeQuote($this->note_ru);
  }

  public function getNew_ndate() {
    return date('d.m.Y', strtotime($this->ndate));
  }

  public function getMainParagSection() {
    $mainParagSec = null;
    $mainParag = DBParagraph::find()
      ->where(['status' => Status::ACTIVE, 'norder' => 0])
      ->orderBy(['id' => SORT_ASC])
      ->one();
    if ($mainParag) {
      $p_items = $mainParag->sections;
      if (!empty($p_items)) {
        $mainParagSec = $p_items[0];
      }
    }
    return $mainParagSec;
  }

  public function runBeforeSave() {    
    $this->text_en = Helper::encodeQuote($this->text_en);
    $this->text_ru = Helper::encodeQuote($this->text_ru);
    $this->note_en = Helper::encodeQuote($this->note_en);
    $this->note_ru = Helper::encodeQuote($this->note_ru);
    if ($this->isNewRecord) {
      $this->number_id = 0;
    }
  }

  public function runAfterInsert() {
    if ($this->number_id == 0) {
      $num = new Contract2Numbers();
      $code = !empty($this->code) ? $this->code : 'DL';
      if($num->saveAuto($code)){
        $this->number_id = $num->id;
        $this->update();
      }
    }
    $this->sendMessage('Создали контракт D&B: '.$this->number->number, Url::to(['odil_buyer/d-b-contract/update', 'id' => $this->id]));
  }

  public function runBeforeDelete() {
    $db_od = $this->db_od;
    $invoices = $this->invoices;
    $products = $this->products;
    $this->clearParagraphs();
    $this->clear($db_od);
    $this->clear($invoices);
    $this->clear($products);
    $number = $this->number;
    if (!is_null($number)) {
      $number->delete();
    }
    Yii::$app->session->setFlash('success', 'Suuccessfully deleted.');
  }

  public function runAfterDelete() {
    $this->sendMessage('Удалили контракт D&B: '.$this->number->number);
  }

  public function sendUpdatedMessage() {
    $this->sendMessage('Обнавили контракт D&B: '.$this->number->number, Url::to(['dil_buyer/d-b-contract/update', 'id' => $this->id]));
  }

  public function clearParagraphs() {
    DBConParag::deleteAll(['contract_id' => $this->id]);
    DBConParagSec::deleteAll(['contract_id' => $this->id]);
  }

  public function applyDefaults() {
    $this->adres_en = Adres::TASHKENT_EN;
    $this->adres_ru = Adres::TASHKENT_RU;
    $this->date_en = NDate::en($this->ndate);
    $this->date_ru = NDate::ru($this->ndate);
    $this->status = Status::ACTIVE;
  }

  public static function replEn($value, $seller, $buyer) {
    $search_en = [
      '{{SELLER_EN}}',
      '{{SEL_ADRES_EN}}',
      '{{BUYER_EN}}',
      '{{BUY_ADRES_EN}}',
      '{{BUY_COUNTRY_EN}}',
      '{{CORPORATE_ID}}', 
      '{{BANK}}', 
      '{{BANK_ADDRESS}}', 
      '{{SWIFT_CODE}}', 
      '{{IBAN}}', 
      '{{CIF}}', 
      '{{ACCOUNT_NO}}', 
      '{{BANK_CORRESPONDENT}}'
    ];
    $buy_country = $buyer->country;
    $country_title = '';
    if (!is_null($buy_country)) {
      $country_title = $buy_country->title_en;
    }
    $replace_en= [
      $seller->title_en,
      $seller->adres_en,
      $buyer->title_en,
      $buyer->adres_en,
      $country_title,
      !empty($buyer->corporate_id)?'Corporate ID: '.$buyer->corporate_id:'', 
      !empty($buyer->swift_code)?'SWIFT CODE: '.$buyer->swift_code:'', 
      !empty($buyer->cif)?'CIF: '.$buyer->cif:'', 
      !empty($buyer->account_no)?'ACCOUNT NO: '.$buyer->account_no:'', 
      !empty($buyer->bank)?'Bank: '.$buyer->bank:'', 
      !empty($buyer->bank_address)?'Address: '.$buyer->bank_address:'', 
      !empty($buyer->iban)?'IBAN: '.$buyer->iban:'', 
      !empty($buyer->bank_correspondent)?'CORRESPONDENT: '.$buyer->bank_correspondent:''
    ];
    return str_replace($search_en, $replace_en, $value);
  }

  public static function replRu($value, $seller, $buyer) {    
    $search_ru = [
      '{{SELLER_RU}}',
      '{{SEL_ADRES_RU}}',
      '{{BUYER_RU}}',
      '{{BUY_ADRES_RU}}',
      '{{BUY_COUNTRY_RU}}',
      '{{CORPORATE_ID}}', 
      '{{BANK}}', 
      '{{BANK_ADDRESS}}', 
      '{{SWIFT_CODE}}', 
      '{{IBAN}}', 
      '{{CIF}}', 
      '{{ACCOUNT_NO}}', 
      '{{BANK_CORRESPONDENT}}'
    ];
    $buy_country = $buyer->country;
    $country_title = '';
    if (!is_null($buy_country)) {
      $country_title = $buy_country->title_ru;
    }
    $replace_ru= [
      $seller->title_ru,
      $seller->adres_ru,
      $buyer->title_ru,
      $buyer->adres_ru,
      $country_title,
      !empty($buyer->corporate_id)?'Corporate ID: '.$buyer->corporate_id:'', 
      !empty($buyer->swift_code)?'SWIFT CODE: '.$buyer->swift_code:'', 
      !empty($buyer->cif)?'CIF: '.$buyer->cif:'', 
      !empty($buyer->account_no)?'ACCOUNT NO: '.$buyer->account_no:'', 
      !empty($buyer->bank)?'Банк: '.$buyer->bank:'', 
      !empty($buyer->bank_address)?'Адрес банка: '.$buyer->bank_address:'', 
      !empty($buyer->iban)?'IBAN: '.$buyer->iban:'', 
      !empty($buyer->bank_correspondent)?'CORRESPONDENT: '.$buyer->bank_correspondent:''
    ];
    return str_replace($search_ru, $replace_ru, $value);
  }

  public function saveParagraphs() {
    $id = $this->id;
    $paragraphs = Yii::$app->request->post('Paragraphs');
    $this->clearParagraphs();
    $diller = $this->diller;
    $buyer = $this->buyer;
    if (!empty($paragraphs)) {
      foreach ($paragraphs as $key => $value) {
        $paragraph = new DBConParag();
        $paragraph->contract_id = $id;
        $paragraph->title_en = @$value['title_en'];
        $paragraph->title_ru = @$value['title_ru'];
        if ($paragraph->save()) {
          if (isset($value['items']) AND is_array($value['items']) AND !empty($value['items'])) {
            foreach ($value['items'] as $key2 => $value2) {
              $item = new DBConParagSec();
              $item->contract_id = $id;
              $item->parag_id = $paragraph->id;
              $item->title_en = static::replEn($value2['title_en'], $diller, $buyer);
              $item->title_ru = static::replRu($value2['title_ru'], $diller, $buyer);
              $item->save();
            }
          }
        }
      }
    }
  }

  public function selfProducts() {
    $self_prods = $this->products;
    $new_self_prods = [];
    if (!empty($self_prods)) {
      foreach ($self_prods as $key => $value) {
        $new_self_prods[$value->od_contract_product_id] = $value;
      }
    }
    return $new_self_prods;
  }

  public function availableProducts() {
    $products = Yii::$app->request->post('Products');
    $a_products = [];
    if (!empty($products)) {
      $prods = [];
      $qtys = [];
      $prss = [];
      foreach ($products as $key => $value) {
        $p_id = isset($value['product_id']) ? (int) $value['product_id'] : 0;
        if ($p_id > 0) {
          $prods[$p_id] = $p_id;
          if (isset($value['quantity']) && isset($value['price'])) {

            // $qty = (isset($value['quantity']) && (int)$value['quantity'] > 0)?(int)$value['quantity']:1;
            $qty = isset($value['quantity'])?str_replace(' ', '', $value['quantity']):'';
            $qty = ((int)$qty > 0)?(int)$qty:1;
            if (isset($qtys[$p_id])) {$qtys[$p_id] += $qty;} else {$qtys[$p_id] = $qty;}

            // $prs = (isset($value['price']) && (float)$value['price'] > 0)?(float)$value['price']:0;
            $prs = isset($value['price'])?str_replace(' ', '', $value['price']):'';
            $prs = ((float)$prs > 0)?(float)$prs:0;
            if (isset($prss[$p_id])) {$prss[$p_id] += $prs;} else {$prss[$p_id] = $prs;}

          } else {
            $qtys[$p_id] = -1;
            $prss[$p_id] = -1;
          }
        }
      }
      $new_prods = ODContractProd::find()->where(['in', 'id', $prods])->all();
      if (!empty($new_prods)) {
        foreach ($new_prods as $key2 => $value2) {
          $a_products[$value2->id]=['product' => $value2,'qty' => $qtys[$value2->id], 'prs' => $prss[$value2->id]];
        }
      }
    }
    return $a_products;
  }

  public function saveOne($post) {
    $product = $post['product'];
    $model = new DBContractProd();
    $model->contract_id = $this->id;
    $model->od_contract_product_id = $product->id;
    $model->title_en = $product->title_en;
    $model->title_ru = $product->title_ru;
    $model->description_en = $product->description_en;
    $model->description_ru = $product->description_ru;
    $model->unit = $product->unit;
    $model->price = ($post['prs'] > 0) ? $post['prs'] : $product->price;
    $model->quantity = ($post['qty'] > 0) ? $post['qty'] : 1;
    $max = $post['product']->qty_db_contract;
    if ($model->quantity > $max) {
      $model->quantity = $max;
      $txt = "Максимальная кол-во для продукта `{$model->description_en}` равно на {$max}! доступное кол-во сохранилься автоматически";
      $this->response['messages'][] = ['id' => $model->od_contract_product_id, 'text' => $txt];
    }
    $model->total_price = ($model->quantity * $model->price);
    $model->qty_db_invoice = $model->quantity;
    $model->summ_db_invoice = $model->total_price;
    $model->podtverjden = Podtverjdeno::NET;
    $model->code = $product->code;
    $model->country_en = $product->country_en;
    $model->country_ru = $product->country_ru;
    $model->save();
    $this->response['messages'][] = [
      'id' => $product->id, 
      'text' => "Продукт `{$product->description_en}` успешно добавлен"
    ];
  }

  public function deleteOne($sp) {
    if(!empty($sp->dBInvoiceProducts)){
      $this->response['messages'][] = [
        'id' => $sp->od_contract_product_id,
        'text' => "Продукту `{$sp->description_en}` не можем убрать из список, так как она используется для Инвойса"
      ];
    } else {
      $sp->delete();
      $this->response['messages'][] = [
        'id' => $sp->od_contract_product_id,
        'text' => "Продукт `{$sp->description_en}` успешно удален"
      ];
    }
  }

  public function updateOne($sp, $post) {
    $product = $post['product'];
    $all_inv_prod_qty = $this->getInvoiceProdQty($sp->id);
    $all_con_prod_qty = $this->getContractProdQty($product->id, $sp->id);
    $txt = "";
    $o_qty = (int)$sp->quantity;
    $o_prs = (float)$sp->price;
    if ($post['qty'] != -1) {
      $sp->quantity = ($post['qty'] > 0) ? $post['qty'] : 1;
    }
    if ($post['prs'] != -1 && $post['prs'] > 0) {
      $sp->price = $post['prs'];
      Yii::$app->db->createCommand("UPDATE ".DBInvoiceProd::tableName()." SET price=:price, total_price=quantity*:price WHERE db_contract_product_id = :prod_id", [':price' => $sp->price, ':prod_id' => $sp->id])->execute();
    }
    $min = $all_inv_prod_qty;
    if ($sp->quantity < $min) {
      $sp->quantity = $min;
      $txt = "Продукт `{$sp->description_en}` используется для инвойса! минимальная количество {$all_inv_prod_qty} автоматом сохранилься";
      $this->response['messages'][$sp->od_contract_product_id] = ['id' => $sp->od_contract_product_id, 'text' => $txt];
    }

    $max = (int)($product->quantity - $all_con_prod_qty);
    if ($sp->quantity > $max) {
      $sp->quantity = $max;
      $txt = "Максимальная кол-во для продукта `{$sp->description_en}` равно на {$max}! доступное кол-во сохранилься автоматически";
      $this->response['messages'][$sp->od_contract_product_id] = ['id' => $sp->od_contract_product_id, 'text' => $txt];
    }

    $sp->qty_db_invoice = ($sp->quantity - $all_inv_prod_qty);
    $sp->summ_db_invoice = ($sp->qty_db_invoice * $sp->price);
    $sp->total_price = ($sp->quantity * $sp->price);
    if ($sp->quantity != $o_qty || $sp->price != $o_prs) {
      $sp->podtverjden = Podtverjdeno::NET;
    }
    $sp->save();
    if (($sp->quantity != $o_qty || $sp->price != $o_prs) && !isset($this->response['messages'][$sp->od_contract_product_id])) {
      $txt = "Продукт `{$sp->description_en}` успешно обнавлен";
      $this->response['messages'][$sp->od_contract_product_id] = ['id' => $sp->od_contract_product_id, 'text' => $txt];
    }
  }

  public function saveProducts() {

    $contract_id = $this->id;
    $products = $this->availableProducts();
    $self_prods = $this->selfProducts();
    if (!empty($products)) {
      if (!empty($self_prods)) {
        $ostatok = $self_prods;
        foreach ($self_prods as $kp => $sp) {
          if (isset($products[$kp])) {
            $this->updateOne($sp, $products[$kp]);
            unset($ostatok[$kp]);
            unset($products[$kp]);
          }
        }
        if (!empty($ostatok)) {
          foreach ($ostatok as $sp) {
            $this->deleteOne($sp);
          }
        }
        if (!empty($products)) {
          foreach ($products as $product) {
            $this->saveOne($product);
          }
        }
        $this->response['code'] = 253;
      } else {
        foreach ($products as $product) {
          $this->saveOne($product);
        }
        $this->response['code'] = 252;
      }
    } else {
      if (!empty($self_prods)) {
        foreach ($self_prods as $sp) {
          $this->deleteOne($sp);
        }
        $this->response['code'] = 251;
      }
    }

    $this->refreshBalance();
    $this->refreshBalanceOfInvoices();

  }

  public function getInvoiceProdQty($prod_id) {
    return (int)DBInvoiceProd::find()->where([
      'db_contract_product_id' => $prod_id,
      'contract_id' => $this->id
    ])->sum('quantity');
  }

  public function getContractProdQty($prod_id, $current_id = 0) {
    return (int)DBContractProd::find()->joinWith('contract')->where([
      DBContractProd::tableName() . '.podtverjden' => Podtverjdeno::DA,
      'od_contract_product_id' => $prod_id
    ])->andWhere(['!=', DBContractProd::tableName() . '.id', $current_id])->sum('quantity');
  }

  public function refreshBalanceOfInvoices() {
    $invoices = $this->invoices;
    if (!empty($invoices)) {
      foreach ($invoices as $key => $value) {
        $value->refreshBalance();
      }
    }
  }

  public function refreshBalanceOfParentContracts() {
    $parentContracts = $this->parentContracts;
    if (!empty($parentContracts)) {
      foreach ($parentContracts as $contr) {
        $contr->refreshBalance();
      }
    }
  }

  public function refreshBalance() {
    $inv_prod_qty = 0;
    $inv_prod_nprice = 0;
    $inv_prod_lprice = 0;
    $total_nprice = 0;
    $total_lprice = 0;
    $products = DBContractProd::find()->where(['contract_id' => $this->id])->all();
    if (!empty($products)) {
      foreach ($products as $key => $value) {

        $all_inv_prod_qty = $this->getInvoiceProdQty($value->id);
        $new_inv_qty = ($value->quantity-$all_inv_prod_qty)>=0 ? ($value->quantity-$all_inv_prod_qty) : 0;
        $inv_prod_qty+=$new_inv_qty;
        $value->qty_db_invoice = $new_inv_qty;
        $value->summ_db_invoice = ($value->qty_db_invoice * $value->price);

        $value->total_price = ($value->quantity * $value->price);
        $value->update();

        $inv_prod_nprice += $value->summ_db_invoice;
        $total_nprice += $value->total_price;
        if ($value->podtverjden != Podtverjdeno::DA) {
          $inv_prod_lprice += $value->summ_db_invoice;
          $total_lprice += $value->total_price;
        }

      }
    }

    $this->refreshBalanceOfParentContracts();

    $this->inv_prod_qty = $inv_prod_qty;
    $this->inv_prod_nprice = $inv_prod_nprice;
    $this->inv_prod_lprice = $inv_prod_lprice;

    $this->total_nprice = $total_nprice;
    $this->total_lprice = $total_lprice;

    $this->receive_sum = (int)DBInvoice::find()->where(['contract_id' => $this->id])->sum('receive_sum');
    $this->update();
  }

  public function getModelName() {
    $name = explode('\\', __CLASS__);
    return array_pop($name);
  }

}
