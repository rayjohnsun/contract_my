<?php
namespace app\models\dil_buyer;

use Yii;
use app\components\Helper;
use app\components\Status;
use app\models\Invoice2Numbers;
use app\models\companies\Buyers;
use app\models\companies\Consignees;
use app\models\companies\Dillers;
use app\models\org_diller\ODInvoice;
use app\models\org_diller\ODInvoiceProd;
use yii\helpers\Url;

class DBInvoice extends \app\base\AModel {

  public static function tableName() {
    return 'f_db_invoice';
  }

  public function rules() {
    return [
      [['ndate', 'consignee_id'], 'required'],
      [['contract_id', 'po_id'], 'required', 'on' => static::SC_CREATE],
      [['dil_text_ru', 'dil_text_en', 'buy_text_ru', 'buy_text_en', 'cons_text_en', 'cons_text_ru', 'notes'], 'string'],
      [['receive_date', 'ndate'], 'string', 'max' => 50],
      [['receive_sum'], 'number'],
      [['receive_sum'], 'default', 'value' => 0],
      [['position', 'director_dil_en', 'director_dil_ru', 'place'], 'string', 'max' => 250]
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'ndate' => 'Дата',
      'number_id' => 'Number',
      'contract_id' => 'Контракт',
      'diller_id' => 'Продавец',
      'dil_text_en' => 'Seller En',
      'dil_text_ru' => 'Seller Ru',
      'buyer_id' => 'Покупатель',
      'buy_text_en' => 'Buyer En',
      'buy_text_ru' => 'Buyer Ru',
      'consignee_id' => 'Грузополучатель',
      'cons_text_en' => 'Consignee En',
      'cons_text_ru' => 'Consignee Ru',
      'notes' => 'Notes',
      'position' => 'Position',
      'director_dil_en' => 'Name',
      'director_dil_ru' => 'Name',
      'place' => 'Place',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'status' => 'Status',
      'nprice' => 'Итоговая цена',
      'receive_sum' => 'Сумма поступление',
      'receive_date' => 'Дата поступление',
      'currency' => 'Currency',
      'den' => 'День',
      'mesyats' => 'Месяц',
      'god' => 'Год',
      'do_ligistic' => 'Логистическая компания',
      'do_price' => 'Стоимость',
      'do_blno' => 'BL №',
      'do_date' => 'Дата',
      'do_pay' => 'Оплата',
      'do_delivery_date' => 'Дата Поставки',
      'do_return_date' => 'Дата возврата контейнера',
      'po_id' => 'Purchase Order',
      'primechanie' => 'Примечание',
    ];
  }

  public function getContract() {
    return $this->hasOne(DBContract::className(), ['id' => 'contract_id']);
  }

  public function getPoInvoice() {
    return $this->hasOne(ODInvoice::className(), ['id' => 'po_id']);
  }

  public function getDiller() {
    return $this->hasOne(Dillers::className(), ['id' => 'diller_id']);
  }

  public function getBuyer() {
    return $this->hasOne(Buyers::className(), ['id' => 'buyer_id']);
  }

  public function getConsignee() {
    return $this->hasOne(Consignees::className(), ['id' => 'consignee_id']);
  }

  public function getNumber() {
    return $this->hasOne(Invoice2Numbers::className(), ['id' => 'number_id']);
  }

  public function getProducts() {
    return $this->hasMany(DBInvoiceProd::className(), ['invoice_id' => 'id']);
  }

  public function getPackinglists() {
    return $this->hasMany(DBPackList::className(), ['invoice_id' => 'id']);
  }

  public function getNew_dil_text_en() {
    return Helper::decodeQuote($this->dil_text_en);
  }

  public function getNew_dil_text_ru() {
    return Helper::decodeQuote($this->dil_text_ru);
  }

  public function getNew_buy_text_en() {
    return Helper::decodeQuote($this->buy_text_en);
  }

  public function getNew_buy_text_ru() {
    return Helper::decodeQuote($this->buy_text_ru);
  }

  public function getNew_cons_text_en() {
    return Helper::decodeQuote($this->cons_text_en);
  }

  public function getNew_cons_text_ru() {
    return Helper::decodeQuote($this->cons_text_ru);
  }

  public function getNew_ndate() {
    return date('d.m.Y', strtotime($this->ndate));
  }

  public function getNew_notes() {
    return Helper::decodeQuote($this->notes);
  }

  public function runBeforeSave() {
    $this->dil_text_en = Helper::encodeQuote($this->dil_text_en);
    $this->dil_text_ru = Helper::encodeQuote($this->dil_text_ru);
    $this->buy_text_en = Helper::encodeQuote($this->buy_text_en);
    $this->buy_text_ru = Helper::encodeQuote($this->buy_text_ru);
    $this->cons_text_en = Helper::encodeQuote($this->cons_text_en);
    $this->cons_text_ru = Helper::encodeQuote($this->cons_text_ru);
    $this->notes = Helper::encodeQuote($this->notes);
    $this->ndate = date("Y-m-d", strtotime($this->ndate));
    if ($this->isNewRecord) {
      $this->number_id = 0;
    }
    if (!empty($this->receive_date)) {
      if (strtotime($this->receive_date) < strtotime($this->ndate)) {
        $this->receive_date = $this->ndate;
      }
    }
  }

  public function runAfterInsert() {
    if ($this->number_id == 0) {
      $num = new Invoice2Numbers();
      if($num->saveAuto('#VT', @$this->contract->number->num)){
        $this->number_id = $num->id;
        $this->update();
      }
    }
    $this->sendMessage('Создали Инвойс D&B: '.$this->number->number, Url::to(['dil_buyer/d-b-invoice/update', 'id' => $this->id]));
  }

  public function runBeforeDelete() {
    $packinglists = $this->packinglists;
    $products = $this->products;
    $this->clear($packinglists);
    $this->clear($products);
    $number = $this->number;
    if (!is_null($number)) {
      $number->delete();
    }
    Yii::$app->session->setFlash('success', 'Suuccessfully deleted.');
  }

  public function runAfterDelete() {
    $this->sendMessage('Удалили Инвойс D&B: '.$this->number->number);
  }

  public function sendUpdatedMessage() {
    $this->sendMessage('Обнавили Инвойс D&B: '.$this->number->number, Url::to(['dil_buyer/d-b-invoice/update', 'id' => $this->id]));
  }

  public function applyDefaults() {
    $this->position = 'Директор / Director';
    $this->place = 'г. Ташкент, Узбекистан / Tashkent, Uzbekistan';
    $this->status = Status::ACTIVE;
  }

  public function selfProducts() {
    $self_prods = $this->products;
    $new_self_prods = [];
    if (!empty($self_prods)) {
      foreach ($self_prods as $key => $value) {
        if ($this->po_id > 0) {
          $new_self_prods[$value->od_po_invoice_product_id] = $value;
        } else {
          $new_self_prods[$value->db_contract_product_id] = $value;
        }
      }
    }
    return $new_self_prods;
  }

  public function availableProducts() {
    $products = Yii::$app->request->post('Products');
    $a_products = [];
    if (!empty($products)) {
      $prods = [];
      $qtys = [];
      foreach ($products as $key => $value) {
        $p_id = isset($value['product_id']) ? (int) $value['product_id'] : 0;
        if ($p_id > 0) {
          $prods[$p_id] = $p_id;
          if (isset($value['quantity'])) {
            // $qty = (isset($value['quantity']) && (int)$value['quantity'] > 0)?(int)$value['quantity']:1;
            $qty = isset($value['quantity'])?str_replace(' ', '', $value['quantity']):'';
            $qty = ((int)$qty > 0)?(int)$qty:1;
            if (isset($qtys[$p_id])) {
              $qtys[$p_id] += $qty;
            } else {
              $qtys[$p_id] = $qty;
            }
          } else {
            $qtys[$p_id] = -1;
          }
        }
      }
      $wh = ['in', 'id', $prods];
      $new_prods = $this->po_id > 0 ? ODInvoiceProd::find()->where($wh)->andWhere(['invoice_id' => $this->po_id])->all() : DBContractProd::find()->where($wh)->andWhere(['contract_id' => $this->contract_id])->all();
      if (!empty($new_prods)) {
        foreach ($new_prods as $key2 => $value2) {
          $a_products[$value2->id]=['product' => $value2,'qty' => $qtys[$value2->id]];
        }
      }
    }
    return $a_products;
  }

  public function saveOne($post) {
    $product = $post['product'];
    $check_qty = $this->po_id > 0 ? $product->qty_po_db_invoice : $product->qty_db_invoice;
    if ($check_qty > 0) {
      $model = new DBInvoiceProd();
      $model->invoice_id = $this->id;
      $model->od_contract_product_id = $product->od_contract_product_id;
      if ($this->po_id > 0) {
        $model->contract_id = 0;
        $model->db_contract_product_id = 0;
        $model->po_id = $this->po_id;
        $model->od_po_invoice_product_id = $product->id;
      } else {
        $model->contract_id = $this->contract_id;
        $model->db_contract_product_id = $product->id;
        $model->po_id = 0;
        $model->od_po_invoice_product_id = 0;
      }
      $model->title_en = $product->title_en;
      $model->title_ru = $product->title_ru;
      $model->description_en = $product->description_en;
      $model->description_ru = $product->description_ru;
      $model->unit = $product->unit;
      $model->code = $product->code;
      $model->country_en = $product->country_en;
      $model->country_ru = $product->country_ru;
      $model->price = $product->price;
      $model->quantity = $post['qty'];
      if ($model->quantity > $check_qty) {
        $model->quantity = $check_qty;
        $this->response['messages'][] = ['id' => $product->id, 'text' => "Доступное количество для продукта `{$model->description_en}` -> `{$model->quantity}`, автоматом сохранили максимальное количество."];
      } else {
        $this->response['messages'][] = [
          'id' => $product->id, 
          'text' => "Продукт `{$product->description_en}` успешно добавлен"
        ];
      }
      $model->total_price = ($model->quantity * $model->price);
      $model->net_weight = 0;
      $model->gross_weight = 0;
      $model->save();
    } else {
      $this->response['messages'][] = ['id' => $product->id, 'text' => "Нет доступное количество продукт `{$product->description_en}`."];
    }
  }

  public function deleteOne($sp) {
    $sp->delete();
    if ($this->po_id > 0) {
      $this->response['messages'][] = [
        'id' => $sp->od_po_invoice_product_id,
        'text' => "Продукт `{$sp->description_en}` успешно удален"
      ];
    } else {
      $this->response['messages'][] = [
        'id' => $sp->db_contract_product_id,
        'text' => "Продукт `{$sp->description_en}` успешно удален"
      ];
    }
  }

  public function updateOne($sp, $post) {
    $product = $post['product'];
    $o_qty = $sp->quantity;
    if ($post['qty'] != -1) {
      $sp->quantity = $post['qty'];
    }
    $sodb = true;
    if ($this->po_id > 0) {
      if ($sp->quantity > ($o_qty+$product->qty_po_db_invoice)) {
        $sp->quantity = ($o_qty+$product->qty_po_db_invoice);
        if ($sp->quantity > 0) {
          $this->response['messages'][] = ['id' => $product->id, 'text' => "Доступное количество для продукта `{$sp->description_en}` -> `{$sp->quantity}`, автоматом сохранили максимальное количество."];
          $sodb = false;
        }
      }
    } else {
      if ($sp->quantity > ($o_qty+$product->qty_db_invoice)) {
        $sp->quantity = ($o_qty+$product->qty_db_invoice);
        if ($sp->quantity > 0) {
          $this->response['messages'][] = ['id' => $product->id, 'text' => "Доступное количество для продукта `{$sp->description_en}` -> `{$sp->quantity}`, автоматом сохранили максимальное количество."];
          $sodb = false;
        }
      }
    }
    if ($sp->quantity > 0) {
      if ($sp->quantity != $o_qty && $sodb == true) {
        $this->response['messages'][] = ['id' => $product->id, 'text' => "Продукт `{$sp->description_en}` успешно обнавлен"];
      }
      $sp->total_price = ($sp->quantity * $sp->price);
      $sp->save();
    } else {
      $this->deleteOne($sp);
    }
  }

  public function saveProducts() {

    $parent = $this->po_id > 0 ? $this->poInvoice : $this->contract;
    if ($parent) {
      $invoice_id = $this->id;
      $products = $this->availableProducts();
      $self_prods = $this->selfProducts();
      if (!empty($products)) {
        if (!empty($self_prods)) {
          $ostatok = $self_prods;
          foreach ($self_prods as $kp => $sp) {
            if (isset($products[$kp])) {
              $this->updateOne($sp, $products[$kp]);
              unset($ostatok[$kp]);
              unset($products[$kp]);
            }
          }
          if (!empty($ostatok)) {
            foreach ($ostatok as $sp) {
              $this->deleteOne($sp);
            }
          }
          if (!empty($products)) {
            foreach ($products as $product) {
              $this->saveOne($product);
            }
          }
          $this->response['code'] = 253;
        } else {
          foreach ($products as $product) {
            $this->saveOne($product);
          }
          $this->response['code'] = 252;
        }
      } else {
        if (!empty($self_prods)) {
          foreach ($self_prods as $sp) {
            $this->deleteOne($sp);
          }
          $this->response['code'] = 251;
        }
      }
      $this->refreshBalance();
      $parent->refreshBalance();
    } else {
      $this->response['message'] = 'Контракт не найден';
      $this->response['code'] = 170;
      $this->response['status'] = false;
    }
  }

  public function refreshBalance() {
    $new_nprice = (float)DBInvoiceProd::find()->where([
      'contract_id' => $this->contract_id, 
      'invoice_id' => $this->id
    ])->sum('total_price');
    if ((float)$this->nprice != $new_nprice) {
      $this->nprice = $new_nprice;
      $this->update();
    }
  }

  public function getModelName() {
    $name = explode('\\', __CLASS__);
    return array_pop($name);
  }
  
}
