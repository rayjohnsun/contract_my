<?php
namespace app\models\dil_buyer\search;

use app\components\Helper;
use app\models\Invoice2Numbers;
use app\models\dil_buyer\DBInvoice;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class DBInvoiceSearch extends DBInvoice {

  public $invoice_ids = [];
  public $nnumber='';

  public function rules() {
    return [
      [['id', 'contract_id', 'po_id', 'diller_id', 'buyer_id', 'consignee_id', 'status', 'number_id'], 'integer'],
      [['invoice_ids', 'nnumber', 'ndate', 'dil_text_ru', 'dil_text_en', 'buy_text_ru', 'buy_text_en', 'cons_text_en', 'cons_text_ru', 'notes', 'position', 'director_dil_en', 'director_dil_ru', 'place', 'created_at', 'updated_at'], 'safe'],
      [['nprice', 'receive_sum'], 'number'],
    ];
  }

  public function scenarios() {
    return Model::scenarios();
  }

  public function search($params) {
    $query = DBInvoice::find()->joinWith('number');

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'number_id' => $this->number_id,
      'contract_id' => $this->contract_id,
      'po_id' => $this->po_id,
      'diller_id' => $this->diller_id,
      'buyer_id' => $this->buyer_id,
      'consignee_id' => $this->consignee_id,
      'status' => $this->status
    ]);

    $query->andFilterWhere(['like', 'dil_text_ru', $this->dil_text_ru])
      ->andFilterWhere(['like', Invoice2Numbers::tableName().'.number', $this->nnumber])
      ->andFilterWhere(['like', 'dil_text_en', $this->dil_text_en])
      ->andFilterWhere(['like', 'buy_text_ru', $this->buy_text_ru])
      ->andFilterWhere(['like', 'buy_text_en', $this->buy_text_en])
      ->andFilterWhere(['like', 'cons_text_en', $this->cons_text_en])
      ->andFilterWhere(['like', 'cons_text_ru', $this->cons_text_ru])
      ->andFilterWhere(['like', 'notes', $this->notes])
      ->andFilterWhere(['like', 'position', $this->position])
      ->andFilterWhere(['like', 'director_dil_en', $this->director_dil_en])
      ->andFilterWhere(['like', 'director_dil_ru', $this->director_dil_ru])
      ->andFilterWhere(['like', 'place', $this->place])
      ->andFilterWhere(['like', 'nprice', $this->nprice])
      ->andFilterWhere(['like', 'receive_sum', $this->receive_sum])
      ->andFilterWhere(['like', 'ndate', Helper::dt($this->ndate)])
      ->andFilterWhere(['in', DBInvoice::tableName().'.id', array_keys($this->invoice_ids)])
      ->andFilterWhere(['like', 'created_at', Helper::dt($this->created_at)]);

    return $dataProvider;
  }
}
