<?php
namespace app\models\dil_buyer\search;

use app\models\dil_buyer\DBParagraph;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class DBParagraphSearch extends DBParagraph {

  public function rules() {
    return [
      [['id', 'norder', 'status'], 'integer'],
      [['title_en', 'title_ru'], 'safe'],
    ];
  }

  public function scenarios() {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  public function search($params) {
    $query = DBParagraph::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['norder' => SORT_ASC]],
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'norder' => $this->norder,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', 'title_en', $this->title_en])
      ->andFilterWhere(['like', 'title_ru', $this->title_ru]);

    return $dataProvider;

  }
}
