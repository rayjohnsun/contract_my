<?php
namespace app\models\dil_buyer\search;

use app\models\dil_buyer\DBParagraphSec;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class DBParagraphSecSearch extends DBParagraphSec {

  public function rules() {
    return [
      [['id', 'paragraph_id', 'norder', 'status'], 'integer'],
      [['title_en', 'title_ru'], 'safe'],
    ];
  }

  public function scenarios() {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  public function search($params) {
    $query = DBParagraphSec::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {

      // $query->where('0=1');
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'paragraph_id' => $this->paragraph_id,
      'norder' => $this->norder,
      'status' => $this->status,
    ]);

    $query->andFilterWhere(['like', 'title_en', $this->title_en])
      ->andFilterWhere(['like', 'title_ru', $this->title_ru]);

    return $dataProvider;
  }
}
