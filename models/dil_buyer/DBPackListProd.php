<?php
namespace app\models\dil_buyer;

use app\models\Countries;
use app\models\Tables;

class DBPackListProd extends \app\base\AModel {

  public static function tableName() {
    return 'f_db_pack_list_prod';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract (O&D)',
      'po_id' => 'Purchase Order id',
      'invoice_id' => 'Invoice (O&D)',
      'packlist_id' => 'Packing List (O&D)',
      'db_invoice_product_id' => 'Contract product (O&D)',
      'title_en' => 'Название O&D (en)',
      'title_ru' => 'Название O&D (ru)',
      'description_en' => 'Название D&B (en)',
      'description_ru' => 'Название D&B (ru)',
      'unit' => 'Unit',
      'code' => 'Code',
      'price' => 'Price',
      'quantity' => 'Quantity',
      'total_price' => 'Total Price',
      'net_weight' => 'Net Weight',
      'gross_weight' => 'Gross Weight',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'country_en' => 'Country',
      'country_ru' => 'Country',
      'container' => 'Container No.',
      't_company' => 'Транспортная компания',
      't_price' => 'Год',
      'sebestoimost' => 'Себестоимость товара'
    ];
  }

  public function getPacklist() {
    return $this->hasOne(DBPackList::className(), ['id' => 'packlist_id']);
  }

  public function getInvoice() {
    return $this->hasOne(DBInvoice::className(), ['id' => 'invoice_id']);
  }

  public function getContract() {
    return $this->hasOne(DBContract::className(), ['id' => 'contract_id']);
  }

  public function getDBInvoiceProduct() {
    return $this->hasOne(DBInvoiceProd::className(), ['id' => 'db_invoice_product_id']);
  }

  public function getTitleRuEn() {
    return $this->title_ru . ' / ' . $this->title_en;
  }

  public function getDescrRuEn() {
    return $this->description_ru . ' / ' . $this->description_en;
  }

  public function getModelName() {
    $name = explode('\\', __CLASS__);
    return array_pop($name);
  }

}
