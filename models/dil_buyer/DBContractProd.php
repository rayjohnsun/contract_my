<?php
namespace app\models\dil_buyer;

use app\models\org_diller\ODContractProd;

class DBContractProd extends \app\base\AModel {

  public static function tableName() {
    return 'f_db_contract_prod';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract ID',
      'od_contract_product_id' => 'Contract product (O&D)',
      'title_en' => 'Название D&B (en)',
      'title_ru' => 'Название D&B (ru)',
      'description_en' => 'Название D&B (en)',
      'description_ru' => 'Название D&B (ru)',
      'unit' => 'Unit',
      'price' => 'Price',
      'quantity' => 'Quantity',
      'code' => 'Code',
      'total_price' => 'Total Price',
      'qty_db_invoice' => 'Qty DB Invoice',
      'summ_db_invoice' => 'Summ DB Invoice',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'podtverjden' => 'Podtverjden',
      'country_en' => 'Country',
      'country_ru' => 'Country'
    ];
  }

  /******************************GETTERS******************************/

  public function getContract() {
    return $this->hasOne(DBContract::className(), ['id' => 'contract_id']);
  }

  public function getODContractProduct() {
    return $this->hasOne(ODContractProd::className(), ['id' => 'od_contract_product_id']);
  }

  public function getDBInvoiceProducts() {
    return $this->hasMany(DBInvoiceProd::className(), ['ob_contract_product_id' => 'id']);
  }

  public function getTitleRuEn() {
    return $this->title_ru . ' / ' . $this->title_en;
  }

  public function getDescrRuEn() {
    return $this->description_ru . ' / ' . $this->description_en;
  }

  // public function getHproducts() {
  //   return $this->hasMany(ContractProductsLTZ::className(), ['g_product_id' => 'id']);
  // }

  // public function beforeDelete() {

  //   if (parent::beforeDelete()) {

  //     if (!empty($this->hproducts)) {

  //       foreach ($this->hproducts as $key => $value) {

  //         $value->delete();

  //       }

  //     }

  //     return true;

  //   }

  //   return false;

  // }

}
