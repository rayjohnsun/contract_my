<?php
namespace app\models\dil_buyer;

use app\models\Countries;
use app\models\dil_buyer\DBContractProd;
use app\models\org_diller\ODContractProd;

class DBInvoiceProd extends \app\base\AModel {

  public static function tableName() {
    return 'f_db_invoice_prod';
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'contract_id' => 'Contract (D&B)',
      'invoice_id' => 'Invoice (D&B)',
      'od_contract_product_id' => 'Contract product (O&D)',
      'db_contract_product_id' => 'Contract product (D&B)',
      'po_id' => 'PO Invoice (D&B)',
      'od_po_invoice_product_id' => 'PO Invoice product (D&B)',
      'title_en' => 'Название D&B (en)',
      'title_ru' => 'Название D&B (ru)',
      'description_en' => 'Название D&B (en)',
      'description_ru' => 'Название D&B (ru)',
      'unit' => 'Unit',
      'code' => 'Code',
      'price' => 'Price',
      'quantity' => 'Quantity',
      'total_price' => 'Total Price',
      'net_weight' => 'Net Weight',
      'gross_weight' => 'Gross Weight',
      'created_at' => 'Создан',
      'updated_at' => 'Updated At',
      'country_en' => 'Country',
      'country_ru' => 'Country',
      'qty_db_invoice' => 'Qty DB Invoice',
      'summ_db_invoice' => 'Summ DB Invoice',
    ];
  }

  /******************************GETTERS******************************/

  public function getInvoice() {
    return $this->hasOne(DBInvoice::className(), ['id' => 'invoice_id']);
  }

  public function getContract() {
    return $this->hasOne(DBContract::className(), ['id' => 'contract_id']);
  }

  public function getODContractProduct() {
    return $this->hasOne(ODContractProd::className(), ['id' => 'od_contract_product_id']);
  }

  public function getDBContractProduct() {
    return $this->hasOne(DBContractProd::className(), ['id' => 'db_contract_product_id']);
  }

  public function getTitleRuEn() {
    return $this->title_ru . ' / ' . $this->title_en;
  }

  public function getDescrRuEn() {
    return $this->description_ru . ' / ' . $this->description_en;
  }

}
