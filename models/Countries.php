<?php
namespace app\models;

use Yii;
use app\components\Status;
use app\models\companies\Buyers;
use app\models\companies\Consignees;
use app\models\companies\Dillers;
use app\models\companies\Organizations;

class Countries extends \app\base\AModel {
  public static function tableName() {
    return 'b_countries';
  }

  public function rules() {
    return [
      [['title_en', 'title_ru'], 'required'],
      [['title_en', 'title_ru'], 'string', 'max' => 250],
      [['title_en', 'title_ru'], 'trim'],
      [['title_en'], 'unique'],
      [['title_ru'], 'unique'],
      [['status'], 'integer'],
      [['status'], 'in', 'range' => Status::list()],
    ];
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'title_en' => 'Название (en)',
      'title_ru' => 'Название (ru)',
      'status' => 'Статус',
      'created_at' => 'Создан',
      'updated_at' => 'Редактирован',
    ];
  }

  public function returnBeforeDelete() {
    $cnt1 = Organizations::find()->where([
      'country_id' => $this->id,
    ])->count();
    $cnt2 = Dillers::find()->where([
      'country_id' => $this->id,
    ])->count();
    $cnt3 = Buyers::find()->where([
      'country_id' => $this->id,
    ])->count();
    $cnt4 = Consignees::find()->where([
      'country_id' => $this->id,
    ])->count();

    if ($cnt1<1&&$cnt2<1&&$cnt3<1&&$cnt4<1) {
      return true;
    } else {
      Yii::$app->session->setFlash('message', "Error to delete: Organization <strong>{$this->nTitle}</strong> is used.");
    }
    return false;
  }
  
}
