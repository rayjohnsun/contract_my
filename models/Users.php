<?php
namespace app\models;

use Yii;
use app\components\Status;
use app\rbac\Roles;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

class Users extends \app\base\AModel implements IdentityInterface {
  public $password;

  public static function tableName() {
    return 'b_users';
  }

  public function rules() {
    return [
      [['username', 'fio'], 'required'],
      [['password', 'role_id'], 'required', 'on' => static::SC_CREATE],
      [['status'], 'integer'],
      [['status'], 'in', 'range' => Status::list()],
      [['status'], 'default', 'value' => Status::ACTIVE],
      [['fio'], 'string', 'max' => 250],
      [['username'], 'trim'],
      [['username'], 'unique', 'message' => 'В системе зарегистрирован такой логин!'],
      [['username', 'password'], 'string', 'min' => 5, 'max' => 100],
      [['email'], 'trim'],
      [['email'], 'unique', 'message' => 'В системе зарегистрирован пользователь с такой электронной почтой', 'when' => function ($m) {
        return !empty($m->email);
      }],
      [['email'], 'string', 'min' => 5, 'max' => 200],
      [['email'], 'default', 'value' => null],
      [['role_id'], 'in', 'range' => Roles::list(), 'on' => static::SC_CREATE],
      ['role_id', 'validateRole', 'on' => static::SC_CREATE]
    ];
  }

  public function validateRole($attribute, $params, $validator) {
    if ($this->isNewRecord) {
      if ($this->$attribute == Roles::ADMIN) {
        $this->addError($attribute, 'User alreay created by role ' . Roles::get($this->$attribute));
      }
    }
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'username' => 'Логин',
      'email' => 'Email',
      'password_hash' => 'Пароль',
      'auth_key' => 'Auth Key',
      'access_token' => 'Access Token',
      'role_id' => 'Роль',
      'fio' => 'ФИО',
      'password_reset_token' => 'Password Reset Token',
      'password' => 'Пароль',
      'balance' => 'Баланс',
      'status' => 'Статус',
      'created_at' => 'Создан',
      'updated_at' => 'Редактирован',
    ];
  }

  public function runBeforeSave() {
    if ($this->isNewRecord) {
      $this->setPassword($this->password);
      $this->generateAuthKey();
      $this->generateAccessToken();
    } else {
      if (!empty($this->password)) {
        $this->setPassword($this->password);
      }
    }
  }

  public function returnBeforeDelete() {
    if ($this->id == Yii::$app->user->identity->id) {
      Yii::$app->session->setFlash('message', "You can not delete <strong>yourself</strong>.");
      return false;
    }
    if (static::find()->where(['role_id' => $this->role_id])->count() > 1) {
      return true;
    }
    Yii::$app->session->setFlash('message', "Can not be delete User <strong>{$this->fio}</strong>.");
    return false;
  }

  public static function findIdentity($id) {
    return static::findOne(['id' => $id, 'status' => Status::ACTIVE]);
  }

  public static function findIdentityByAccessToken($token, $type = null) {
    throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
  }

  public static function findByUsername($username) {
    return static::findOne(['username' => $username, 'status' => Status::ACTIVE]);
  }

  public function getId() {
    return $this->getPrimaryKey();
  }

  public function getAuthKey() {
    return $this->auth_key;
  }

  public function validateAuthKey($auth_key) {
    return $this->getAuthKey() === $auth_key;
  }

  public function validatePassword($password) {
    return Yii::$app->security->validatePassword($password, $this->password_hash);
  }

  public function setPassword($password) {
    $this->password_hash = Yii::$app->security->generatePasswordHash($password);
  }

  public function generateAuthKey() {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }

  public function generateAccessToken() {
    $this->access_token = 'acc' . Yii::$app->security->generateRandomString();
  }

  public static function findByPasswordResetToken($token) {
    if (!static::isPasswordResetTokenValid($token)) {
      return null;
    }
    return static::findOne(['password_reset_token' => $token, 'status' => Status::ACTIVE]);
  }

  public static function isPasswordResetTokenValid($token) {
    if (empty($token)) {
      return false;
    }
    $timestamp = (int) substr($token, strrpos($token, '_') + 1);
    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
    return ($timestamp + $expire) >= time();
  }

  public function generatePasswordResetToken() {
    $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
  }

  public function removePasswordResetToken() {
    $this->password_reset_token = null;
  }
}
