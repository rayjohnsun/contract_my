<?php

namespace app\commands;

use Yii;
use app\rbac\Roles;
use app\rbac\UserRule;
use yii\console\Controller;
use yii\console\ExitCode;

class RbacController extends Controller
{

    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        //Включаем наш обработчик
        $rule = new UserRule();
        $auth->add($rule);

        //Создадим для примера права для доступа к админке
        $dashboard = $auth->createPermission('dashboard');
        $dashboard->description = 'Админ панель';
        $dashboard->ruleName = $rule->name;
        $auth->add($dashboard);

        //Добавляем роли
        $admin = $auth->createRole('admin');
        $admin->description = Roles::get(Roles::ADMIN);
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        $manager = $auth->createRole('manager');
        $manager->description = Roles::get(Roles::MANAGER);
        $manager->ruleName = $rule->name;
        $auth->add($manager);

        $organization = $auth->createRole('organization');
        $organization->description = Roles::get(Roles::ORGANIZATION);
        $organization->ruleName = $rule->name;
        $auth->add($organization);

        $diller = $auth->createRole('diller');
        $diller->description = Roles::get(Roles::DILLER);
        $diller->ruleName = $rule->name;
        $auth->add($diller);

        //Добавляем потомков
        $auth->addChild($admin, $manager);
        $auth->addChild($admin, $organization);
        $auth->addChild($admin, $diller);
        $auth->addChild($admin, $dashboard);

        return ExitCode::OK;
    }
}
