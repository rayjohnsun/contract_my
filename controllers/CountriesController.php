<?php
namespace app\controllers;

use app\components\Status;
use app\models\Countries;
use app\models\search\CountriesSearch;
use Yii;

class CountriesController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('some_admin');
  }

  protected function findModel($id) {
    return parent::fModel(Countries::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Countries";
    $searchModel = new CountriesSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionCreate() {
    $this->setBred('Countries', 'index');
    $this->title = 'create';
    $model = new Countries();
    $model->status = Status::ACTIVE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create');
  }

  public function actionUpdate($id) {
    $this->setBred('Countries', ['index']);
    $model = $this->findModel($id);
    $this->title = 'Update: ' . $model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update');
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
