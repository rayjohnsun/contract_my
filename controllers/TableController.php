<?php

namespace app\controllers;

use Yii;
use app\components\POInvoice;
use app\models\InvoiceProductsVT;
use app\models\MyTables;
use app\models\dil_buyer\DBContract;
use app\models\dil_buyer\DBInvoice;
use app\models\dil_buyer\DBPackListProd;
use app\models\org_buyer\OBContract;
use app\models\org_buyer\OBInvoice;
use app\models\org_buyer\OBPackListProd;
use app\models\org_diller\ODContract;
use app\models\org_diller\ODInvoice;
use app\models\org_diller\ODPackListProd;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;

class TableController extends \app\base\AController {

	public function behaviors() {
    return $this->generateRules([
      [
        'allow' => true,
        'actions' => ['index', 'list', 'save'],
        'roles' => ['table'],
      ],
      // [
      //   'allow' => true,
      //   'actions' => ['save'],
      //   'roles' => ['manager'],
      // ],
    ]);
  }

	public function actionIndex() {
		$user_table = MyTables::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
		if (Yii::$app->request->post()) {
			if (!$user_table) {
				$user_table = new MyTables();
				$user_table->user_id = Yii::$app->user->identity->id;
			}
			if ($user_table->load(Yii::$app->request->post())) {
				if($user_table->save()){
					Yii::$app->session->setFlash('success', 'Успешно сохранен');
					$this->redirect(['table/index']);
				} else {
					Yii::$app->session->setFlash('error', 'Ошибка!');
				}
			}
		}
		Yii::$app->params['full_height'] = true;
		$data1 = ODContract::find()->joinWith([
			'number', 
			'invoices', 
			'invoices.number',
			'invoices.packinglists',
			'invoices.packinglists.products',
			'invoices.consignee',
			'diller',
			'od_db',
			'od_db.dBContract',
			'od_db.dBContract.number',
			'od_db.dBContract.buyer',
			'od_db.dBContract.invoices',
			'od_db.dBContract.invoices.number',
			'od_db.dBContract.invoices.products'
		])->orderBy(['ndate' => SORT_ASC])->all();
		$data2 = OBContract::find()->joinWith([
			'number', 
			'invoices', 
			'invoices.number',
			'invoices.packinglists',
			'invoices.packinglists.products',
			'invoices.consignee',
			'buyer',
		])->orderBy(['ndate' => SORT_ASC])->all();
		$data = array_merge($data1, $data2);
		usort($data, function ($a, $b){
			$date1 = strtotime($a->ndate);
			$date2 = strtotime($b->ndate);
			if ($date1 != $date2) {
				return $date1 > $date2;
			} else {
				$cdate1 = strtotime($a->created_at);
				$cdate2 = strtotime($b->created_at);
				return $cdate1 > $cdate2;
			}
		});
		$headers = [
			'column1' => ['title' => "Номер инвойса", 'visible' => true],
			'column2' => ['title' => "Дата", 'visible' => true],
			'column3' => ['title' => "День", 'visible' => true],
			'column4' => ['title' => "Месяц", 'visible' => true],
			'column5' => ['title' => "Год", 'visible' => true],
			'column6' => ['title' => "Наименование товара", 'visible' => true],
			'column7' => ['title' => "Номер контейнер / авто", 'visible' => true],
			'column8' => ['title' => "Поддон", 'visible' => true],
			'column9' => ['title' => "Ед. изм.", 'visible' => true],
			'column10' => ['title' => "Кол-во", 'visible' => true],
			'column11' => ['title' => "Цена", 'visible' => true],
			'column12' => ['title' => "Сумма", 'visible' => true],
			'column13' => ['title' => "Валюта", 'visible' => true],
			'column14' => ['title' => "Страна", 'visible' => true],
			'column15' => ['title' => "Грузополучатель", 'visible' => false],
			'column16' => ['title' => "Номер контракта", 'visible' => false],
			'column17' => ['title' => "Покупатель VT", 'visible' => false],
			'column18' => ['title' => "Покупатель", 'visible' => false],
			'column19' => ['title' => "Контракт №	", 'visible' => false],
			'column20' => ['title' => "Дата", 'visible' => false],
			'column21' => ['title' => "Инвойс №", 'visible' => false],
			'column22' => ['title' => "Дата", 'visible' => false],
			'column23' => ['title' => "Наименование товара", 'visible' => false],
			'column24' => ['title' => "Ед. Изм", 'visible' => false],
			'column25' => ['title' => "Кол-во", 'visible' => false],
			'column26' => ['title' => "Цена", 'visible' => false],
			'column27' => ['title' => "Сумма", 'visible' => false],
			'column28' => ['title' => "Логистическая компания", 'visible' => false],
			'column29' => ['title' => "Стоимость", 'visible' => false],
			'column30' => ['title' => "BL №", 'visible' => false],
			'column31' => ['title' => "Дата", 'visible' => false],
			'column32' => ['title' => "Оплата", 'visible' => false],
			'column33' => ['title' => "Дата Поставки", 'visible' => false],
			'column34' => ['title' => "Дата возврата контейнера", 'visible' => false]
		];
		return $this->render('index', ['data' => $data, 'headers'=> $headers, 'user_table' => $user_table]);
	}

	public function actionList() {
		$user_table = MyTables::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
		if (Yii::$app->request->post()) {
			if (!$user_table) {
				$user_table = new MyTables();
				$user_table->user_id = Yii::$app->user->identity->id;
			}
			if ($user_table->load(Yii::$app->request->post())) {
				if($user_table->save()){
					Yii::$app->session->setFlash('success', 'Успешно сохранен');
					$this->redirect(['table/list']);
				} else {
					Yii::$app->session->setFlash('error', 'Ошибка!');
				}
			}
		}
		Yii::$app->params['full_height'] = true;
		$data0 = ODContract::find()->joinWith([
			'number', 
			'invoices', 
			'invoices.number',
			'invoices.packinglists',
			'invoices.packinglists.products',
			'invoices.consignee',
			'diller',
		])->where(['c_od_invoice.po' => POInvoice::YES])->orderBy(['ndate' => SORT_ASC])->all();
		$data1 = DBContract::find()->joinWith([
			'number', 
			'invoices', 
			'invoices.number',
			'invoices.packinglists',
			'invoices.packinglists.products',
			'invoices.consignee',
			'buyer',
		])->orderBy(['ndate' => SORT_ASC])->all();
		$data2 = OBContract::find()->joinWith([
			'number', 
			'invoices', 
			'invoices.number',
			'invoices.packinglists',
			'invoices.packinglists.products',
			'invoices.consignee',
			'buyer',
		])->orderBy(['ndate' => SORT_ASC])->all();
		$data = array_merge($data0, $data1, $data2);
		usort($data, function ($a, $b){
			$date1 = strtotime($a->ndate);
			$date2 = strtotime($b->ndate);
			if ($date1 != $date2) {
				return $date1 > $date2;
			} else {
				$cdate1 = strtotime($a->created_at);
				$cdate2 = strtotime($b->created_at);
				return $cdate1 > $cdate2;
			}
		});

		$headers['column1'] = ['title' => "Номер контракта/PO", 'visible' => true];
		$headers['column2'] = ['title' => "Дата контракта/PO", 'visible' => true];
		$headers['column3'] = ['title' => "Продавец", 'visible' => true];
		$headers['column4'] = ['title' => "Покупатель", 'visible' => true];
		$headers['column5'] = ['title' => "Грузополучатель", 'visible' => true];
		$headers['column6'] = ['title' => "Страна", 'visible' => true];
		$headers['column7'] = ['title' => "Номер инвойса", 'visible' => true];
		$headers['column8'] = ['title' => "Дата", 'visible' => true];
		if (Yii::$app->user->can('manage_table')) {
			$headers['column9'] = ['title' => "День", 'visible' => true];
			$headers['column10'] = ['title' => "Месяц", 'visible' => true];
			$headers['column11'] = ['title' => "Год", 'visible' => true];
		}
		$headers['column12'] = ['title' => "Наименование товара", 'visible' => true];
		$headers['column13'] = ['title' => "Номер контейнер / авто", 'visible' => true];
		$headers['column14'] = ['title' => "Кол-во (поддон)", 'visible' => true];
		$headers['column15'] = ['title' => "Ед. изм.", 'visible' => true];
		$headers['column16'] = ['title' => "Кол-во (тн)", 'visible' => true];
		$headers['column17'] = ['title' => "Цена", 'visible' => true];
		$headers['column18'] = ['title' => "Сумма", 'visible' => true];
		$headers['column19'] = ['title' => "Валюта", 'visible' => true];
		if (Yii::$app->user->can('manage_table')) {
			$headers['column20'] = ['title' => "Транспортная компания", 'visible' => true];
			$headers['column21'] = ['title' => "Ставка на транспорта", 'visible' => true];
		}
		$headers['column22'] = ['title' => "Цена транспортировки за 1 тн.", 'visible' => true];
		$headers['column23'] = ['title' => "Доход от продажи 1 тн.", 'visible' => true];
		$headers['column24'] = ['title' => "Итого доход", 'visible' => false];
		if (Yii::$app->user->can('manage_table')) {
			$headers['column25'] = ['title' => "Себестоимость товара", 'visible' => false];
		}
		$headers['column26'] = ['title' => "Цена между VT-Dealer", 'visible' => false];
		$headers['column27'] = ['title' => "Разница", 'visible' => false];
		$headers['column28'] = ['title' => "Итого", 'visible' => false];
		$headers['column29'] = ['title' => "Прибыль", 'visible' => false];
		$headers['column30'] = ['title' => "Итого прибыль", 'visible' => false];
		$headers['column31'] = ['title' => "Примечание", 'visible' => false];
		return $this->render('list', ['data' => $data, 'headers'=> $headers, 'user_table' => $user_table]);
	}

	public function getPostData($value, $in_attrs) {
		$post = Yii::$app->request->post($value);
		if (!empty($post)) {
			$p_id = (int)key($post);
			$post_param = (array)reset($post);
			$p_attr = (string)key($post_param);
			$p_val = (string)reset($post_param);
			if ($p_attr == 't_price' || $p_attr == 'sebestoimost') {
				$p_val = (float)str_replace(' ', '', $p_val);
				$p_val = $p_val*100;
				$p_val = round($p_val)/100;
			}
			if ($p_id > 0 && in_array($p_attr, $in_attrs)) {
				return ['id' => $p_id, 'attr' => $p_attr, 'value' => $p_val];
			}
		}
		return [];
	}

	public function savePostData($model, $post) {
		$attr = $post['attr'];
		$value= $post['value'];
		if ($model) {
			$model->$attr = $value;
			if($model->save()){
				$this->setResponse("Suuccessfully saved", 341, true, ['value' => $value]);
			} else {
				$this->setResponse("Not saved, try again please", 241, false);
			}
		} else {
			$this->setResponse("Invoice is not found", 242, false);
		}
	}

	public function actionSave(){
		if (Yii::$app->request->isAjax) {
			$in_attrs = ['den', 'mesyats', 'god', 'contract_no', 'primechanie'];
			$in_attrs2 = ['t_company', 't_price', 'sebestoimost'];
			// $in_attrs2= ['do_ligistic', 'do_price', 'do_blno', 'do_date', 'do_pay', 'do_delivery_date', 'do_return_date'];
			$od = $this->getPostData('ODInvoice', $in_attrs);
			$ob = $this->getPostData('OBInvoice', $in_attrs);
			$db = $this->getPostData('DBInvoice', $in_attrs);

			$odp= $this->getPostData('ODPackListProd', $in_attrs2);
			$obp= $this->getPostData('OBPackListProd', $in_attrs2);
			$dbp= $this->getPostData('DBPackListProd', $in_attrs2);

			if (!empty($od)) {
				$model = ODInvoice::findOne($od['id']);
				$this->savePostData($model, $od);
			} else if (!empty($ob)) {
				$model = OBInvoice::findOne($ob['id']);
				$this->savePostData($model, $ob);
			} else if (!empty($db)) {
				$model = DBInvoice::findOne($db['id']);
				$this->savePostData($model, $db);
			} else if (!empty($odp)) {
				$model = ODPackListProd::findOne($odp['id']);
				$this->savePostData($model, $odp);
			} else if (!empty($obp)) {
				$model = OBPackListProd::findOne($obp['id']);
				$this->savePostData($model, $obp);
			} else if (!empty($dbp)) {
				$model = DBPackListProd::findOne($dbp['id']);
				$this->savePostData($model, $dbp);
			} else {
				$this->setResponse("Nothing to save", 243, false);
			}
		} else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
	}

}