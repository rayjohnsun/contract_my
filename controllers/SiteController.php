<?php

namespace app\controllers;

use app\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SiteController extends Controller {
  public $layout = 'main-login';
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout'],
        'rules' => [
          [
            'actions' => ['logout'],
            'allow' => true,
            'roles' => ['@'],
          ],
          [
            'actions' => ['index', 'error'],
            'allow' => true,
          ],
          [
            'actions' => ['login', 'request-password-reset', 'reset-password'],
            'allow' => true,
            'roles' => ['?'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  public function actions() {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
      // 'captcha' => [
      //     'class' => 'yii\captcha\CaptchaAction',
      //     'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      // ],
    ];
  }

  public function actionIndex() {
    $this->layout = 'main';
    if (Yii::$app->user->isGuest) {
      return $this->redirect('login');
    }
    return $this->render('index');
  }

  public function actionLogin() {

    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    $model->password = '';
    return $this->render('login', ['model' => $model]);
  }

  public function actionLogout() {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  // public function actionAaa (){

  //     $pasword = Yii::$app->security->generatePasswordHash('admin');

  //     return $pasword;
  // }

  // public function actionRequestPasswordReset()
  // {
  //     $this->layout = 'main-login';

  //     $model = new PasswordResetRequestForm();

  //     if ($model->load(Yii::$app->request->post()) && $model->validate()) {
  //         if ($model->sendEmail()) {
  //             Yii::$app->session->setFlash('success', 'Проверите свой email для дальнейших инструкций.');
  //             return $this->goHome();
  //         } else {
  //             Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
  //         }
  //     }

  //     return $this->render('passwordResetRequestForm', [
  //         'model' => $model,
  //     ]);
  // }

  // public function actionResetPassword($token)
  // {
  //     try {
  //         $model = new ResetPasswordForm($token);
  //     } catch (InvalidParamException $e) {
  //         throw new BadRequestHttpException($e->getMessage());
  //     }

  //     if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
  //         Yii::$app->session->setFlash('success', 'New password was saved.');
  //         return $this->goHome();
  //     }

  //     return $this->render('resetPasswordForm', ['model' => $model]);
  // }

  // public function actionContact()
  // {
  //     $model = new ContactForm();
  //     if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
  //         Yii::$app->session->setFlash('contactFormSubmitted');

  //         return $this->refresh();
  //     }
  //     return $this->render('contact', [
  //         'model' => $model,
  //     ]);
  // }

  // public function actionAbout()
  // {
  //     return $this->render('about');
  // }
}
