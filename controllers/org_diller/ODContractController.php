<?php
namespace app\controllers\org_diller;

use Yii;
use app\components\Currencies;
use app\components\Helper;
use app\components\Podtverjdeno;
use app\components\Status;
use app\components\Zapolnen;
use app\components\text\ODTxt;
use app\export\ExportODContract;
use app\models\Products;
use app\models\companies\Dillers;
use app\models\companies\Organizations;
use app\models\org_diller\ODContract;
use app\models\org_diller\ODContractProd;
use app\models\org_diller\ODParagraph;
use app\models\org_diller\search\ODContractSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class ODContractController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules([
      [
        'allow' => true,
        'actions' => ['index', 'create', 'update', 'delete', 'to-word', 'podtverdit', 'edit', 'get-products', 'add-products', 'balance-products'],
        'roles' => ['organization', 'diller'],
      ],
    ]);
  }

  protected function findModel($id) {
    return parent::fModel(ODContract::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Contract";
    $searchModel = new ODContractSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $organizations = Organizations::nList('id', 'title_ru');
    $dillers = Dillers::nList('id', 'title_ru');
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'organizations' => $organizations,
      'dillers' => $dillers
    ]);
  }

  public function actionCreate($od_id=0, $db_id=0, $cy=0) {
    $organization = Organizations::findOne($od_id);
    $diller = Dillers::findOne($db_id);
    if (!empty($organization) && !empty($diller) && in_array((int)$cy, Currencies::list())) {
      $model = new ODContract();
      $model->scenario = ODContract::SC_CREATE;
      $model->organization_id = $organization->id;
      $model->diller_id = $diller->id;
      $now = date("Y-m-d");
      $model->ndate = $now;
      $model->currency = $cy;
      $model->zapolnen = Zapolnen::NO;
      $model->director_org_en = $organization->director_en;
      $model->director_org_ru = $organization->director_ru;
      $model->director_dil_en = $diller->director_en;
      $model->director_dil_ru = $diller->director_ru;
      $mainParagSection = $model->mainParagSection;
      $model->text_en = '';
      $model->text_ru = '';
      if (!is_null($mainParagSection)) {
        $model->text_en = Helper::replacer($mainParagSection->title_en, [
          '{{seller_en}}' => $organization->title_en, 
          '{{buyer_en}}' => $diller->title_en, 
          '{{seller_director_en}}' => $model->director_org_en, 
          '{{buyer_director_en}}' => $model->director_dil_en, 
          '{{seller_country_en}}' => $organization->country->title_en,
          '{{buyer_country_en}}' => $diller->country->title_en
        ]);
        $model->text_ru = Helper::replacer($mainParagSection->title_ru, [
          '{{seller_ru}}' => $organization->title_ru, 
          '{{buyer_ru}}' => $diller->title_ru, 
          '{{seller_director_ru}}' => $model->director_org_ru, 
          '{{buyer_director_ru}}' => $model->director_dil_ru, 
          '{{seller_country_ru}}' => $organization->country->title_ru,
          '{{buyer_country_ru}}' => $diller->country->title_ru
        ]);
      }
      $model->note_en = ODTxt::noteEn(['{{seller_en}}' => $organization->title_en]);
      $model->note_ru = ODTxt::noteRu(['{{seller_ru}}' => $organization->title_ru]);
      $model->applyDefaults();
      if($model->save()){
        return $this->redirect(['update', 'id' => $model->id]);
      } else {
        Yii::$app->session->setFlash('error', 'Can not create Contract, please try again');
        return $this->redirect(['index']);
      }
    } else {
      Yii::$app->session->setFlash('error', 'Please Select Organization, Dailer and currency');
      return $this->redirect(['index']);
    }
  }

  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $organization = $model->organization;
    $diller = $model->diller;
    if ($model->zapolnen == Zapolnen::YES) {
      $paragraphs = $model->paragraphs;
    } else {
      $parags = ODParagraph::find()
        ->where(['status' => Status::ACTIVE])
        ->andWhere(['>', 'norder', 0])
        ->orderBy(['norder' => SORT_ASC])
        ->all();
      $paragraphs = [];
      if (!empty($parags)) {
        foreach ($parags as $key => $value) {
          $sections = $value->sections;
          $valsec = $value;
          if (!empty($sections)) {
            $new_sections = [];
            foreach ($sections as $key2 => $value2) {
              $value2->title_en = ODContract::replEn($value2->title_en, $organization, $diller);
              $value2->title_ru = ODContract::replRu($value2->title_ru, $organization, $diller);
              $new_sections[] = $value2;
            }
            $valsec->setSections($new_sections);
          }
          $paragraphs[] = $valsec;
        }
      }
    }

    return $this->render('edit_contract', [
      'model' => $model,
      'organization' => $organization,
      'diller' => $diller,
      'paragraphs' => $paragraphs
    ]);
  }

  public function actionEdit($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      if ($model->load(Yii::$app->request->post())) {
        $products = Yii::$app->request->post('Products');
        if (!empty($products)) {
          $model->zapolnen = Zapolnen::YES;
          if ($model->save()) {
            $model->saveParagraphs();
            $model->saveProducts();
            $model->sendUpdatedMessage();
            $this->loadResponse($model);
            $this->setResponse("Contract Suuccessfully saved", null, true, ['applayed' => ($model->total_lprice <= 0)]);
          } else {
            $this->setResponse($model->allErrors, 123);
          }
        } else {
          $this->setResponse("Не выбран продукт", 122);
        }
      } else {
        $this->setResponse("Can not get Post data", 121);
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionGetProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $products = $model->products;
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $p_data[] = [
            'p_id' => $value->product_id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->quantity
          ];
        }
      }
      $this->setResponse("Success", 301, true, $p_data);
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionAddProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $self_prods = ArrayHelper::map($model->products, 'product_id', 'product_id');
      $products = Products::find()->joinWith('country')->where(['not in', Products::tableName().'.id', $self_prods])->all();
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $p_data[] = [
            'p_id' => $value->id, 
            'p_code' => $value->code, 
            'p_country' => $value->country->title_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => 1
          ];
        }
        $this->setResponse("Suuccessfully", 302, true, $p_data);
      } else {
        $this->setResponse("Продуктов нет для добавлении");
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionBalanceProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $total = (float)ODContractProd::find()->where(['contract_id' => $model->id])->sum('total_price');
      $this->setResponse('Success', 333, true, ['balance' => $total]);
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionPodtverdit($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      if ($model->total_lprice > 0) {
        ODContractProd::updateAll(['podtverjden' => Podtverjdeno::DA], ['contract_id' => $model->id]);
        $model->refreshBalance();
        $invs = $model->cominvoices;
        $links = [];
        if (!empty($invs)) {
          foreach ($invs as $key => $value) {
            $links[] = [
              'text' => 'Перейти к Ком. Инвойс №'.$value->number->number,
              'link' => Url::to(['org_diller/o-d-com-invoice/update', 'id' => $value->id])
            ];
          }
        }
        $links[] = [
          'text' => 'Создать новый Ком. Инвойс',
          'link' => Url::to(['org_diller/o-d-com-invoice/create', 'contract_id' => $model->id])
        ];
        $this->setResponse("Контракт подтвержден", 312, true, null, $links);
      } else {
        $this->setResponse("Этот контракт раньше подтвержден", 310, true);
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }

  public function actionToWord($id) {
    $model = $this->findModel($id);
    ExportODContract::toWord($model);
    exit;
  }
}
