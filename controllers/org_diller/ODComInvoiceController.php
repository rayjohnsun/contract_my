<?php
namespace app\controllers\org_diller;

use Yii;
use app\components\Podtverjdeno;
use app\components\Zapolnen;
use app\components\text\ODTxt;
use app\export\cominvoices\ExportODCominvoice;
use app\models\ContractNumbers;
use app\models\companies\Consignees;
use app\models\org_diller\ODComInvoice;
use app\models\org_diller\ODContract;
use app\models\org_diller\ODContractProd;
use app\models\org_diller\ODPackList;
use app\models\org_diller\search\ODComInvoiceSearch;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class ODComInvoiceController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules([
      [
        'allow' => true,
        'actions' => ['index', 'create', 'update', 'delete', 'to-word', 'edit', 'get-products', 'add-products'],
        'roles' => ['organization', 'diller'],
      ],
    ]);
  }

  protected function findModel($id) {
    return parent::fModel(ODComInvoice::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "ComInvoice";
    $searchModel = new ODComInvoiceSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $contracts = $contracts2 = [];
    $cts = ODContract::find()->joinWith(['number' => function (ActiveQuery $query){
      return $query->andWhere(['IS NOT', ContractNumbers::tableName().'.id', NULL]);
    }])->where(['zapolnen' => Zapolnen::YES])->all();
    if (!empty($cts)) {
      foreach ($cts as $key => $value) {
        $contracts[$value->id] = 'Contract: ' . @$value->number->number . ' (' . $value->total_nprice . ')';
        if ($value->com_inv_prod_qty > 0) {
          $contracts2[$value->id] = 'Contract: ' . @$value->number->number . ' (' . $value->total_nprice . ')';
        }
      }
    }
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'contracts' => $contracts,
      'contracts2' => $contracts2
    ]);
  }

  public function actionCreate($contract_id=0) {
    $contract = ODContract::findOne($contract_id);
    $organization = $contract->organization;
    $diller = $contract->diller;
    if (!empty($contract) && !empty($organization) && !empty($diller)) {
      if ($contract->zapolnen == Zapolnen::YES) {
        if ($contract->com_inv_prod_qty > 0) {
          $model = new ODComInvoice();
          $model->scenario = ODComInvoice::SC_CREATE;
          $model->contract_id = $contract->id;
          $model->organization_id = $contract->organization_id;
          $model->diller_id = $contract->diller_id;
          $model->consignee_id = 0;
          $model->cons_text_en = '';
          $model->cons_text_ru = '';
          $model->ndate = date("Y-m-d");
          $model->org_text_en = ODTxt::invOrgEn([
            '{{company}}' => $organization->title_en, '{{address}}' => $organization->adres_en,
            '{{phone}}' => $organization->phone, '{{email}}' => $organization->email
          ]);
          $model->org_text_ru = ODTxt::invOrgRu([
            '{{company}}' => $organization->title_ru, '{{address}}' => $organization->adres_ru,
            '{{phone}}' => $organization->phone, '{{email}}' => $organization->email
          ]);
          $model->dil_text_en = ODTxt::invDilEn([
            '{{company}}' => $diller->title_en, '{{address}}' => $diller->adres_en,
            '{{phone}}' => $diller->phone, '{{email}}' => $diller->email
          ]);
          $model->dil_text_ru = ODTxt::invDilRu([
            '{{company}}' => $diller->title_ru, '{{address}}' => $diller->adres_ru,
            '{{phone}}' => $diller->phone, '{{email}}' => $diller->email
          ]);
          $c_num = @$contract->number->number;
          $c_date= date("d.m.Y", strtotime($contract->ndate));
          $model->notes = ODTxt::comInvNotes([
            '{{C_NUM}}' => $c_num, 
            '{{C_DATE}}' => $c_date, 
            '{{company_en}}' => $organization->title_en, 
            '{{company_ru}}' => $organization->title_ru, 
            '{{region_en}}' => $contract->region_en, 
            '{{region_ru}}' => $contract->region_ru
          ]);
          $model->director_org_en = $contract->director_org_en;
          $model->director_org_ru = $contract->director_org_ru;
          $model->currency = $contract->currency;
          $model->applyDefaults();
          if($model->save()){
            return $this->redirect(['update', 'id' => $model->id]);
          } else {
            Yii::$app->session->setFlash('error', 'Can not create Contract, please try again');
            return $this->redirect(['index']);
          }
        } else {
          Yii::$app->session->setFlash('error', 'Нет доступное количество продуктов у контракта №'.@$contract->number->number);
          return $this->redirect(['index']);
        }
      } else {
        Yii::$app->session->setFlash('error', 'Контракт не подтвержден');
        return $this->redirect(['index']);
      }
    } else {
      Yii::$app->session->setFlash('error', 'Please Select Contract, Consignee');
      return $this->redirect(['index']);
    }
  }

  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $contract = $model->contract;
    if ((float)$model->receive_sum == 0) {
      $model->receive_sum = '';
    }
    return $this->render('edit_com_invoice', [
      'model' => $model, 
      'contract_num' => @$contract->number->number.' ('.$contract->new_ndate.')'
    ]);
  }

  public function actionEdit($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $contract = $model->contract;
      if ($contract->zapolnen == Zapolnen::YES && (float)$contract->total_lprice == 0) {
        if ($model->load(Yii::$app->request->post())) {
          $products = Yii::$app->request->post('Products');
          if (!empty($products)) {
            if ($model->save()) {
              $model->saveProducts();
              $model->sendUpdatedMessage();
              $this->loadResponse($model);
              $this->setResponse("ComInvoice Suuccessfully saved", null, true, [
                'receive_date' => $model->receive_date
              ]);
            } else {
              $this->setResponse($model->allErrors, 123);
            }
          } else {
            $this->setResponse("Не выбран продукт", 122);
          }
        } else {
          $this->setResponse("Can not get Post data", 121);
        }
      } else {
        $this->setResponse("Сначало подтвердите контракт!", 124, null, null, [
          'text' => 'Перейти к контракт #'.@$contract->number->number, 
          'link' => Url::to(['org_diller/o-d-contract/update', 'id' => $contract->id])
        ]);
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionGetProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $products = $model->products;
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          // $od_con_prod = $value->oDContractProduct;
          $p_data[] = [
            'p_id' => $value->od_contract_product_id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->quantity
          ];
        }
      }
      $this->setResponse("Success", 301, true, $p_data);
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionAddProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $self_prods = ArrayHelper::map($model->products, 'od_contract_product_id', 'od_contract_product_id');
      $products = ODContractProd::find()->where(['not in', 'id', $self_prods])->andWhere(['contract_id' => $model->contract_id])->andWhere(['>', 'qty_od_com_invoice', 0])->all();
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $p_data[] = [
            'p_id' => $value->id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->qty_od_com_invoice
          ];
        }
        $this->setResponse("Suuccessfully", 302, true, $p_data);
      } else {
        $this->setResponse("Продуктов нет для добавлении");
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionDelete($id) {
    $model = $this->findModel($id);
    $contract = $model->contract;
    $model->delete();
    $contract->refreshBalance();
    return $this->redirect(['index']);
  }

  public function actionToWord($id) {
    $model = $this->findModel($id);
    ExportODCominvoice::toWord($model);
    exit;
  }
}
