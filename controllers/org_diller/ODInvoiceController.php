<?php
namespace app\controllers\org_diller;

use Yii;
use app\components\POInvoice;
use app\components\Podtverjdeno;
use app\components\Zapolnen;
use app\components\text\ODTxt;
use app\export\invoices\ExportODInvoice;
use app\models\ContractNumbers;
use app\models\companies\Consignees;
use app\models\org_diller\ODContract;
use app\models\org_diller\ODContractProd;
use app\models\org_diller\ODInvoice;
use app\models\org_diller\ODPackList;
use app\models\org_diller\search\ODInvoiceSearch;
use app\models\org_diller\search\ODPOInvoiceSearch;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class ODInvoiceController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules([
      [
        'allow' => true,
        'actions' => ['index', 'po', 'create', 'update', 'delete', 'to-word', 'edit', 'get-products', 'add-products'],
        'roles' => ['organization', 'diller'],
      ],
    ]);
  }

  protected function findModel($id) {
    return parent::fModel(ODInvoice::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Invoice";
    $searchModel = new ODInvoiceSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $contracts = $contracts2 = [];
    $cts = ODContract::find()->joinWith(['number' => function (ActiveQuery $query){
      return $query->andWhere(['IS NOT', ContractNumbers::tableName().'.id', NULL]);
    }])->where(['zapolnen' => Zapolnen::YES])->all();
    if (!empty($cts)) {
      foreach ($cts as $key => $value) {
        $contracts[$value->id] = 'Contract: ' . @$value->number->number . ' (' . $value->total_nprice . ')';
        if ($value->inv_prod_qty > 0) {
          $contracts2[$value->id] = 'Contract: ' . @$value->number->number . ' (' . $value->total_nprice . ')';
        }
      }
    }
    $consignees = Consignees::nList('id', 'title_ru');
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'contracts' => $contracts,
      'contracts2' => $contracts2,
      'consignees' => $consignees,
      'po' => POInvoice::NO
    ]);
  }

  public function actionPo() {
    $this->clas = false;
    $this->title = "Purchase Order";
    $searchModel = new ODPOInvoiceSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $contracts = $contracts2 = [];
    $cts = ODContract::find()->joinWith(['number' => function (ActiveQuery $query){
      return $query->andWhere(['IS NOT', ContractNumbers::tableName().'.id', NULL]);
    }])->where(['zapolnen' => Zapolnen::YES])->all();
    if (!empty($cts)) {
      foreach ($cts as $key => $value) {
        $contracts[$value->id] = 'Contract: ' . @$value->number->number . ' (' . $value->total_nprice . ')';
        if ($value->inv_prod_qty > 0) {
          $contracts2[$value->id] = 'Contract: ' . @$value->number->number . ' (' . $value->total_nprice . ')';
        }
      }
    }
    $consignees = Consignees::nList('id', 'title_ru');
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'contracts' => $contracts,
      'contracts2' => $contracts2,
      'consignees' => $consignees,
      'po' => POInvoice::YES
    ]);
  }

  public function actionCreate($contract_id=0, $consignee_id=0, $po=0) {
    $contract = ODContract::findOne($contract_id);
    $organization = $contract->organization;
    $diller = $contract->diller;
    $consignee = Consignees::findOne($consignee_id);
    if (!$consignee) {
      $consignee = Consignees::find()->orderBy(['id' => SORT_DESC])->one();
    }
    if (!empty($contract) && !empty($consignee) && !empty($organization) && !empty($diller)) {
      if ($contract->zapolnen == Zapolnen::YES) {
        if ($contract->inv_prod_qty > 0) {
          $model = new ODInvoice();
          $model->scenario = ODInvoice::SC_CREATE;
          $model->contract_id = $contract->id;
          $model->organization_id = $contract->organization_id;
          $model->diller_id = $contract->diller_id;
          $model->consignee_id = $consignee->id;
          $model->ndate = date("Y-m-d");
          $model->po = $po == POInvoice::YES ? POInvoice::YES : POInvoice::NO;
          $model->org_text_en = ODTxt::invOrgEn([
            '{{company}}' => $organization->title_en, '{{address}}' => $organization->adres_en,
            '{{phone}}' => $organization->phone, '{{email}}' => $organization->email
          ]);
          $model->org_text_ru = ODTxt::invOrgRu([
            '{{company}}' => $organization->title_ru, '{{address}}' => $organization->adres_ru,
            '{{phone}}' => $organization->phone, '{{email}}' => $organization->email
          ]);
          $model->dil_text_en = ODTxt::invDilEn([
            '{{company}}' => $diller->title_en, '{{address}}' => $diller->adres_en,
            '{{phone}}' => $diller->phone, '{{email}}' => $diller->email
          ]);
          $model->dil_text_ru = ODTxt::invDilRu([
            '{{company}}' => $diller->title_ru, '{{address}}' => $diller->adres_ru,
            '{{phone}}' => $diller->phone, '{{email}}' => $diller->email
          ]);
          $model->cons_text_en = ODTxt::invConsEn([
            '{{company}}' => $consignee->title_en, '{{address}}' => $consignee->adres_en,
            '{{phone}}' => $consignee->phone, '{{email}}' => $consignee->email
          ]);
          $model->cons_text_ru = ODTxt::invConsRu([
            '{{company}}' => $consignee->title_ru, '{{address}}' => $consignee->adres_ru,
            '{{phone}}' => $consignee->phone, '{{email}}' => $consignee->email
          ]);
          $c_num = $model->po==POInvoice::YES?'&nbsp;':@$contract->number->number;
          $c_date= date("d.m.Y", strtotime($contract->ndate));
          $model->notes = ODTxt::invNotes([
            '{{C_NUM}}' => $c_num, 
            '{{C_DATE}}' => $c_date, 
            '{{region_en}}' => $contract->region_en, 
            '{{region_ru}}' => $contract->region_ru
          ]);
          $model->contract_no = $model->po==POInvoice::YES?'Заказ по почте':'';
          $model->director_org_en = $contract->director_org_en;
          $model->director_org_ru = $contract->director_org_ru;
          $model->director_dil_en = $contract->director_dil_en;
          $model->director_dil_ru = $contract->director_dil_ru;
          $model->currency = $contract->currency;
          $model->applyDefaults();
          if($model->save()){
            return $this->redirect(['update', 'id' => $model->id]);
          } else {
            Yii::$app->session->setFlash('error', 'Can not create Contract, please try again');
            return $this->redirect(['index']);
          }
        } else {
          Yii::$app->session->setFlash('error', 'Нет доступное количество продуктов у контракта №'.@$contract->number->number);
          return $this->redirect(['index']);
        }
      } else {
        Yii::$app->session->setFlash('error', 'Контракт не подтвержден');
        return $this->redirect(['index']);
      }
    } else {
      Yii::$app->session->setFlash('error', 'Please Select Contract, Consignee');
      return $this->redirect(['index']);
    }
  }

  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $contract = $model->contract;
    $consignees = Consignees::find()->all();
    if ((float)$model->receive_sum == 0) {
      $model->receive_sum = '';
    }
    return $this->render('edit_invoice', [
      'model' => $model, 
      'contract_num' => @$contract->number->number.' ('.$contract->new_ndate.')', 
      'consignees' => $consignees
    ]);
  }

  public function actionEdit($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $contract = $model->contract;
      if ($contract->zapolnen == Zapolnen::YES && (float)$contract->total_lprice == 0) {
        if ($model->load(Yii::$app->request->post())) {
          $products = Yii::$app->request->post('Products');
          if (!empty($products)) {
            if ($model->save()) {
              $model->saveProducts();
              $model->sendUpdatedMessage();
              $this->loadResponse($model);
              $packList = ODPackList::find()->where(['invoice_id' => $model->id, 'contract_id' => $contract->id])->all();
              $cpp = null;
              if (!empty($packList)) {
                $cpp = $packList[0];
              }
              $this->setResponse("Invoice Suuccessfully saved", null, true, [
                'receive_date' => $model->receive_date
              ], [
                'text' => ($cpp) ? 'Перейти к Упаковочный лист' : 'Создать Упаковочный лист',
                'link' => ($cpp) ? Url::to(['org_diller/o-d-pack-list/update', 'id' => $cpp->id]) : Url::to(['org_diller/o-d-pack-list/create', 'invoice_id' => $model->id])
              ]);
            } else {
              $this->setResponse($model->allErrors, 123);
            }
          } else {
            $this->setResponse("Не выбран продукт", 122);
          }
        } else {
          $this->setResponse("Can not get Post data", 121);
        }
      } else {
        $this->setResponse("Сначало подтвердите контракт!", 124, null, null, [
          'text' => 'Перейти к контракт #'.@$contract->number->number, 
          'link' => Url::to(['org_diller/o-d-contract/update', 'id' => $contract->id])
        ]);
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionGetProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $products = $model->products;
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          // $od_con_prod = $value->oDContractProduct;
          $p_data[] = [
            'p_id' => $value->od_contract_product_id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->quantity
          ];
        }
      }
      $this->setResponse("Success", 301, true, $p_data);
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionAddProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $self_prods = ArrayHelper::map($model->products, 'od_contract_product_id', 'od_contract_product_id');
      $products = ODContractProd::find()->where(['not in', 'id', $self_prods])->andWhere(['contract_id' => $model->contract_id])->andWhere(['>', 'qty_od_invoice', 0])->all();
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $p_data[] = [
            'p_id' => $value->id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->qty_od_invoice
          ];
        }
        $this->setResponse("Suuccessfully", 302, true, $p_data);
      } else {
        $this->setResponse("Продуктов нет для добавлении");
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionDelete($id) {
    $model = $this->findModel($id);
    $red = $model->po==POInvoice::YES?['po']:['index'];
    $contract = $model->contract;
    $model->delete();
    $contract->refreshBalance();
    return $this->redirect($red);
  }

  public function actionToWord($id) {
    $model = $this->findModel($id);
    ExportODInvoice::toWord($model);
    exit;
  }
}
