<?php
namespace app\controllers\org_diller;

use Yii;
use app\components\Status;
use app\models\org_diller\ODParagraph;
use app\models\org_diller\search\ODParagraphSearch;

class ODParagraphController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('admin');
  }

  protected function findModel($id) {
    return parent::fModel(ODParagraph::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Paragraphs (Organization & Diller)";
    $searchModel = new ODParagraphSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider
    ]);
  }

  public function actionCreate() {
    $this->setBred('Paragraphs (Organization & Diller)', 'index');
    $this->title = 'create';
    $model = new ODParagraph();
    $model->status = Status::ACTIVE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create');
  }

  public function actionUpdate($id) {
    $this->setBred('Paragraphs (Organization & Diller)', 'index');
    $model = $this->findModel($id);
    $this->title = 'Update: ' . $model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update');
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
