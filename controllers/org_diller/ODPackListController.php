<?php
namespace app\controllers\org_diller;

use Yii;
use app\components\POInvoice;
use app\components\Zapolnen;
use app\components\text\ODTxt;
use app\export\packlists\ExportODPackList;
use app\models\ContractNumbers;
use app\models\org_diller\ODContract;
use app\models\org_diller\ODInvoice;
use app\models\org_diller\ODInvoiceProd;
use app\models\org_diller\ODPackList;
use app\models\org_diller\search\ODPackListSearch;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class ODPackListController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules([
      [
        'allow' => true,
        'actions' => ['index', 'create', 'update', 'delete', 'to-word', 'edit', 'get-products', 'add-products'],
        'roles' => ['organization', 'diller'],
      ],
    ]);
  }

  protected function findModel($id) {
    return parent::fModel(ODPackList::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Invoice";
    $searchModel = new ODPackListSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $contracts = $invoices = $invoices2 = [];
    $cts = ODContract::find()->joinWith(['number' => function (ActiveQuery $query){
      return $query->andWhere(['IS NOT', ContractNumbers::tableName().'.id', NULL]);
    }])->where(['zapolnen' => Zapolnen::YES])->all();
    $contracts = ArrayHelper::map($cts, 'id', function ($m) {
      return 'Contract: ' . @$m->number->number . ' (' . $m->total_nprice . ')';
    });

    $invs = ODInvoice::find()->joinWith(['number', 'packinglists'])->orderBy(['number_id' => SORT_DESC])->all();
    if (!empty($invs)) {
      foreach ($invs as $key => $value) {
        $txt = $value->po==POInvoice::YES?'Purchase Order: ':'Invoice: ';
        $invoices[$value->id] = $txt . @$value->number->number . ' (' . $value->nprice . ')';
        if (empty($value->packinglists)) {
          $invoices2[$value->id] = $txt . @$value->number->number . ' (' . $value->nprice . ')';
        }
      }
    }

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'contracts' => $contracts,
      'invoices' => $invoices,
      'invoices2' => $invoices2
    ]);
  }

  public function actionCreate($invoice_id = 0) {
    if ($model = ODPackList::find()->where(['invoice_id' => $invoice_id])->one()) {
      return $this->redirect(['update', 'id' => $model->id]);
    } else {
      $invoice = ODInvoice::findOne($invoice_id);
      $contract = @$invoice->contract;
      if ($invoice && $contract) {
        if ($contract->zapolnen == Zapolnen::YES && (float)$contract->total_lprice == 0) {
          $model = new ODPackList();
          $model->number_id = $invoice->number_id;
          $model->ndate = date("Y-m-d");
          $model->invoice_id = $invoice->id;
          $model->contract_id = $contract->id;
          $model->organization_id = $invoice->organization_id;
          $model->diller_id = $invoice->diller_id;
          $model->consignee_id = $invoice->consignee_id;
          $c_num = @$contract->number->number;
          $c_date= date("d.m.Y", strtotime($contract->ndate));
          $model->notes = ODTxt::packNotes(['{{C_NUM}}' => $c_num, '{{C_DATE}}' => $c_date]);
          $model->director_org_en = $contract->director_org_en;
          $model->director_org_ru = $contract->director_org_ru;
          $model->applyDefaults();
          if($model->save()){
            return $this->redirect(['update', 'id' => $model->id]);
          } else {
            Yii::$app->session->setFlash('error', 'Can not create Packing List, please try again');
            return $this->redirect(['index']);
          }
        } else {
          Yii::$app->session->setFlash('error', 'Контракт не подтвержден');
          return $this->redirect(['index']);
        }
      } else {
        Yii::$app->session->setFlash('error', 'Please Select Invoice');
        return $this->redirect(['index']);
      }      
    }
  }

  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $contract = $model->contract;
    $podv_class = (float)$contract->total_lprice == 0 ? 'green' : 'red';
    return $this->render('edit_packlist', [
      'model' => $model, 
      'contract_num' => '<strong class="ui '.$podv_class.'">'.@$contract->number->number.'</strong> ('.$contract->new_ndate.')'
    ]);
  }

  public function actionEdit($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $invoice = $model->invoice;
      if ($invoice) {
        $contract = $invoice->contract;
        if ($contract) {
          if ($contract->zapolnen == Zapolnen::YES && (float)$contract->total_lprice == 0) {
            $model->scenario = ODPackList::SC_UPDATE;
            if ($model->load(Yii::$app->request->post())) {
              if ($model->save()) {
                $model->saveProducts();
                $model->sendUpdatedMessage();
                $this->loadResponse($model);
                $this->setResponse("Packing List saved");
              } else {
                $this->setResponse($model->allErrors, 123);
              }
            } else {
              $this->setResponse("Can not get Post data", 121);
            }
          } else {
            $this->setResponse("Сначало подтвердите контракт!", 124, null, null, [
              'text' => 'Перейти к контракт #'.@$contract->number->number, 
              'link' => Url::to(['org_diller/o-d-contract/update', 'id' => $contract->id])
            ]);
          }
        } else {
          $this->setResponse("Contract Not Found", 126);
        }
      } else {
        $this->setResponse("Invoice Not Found", 125);
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionGetProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $products = $model->products;
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $p_data[] = [
            'p_id' => $value->od_invoice_product_id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => $value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->quantity,
            'p_net_w' => $value->net_weight,
            'p_gross_w' => $value->gross_weight,
            'p_container' => $value->container,
          ];
        }
      }
      $this->setResponse("Success", 301, true, $p_data);
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionAddProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      // $self_prods = ArrayHelper::map($model->products, 'od_invoice_product_id', 'od_invoice_product_id');
      $products = ODInvoiceProd::find()->andWhere(['invoice_id' => $model->invoice_id])->all(); 
      //->where(['not in', 'id', $self_prods])
      //->andWhere(['>', 'qty_od_invoice', 0])
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $p_data[] = [
            'p_id' => $value->id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->quantity
          ];
        }
        $this->setResponse("Suuccessfully", 302, true, $p_data);
      } else {
        $this->setResponse("Продуктов нет для добавлении");
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }

  public function actionToWord($id) {
    $model = $this->findModel($id);
    ExportODPackList::toWord($model);
    exit;
  }
}
