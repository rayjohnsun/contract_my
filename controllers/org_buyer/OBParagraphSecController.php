<?php
namespace app\controllers\org_buyer;

use Yii;
use app\components\Status;
use app\models\org_buyer\OBParagraph;
use app\models\org_buyer\OBParagraphSec;
use app\models\org_buyer\search\OBParagraphSecSearch;

class OBParagraphSecController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('admin');
  }

  protected function findModel($id) {
    return parent::fModel(OBParagraphSec::findOne($id));
  }

  public function getParagraphs() {
    return OBParagraph::nList('id', 'title_ru', Status::ACTIVE, ['norder' => SORT_ASC]);
  }

  public function actionIndex() {
    $this->clas = false;
    $this->setBred('Paragraphs (Organization & Buyer)', '/org_buyer/o-b-paragraph');
    $this->title = "Paragraph Sections";
    $searchModel = new OBParagraphSecSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'paragraphs' => $this->paragraphs
    ]);
  }

  public function actionCreate() {
    $this->setBred('Paragraphs (Organization & Buyer)', '/org_buyer/o-b-paragraph');
    $this->setBred('Sections', 'index');
    $this->title = 'create';
    $model = new OBParagraphSec();
    $model->status = Status::ACTIVE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create', ['paragraphs' => $this->paragraphs]);
  }

  public function actionUpdate($id) {
    $this->setBred('Paragraphs (Organization & Buyer)', '/org_buyer/o-b-paragraph');
    $this->setBred('Sections', 'index');
    $model = $this->findModel($id);
    $this->title = 'Update: ' . $model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update', ['paragraphs' => $this->paragraphs]);
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
