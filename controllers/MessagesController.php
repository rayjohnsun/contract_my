<?php
namespace app\controllers;

use Yii;
use app\components\Status;
use app\models\Messages;
use app\models\Users;
use app\models\search\MessagesSearch;

class MessagesController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('manageMessages', ['index', 'watched', 'delete']);
  }

  protected function findModel($id) {
    return parent::fModel(Messages::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Messages";
    $searchModel = new MessagesSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $users = Users::nList('id', 'fio');
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'users' => $users
    ]);
  }

  public function actionWatched($id) {
    $model = $this->findModel($id);
    if ($model->status != Status::ACTIVE) {
      $model->status = Status::ACTIVE;
      $model->readed_at = date("Y-m-d H:i:s");
      if($model->save()){
        Yii::$app->session->setFlash('success', 'Успешно выделен как прочитан');
      } else {
        Yii::$app->session->setFlash('error', 'Ощибка! Повторите еще раз');
      }
    } else {
      Yii::$app->session->setFlash('error', 'Сообшения прочитан давно');
    }
    return $this->redirect(['index']);
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    Yii::$app->session->setFlash('success', 'Успешно удален');
    return $this->redirect(['index']);
  }
}
