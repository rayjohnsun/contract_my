<?php

namespace app\controllers\companies;

use Yii;
use app\components\Status;
use app\components\Type;
use app\models\companies\Consignees;
use app\models\companies\search\ConsigneesSearch;
use app\models\Countries;

class ConsigneesController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('admin');
  }

  public function getCountries() {
    return Countries::nList('id', 'title_ru', Status::ACTIVE);
  }

  protected function findModel($id) {
    return parent::fModel(Consignees::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Consignees";
    $searchModel = new ConsigneesSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'countries' => $this->countries
    ]);
  }

  public function actionCreate() {
    $this->setBred('Consignees', 'index');
    $this->title = 'create';
    $model = new Consignees();
    $model->status = Status::ACTIVE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create', ['countries' => $this->countries]);
  }

  public function actionUpdate($id) {
    $this->setBred('Consignees', 'index');
    $model = $this->findModel($id);
    $this->title = 'Update: '.$model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update', ['countries' => $this->countries]);
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
