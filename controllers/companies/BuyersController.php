<?php

namespace app\controllers\companies;

use Yii;
use app\components\Status;
use app\components\Type;
use app\models\companies\Buyers;
use app\models\companies\search\BuyersSearch;
use app\models\Countries;

class BuyersController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('admin');
  }

  public function getCountries() {
    return Countries::nList('id', 'title_ru', Status::ACTIVE);
  }

  protected function findModel($id) {
    return parent::fModel(Buyers::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Buyers";
    $searchModel = new BuyersSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'countries' => $this->countries
    ]);
  }

  public function actionCreate() {
    $this->setBred('Buyers', 'index');
    $this->title = 'create';
    $model = new Buyers();
    $model->status = Status::ACTIVE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create', ['countries' => $this->countries]);
  }

  public function actionUpdate($id) {
    $this->setBred('Buyers', 'index');
    $model = $this->findModel($id);
    $this->title = 'Update: '.$model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update', ['countries' => $this->countries]);
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
