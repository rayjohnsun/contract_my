<?php
namespace app\controllers\companies;

use app\components\Status;
use app\models\companies\Organizations;
use app\models\companies\search\OrganizationsSearch;
use app\models\Countries;
use Yii;

class OrganizationsController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('admin');
  }

  protected function findModel($id) {
    return parent::fModel(Organizations::findOne($id));
  }

  public function getCountries() {
    return Countries::nList('id', 'title_ru', Status::ACTIVE);
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Organizations";
    $searchModel = new OrganizationsSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'countries' => $this->countries
    ]);
  }

  public function actionCreate() {
    $this->setBred('Organizations', 'index');
    $this->title = 'create';
    $model = new Organizations();
    $model->status = Status::ACTIVE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create', ['countries' => $this->countries]);
  }

  public function actionUpdate($id) {
    $this->setBred('Organizations', 'index');
    $model = $this->findModel($id);
    $this->title = 'Update: ' . $model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update', ['countries' => $this->countries]);
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
