<?php
namespace app\controllers\dil_buyer;

use Yii;
use app\components\Currencies;
use app\components\POInvoice;
use app\components\Podtverjdeno;
use app\components\Zapolnen;
use app\components\text\DBTxt;
use app\export\invoices\ExportDBInvoice;
use app\models\Contract2Numbers;
use app\models\InvoiceNumbers;
use app\models\companies\Buyers;
use app\models\companies\Consignees;
use app\models\dil_buyer\DBContract;
use app\models\dil_buyer\DBContractProd;
use app\models\dil_buyer\DBInvoice;
use app\models\dil_buyer\DBPackList;
use app\models\dil_buyer\search\DBInvoiceSearch;
use app\models\org_diller\ODInvoice;
use app\models\org_diller\ODInvoiceProd;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class DBInvoiceController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules([
      [
        'allow' => true,
        'actions' => ['index', 'create', 'update', 'delete', 'to-word', 'edit', 'get-products', 'add-products'],
        'roles' => ['diller'],
      ],
    ]);
  }

  protected function findModel($id) {
    return parent::fModel(DBInvoice::findOne($id));
  }

  public function actionIndex($po_id = 0) {
    $this->clas = false;
    $this->title = "Invoice";
    $searchModel = new DBInvoiceSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $contracts = [0 => 'Пустой'];
    $contracts2 = [];
    $pur_orders = [0 => 'Пустой'];
    $pur_orders2 = [];
    $cts = DBContract::find()->joinWith(['number' => function (ActiveQuery $query){
      return $query->andWhere(['IS NOT', Contract2Numbers::tableName().'.id', NULL]);
    }])->where(['zapolnen' => Zapolnen::YES])->all();
    if (!empty($cts)) {
      foreach ($cts as $key => $value) {
        $contracts[$value->id] = 'Contract: ' . @$value->number->number . ' (' .number_format($value->total_nprice, 2, '.', ' '). ')';
        if ($value->inv_prod_qty > 0) {
          $contracts2[$value->id] = 'Contract: ' . @$value->number->number . ' (' .number_format($value->total_nprice, 2, '.', ' '). ')';
        }
      }
    }

    $pos = ODInvoice::find()->joinWith(['number' => function (ActiveQuery $query){
      return $query->andWhere(['IS NOT', InvoiceNumbers::tableName().'.id', NULL]);
    }])->where(['po' => POInvoice::YES])->all();
    if (!empty($pos)) {
      foreach ($pos as $key2 => $value2) {
        $pur_orders[$value2->id] = 'Purchase Order: ' . @$value2->number->number.' ('.number_format($value2->po_db_inv_nprice, 2, '.', ' ').')';
        if ($value2->po_db_inv_qty > 0) {
          $pur_orders2[$value2->id] = 'Purchase Order: ' . @$value2->number->number.' ('.number_format($value2->po_db_inv_nprice, 2, '.', ' ').')';
          
        }
      }
    }

    $buyers = Buyers::nList('id', 'title_ru');
    $consignees = Consignees::nList('id', 'title_ru');
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'contracts' => $contracts,
      'contracts2' => $contracts2,
      'pur_orders' => $pur_orders,
      'pur_orders2' => $pur_orders2,
      'consignees' => $consignees,
      'buyers' => $buyers,
      'po_id' => (int)$po_id
    ]);
  }

  public function createInvoice($contract_id, $consignee_id) {
    $contract = DBContract::findOne($contract_id);
    $diller = $contract->diller;
    $buyer = $contract->buyer;
    $consignee = Consignees::findOne($consignee_id);
    if (!$consignee) {
      $consignee = Consignees::find()->orderBy(['id' => SORT_DESC])->one();
    }
    if (!empty($contract) && !empty($consignee) && !empty($diller) && !empty($buyer)) {
      if ($contract->zapolnen == Zapolnen::YES) {
        if ($contract->inv_prod_qty > 0) {
          $model = new DBInvoice();
          $model->scenario = DBInvoice::SC_CREATE;
          $model->contract_id = $contract->id;
          $model->po_id = 0;
          $model->diller_id = $contract->diller_id;
          $model->buyer_id = $contract->buyer_id;
          $model->consignee_id = $consignee->id;
          $model->ndate = date("Y-m-d");
          $model->dil_text_en = DBTxt::invDilEn([
            '{{company}}' => $diller->title_en, '{{address}}' => $diller->adres_en,
            '{{phone}}' => $diller->phone, '{{email}}' => $diller->email
          ]);
          $model->dil_text_ru = DBTxt::invDilRu([
            '{{company}}' => $diller->title_ru, '{{address}}' => $diller->adres_ru,
            '{{phone}}' => $diller->phone, '{{email}}' => $diller->email
          ]);
          $model->buy_text_en = DBTxt::invBuyEn([
            '{{company}}' => $buyer->title_en, '{{address}}' => $buyer->adres_en,
            '{{phone}}' => $buyer->phone, '{{email}}' => $buyer->email
          ]);
          $model->buy_text_ru = DBTxt::invBuyRu([
            '{{company}}' => $buyer->title_ru, '{{address}}' => $buyer->adres_ru,
            '{{phone}}' => $buyer->phone, '{{email}}' => $buyer->email
          ]);
          $model->cons_text_en = DBTxt::invConsEn([
            '{{company}}' => $consignee->title_en, '{{address}}' => $consignee->adres_en,
            '{{phone}}' => $consignee->phone, '{{email}}' => $consignee->email
          ]);
          $model->cons_text_ru = DBTxt::invConsRu([
            '{{company}}' => $consignee->title_ru, '{{address}}' => $consignee->adres_ru,
            '{{phone}}' => $consignee->phone, '{{email}}' => $consignee->email
          ]);
          // $c_num = @$contract->number->number;
          // $c_date= date("d.m.Y", strtotime($contract->ndate));
          // $model->notes = DBTxt::invNotes([
          //   '{{C_NUM}}' => $c_num, 
          //   '{{C_DATE}}' => $c_date, 
          //   '{{region_en}}' => $contract->region_en, 
          //   '{{region_ru}}' => $contract->region_ru
          // ]);
          $model->notes = DBTxt::invNotes([
            '{{CURRENCY}}' => Currencies::getShort($contract->currency), 
            '{{REGION_EN}}' => $contract->region_en,
            '{{SELLER}}' => $diller->title_en
          ]);
          $model->director_dil_en = $contract->director_dil_en;
          $model->director_dil_ru = $contract->director_dil_ru;
          $model->currency = $contract->currency;
          $model->applyDefaults();
          if($model->save()){
            return $this->redirect(['update', 'id' => $model->id]);
          } else {
            Yii::$app->session->setFlash('error', 'Can not create Contract, please try again');
            return $this->redirect(['index']);
          }
        } else {
          Yii::$app->session->setFlash('error', 'Нет доступное количество продуктов у контракта №'.@$contract->number->number);
          return $this->redirect(['index']);
        }
      } else {
        Yii::$app->session->setFlash('error', 'Контракт не подтвержден');
        return $this->redirect(['index']);
      }
    } else {
      Yii::$app->session->setFlash('error', 'Please Select Contract, Consignee');
      return $this->redirect(['index']);
    }
  }

  public function createPo($po_id, $buyer_id, $consignee_id) {
    $purchord = ODInvoice::findOne(['id' => $po_id, 'po' => POInvoice::YES]);
    $diller = @$purchord->contract->diller;
    $buyer = Buyers::findOne($buyer_id);
    $consignee = Consignees::findOne($consignee_id);
    if (!$consignee) {
      $consignee = Consignees::find()->orderBy(['id' => SORT_DESC])->one();
    }
    $contract = @$purchord->contract;
    if (!empty($purchord) && !empty($contract) && !empty($buyer) && !empty($consignee) && !empty($diller)) {
      if ($contract->zapolnen == Zapolnen::YES) {
        if ($purchord->po_db_inv_qty > 0) {
          $model = new DBInvoice();
          $model->scenario = DBInvoice::SC_CREATE;
          $model->contract_id = 0;
          $model->po_id = $purchord->id;
          $model->diller_id = $purchord->diller_id;
          $model->buyer_id = $buyer->id;
          $model->consignee_id = $consignee->id;
          $model->ndate = date("Y-m-d");
          $model->dil_text_en = DBTxt::invDilEn([
            '{{company}}' => $diller->title_en, '{{address}}' => $diller->adres_en,
            '{{phone}}' => $diller->phone, '{{email}}' => $diller->email
          ]);
          $model->dil_text_ru = DBTxt::invDilRu([
            '{{company}}' => $diller->title_ru, '{{address}}' => $diller->adres_ru,
            '{{phone}}' => $diller->phone, '{{email}}' => $diller->email
          ]);
          $model->buy_text_en = DBTxt::invBuyEn([
            '{{company}}' => $buyer->title_en, '{{address}}' => $buyer->adres_en,
            '{{phone}}' => $buyer->phone, '{{email}}' => $buyer->email
          ]);
          $model->buy_text_ru = DBTxt::invBuyRu([
            '{{company}}' => $buyer->title_ru, '{{address}}' => $buyer->adres_ru,
            '{{phone}}' => $buyer->phone, '{{email}}' => $buyer->email
          ]);
          $model->cons_text_en = DBTxt::invConsEn([
            '{{company}}' => $consignee->title_en, '{{address}}' => $consignee->adres_en,
            '{{phone}}' => $consignee->phone, '{{email}}' => $consignee->email
          ]);
          $model->cons_text_ru = DBTxt::invConsRu([
            '{{company}}' => $consignee->title_ru, '{{address}}' => $consignee->adres_ru,
            '{{phone}}' => $consignee->phone, '{{email}}' => $consignee->email
          ]);
          $c_num = @$contract->number->number;
          $c_date= date("d.m.Y", strtotime($contract->ndate));
          // $model->notes = DBTxt::invNotes([
          //   '{{C_NUM}}' => $c_num, 
          //   '{{C_DATE}}' => $c_date, 
          //   '{{region_en}}' => $contract->region_en, 
          //   '{{region_ru}}' => $contract->region_ru
          // ]);
          $model->notes = DBTxt::invNotes([
            '{{CURRENCY}}' => Currencies::getShort($contract->currency), 
            '{{REGION_EN}}' => $contract->region_en,
            '{{SELLER}}' => $diller->title_en
          ]);
          $model->director_dil_en = $contract->director_dil_en;
          $model->director_dil_ru = $contract->director_dil_ru;
          $model->currency = $contract->currency;
          $model->applyDefaults();
          if($model->save()){
            return $this->redirect(['update', 'id' => $model->id]);
          } else {
            Yii::$app->session->setFlash('error', 'Can not create Contract, please try again');
            return $this->redirect(['index']);
          }
        } else {
          Yii::$app->session->setFlash('error', 'Нет доступное количество продуктов у PO №'.@$purchord->number->number);
          return $this->redirect(['index']);
        }
      } else {
        Yii::$app->session->setFlash('error', 'Контракт VT&D не подтвержден');
        return $this->redirect(['index']);
      }
    } else {
      Yii::$app->session->setFlash('error', 'Please Select Purchase Order, Consignee');
      return $this->redirect(['index']);
    }
  }

  public function actionCreate($contract_id=0, $po_id=0, $consignee_id=0, $po=0, $buyer_id=0) {
    if ($po == POInvoice::YES) {
      $this->createPo($po_id, $buyer_id, $consignee_id);
    } else {
      $this->createInvoice($contract_id, $consignee_id);
    }
  }

  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $parent = $model->po_id > 0 ? $model->poInvoice : $model->contract;
    $consignees = Consignees::find()->all();
    if ((float)$model->receive_sum == 0) {
      $model->receive_sum = '';
    }
    return $this->render('edit_invoice', [
      'model' => $model, 
      'contract_num' => @$parent->number->number.' ('.$parent->new_ndate.')', 
      'consignees' => $consignees
    ]);
  }

  public function actionEdit($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      if ($model->po_id > 0) {
        $poInvoice = $model->poInvoice;
        if ($poInvoice) {
          if ($model->load(Yii::$app->request->post())) {
            $products = Yii::$app->request->post('Products');
            if (!empty($products)) {
              if ($model->save()) {
                $model->saveProducts();
                $model->sendUpdatedMessage();
                $this->loadResponse($model);
                $packList = DBPackList::find()->where(['invoice_id' => $model->id, 'po_id' => $poInvoice->id])->all();
                $cpp = null;
                if (!empty($packList)) {
                  $cpp = $packList[0];
                }
                $this->setResponse("Invoice Suuccessfully saved", null, true, [
                  'receive_date' => $model->receive_date
                ], [
                  'text' => ($cpp) ? 'Перейти к Упаковочный лист' : 'Создать Упаковочный лист',
                  'link' => ($cpp) ? Url::to(['dil_buyer/d-b-pack-list/update', 'id' => $cpp->id]) : Url::to(['dil_buyer/d-b-pack-list/create', 'invoice_id' => $model->id])
                ]);
              } else {
                $this->setResponse($model->allErrors, 131);
              }
            } else {
              $this->setResponse("Не выбран продукт", 130);
            }
          } else {
            $this->setResponse("Can not get Post data", 129);
          }
        } else {
          $this->setResponse("Purchase Order is not found", 128);
        }
      } else {
        $contract = $model->contract;
        if ($contract) {
          if ($contract->zapolnen == Zapolnen::YES && (float)$contract->total_lprice == 0) {
            if ($model->load(Yii::$app->request->post())) {
              $products = Yii::$app->request->post('Products');
              if (!empty($products)) {
                if ($model->save()) {
                  $model->saveProducts();
                  $model->sendUpdatedMessage();
                  $this->loadResponse($model);
                  $packList = DBPackList::find()->where(['invoice_id' => $model->id, 'contract_id' => $contract->id])->all();
                  $cpp = null;
                  if (!empty($packList)) {
                    $cpp = $packList[0];
                  }
                  $this->setResponse("Invoice Suuccessfully saved", null, true, [
                    'receive_date' => $model->receive_date
                  ], [
                    'text' => ($cpp) ? 'Перейти к Упаковочный лист' : 'Создать Упаковочный лист',
                    'link' => ($cpp) ? Url::to(['dil_buyer/d-b-pack-list/update', 'id' => $cpp->id]) : Url::to(['dil_buyer/d-b-pack-list/create', 'invoice_id' => $model->id])
                  ]);
                } else {
                  $this->setResponse($model->allErrors, 123);
                }
              } else {
                $this->setResponse("Не выбран продукт", 122);
              }
            } else {
              $this->setResponse("Can not get Post data", 121);
            }
          } else {
            $this->setResponse("Сначало подтвердите контракт!", 124, null, null, [
              'text' => 'Перейти к контракт #'.@$contract->number->number, 
              'link' => Url::to(['dil_buyer/d-b-contract/update', 'id' => $contract->id])
            ]);
          }
        } else {
          $this->setResponse("Contract is not found", 125);
        }
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionGetProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $products = $model->products;
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $tit = $model->po_id>0?$value->titleRuEn:$value->titleRuEn.' <small>('.@$value->oDContractProduct->contract->number->number.')</small>';
          $p_data[] = [
            'p_id' => $model->po_id > 0 ? $value->od_po_invoice_product_id : $value->db_contract_product_id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $tit,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->quantity
          ];
        }
      }
      $this->setResponse("Success", 301, true, $p_data);
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionAddProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      if ($model->po_id > 0) {
        $self_prods = ArrayHelper::map($model->products, 'od_po_invoice_product_id', 'od_po_invoice_product_id');
        $products = ODInvoiceProd::find()->where(['not in', 'id', $self_prods])->andWhere(['invoice_id' => $model->po_id])->andWhere(['>', 'qty_po_db_invoice', 0])->all();
      } else {
        $self_prods = ArrayHelper::map($model->products, 'db_contract_product_id', 'db_contract_product_id');
        $products = DBContractProd::find()->where(['not in', 'id', $self_prods])->andWhere(['contract_id' => $model->contract_id])->andWhere(['>', 'qty_db_invoice', 0])->all();
      }
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $tit = $model->po_id>0?$value->titleRuEn:$value->titleRuEn.' <small>('.@$value->oDContractProduct->contract->number->number.')</small>';
          $p_data[] = [
            'p_id' => $value->id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $tit,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $model->po_id > 0 ? $value->qty_po_db_invoice : $value->qty_db_invoice
          ];
        }
        $this->setResponse("Suuccessfully", 302, true, $p_data);
      } else {
        $this->setResponse("Продуктов нет для добавлении");
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionDelete($id) {
    $model = $this->findModel($id);
    if ($model->po_id > 0) {
      $invoice = $model->poInvoice;
      $model->delete();
      if ($invoice) {
        $invoice->refreshBalance();
      }
    } else {
      $contract = $model->contract;
      $model->delete();
      if ($contract) {
        $contract->refreshBalance();
      }
    }
    return $this->redirect(['index']);
  }

  public function actionToWord($id) {
    $model = $this->findModel($id);
    ExportDBInvoice::toWord($model);
    exit;
  }
}
