<?php
namespace app\controllers\dil_buyer;

use Yii;
use app\components\POInvoice;
use app\components\Zapolnen;
use app\components\text\DBTxt;
use app\export\packlists\ExportDBPackList;
use app\models\Contract2Numbers;
use app\models\Invoice2Numbers;
use app\models\InvoiceNumbers;
use app\models\dil_buyer\DBContract;
use app\models\dil_buyer\DBInvoice;
use app\models\dil_buyer\DBInvoiceProd;
use app\models\dil_buyer\DBPackList;
use app\models\dil_buyer\search\DBPackListSearch;
use app\models\org_diller\ODInvoice;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class DBPackListController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules([
      [
        'allow' => true,
        'actions' => ['index', 'create', 'update', 'delete', 'to-word', 'edit', 'get-products', 'add-products'],
        'roles' => ['diller'],
      ],
    ]);
  }

  protected function findModel($id) {
    return parent::fModel(DBPackList::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Invoice";
    $searchModel = new DBPackListSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $contracts = $invoices = $invoices2 = $purchorders = [];

    $cts = DBContract::find()->joinWith(['number' => function (ActiveQuery $query){
      return $query->andWhere(['IS NOT', Contract2Numbers::tableName().'.id', NULL]);
    }])->where(['zapolnen' => Zapolnen::YES])->all();
    $contracts = ArrayHelper::map($cts, 'id', function ($m) {
      return 'Contract: ' . @$m->number->number . ' (' . $m->total_nprice . ')';
    });
    $contracts[0] = 'Empty';
    ksort($contracts);

    $pchs = ODInvoice::find()->joinWith(['number' => function (ActiveQuery $query){
      return $query->andWhere(['IS NOT', InvoiceNumbers::tableName().'.id', NULL]);
    }])->where([ODInvoice::tableName().'.po' => POInvoice::YES])->all();
    $purchorders = ArrayHelper::map($pchs, 'id', function ($m) {
      return 'Purchase Order: ' . @$m->number->number;
    });
    $purchorders[0] = 'Empty';
    ksort($purchorders);

    $invs = DBInvoice::find()->joinWith('number')->orderBy(['number_id' => SORT_DESC])->all();
    if (!empty($invs)) {
      foreach ($invs as $key => $value) {
        $invoices[$value->id] = 'Invoice: ' . @$value->number->number . ' (' . $value->nprice . ')';
        if (empty($value->packinglists)) {
          $invoices2[$value->id] = 'Invoice: ' . @$value->number->number . ' (' . $value->nprice . ')';
        }
      }
    }

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'contracts' => $contracts,
      'purchorders' => $purchorders,
      'invoices' => $invoices,
      'invoices2' => $invoices2
    ]);
  }

  public function actionCreate($invoice_id = 0) {
    if ($model = DBPackList::find()->where(['invoice_id' => $invoice_id])->one()) {
      return $this->redirect(['update', 'id' => $model->id]);
    } else {
      $invoice = DBInvoice::findOne($invoice_id);
      $parent = $invoice->po_id > 0 ? @$invoice->poInvoice : @$invoice->contract;
      if ($invoice && $parent) {
        if ($invoice->po_id > 0 || ($parent->zapolnen == Zapolnen::YES && (float)$parent->total_lprice == 0)) {
          $model = new DBPackList();
          $model->number_id = $invoice->number_id;
          $model->ndate = date("Y-m-d");
          $model->invoice_id = $invoice->id;
          if ($invoice->po_id > 0) {
            $model->contract_id = 0;
            $model->po_id = $parent->id;
          } else {
            $model->contract_id = $parent->id;
            $model->po_id = 0;
          }
          $model->diller_id = $invoice->diller_id;
          $model->buyer_id = $invoice->buyer_id;
          $model->consignee_id = $invoice->consignee_id;
          $c_num = @$parent->number->number;
          $c_date= date("d.m.Y", strtotime($parent->ndate));
          $model->notes = DBTxt::packNotes(['{{C_NUM}}' => $c_num, '{{C_DATE}}' => $c_date]);
          $model->director_dil_en = $parent->director_dil_en;
          $model->director_dil_ru = $parent->director_dil_ru;
          $model->applyDefaults();
          if($model->save()){
            return $this->redirect(['update', 'id' => $model->id]);
          } else {
            Yii::$app->session->setFlash('error', 'Can not create Packing List, please try again');
            return $this->redirect(['index']);
          }
        } else {
          Yii::$app->session->setFlash('error', 'Контракт не подтвержден');
          return $this->redirect(['index']);
        }
      } else {
        Yii::$app->session->setFlash('error', 'Please Select Invoice');
        return $this->redirect(['index']);
      }      
    }
  }

  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $parent = $model->po_id > 0 ? $model->poInvoice : $model->contract;
    $podv_class = $model->po_id > 0 ? '' : (float)$parent->total_lprice == 0 ? 'green' : 'red';
    return $this->render('edit_packlist', [
      'model' => $model, 
      'contract_num' => '<strong class="ui '.$podv_class.'">'.@$parent->number->number.'</strong> ('.$parent->new_ndate.')'
    ]);
  }

  public function actionEdit($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $invoice = $model->invoice;
      if ($invoice) {
        $parent = $model->po_id > 0 ? $invoice->poInvoice : $invoice->contract;
        if ($parent) {
          if ($invoice->po_id > 0 || ($parent->zapolnen == Zapolnen::YES && (float)$parent->total_lprice == 0)) {
            $model->scenario = DBPackList::SC_UPDATE;
            if ($model->load(Yii::$app->request->post())) {
              if ($model->save()) {
                $model->saveProducts();
                $model->sendUpdatedMessage();
                $this->loadResponse($model);
                $this->setResponse("Packing List saved");
              } else {
                $this->setResponse($model->allErrors, 123);
              }
            } else {
              $this->setResponse("Can not get Post data", 121);
            }
          } else {
            $this->setResponse("Сначало подтвердите контракт!", 124, null, null, [
              'text' => 'Перейти к контракт #'.@$parent->number->number, 
              'link' => Url::to(['dil_buyer/o-b-contract/update', 'id' => $parent->id])
            ]);
          }
        } else {
          $this->setResponse("Contract Not Found", 126);
        }
      } else {
        $this->setResponse("Invoice Not Found", 125);
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionGetProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      $products = $model->products;
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $p_data[] = [
            'p_id' => $value->db_invoice_product_id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => $value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->quantity,
            'p_net_w' => $value->net_weight,
            'p_gross_w' => $value->gross_weight,
            'p_container' => $value->container,
          ];
        }
      }
      $this->setResponse("Success", 301, true, $p_data);
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionAddProducts($id) {
    if (Yii::$app->request->isAjax) {
      $model = $this->findModel($id);
      // $self_prods = ArrayHelper::map($model->products, 'od_invoice_product_id', 'od_invoice_product_id');
      $products = DBInvoiceProd::find()->andWhere(['invoice_id' => $model->invoice_id])->all(); 
      //->where(['not in', 'id', $self_prods])
      //->andWhere(['>', 'qty_od_invoice', 0])
      $p_data = [];
      if (!empty($products)) {
        foreach ($products as $key => $value) {
          $p_data[] = [
            'p_id' => $value->id, 
            'p_code' => $value->code, 
            'p_country' => $value->country_ru, 
            'p_title' => $value->titleRuEn,
            'p_price' => (float)$value->price,
            'p_unit' => $value->unit,
            'p_qty' => $value->quantity
          ];
        }
        $this->setResponse("Suuccessfully", 302, true, $p_data);
      } else {
        $this->setResponse("Продуктов нет для добавлении");
      }
    } else {
      throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    return $this->getResponse();
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }

  public function actionToWord($id) {
    $model = $this->findModel($id);
    ExportDBPackList::toWord($model);
    exit;
  }
}
