<?php
namespace app\controllers\dil_buyer;

use Yii;
use app\components\Currencies;
use app\components\Helper;
use app\components\Podtverjdeno;
use app\components\Status;
use app\components\Zapolnen;
use app\components\text\DBTxt;
use app\export\ExportDBContract;
use app\models\ContractNumbers;
use app\models\OD_DB;
use app\models\companies\Buyers;
use app\models\companies\Dillers;
use app\models\companies\Organizations;
use app\models\dil_buyer\DBContract;
use app\models\dil_buyer\DBContractProd;
use app\models\dil_buyer\DBParagraph;
use app\models\dil_buyer\search\DBContractSearch;
use app\models\org_diller\ODContract;
use app\models\org_diller\ODContractProd;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class DBContractController extends \app\base\AController {

	public function behaviors() {
		return $this->generateRules([
			[
				'allow' => true,
				'actions' => ['index', 'create', 'update', 'delete', 'to-word', 'podtverdit', 'edit', 'get-products', 'add-products', 'balance-products', 'get-contracts'],
				'roles' => ['diller'],
			],
		]);
	}

	protected function findModel($id) {
		return parent::fModel(DBContract::findOne($id));
	}

	public function actionIndex() {
		$this->clas = false;
		$this->title = "Contract";
		$searchModel = new DBContractSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dillers = Dillers::nList('id', 'title_ru');
		$buyers = Buyers::nList('id', 'title_ru');
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'dillers' => $dillers,
			'buyers' => $buyers
		]);
	}

	public function actionCreate($d_id = 0, $od_ids='', $b_id=0, $cy=0) {
		$od_ids = explode(',', (string)$od_ids);
		$diller = Dillers::findOne($d_id);
		$organization = Organizations::find()->one();
		$cts = ODContract::find()->joinWith(['number' => function (ActiveQuery $query){
			return $query->andWhere(['IS NOT', ContractNumbers::tableName().'.id', NULL]);
		}])->where(['zapolnen' => Zapolnen::YES, 'diller_id' => $diller->id])
			->andWhere(['in', ODContract::tableName().'.id', $od_ids])
			->andWhere(['>', 'dbcon_prod_nprice', 0])->all();
		$buyer = Buyers::findOne($b_id);
		if (!is_null($organization) && !is_null($diller) && !empty($cts) && !is_null($buyer) && in_array((int)$cy, Currencies::list())) {
			$model = new DBContract();
			$model->scenario = DBContract::SC_CREATE;
			$model->diller_id = $diller->id;
			$model->buyer_id = $buyer->id;
			$now = date("Y-m-d");
			$model->ndate = $now;
			$model->currency = $cy;
			$model->zapolnen = Zapolnen::NO;
			$model->director_dil_en = $diller->director2_en;
			$model->director_dil_ru = $diller->director2_ru;
			$model->director_buy_en = $buyer->director_en;
			$model->director_buy_ru = $buyer->director_ru;
			$mainParagSection = $model->mainParagSection;
			$model->text_en = '';
			$model->text_ru = '';
			if (!is_null($mainParagSection)) {
				$model->text_en = Helper::replacer($mainParagSection->title_en, [
					'{{seller_en}}' => $diller->title_en,
					'{{buyer_en}}' => $buyer->title_en,
					'{{seller_director_en}}' => $model->director_dil_en,
					'{{buyer_director_en}}' => $model->director_buy_en,
	        '{{seller_country_en}}' => $diller->country->title_en,
	        '{{buyer_country_en}}' => $buyer->country->title_en
				]);
				$model->text_ru = Helper::replacer($mainParagSection->title_ru, [
					'{{seller_ru}}' => $diller->title_ru,
					'{{buyer_ru}}' => $buyer->title_ru,
					'{{seller_director_ru}}' => $model->director_dil_ru,
					'{{buyer_director_ru}}' => $model->director_buy_ru,
	        '{{seller_country_ru}}' => $diller->country->title_ru,
	        '{{buyer_country_ru}}' => $buyer->country->title_ru
				]);
			}
			$model->note_en = DBTxt::noteEn(['{{seller_en}}' => $organization->title_en]);
			$model->note_ru = DBTxt::noteRu(['{{seller_ru}}' => $organization->title_ru]);
			$model->code = $diller->code;
			$model->applyDefaults();
			if($model->save()){
				foreach ($cts as $key => $value) {
					$db_od_cnt = (int)OD_DB::find()->where(['od_id' => $value->id, 'db_id' => $model->id])->count();
					if ($db_od_cnt < 1) {
						$od_db = new OD_DB();
						$od_db->od_id = $value->id;
						$od_db->db_id = $model->id;
						$od_db->save();
					}
				}
				return $this->redirect(['update', 'id' => $model->id]);
			} else {
				Yii::$app->session->setFlash('error', 'Can not create Contract, please try again');
				return $this->redirect(['index']);
			}
		} else {
			Yii::$app->session->setFlash('error', 'Please Select Diller, DIller contracts, Buyer and currency');
			return $this->redirect(['index']);
		}
	}

	public function actionUpdate($id) {
		$model = $this->findModel($id);
		$diller = $model->diller;
		$buyer = $model->buyer;
		if ($model->zapolnen == Zapolnen::YES) {
			$paragraphs = $model->paragraphs;
		} else {
			$parags = DBParagraph::find()
				->where(['status' => Status::ACTIVE])
        ->andWhere(['>', 'norder', 0])
				->orderBy(['norder' => SORT_ASC])
				->all();
      $paragraphs = [];
      if (!empty($parags)) {
        foreach ($parags as $key => $value) {
          $sections = $value->sections;
          $valsec = $value;
          if (!empty($sections)) {
            $new_sections = [];
            foreach ($sections as $key2 => $value2) {
              $value2->title_en = DBContract::replEn($value2->title_en, $diller, $buyer);
              $value2->title_ru = DBContract::replRu($value2->title_ru, $diller, $buyer);
              $new_sections[] = $value2;
            }
            $valsec->setSections($new_sections);
          }
          $paragraphs[] = $valsec;
        }
      }
		}

		return $this->render('edit_contract', [
			'model' => $model,
			'diller' => $diller,
			'buyer' => $buyer,
			'paragraphs' => $paragraphs
		]);
	}

	public function actionEdit($id) {
		if (Yii::$app->request->isAjax) {
			$model = $this->findModel($id);
			if ($model->load(Yii::$app->request->post())) {
				$products = Yii::$app->request->post('Products');
				if (!empty($products)) {
					$model->zapolnen = Zapolnen::YES;
					if ($model->save()) {
						$model->saveParagraphs();
						$model->saveProducts();
            $model->sendUpdatedMessage();
						$this->loadResponse($model);
						$this->setResponse("Contract Suuccessfully saved", null, true, ['applayed' => ($model->total_lprice <= 0)]);
					} else {
						$this->setResponse($model->allErrors, 123);
					}
				} else {
					$this->setResponse("Не выбран продукт", 122);
				}
			} else {
				$this->setResponse("Can not get Post data", 121);
			}
		} else {
			throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
		}
		return $this->getResponse();
	}

	public function actionGetContracts($id) {
		if (Yii::$app->request->isAjax) {
			$diller = Dillers::findOne($id);
			$p_data = [];
			$cts = ODContract::find()->joinWith(['number' => function (ActiveQuery $query){
				return $query->andWhere(['IS NOT', ContractNumbers::tableName().'.id', NULL]);
			}])->where(['zapolnen' => Zapolnen::YES, 'diller_id' => $diller->id])
			->andWhere(['>', 'dbcon_prod_nprice', 0])->all();
			if (!empty($cts)) {
				foreach ($cts as $key => $value) {
					$sum1 = number_format($value->dbcon_prod_nprice, 2, '.', ' ');
					$sum2 = number_format($value->total_nprice, 2, '.', ' ');
					$p_data[] = [
						'c_id' => $value->id,
						'c_title' => 'Contract: '.@$value->number->number.' ('.$sum1.' / '.$sum2.')',
						'c_number' => @$value->number->number,
						'c_total_nprice' => $value->total_nprice
					];
				}
				$this->setResponse("Success", 307, true, $p_data);
			} else {
				$this->setResponse("Contract not found", 216, false, []);
			}
		} else {
			throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
		}
		return $this->getResponse();
	}

	public function actionGetProducts($id) {
		if (Yii::$app->request->isAjax) {
			$model = $this->findModel($id);
			$products = $model->products;
			$p_data = [];
			if (!empty($products)) {
				foreach ($products as $key => $value) {
					$p_data[] = [
						'p_id' => $value->od_contract_product_id, 
						'p_code' => $value->code, 
						'p_country' => $value->country_ru, 
						'p_title' => $value->titleRuEn . ' <small>('.@$value->oDContractProduct->contract->number->number.')</small>',
						'p_price' => (float)$value->price,
						'p_unit' => $value->unit,
						'p_qty' => $value->quantity
					];
				}
			}
			$this->setResponse("Success", 301, true, $p_data);
		} else {
			throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
		}
		return $this->getResponse();
	}

	public function actionAddProducts($id) {
		if (Yii::$app->request->isAjax) {
			$model = $this->findModel($id);
			$c_ids = ArrayHelper::map($model->db_od, 'od_id', 'od_id');
			$self_prods = ArrayHelper::map($model->products, 'od_contract_product_id', 'od_contract_product_id');
			$products = ODContractProd::find()->joinWith('contract')->where(['not in', ODContractProd::tableName().'.id', $self_prods])
			 ->andWhere(['in', ODContractProd::tableName().'.contract_id', $c_ids])
			 ->andWhere(['>', 'qty_db_contract', 0])->all();
			$p_data = [];
			if (!empty($products)) {
				foreach ($products as $key => $value) {
					$p_data[] = [
						'p_id' => $value->id, 
						'p_code' => $value->code, 
						'p_country' => $value->country_ru, 
						'p_title' => $value->titleRuEn.' <small>('.@$value->contract->number->number.')</small>',
						'p_price' => (float)$value->price,
						'p_unit' => $value->unit,
						'p_qty' => $value->qty_db_contract
					];
				}
				$this->setResponse("Suuccessfully", 302, true, $p_data);
			} else {
				$this->setResponse("Продуктов нет для добавлении");
			}
		} else {
			throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
		}
		return $this->getResponse();
	}

	public function actionBalanceProducts($id) {
		if (Yii::$app->request->isAjax) {
			$model = $this->findModel($id);
			$total = (float)DBContractProd::find()->where(['contract_id' => $model->id])->sum('total_price');
			$this->setResponse('Success', 333, true, ['balance' => $total]);
		} else {
			throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
		}
		return $this->getResponse();
	}

	public function actionPodtverdit($id) {
		if (Yii::$app->request->isAjax) {
			$model = $this->findModel($id);
			if ($model->total_lprice > 0) {
				DBContractProd::updateAll(['podtverjden' => Podtverjdeno::DA], ['contract_id' => $model->id]);
				$model->refreshBalance();
				$invs = $model->invoices;
				$links = [];
				if (!empty($invs)) {
					foreach ($invs as $key => $value) {
						$links[] = [
							'text' => 'Перейти к Инвойс №'.$value->number->number,
							'link' => Url::to(['dil_buyer/d-b-invoice/update', 'id' => $value->id])
						];
					}
				}
				$links[] = [
					'text' => 'Создать новый Инвойс',
					'link' => Url::to(['dil_buyer/d-b-invoice/create', 'contract_id' => $model->id])
				];
				$this->setResponse("Контракт подтвержден", 312, true, null, $links);
			} else {
				$this->setResponse("Этот контракт раньше подтвержден", 310, true);
			}
		} else {
			throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
		}
		return $this->getResponse();
	}

	public function actionDelete($id) {
		$model = $this->findModel($id);
		$parentContracts = $model->parentContracts;
		$model->delete();
    if (!empty($parentContracts)) {
      foreach ($parentContracts as $contr) {
        $contr->refreshBalance();
      }
    }
		return $this->redirect(['index']);
	}

  public function actionToWord($id) {
    $model = $this->findModel($id);
    ExportDBContract::toWord($model);
    exit;
  }
}
