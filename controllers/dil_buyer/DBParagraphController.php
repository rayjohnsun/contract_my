<?php
namespace app\controllers\dil_buyer;

use Yii;
use app\components\Status;
use app\models\dil_buyer\DBParagraph;
use app\models\dil_buyer\search\DBParagraphSearch;

class DBParagraphController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('admin');
  }

  protected function findModel($id) {
    return parent::fModel(DBParagraph::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Paragraphs (Diller & Buyer)";
    $searchModel = new DBParagraphSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider
    ]);
  }

  public function actionCreate() {
    $this->setBred('Paragraphs (Diller & Buyer)', 'index');
    $this->title = 'create';
    $model = new DBParagraph();
    $model->status = Status::ACTIVE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create');
  }

  public function actionUpdate($id) {
    $this->setBred('Paragraphs (Diller & Buyer)', 'index');
    $model = $this->findModel($id);
    $this->title = 'Update: ' . $model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update');
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
