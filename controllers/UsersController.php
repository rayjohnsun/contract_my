<?php
namespace app\controllers;

use app\components\Status;
use app\models\search\UsersSearch;
use app\models\Users;
use Yii;

class UsersController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('manageUsers');
  }

  protected function findModel($id) {
    return parent::fModel(Users::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Users";
    $searchModel = new UsersSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionCreate() {
    $this->setBred('Users', 'index');
    $this->title = "create";
    $model = new Users();
    $model->status = Status::ACTIVE;
    $model->scenario = Users::SC_CREATE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create');
  }

  public function actionUpdate($id) {
    $this->setBred('Users', 'index');
    $model = $this->findModel($id);
    $this->title = 'Update: ' . $model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update');
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
