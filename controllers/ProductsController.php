<?php
namespace app\controllers;

use app\components\Status;
use app\models\Countries;
use app\models\Products;
use app\models\search\ProductsSearch;
use Yii;

class ProductsController extends \app\base\AController {

  public function behaviors() {
    return $this->generateRules('admin');
  }

  public function getCountries() {
    return Countries::nList('id', 'title_ru', Status::ACTIVE);
  }

  protected function findModel($id) {
    return parent::fModel(Products::findOne($id));
  }

  public function actionIndex() {
    $this->clas = false;
    $this->title = "Products";
    $searchModel = new ProductsSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'countries' => $this->countries,
    ]);
  }

  public function actionCreate() {
    $this->setBred('Products', 'index');
    $this->title = 'create';
    $model = new Products();
    $model->status = Status::ACTIVE;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('create', ['countries' => $this->countries]);
  }

  public function actionUpdate($id) {
    $this->setBred('Products', ['index']);
    $model = $this->findModel($id);
    $this->title = 'Update: ' . $model->nTitle;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    }
    $this->setModel($model);
    return $this->render('update', ['countries' => $this->countries]);
  }

  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(['index']);
  }
}
