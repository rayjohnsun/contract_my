<?php
namespace app\base;

use Yii;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface {
  public function bootstrap($app) {
    Yii::$container->set(\yii\grid\GridView::className(), \Zelenin\yii\SemanticUI\widgets\GridView::className());
  }
}