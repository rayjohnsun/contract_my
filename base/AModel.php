<?php
namespace app\base;

use Yii;
use app\components\Status;
use app\models\Messages;

class AModel extends \yii\db\ActiveRecord {
  const SC_CREATE = 'create';
  const SC_UPDATE = 'update';

  /*
   * for saving all eroor IDs
   */
  protected $err_ids = [];

  /*
   * for saving response
   */
  protected $response = ['message' => '', 'messages' => [], 'links' => [], 'code' => 250, 'status' => true, 'data' => []];

  /*
   * for getting response
   */
  public function getResponse() {
    return $this->response;
  }

  /*
   * for getting response
   */
  public function setResponse($message, $code = null, $status = null, $data = null) {
    if (is_string($message) || is_array($message)) {
      $this->response['message'] = $message;
    }
    if (is_integer($code)) {
      $this->response['code'] = $code;
    }
    if (is_bool($status)) {
      $this->response['status'] = $status;
    }
    if (is_array($data)) {
      $this->response['data'] = $data;
    }
  }

  /*
   * use for getting a list of model for dropdown list
   * if $where is string, then it will be used for status
   * return array
   */
  public static function nList($index, $value, $where = [], $order = []) {
    $res = static::find()->select([$index, $value]);
    if (!empty($where)) {
      if (!is_array($where)) {
        $where = ['status' => $where];
      }
      $res->where($where);
    }
    if (!empty($order)) {
      $res->orderBy($order);
    }
    $result = $res->all();
    return \yii\helpers\ArrayHelper::map($result, $index, $value);
  }

  /*
   * use for getting most used property of model as nTitle
   * auto search title, title_ru, description, description_ru
   * if not found any searched parameters, will be returned id
   * return string
   */
  public function getNTitle() {
    if (isset($this->title)) {
      return $this->title;
    } else if (isset($this->title_ru)) {
      return $this->title_ru;
    } else if (isset($this->description)) {
      return $this->description;
    } else if (isset($this->description_ru)) {
      return $this->description_ru;
    } else {
      return $this->id;
    }
  }

  /*
   * use it before save model
   * if you want return true or false, that use returnBeforeSave()
   * else use runBeforeSave()
   */
  public function beforeSave($insert) {
    if (parent::beforeSave($insert)) {
      if (method_exists($this, 'returnBeforeSave')) {
        return $this->returnBeforeSave();
      } else if (method_exists($this, 'runBeforeSave')) {
        $this->runBeforeSave();
      }
      return true;
    }
    return false;
  }

  /*
   * use it after save model
   * if you want to do anything after update, that use runAfterUpdate()
   * else use runAfterInsert()
   */
  public function afterSave($insert, $changedAttributes) {
    parent::afterSave($insert, $changedAttributes);
    if ($insert) {
      if (method_exists($this, 'runAfterInsert')) {
        $this->runAfterInsert();
      }
    } else {
      if (method_exists($this, 'runAfterUpdate')) {
        $this->runAfterUpdate();
      }
    }
  }

  /*
   * use it before delete model
   * if you want return true or false, that use returnBeforeDelete()
   * else use runBeforeDelete()
   */
  public function beforeDelete() {
    if (parent::beforeDelete()) {
      if (method_exists($this, 'returnBeforeDelete')) {
        return $this->returnBeforeDelete();
      } else if (method_exists($this, 'runBeforeDelete')) {
        $this->runBeforeDelete();
      }
      return true;
    }
    return false;
  }

  /*
   * use it after delete model
   */
  public function afterDelete() {
    parent::afterDelete();
    if (method_exists($this, 'runAfterDelete')) {
      $this->runAfterDelete();
    }
  }

  /*
   * use for adding All Error ids
   */
  public function setErrIds($id) {
    $this->err_ids[$id] = $id;
  }

  /*
   * use for getting All Error ids
   */
  public function getErrIds() {
    return $this->err_ids;
  }

  /*
   * use for clearing all associeted childs
   */
  public function clear($childs) {
    if (!empty($childs)) {
      foreach ($childs as $key => $value) {
        $value->delete();
      }
    }
  }

  /*
   * use for getting all errors in one Array
   */
  public function getAllErrors() {
    $errors = [];
    if (!empty($this->errors)) {
      foreach ($this->errors as $key => $value) {
        if (is_array($value)) {
          if (!empty($value)) {
            foreach ($value as $key2 => $value2) {
              $errors[] = ['id' => $key, 'text' => $value2];
            }
          }
        } else {
          $errors[] = ['id' => $key, 'text' => (string)$value];
        }
      }
    }
    return $errors;
  }

  /*
   * useы for sending message to manager
   */
  public function sendMessage($msg_txt, $link = '') {
    $user_id = Yii::$app->user->id;
    $mess_exists = Messages::find()->where([
      'user_id' => $user_id,
      'txt' => $msg_txt,
      'link' => $link,
      'status' => Status::NOACTIVE
    ])->one();
    if (!$mess_exists) {
      $message = new Messages();
      $message->user_id = $user_id;
      $message->txt = $msg_txt;
      $message->link = $link;
      $message->save();
    }
  }

}
