<?php
namespace app\base;

use Yii;

class AController extends \yii\web\Controller {
  public $title = '';
  public $bread = [];
  public $clas = true;
  public $model = null;

  private $response = ['message' => '', 'messages' => [], 'links' => [], 'code' => 100, 'status' => false, 'data' => []];

  public function loadResponse($model) {
    if ($model) {
      $this->response = $model->getResponse();
    }
  }

  public function setResponse($message, $code = null, $status = null, $data = null, $links = null) {
    if (is_string($message)) {
      $this->response['message'] = $message;
    } else if (is_array($message)) {
      $this->response['messages'] = $message;
    }
    if (is_integer($code)) {
      $this->response['code'] = $code;
    }
    if (is_bool($status)) {
      $this->response['status'] = $status;
    }
    if (is_array($data) && !empty($data)) {
      $this->response['data'] = $data;
    }
    if (is_array($links) && !empty($links)) {
      if (isset($links[0])) {
        $this->response['links'] = $links;
      } else {
        $this->response['links'][] = $links;
      }
    }
  }

  public function getResponse() {
    return json_encode($this->response);
  }

  public function setModel($model) {
    $this->model = $model;
  }

  public function setBred($title, $link = '') {
    $this->bread[] = !empty($link) ? ['label' => $title, 'url' => $link] : $title;
  }

  public function generateRules($newRules, $newActions = []) {
    if (is_array($newRules)) {
      $rules = $newRules;
    } else {
      if (empty($newActions)) {
        $newActions = ['index', 'create', 'update', 'delete'];
      }
      $rules = [
        [
          'allow' => true,
          'actions' => $newActions,
          'roles' => [$newRules],
        ],
      ];
    }
    return [
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => $rules,
      ],
      'verbs' => [
        'class' => \yii\filters\VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  public function generateDP($model) {
    return $model->search(Yii::$app->request->queryParams);
  }

  public function render($view, $params = []) {
    if (!isset($params['title']) AND !isset($params['bread'])) {
      $params['title'] = $this->title;
      if (!empty($this->title)) {
        $this->setBred($this->title);
      }
      $params['bread'] = $this->bread;
      if (is_string($this->clas)) {
        $params['clas'] = $this->clas;
      } elseif ($this->clas === true) {
        $params['clas'] = 'container';
      } else {
        $params['clas'] = '';
      }
      if ($this->model) {
        $params['model'] = $this->model;
      }
      return parent::render($view, $params);
    }
    throw new \Exception("Not allowed params {title} and {bread}", 1);
  }

  public function fModel($model) {
    if ($model) {
      return $model;
    }
    throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
  }
}