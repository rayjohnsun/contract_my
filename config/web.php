<?php

require_once __DIR__ . '/functions.php';
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
	'id' => 'basic',
	'name' => 'Vektan',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log', 'app\base\Bootstrap'],
	'aliases' => [
		'@bower' => '@vendor/bower',
		'@npm'   => '@vendor/npm-asset',
	],
	'components' => [
		'request' => [
			'cookieValidationKey' => 'MZJT9OmIo3eF1sKdxwdx4xf2DrWIwUr7',
			'baseUrl' => '',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => 'app\models\Users',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => $db,        
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				''		=> 'site/index',
				'login'	=> 'site/login',
				'logout'=> 'site/logout',
				'<controller>/<action>/<id:\d+>'		=>'<controller>/<action>',
			],
		],
		'authManager' => [
			'class'         => 'yii\rbac\PhpManager',
			'defaultRoles'  => ['admin', 'organization', 'diller', 'manager'],
			'itemFile'      => '@app/rbac/items.php',
			'ruleFile'      => '@app/rbac/rules.php',
			'assignmentFile'=> '@app/rbac/assignments.php',
		],
	],
	'params' => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
}

return $config;
