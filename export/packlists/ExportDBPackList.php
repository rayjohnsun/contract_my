<?php 

namespace app\export\packlists;

use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\Style\Table;
use app\components\export\EXW;

class ExportDBPackList
{

	public static function toWord($model)
	{

		$phpWord = new PhpWord();
		$phpWord->setDefaultFontName('Times New Roman');
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultParagraphStyle([
    	'align'      => 'both',
    	'spaceAfter' => Converter::pointToTwip(0),
    	'spacing'    => 20,
    ]);

		/*------------------------ Word Styles --------------------------*/
		$spage 	= ['marginTop'=>560, 'marginBottom'=>560, 'marginLeft'=>800, 'marginRight'=>700];

		$stbl1 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>40, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];
		$stbl2 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>80, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];

		$scel1 	= [];
		$scel2 	= ['valign' => 'center'];
		$scel3 	= ['valign' => 'center', 'gridSpan' => 3];
		$scel4 	= [];

		$sf0 	= ['name' => 'Times New Roman', 'size' => 14.5, 'bold' => true];
		$sf1 	= ['name' => 'Times New Roman', 'size' => 12, 'bold' => true];
		$sf2 	= ['name' => 'Times New Roman', 'size' => 10];
		$sf3 	= ['name' => 'Times New Roman', 'size' => 10, 'bold' => true];

		$sp0 	= ['alignment' => 'center', 'lineHeight' => 1, 'spaceAfter' => 0];
		$sp1 	= ['alignment' => 'left', 'lineHeight' => 1, 'spaceAfter' => 0];
		$sp2	= [];
		$sp3	= [];
		$sp4	= [];		

		/*------------------------ Page --------------------------*/
		$section= $phpWord->createSection($spage);

		/*------------------------ Title --------------------------*/
		$section->addText('УПАКОВОЧНЫЙ ЛИСТ / PACKING LIST '.@$model->number->number, $sf0, $sp0);

		/*------------------------ Date --------------------------*/
		$section->addText('Дата / Date: '.$model->new_ndate, $sf1, $sp0);

		/*------------------------ <br> --------------------------*/
		$section->addText('', $sf1, $sp1);

		/*------------------------ TABLE 1 --------------------------*/
		$table1 = $section->addTable($stbl1);

		if ($model->show_siller > 0) {
			$table1->addRow();
			$cell1 	= $table1->addCell(3000, $scel1);
			Html::addHtml($cell1, EXW::html(@$model->invoice->dil_text_ru));
			$cell2 	= $table1->addCell(3000, $scel1);
			Html::addHtml($cell2, EXW::html(@$model->invoice->dil_text_en));
		}

		if ($model->show_buyer > 0) {
			$table1->addRow();
			$cell3 	= $table1->addCell(3000, $scel1);
			Html::addHtml($cell3, EXW::html(@$model->invoice->buy_text_ru));
			$cell4 	= $table1->addCell(3000, $scel1);
			Html::addHtml($cell4, EXW::html(@$model->invoice->buy_text_en));
		}

		if ($model->show_consignee > 0) {
			$table1->addRow();
			$cell5 	= $table1->addCell(3000, $scel1);
			Html::addHtml($cell5, EXW::html(@$model->invoice->cons_text_ru));
			$cell6 	= $table1->addCell(3000, $scel1);
			Html::addHtml($cell6, EXW::html(@$model->invoice->cons_text_en));
		}

		/*------------------------ <br> --------------------------*/
		$section->addText('', $sf1, $sp1);
		
		/*------------------------ TABLE 2 -------------------------*/
		$table2 = $section->addTable($stbl2);
		
		$table2->addRow();
		$table2->addCell(500, $scel2)->addText('#', $sf3, $sp0);
		$table2->addCell(1300, $scel2)->addText('НОМЕР КОНТЕЙНЕРА CONTAINER No.', $sf3, $sp0);
		$table2->addCell(4000, $scel2)->addText('ОПИСАНИЕ ТОВАРА / DESCRIPTION', $sf3, $sp0);
		$table2->addCell(1500, $scel2)->addText('HS Code / Код ТНВЭД', $sf3, $sp0);
		$table2->addCell(1800, $scel2)->addText('КОЛ-ВО (ШТ./МЕШКИ) / QUANTITY (PCS/BAGS)', $sf3, $sp0);
		$table2->addCell(1300, $scel2)->addText('ВЕС НЕТТО (КГ) / NET WEIGHT (KG)', $sf3, $sp0);
		$table2->addCell(1500, $scel2)->addText('ВЕС БРУТТО (КГ) / GROSS WEIGHT (KG)', $sf3, $sp0);

		$total_qty = $total_ves1 = $total_ves2 = 0;
		if (!empty($model->products)) {
			foreach ($model->products as $key => $value) {
				$table2->addRow();
				$table2->addCell(500, $scel2)->addText(($key+1), $sf2, $sp0);
				$table2->addCell(1300, $scel2)->addText($value->container, $sf2, $sp0);
				$table2->addCell(3000, $scel2)->addText($value->description_ru.' / '.$value->description_en, $sf2, $sp1);
				$cll = $table2->addCell(1300, $scel2);
					$cll->addText($value->code, $sf2, $sp0);
				$table2->addCell(1300, $scel2)->addText($value->quantity, $sf2, $sp0);
				$table2->addCell(1300, $scel2)->addText($value->net_weight, $sf2, $sp0);
				$table2->addCell(1300, $scel2)->addText($value->gross_weight, $sf2, $sp0);
				$total_qty += $value->quantity;
				$total_ves1 += $value->net_weight;
				$total_ves2 += $value->gross_weight;
			}
		}

		$table2->addRow();
		$table2->addCell(3000, $scel3)->addText('ИТОГО / TOTAL', $sf3, $sp0);
		$table2->addCell(1300, $scel2)->addText('', $sf3, $sp0);
		$table2->addCell(1300, $scel2)->addText($total_qty, $sf3, $sp0);
		$table2->addCell(1300, $scel2)->addText($total_ves1, $sf3, $sp0);
		$table2->addCell(1300, $scel2)->addText($total_ves2, $sf3, $sp0);

		/*------------------------ <br> --------------------------*/
		$section->addText('', $sf1, $sp1);

		/*------------------------ Notes -------------------------*/
		// Html::addHtml($section, EXW::html($model->notes));

		/*------------------------ <br> --------------------------*/
		$section->addText('', $sf1, $sp1);
		$section->addText('', $sf1, $sp1);
		$section->addText('', $sf1, $sp1);
		
		/*------------------------ TABLE 3 -------------------------*/
		$table3 = $section->addTable();
		
		$table3->addRow();
		$table3->addCell(2400, $scel3)->addText('Директор / Director:', $sf3, $sp1);
		$table3->addCell(6000, $scel3)->addText('', $sf3, $sp1);
		// $table3->addCell(6000, $scel3)->addText($model->director_dil_ru.' / '.$model->director_dil_en, $sf3, $sp1);

		/*------------------------ DOWNLOAD -------------------------*/

		$file = 'Packing List VT '.@$model->number->number.'.docx';
		header("Expires: 0");
		header('Content-Type: application/vnd.ms-word');
		header('Content-Disposition: attachment;filename="' . $file);
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save('php://output');
		
	}

}