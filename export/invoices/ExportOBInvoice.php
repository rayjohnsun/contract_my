<?php 

namespace app\export\invoices;

use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\Style\Table;
use app\components\export\EXW;

class ExportOBInvoice
{

	public static function toWord($model)
	{

		$phpWord = new PhpWord();
		$phpWord->setDefaultFontName('Times New Roman');
		$phpWord->setDefaultFontSize(12);
		$phpWord->setDefaultParagraphStyle([
    	'align'      => 'both',
    	'spaceAfter' => Converter::pointToTwip(0),
    	'spacing'    => 20,
    ]);

		/*------------------------ Word Styles --------------------------*/
		$spage 	= ['marginTop'=>560, 'marginBottom'=>560, 'marginLeft'=>800, 'marginRight'=>700];

		$stbl1 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>40, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];
		$stbl2 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>80, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];

		$scel1 	= [];
		$scel2 	= ['valign' => 'center'];
		$scel3 	= ['valign' => 'center', 'gridSpan' => 5];
		$scel4 	= [];

		$sf0 	= ['name' => 'Times New Roman', 'size' => 14.5, 'bold' => true];
		$sf1 	= ['name' => 'Times New Roman', 'size' => 12, 'bold' => true];
		$sf2 	= ['name' => 'Times New Roman', 'size' => 10];
		$sf3 	= ['name' => 'Times New Roman', 'size' => 10, 'bold' => true];

		$sp0 	= ['lineHeight' => 1, 'spaceAfter' => 0, 'alignment' => 'left'];
		$sp1 	= ['lineHeight' => 1, 'spaceAfter' => 0, 'alignment' => 'center'];
		$sp2	= [];
		$sp3	= [];
		$sp4	= [];

		/*------------------------ Page --------------------------*/
		$section= $phpWord->createSection($spage);

		/*------------------------ Title --------------------------*/
		$section->addText('ИНВОЙС / INVOICE # '.@$model->number->number, $sf0, $sp1);

		/*------------------------ Date --------------------------*/
		$section->addText('Дата / Date: '.$model->new_ndate, $sf1, $sp1);

		/*------------------------ <br> --------------------------*/
		$section->addText('', $sf1, $sp0);

		/*------------------------ TABLE 1 --------------------------*/
		$table1 = $section->addTable($stbl1);

		$table1->addRow();
		$cell1 	= $table1->addCell(3000, $scel1);
		Html::addHtml($cell1, EXW::html($model->org_text_en));
		$cell2 	= $table1->addCell(3000, $scel1);
		Html::addHtml($cell2, EXW::html($model->org_text_ru));

		$table1->addRow();
		$cell3 	= $table1->addCell(3000, $scel1);
		Html::addHtml($cell3, EXW::html($model->buy_text_en));
		$cell4 	= $table1->addCell(3000, $scel1);
		Html::addHtml($cell4, EXW::html($model->buy_text_ru));

		$table1->addRow();
		$cell5 	= $table1->addCell(3000, $scel1);
		Html::addHtml($cell5, EXW::html($model->cons_text_en));
		$cell6 	= $table1->addCell(3000, $scel1);
		Html::addHtml($cell6, EXW::html($model->cons_text_ru));

		/*------------------------ <br> --------------------------*/
		$section->addText('', $sf1, $sp0);
		
		/*------------------------ TABLE 2 -------------------------*/
		$table2 = $section->addTable($stbl2);
		
		$table2->addRow();
		$table2->addCell(500, $scel1)->addText('№', $sf3, $sp1);
		$table2->addCell(3000, $scel1)->addText('Описание товара / Description', $sf3, $sp1);
		$table2->addCell(1300, $scel1)->addText('Код ТН ВЭД / HS code', $sf3, $sp1);
		$table2->addCell(1300, $scel1)->addText('Единица измерения / Unit of measurement', $sf3, $sp1);
		$table2->addCell(1300, $scel1)->addText('Цена за единицу (доллары США) / Unit price (USD)', $sf3, $sp1);
		$table2->addCell(1300, $scel1)->addText('Количество / Quantity', $sf3, $sp1);
		$table2->addCell(1800, $scel1)->addText('Общая сумма (доллары США) / Total price (USD)', $sf3, $sp1);

		$total_qty = $total_price = 0;
		if (!empty($model->products)) {
			foreach ($model->products as $key => $value) {
				$table2->addRow();
				$table2->addCell(500, $scel2)->addText(($key+1), $sf2, $sp1);
				$p_name = $value->description_ru.' / '.$value->description_en;
					$table2->addCell(3000, $scel2)->addText($p_name, $sf2, $sp0);
				$cll = $table2->addCell(1300, $scel2);
					$cll->addText($value->code, $sf2, $sp1);
					$cll->addText($value->country_ru, $sf2, $sp1);
				$table2->addCell(1300, $scel2)->addText($value->unit, $sf2, $sp1);
				$table2->addCell(1300, $scel2)->addText($value->price, $sf2, $sp1);
				$table2->addCell(1300, $scel2)->addText($value->quantity, $sf2, $sp1);
				$table2->addCell(1800, $scel2)->addText($value->total_price, $sf2, $sp1);
				$total_qty += $value->quantity;
				$total_price += $value->total_price;
			}
		}

		$table2->addRow();
		$table2->addCell(3000, $scel3)->addText('ИТОГО / TOTAL', $sf3, $sp1);
		$table2->addCell(1300, $scel2)->addText($total_qty, $sf3, $sp1);
		$table2->addCell(1800, $scel2)->addText($total_price, $sf3, $sp1);

		/*------------------------ <br> --------------------------*/
		$section->addText('', $sf1, $sp0);

		/*------------------------ Notes -------------------------*/
		Html::addHtml($section, EXW::html($model->notes));

		/*------------------------ <br> --------------------------*/
		$section->addText('', $sf1, $sp0);
		$section->addText('', $sf1, $sp0);
		$section->addText('', $sf1, $sp0);
		$section->addText('', $sf1, $sp0);
		$section->addText('', $sf1, $sp0);
		
		/*------------------------ TABLE 3 -------------------------*/
		$table3 = $section->addTable();
		
		$table3->addRow();
		$table3->addCell(2400, $scel3)->addText('Подпись / Signature:', $sf3, $sp0);
		$table3->addCell(4000, $scel3)->addText('_________________________', $sf2, $sp0);

		$table3->addRow();
		$table3->addCell(2400, $scel3)->addText('Должность / Position:', $sf3, $sp0);
		$table3->addCell(4000, $scel3)->addText($model->position, $sf2, $sp0);

		$table3->addRow();
		$table3->addCell(2400, $scel3)->addText('Имя / Name:', $sf3, $sp0);
		$table3->addCell(4000, $scel3)->addText($model->director_org_ru.' / '.$model->director_org_en, $sf2, $sp0);

		$table3->addRow();
		$table3->addCell(2400, $scel3)->addText('Место / Place:', $sf3, $sp0);
		$table3->addCell(4000, $scel3)->addText($model->place, $sf2, $sp0);
		
		/*------------------------ DOWNLOAD -------------------------*/

		$file = 'Invoice VT '.@$model->number->number.'.docx';
		header("Expires: 0");
		header('Content-Type: application/vnd.ms-word');
		header('Content-Disposition: attachment;filename="' . $file);
		header('Cache-Control: max-age=0');
		$objWriter = IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save('php://output');
		
	}

}