<?php 

	namespace app\export\invoices;

	use PhpOffice\PhpWord\IOFactory;
	use PhpOffice\PhpWord\PhpWord;
	use PhpOffice\PhpWord\Shared\Converter;
	use PhpOffice\PhpWord\Shared\Html;
	use PhpOffice\PhpWord\Style\Table;
	use app\components\export\EXW;

	class ExportDBInvoice {

		public static function toWord($model) {

			$phpWord = new PhpWord();
			$phpWord->setDefaultFontName('Tahoma');
			$phpWord->setDefaultFontSize(12);
			$phpWord->setDefaultParagraphStyle([
	    	'align'      => 'both',
	    	'spaceAfter' => Converter::pointToTwip(0),
	    	'spacing'    => 20,
      ]);

			/*------------------------ Word Styles --------------------------*/
			$spage 	= ['marginTop'=>560, 'marginBottom'=>560, 'marginLeft'=>800, 'marginRight'=>700];

			$stbl0 	= ['cellMargin'=>40, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];
			$stbl1 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>40, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];
			$stbl2 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>80, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];

			$scel1 	= [];
			$scel2 	= ['valign' => 'center'];
			$scel3 	= ['valign' => 'center', 'gridSpan' => 2, 'borderLeftColor'=>'ffffff', 'borderLeftSize'=> 0, 'borderBottomColor'=>'ffffff', 'borderBottomSize'=> 0, 'borderRightColor'=>'ffffff', 'borderRightSize'=> 0];
			$scel4 	= ['valign' => 'center', 'borderLeftColor'=>'ffffff', 'borderLeftSize'=> 0, 'borderBottomColor'=>'ffffff', 'borderBottomSize'=> 0];

			$sf0 	= ['size' => 14.5, 'bold' => true];
			$sf1 	= ['bold' => true];
			$sf2 	= ['size' => 10];
			$sf3 	= ['size' => 10, 'bold' => true];
			$sf4 	= ['size' => 20, 'bold' => true, 'color' => '808080'];
			$sf5 	= ['size' => 8, 'bold' => true];

			$sp0 	= ['lineHeight' => 1, 'spaceAfter' => 0, 'alignment' => 'left'];
			$sp1 	= ['lineHeight' => 1, 'spaceAfter' => 0, 'alignment' => 'center'];
			$sp2	= [];
			$sp3	= [];
			$sp4	= ['lineHeight' => 1, 'spaceAfter' => 0, 'alignment' => 'right'];

			/*------------------------ Page --------------------------*/
			$section= $phpWord->createSection($spage);

			/*------------------------ <br> --------------------------*/
			$section->addText('', $sf1, $sp0);

			/*------------------------ TABLE 0 --------------------------*/
			$table0 = $section->addTable($stbl0);

			$table0->addRow();

			$cell0 = $table0->addCell(3000, $scel1);
			Html::addHtml($cell0, EXW::html($model->dil_text_en));

			$table0->addCell(3000, $scel1)->addText('INVOICE', $sf4, $sp4);

			/*------------------------ <br> --------------------------*/
			$section->addText('', $sf1, $sp0);

			/*------------------------ Title --------------------------*/
			$section->addText('INVOICE '.@$model->number->number, $sf5, $sp4);

			/*------------------------ Date --------------------------*/
			$section->addText('Date: '.$model->new_ndate, $sf5, $sp4);

			/*------------------------ <br> --------------------------*/
			$section->addText('', $sf1, $sp0);
			$section->addText('', $sf1, $sp0);

			/*------------------------ TABLE 1 --------------------------*/
			$table1 = $section->addTable($stbl1);

			$table1->addRow();
			$cell1 	= $table1->addCell(5000, $scel1);
			Html::addHtml($cell1, EXW::html($model->buy_text_en));

			/*------------------------ <br> --------------------------*/
			$section->addText('', $sf1, $sp0);
			$section->addText('', $sf1, $sp0);
			$section->addText('', $sf1, $sp0);
			
			/*------------------------ TABLE 2 -------------------------*/
			$table2 = $section->addTable($stbl2);
			
			$table2->addRow();
			$table2->addCell(3000, $scel1)->addText('DESCRIPTION', $sf3, $sp1);
			$table2->addCell(1300, $scel1)->addText('QUANTITY IN METRIC TONS', $sf3, $sp1);
			$table2->addCell(1300, $scel1)->addText('UNIT PRICE   IN USD', $sf3, $sp1);
			$table2->addCell(1800, $scel1)->addText('TOTAL PRICE IN USD', $sf3, $sp1);

			$total_qty = $total_price = 0;

			if (!empty($model->products)) {

				foreach ($model->products as $key => $value) {

					$p_name = ($key+1).'.  '.$value->description_en;

					$table2->addRow();

					$table2->addCell(3000, $scel2)->addText($p_name, $sf2, $sp1);
					$table2->addCell(1300, $scel2)->addText($value->quantity, $sf2, $sp1);
					$table2->addCell(1300, $scel2)->addText($value->price, $sf2, $sp1);
					$table2->addCell(1800, $scel2)->addText($value->total_price, $sf2, $sp1);

					$total_qty += $value->quantity;
					$total_price += $value->total_price;

				}

			}

			$table2->addRow();

			$table2->addCell(4300, $scel3)->addText('', $sf2, $sp4);
			$table2->addCell(1300, $scel4)->addText('TOTAL AMOUNT IN USD', $sf2, $sp1);
			$table2->addCell(1800, $scel2)->addText($total_price, $sf3, $sp1);

			/*------------------------ <br> --------------------------*/
			$section->addText('', $sf1, $sp0);

			/*------------------------ Notes -------------------------*/
			Html::addHtml($section, EXW::html($model->notes));

			/*------------------------ <br> --------------------------*/
			$section->addText('', $sf1, $sp0);
			$section->addText('', $sf1, $sp0);
			$section->addText('', $sf1, $sp0);
			
			/*------------------------ TABLE 3 -------------------------*/
			$table3 = $section->addTable($stbl0);
			
			$table3->addRow();
			$table3->addCell(1500, $scel2)->addText('Signature:', $sf3, $sp4);
			$table3->addCell(1000, $scel2)->addText('', $sf3, $sp0);
			$table3->addCell(2500, $scel2)->addText($model->director_dil_en, $sf3, $sp0);
			
			/*------------------------ DOWNLOAD -------------------------*/

			$file = 'Invoice LTZ '.@$model->number->number.'.docx';
			header("Expires: 0");
			header('Content-Type: application/vnd.ms-word');
			header('Content-Disposition: attachment;filename="' . $file);
			header('Cache-Control: max-age=0');
			$objWriter = IOFactory::createWriter($phpWord, 'Word2007');
			$objWriter->save('php://output');
			
		}

	}