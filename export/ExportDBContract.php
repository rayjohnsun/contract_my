<?php 

	namespace app\export;

	use PhpOffice\PhpWord\IOFactory;
	use PhpOffice\PhpWord\PhpWord;
	use PhpOffice\PhpWord\Shared\Converter;
	use PhpOffice\PhpWord\Shared\Html;
	use PhpOffice\PhpWord\Style\Table;
	use app\components\export\EXW;

	class ExportDBContract {

		public static function toWord($model) {

			$phpWord = new PhpWord();
			$phpWord->setDefaultFontName('Times New Roman');
			$phpWord->setDefaultFontSize(12);
			$phpWord->setDefaultParagraphStyle([
	    	'align'      => 'both',
	    	'spaceAfter' => Converter::pointToTwip(0),
	    	'spacing'    => 20,
	    ]);

			/*------------------------ Word Styles --------------------------*/
			$spage 	= ['marginTop'=>560, 'marginBottom'=>560, 'marginLeft'=>800, 'marginRight'=>700];

			$stbl1 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>80, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];
			$stbl2 	= ['cellMargin'=>80, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];
//            $stbl11 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>40, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];
            $stbl21 	= ['borderColor'=>'000000','borderSize'=>1,'cellMargin'=>80, 'unit' => Table::WIDTH_PERCENT, 'width' => 5000];

			$scel1 	= ['gridSpan' => 2, 'valign' => 'center'];
			$scel2 	= [];
			$scel3 	= ['borderColor'=>'000000','borderSize'=>1];
            $scel33 = ['valign' => 'center', 'gridSpan' => 4];
			$scel4 	= ['borderColor'=>'000000','borderLeftSize'=>1,'borderRightSize'=>1];

			$sf0 	= [];
			$sf1 	= ['bold'=> true];
			$sf2 	= ['italic'=>true];
            $sf22 	= ['name' => 'Times New Roman', 'size' => 10];
            $sf3 	= ['bold'=> true, 'size' => 10];
            $sf4 	= ['italic'=> true, 'size' => 9];

			$sp0 	= ['lineHeight'=>1,'spaceAfter'=>0,'alignment'=>'left'];
			$sp1 	= ['lineHeight'=>1,'spaceAfter'=>50,'alignment'=>'center','spaceBefore'=>40];
            $sp11 	= ['lineHeight' => 1, 'spaceAfter' => 0, 'alignment' => 'center'];
//			$sp2 	= ['lineHeight'=>1,'spaceAfter'=>0];
			$sp3 	= ['lineHeight'=>1,'spaceAfter'=>0,'alignment'=>'center','spaceBefore'=>8];
			$sp4 	= ['lineHeight'=>1,'spaceAfter'=>0,'alignment'=>'left','indentation'=>[
				'left' => 60
			]];
			$sp5 	= ['lineHeight'=>1,'spaceAfter'=>0,'alignment'=>'right','indentation'=>[
				'right' => 60
			]];

			/*------------------------ Page --------------------------*/
			$section= $phpWord->createSection($spage);

			/*------------------------ TABLE 1 --------------------------*/
			$table1 = $section->addTable($stbl1);

			$table1->addRow();
			$table1->addCell(3000,$scel1)->addText('CONTRACT № '.@$model->number->number,$sf1,$sp1);
			$table1->addCell(3000,$scel1)->addText('КОНТРАКТ № '.@$model->number->number,$sf1,$sp1);

			$table1->addRow();
			$table1->addCell(1500,$scel2)->addText($model->adres_en,$sf2, $sp4);
			$table1->addCell(1500,$scel2)->addText($model->date_en,$sf2, $sp5);
			$table1->addCell(1500,$scel2)->addText($model->adres_ru,$sf2, $sp4);
			$table1->addCell(1500,$scel2)->addText($model->date_ru,$sf2, $sp5);

			/*------------------------ <br> --------------------------*/
			$section->addText('', $sf0, $sp0);
			$section->addText('', $sf0, $sp0);

			/*------------------------ TABLE 2 --------------------------*/
			$table2 = $section->addTable($stbl2);	
			$table2->addRow();
			$cell1 	= $table2->addCell(3000,$scel3);
			Html::addHtml($cell1, EXW::html($model->text_en));
			$cell2 	= $table2->addCell(3000,$scel3);
			Html::addHtml($cell2, EXW::html($model->text_ru));
			
			if (!empty($model->paragraphs)) {
				$number = 1;

				foreach ($model->paragraphs as $paragraph) {
					$text_en = $number . '. ' . $paragraph->title_en;
					$text_ru = $number . '. ' . $paragraph->title_ru;

					$table2->addRow();
					$table2->addCell(3000,$scel3)->addText($text_en,$sf1,$sp3);
					$table2->addCell(3000,$scel3)->addText($text_ru,$sf1,$sp3);

					$number2 = 1;
					if (!empty($paragraph->sections)) {

						foreach ($paragraph->sections as $sec) {
							$num = $number . '.' . $number2 .'.';

							$table2->addRow();

							$sec_txt_en = EXW::normalize($sec->title_en, $num);
							$sec_cel1 = $table2->addCell(3000,$scel4);
							Html::addHtml($sec_cel1, $sec_txt_en);
							$sec_txt_ru = EXW::normalize($sec->title_ru, $num);
							$sec_cel2 = $table2->addCell(3000,$scel4);
							Html::addHtml($sec_cel2, $sec_txt_ru);

							$number2++;

						}
						
					} else {

						$table2->addRow();

						$tt = '<p><strong>'.$number . '.' . $number2 . '.</strong>&nbsp;&nbsp;</p>';

						$e_cel1 = $table2->addCell(3000,$scel3);
							Html::addHtml($e_cel1, $tt);

						$e_cel2 = $table2->addCell(3000,$scel3);
							Html::addHtml($e_cel2, $tt);

					}

					$number++;
				}
			}
			$table2->addRow();
			$cel3 = $table2->addCell(3000,$scel3);
				$cel3->addText('', $sf0,$sp0);
				$cel3->addText('Signatures of the Parties:', $sf1,$sp3);
				$cel3->addText('', $sf0,$sp0);
				$cel3->addText('THE SELLER (Продавец):', $sf1,$sp3);
				$cel3->addText('', $sf0,$sp0);
				$cel3->addText('', $sf0,$sp0);
				$cel3->addText('', $sf0,$sp0);
				$cel3->addText('', $sf0,$sp0);
				$cel3->addText($model->director_dil_en, $sf1,$sp3);
				$cel3->addText('', $sf0,$sp0);

			$cel4 = $table2->addCell(3000,$scel3);
				$cel4->addText('', $sf0,$sp0);
				$cel4->addText('Подписи сторон:', $sf1,$sp3);
				$cel4->addText('', $sf0,$sp0);
				$cel4->addText('THE BUYER (Покупатель):', $sf1,$sp3);
				$cel4->addText('', $sf0,$sp0);
				$cel4->addText('', $sf0,$sp0);
				$cel4->addText('', $sf0,$sp0);
				$cel4->addText('', $sf0,$sp0);
				$cel4->addText($model->director_buy_en, $sf1,$sp3);
				$cel4->addText('', $sf0,$sp0);

			// dd();

            /*------------------------ <br> --------------------------*/
            $section->addText('', $sf0, $sp0);
            $section->addText('', $sf0, $sp0);

            $table3 = $section->addTable($stbl1);
            $table3->addRow();

            $t3cel1 = $table3->addCell(3000,$scel3);
            $t3cel1->addText('APPENDIX №1', $sf3,$sp4);
            $t3cel1->addText('', $sf0, $sp0);
            $t3cel1->addText('to CONTRACT № '.@$model->number->number.' dated '.$model->date_en, $sf3,$sp1);

            $t3cel2 = $table3->addCell(3000,$scel3);
            $t3cel2->addText('ПРИЛОЖЕНИЕ №1', $sf3,$sp5);
            $t3cel2->addText('', $sf0, $sp0);
            $t3cel2->addText('к КОНТРАКТУ № '.@$model->number->number.' от '.$model->date_ru, $sf3,$sp1);

            $section->addText('', $sf0, $sp0);
            $section->addText('Specification of the Goods / Спецификация Товара', $sf1, $sp1);
            $section->addText('', $sf0, $sp0);
            $section->addText('Цены указаны в Евро, НДС 0% / Price indicate in USD, only VAT 0%', $sf4, $sp5);

            /*------------------------ TABLE 4 -------------------------*/
            $table4 = $section->addTable($stbl21);

            $table4->addRow();
            $table4->addCell(500, $scel3)->addText('№', $sf3, $sp11);
            $table4->addCell(3000, $scel3)->addText('Наименование товара / Name of the product', $sf3, $sp11);
            $table4->addCell(1300, $scel3)->addText('Ед. из. / Unit', $sf3, $sp11);
            $table4->addCell(3000, $scel3)->addText('Цена за ед. изм. в долларах США./Price per Unit in USD', $sf3, $sp11);
            $table4->addCell(1300, $scel3)->addText('Количество / Quantity', $sf3, $sp11);
            $table4->addCell(1800, $scel3)->addText('Сумма в Долларах США Total amount in USD', $sf3, $sp11);

            $total_qty = $total_price = 0;
            if (!empty($model->products)) {
                foreach ($model->products as $key => $value) {
                    $table4->addRow();
                    $table4->addCell(500, $scel3)->addText(($key+1), $sf22, $sp11);
                    $p_name = $value->description_ru.' / '.$value->description_en;
                    $table4->addCell(3000, $scel3)->addText($p_name, $sf22, $sp0);
                    $table4->addCell(1300, $scel3)->addText($value->unit, $sf22, $sp11);
                    $table4->addCell(3000, $scel3)->addText(number_format($value->price, 2, '.', ' '), $sf22, $sp11);
                    $table4->addCell(1300, $scel3)->addText($value->quantity, $sf22, $sp11);
                    $table4->addCell(1800, $scel3)->addText(number_format($value->total_price, 2, '.', ' '), $sf22, $sp11);
                    $total_qty += $value->quantity;
                    $total_price += $value->total_price;
                }
            }

            $table4->addRow();
            $txt1 = $table4->addCell(3000, $scel33);
            $txt1->addText('Контрактная стоимость на условиях '.$model->region_en, $sf3, $sp11);
            $txt1->addText('The contract price under conditions on '.$model->region_ru, $sf3, $sp11);
            $table4->addCell(1300, $scel3)->addText($total_qty, $sf3, $sp11);
            $table4->addCell(3000, $scel3)->addText(number_format($total_price, 2, '.', ' '), $sf3, $sp11);

            /*------------------------ <br> --------------------------*/
            $section->addText('', $sf0, $sp0);

            /*------------------------ Notes -------------------------*/
            Html::addHtml($section, EXW::html($model->note_ru));

            /*------------------------ <br> --------------------------*/
            $section->addText('', $sf1, $sp0);

            Html::addHtml($section, EXW::html($model->note_en));

            /*------------------------ <br> --------------------------*/
            $section->addText('', $sf1, $sp0);
            $section->addText('', $sf1, $sp0);


            $section->addText('Signatures of the Parties: / Подписи сторон:', $sf1, $sp1);
            $section->addText('', $sf0, $sp0);
            $section->addText('', $sf0, $sp0);

            $table5 = $section->addTable($stbl2);
            $table5->addRow();
            $cel3 = $table5->addCell(3000,$scel2);
            $cel3->addText('', $sf0,$sp0);
            $cel3->addText('THE SELLER (Продавец):', $sf1,$sp3);
            $cel3->addText('', $sf0,$sp0);
            $cel3->addText('', $sf0,$sp0);
            $cel3->addText('', $sf0,$sp0);
            $cel3->addText('', $sf0,$sp0);
            $cel3->addText('__________________ '.$model->director_dil_en, $sf1,$sp3);
            $cel3->addText('', $sf0,$sp0);

            $cel4 = $table5->addCell(3000,$scel2);
            $cel4->addText('', $sf0,$sp0);
            $cel4->addText('THE BUYER (Покупатель):', $sf1,$sp3);
            $cel4->addText('', $sf0,$sp0);
            $cel4->addText('', $sf0,$sp0);
            $cel4->addText('', $sf0,$sp0);
            $cel4->addText('', $sf0,$sp0);
            $cel4->addText('__________________ '.$model->director_buy_en, $sf1,$sp3);
            $cel4->addText('', $sf0,$sp0);
			
			/*------------------------ DOWNLOAD -------------------------*/

			$file = 'Contract '.@$model->number->number.'.docx';
			header("Expires: 0");
			header('Content-Type: application/vnd.ms-word');
			header('Content-Disposition: attachment;filename="' . $file);
			header('Cache-Control: max-age=0');
			$objWriter = IOFactory::createWriter($phpWord, 'Word2007');
			$objWriter->save('php://output');
			
		}

	}