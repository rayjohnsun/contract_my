<?php
namespace app\rbac;

class Roles {
  const ADMIN = 'admin';
  const MANAGER = 'manager';
  const ORGANIZATION = 'organization';
  const DILLER = 'diller';

  public static function get($val = null) {
    $arr = [
      self::ADMIN => 'Админ',
      self::MANAGER => 'Менеджер',
      self::ORGANIZATION => 'Производитель',
      self::DILLER => 'Диллер',
    ];
    if (!is_null($val)) {
      if (array_key_exists($val, $arr)) {
        return $arr[$val];
      }
      return null;
    }
    return $arr;
  }

  public static function label($val) {
    $arr = [
      self::ADMIN => '<span class="label label-primary">' . self::get(self::ADMIN) . '</span>',
      self::MANAGER => '<span class="label label-success">' . self::get(self::MANAGER) . '</span>',
      self::ORGANIZATION => '<span class="label label-warning">' . self::get(self::ORGANIZATION) . '</span>',
      self::DILLER => '<span class="label label-info">' . self::get(self::DILLER) . '</span>',
    ];
    if (array_key_exists($val, $arr)) {
      return $arr[$val];
    }
    return '';
  }

  public static function list() {
    return [static::ADMIN, static::MANAGER, static::ORGANIZATION, static::DILLER];
  }
}