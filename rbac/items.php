<?php
return [
  'admin' => [
    'type' => 1,
    'description' => 'Админ',
    'ruleName' => 'allusers',
    'children' => [
      'organization',
      'diller',
      'manageUsers'
    ],
  ],
  'organization' => [
    'type' => 1,
    'description' => 'Вектан',
    'ruleName' => 'allusers',
    'children' => ['table'],
  ],
  'diller' => [
    'type' => 1,
    'description' => 'Латуз',
    'ruleName' => 'allusers',
    'children' => ['table'],
  ],
  'manager' => [
    'type' => 1,
    'description' => 'Менеджер',
    'ruleName' => 'allusers',
    'children' => ['organization', 'diller', 'manageMessages', 'manage_table'],
  ],
  'manageUsers' => [
    'type' => 2,
    'description' => 'Manage users',
    'ruleName' => 'allusers',
  ],
  'manageMessages' => [
    'type' => 2,
    'description' => 'Manage messages',
    'ruleName' => 'allusers',
  ],
  'table' => [
    'type' => 2,
    'description' => 'Table',
    'ruleName' => 'allusers'
  ],
  'manage_table' => [
    'type' => 2,
    'description' => 'Manage Table',
    'ruleName' => 'allusers'
  ]
];
