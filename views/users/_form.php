<?php

use app\rbac\Roles;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div>
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-sm-6">
			<?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?> </div>
		<div class="col-sm-6">			
			<?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
			<?php if ($model->isNewRecord): ?>
				<?= $form->field($model, 'role_id')->dropDownlist(Roles::get(), ['prompt' => 'Select role']) ?>
			<?php endif ?>
		</div>
	</div>
	<?= $form->field($model, 'status')->checkbox() ?>
	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>
