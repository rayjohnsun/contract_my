<?php 

use app\components\Currencies;
use app\components\Helper;
use app\components\POInvoice;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
	
$this->title = 'База данных (Экспорт)';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="full-height">
	<div class="itog_table full-height">
		<div style="position: fixed;right: 15px;top: 60px;z-index: 2;">
			<a href="#" onclick="RJS.slideHideShow('#Filter');return false;">Фильтр</a>
		</div>
		<div id="Filter" style="display: none;position: fixed;top: 85px;left: 0;right: 0;z-index: 1;height: 115px;">
			<div style="background:#adeadd;padding: 0 15px;">
				<?php $form = ActiveForm::begin();?>
					<div style="overflow: auto;">
						<div style="white-space: nowrap;">
							<div class="inline field" id="CheckList">
						    <?php if (!empty($headers)): ?>
						    	<?php foreach ($headers as $column => $row): ?>
						    		<?php 
						    			$visible = $row['visible'];
						    			if ($user_table) {
						    				if (isset($user_table->$column) && $user_table->$column == 1) {
						    					$visible = true;
						    				} else {
						    					$visible = false;
						    				}
						    			}
						    		?>
						    		<div class="ui toggle checkbox mine" style="text-align: center;">
						    			<span style="cursor: default;font-size: 11px;font-weight: bold;"><?=$row['title'] ?>&nbsp;&nbsp;&nbsp;</span>
						    			<input type="hidden" name="MyTables[<?=$column ?>]" value="0">
								      <input type="checkbox" uniquid="<?=$column ?>" name="MyTables[<?=$column ?>]" tabindex="0" class="hidden" onchange="RJS.visibleTableItems(this, '#BigTable')" <?=$visible == true?'checked':'' ?> value="1">
								      <label style="width: 63px;margin: 0 auto;">&nbsp;</label>
								    </div>
						    	<?php endforeach ?>
						    <?php endif ?>
						  </div>
						</div>
					</div>
				  <div class="tar" style="padding: 5px;">
				  	<a href="#" class="btn btn-info" onclick="RJS.selectAll('#CheckList');return false;">Select all</a>&nbsp;&nbsp;&nbsp;
				    <button type="submit" class="btn btn-success">Save</button>
				  </div>
		    <?php ActiveForm::end();?>
			</div>
		</div>
		<br>
		<br>
		<table id="BigTable" class="ui table structured celled striped selectable form compact definition">
			<thead class="full-width">
				<tr>
					<th></th>
					<?php foreach ($headers as $column => $row): ?>
						<?php if ($column == 'column7' || $column == 'column8'): ?>
							<?php if ($column == 'column7'): ?>
								<th uniquid="<?=$column ?>" colspan="2">ИНВОЙС</th>
							<?php endif ?>
						<?php elseif($column == 'column9' || $column == 'column10' || $column == 'column11'): ?>
							<?php if ($column == 'column9'): ?>
								<th uniquid="<?=$column ?>" colspan="3">Дата отгрузки</th>
							<?php endif ?>
						<?php elseif($column == 'column14'): ?>
							<th uniquid="<?=$column ?>">Кол-во (поддон)</th>
						<?php else: ?>
							<th uniquid="<?=$column ?>"></th>
						<?php endif ?>
					<?php endforeach ?>
				</tr>
				<tr class="ikkinchi">
					<th><div>№</div><div>п/п</div></th>
					<?php foreach ($headers as $column => $row): ?>
						<th uniquid="<?=$column ?>">
							<div style="display: inline-block;"><?=$row['title'] ?></div>
							<div class="chbox_data" style="position: relative;">
								<div class="chbox_caret">
									<span class="caret"></span>
								</div>
								<div class="filter_menu">
									<!-- <div style="margin-bottom: 10px;">
										<input type="text" placeholder="Search">
									</div> -->
									<div class="chbox_content" style="padding: 4px;" ch_content>
										<div class="chbox_head">
											<label>
												<input type="checkbox" value="-1" ch_all> Select All
											</label>
										</div>
										<hr style="margin: 2px 0 5px;">
										<ul class="checkbox_body"></ul>
									</div>
								</div>
							</div>
						</th>
					<?php endforeach ?>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($data)): ?>
					<?php $index = 1; ?>
					<?php foreach ($data as $key => $value): ?>
						<?php if (!empty($value->invoices)): ?>
							<?php foreach ($value->invoices as $key2 => $value2): ?>
								<tr>
									<td class="tac"><?=$index ?></td>
									<?php foreach ($headers as $column => $row): ?>										

										<?php if ($column == 'column1'): ?>
											<td uniquid="<?=$column ?>" class="tac">
												<?php 
													$ln = [];
													if ($value->modelName=='DBContract') {
														$ln = ['org_diller/o-d-contract/update', 'id' => $value->id];
													}
													else if ($value->modelName=='DBContract') {
														$ln = ['dil_buyer/d-b-contract/update', 'id' => $value->id];
													}
													else if ($value->modelName=='OBContract') {
														$ln = ['org_buyer/o-b-contract/update', 'id' => $value->id];
													}
												?>
												<?php if ($value->modelName=='ODContract' && $value2->po==POInvoice::YES): ?>
													<input type="text" name="<?=$value2->modelName ?>[<?=$value2->id ?>][contract_no]" class="tac save_this" style="width: 120px;" value="<?=$value2->contract_no ?>">
												<?php else: ?>
													<?php if (!empty($ln)): ?>
														<a href="<?=Url::to($ln) ?>" target="_blank"><?=@$value->number->number ?></a>
													<?php else: ?>
														<span><?=@$value->number->number ?></span>
													<?php endif ?>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column2'): ?>
											<td uniquid="<?=$column ?>" class="tac"><?=$value->new_ndate ?></td>

										<?php elseif($column == 'column3'): ?>
											<td uniquid="<?=$column ?>">
												<?=$value->modelName=='DBContract'?$value->diller->title_ru:$value->organization->title_ru ?>
											</td>
											
										<?php elseif($column == 'column4'): ?>
											<td uniquid="<?=$column ?>"><?=$value->modelName=='ODContract'?$value->diller->title_ru:$value->buyer->title_ru ?></td>

										<?php elseif($column == 'column5'): ?>
											<td uniquid="<?=$column ?>"><?=$value2->consignee->title_ru ?></td>

										<?php elseif($column == 'column6'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tac">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->country_ru ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column7'): ?>
											<td uniquid="<?=$column ?>" class="tac">
												<?php if ($value2->modelName == 'ODInvoice'): ?>
													<a href="<?=Url::to(['org_diller/o-d-invoice/update', 'id' => $value2->id]) ?>" target="_blank"><?=$value2->number->number ?></a>
												<?php else: ?>
													<a href="<?=Url::to([$value2->modelName=='DBInvoice'?'dil_buyer/d-b-invoice/update':'org_buyer/o-b-invoice/update', 'id' => $value2->id]) ?>" target="_blank"><?=$value2->number->number ?></a>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column8'): ?>
											<td uniquid="<?=$column ?>" class="tac"><?=$value2->new_ndate ?></td>

										<?php elseif($column == 'column9'): ?>
											<td uniquid="<?=$column ?>" class="pa-3">
												<input type="text" name="<?=$value2->modelName ?>[<?=$value2->id ?>][den]" class="tac save_this" style="width: 50px;" value="<?=$value2->den ?>">
											</td>

										<?php elseif($column == 'column10'): ?>
											<td uniquid="<?=$column ?>" class="pa-3">
												<input type="text" name="<?=$value2->modelName ?>[<?=$value2->id ?>][mesyats]" class="tac save_this" style="width: 100px;" value="<?=$value2->mesyats ?>">
											</td>

										<?php elseif($column == 'column11'): ?>
											<td uniquid="<?=$column ?>" class="pa-3">
												<input type="text" name="<?=$value2->modelName ?>[<?=$value2->id ?>][god]" class="tac save_this" style="width: 70px;" value="<?=$value2->god ?>">
											</td>

										<?php elseif($column == 'column12'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->title_ru ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td"><code>no products</code></div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column13'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->container ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td"><code>no PL container</code></div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column14'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tac">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<div role="td"><?=$value5->poddon ?></div>
													<?php endforeach ?>
												<?php else: ?>
												<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column15'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tac">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->unit ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column16'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tar">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=number_format($value6->quantity, 0, '', ' ') ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column17'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tar">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=number_format($value6->price, 2, '.', ' ') ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column18'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tar">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=number_format($value6->total_price, 2, '.', ' ') ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column19'): ?>
											<td uniquid="<?=$column ?>" class="tac"><?=Currencies::get($value->currency) ?></td>
										
										<?php elseif($column == 'column20'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td">
																	<input type="text" name="<?=$value6->modelName ?>[<?=$value6->id ?>][t_company]" class="tac save_this inp" style="width: 150px;" value="<?=$value6->t_company ?>">
																</div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>
										
										<?php elseif($column == 'column21'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td">
																	<input type="text" name="<?=$value6->modelName ?>[<?=$value6->id ?>][t_price]" class="tac save_this inp" sum_format="true" style="width: 150px;" value="<?=number_format($value6->t_price, 2, '.', ' ') ?>">
																</div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>
										
										<?php elseif($column == 'column22'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=number_format($value6->t_price/$value6->quantity, 2, '.', ' ') ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>
										
										<?php elseif($column == 'column23'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=number_format(($value6->price - $value6->t_price/$value6->quantity), 2, '.', ' ') ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>
										
										<?php elseif($column == 'column24'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=number_format(($value6->quantity * ($value6->price - $value6->t_price/$value6->quantity)), 2, '.', ' ') ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column25'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td">
																	<input type="text" name="<?=$value6->modelName ?>[<?=$value6->id ?>][sebestoimost]" class="tac save_this inp" sum_format="true" style="width: 150px;" value="<?=$value6->sebestoimost ?>">
																</div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif(in_array($column, ['column25', 'column26', 'column27', 'column28', 'column29', 'column30'])): ?>
											<td uniquid="<?=$column ?>">&nbsp;</td>

										<?php elseif($column == 'column31'): ?>
											<td uniquid="<?=$column ?>">
												<input type="text" name="<?=$value2->modelName ?>[<?=$value2->id ?>][primechanie]" class="tac save_this inp" style="width: 120px;" value="<?=$value2->primechanie ?>">
											</td>

										<?php endif ?>										
									<?php endforeach ?>
								</tr>
								<?php $index++ ?>
							<?php endforeach ?>
						<?php else: ?>
							<tr>
								<td class="tac"><?=$index ?></td>
								<?php foreach ($headers as $column => $row): ?>
									
									<?php if ($column == 'column1'): ?>
										<td uniquid="<?=$column ?>" class="tac">
											<?php 
												$ln = [];
												if ($value->modelName=='ODContract') {
													$ln = ['org_diller/o-d-contract/update', 'id' => $value->id];
												}
												else if ($value->modelName=='DBContract') {
													$ln = ['dil_buyer/d-b-contract/update', 'id' => $value->id];
												}
												else if ($value->modelName=='OBContract') {
													$ln = ['org_buyer/o-b-contract/update', 'id' => $value->id];
												}
											?>
											<a href="<?=Url::to($ln) ?>" target="_blank"><?=$value->number->number ?></a>
										</td>

									<?php elseif($column == 'column2'): ?>
										<td uniquid="<?=$column ?>" class="tac"><?=$value->new_ndate ?></td>

									<?php elseif($column == 'column3'): ?>
										<td uniquid="<?=$column ?>">
											<?=$value->modelName=='DBContract'?$value->diller->title_ru:$value->organization->title_ru ?>
										</td>
										
									<?php elseif($column == 'column4'): ?>
										<td uniquid="<?=$column ?>"><?=$value->modelName=='ODContract'?$value->diller->title_ru:$value->buyer->title_ru ?></td>
										
									<?php elseif($column == 'column5'): ?>
										<td uniquid="<?=$column ?>"></td>

									<?php elseif($column == 'column6'): ?>
										<td uniquid="<?=$column ?>">&nbsp;</td>
										
									<?php elseif($column == 'column7'): ?>
										<td uniquid="<?=$column ?>"><code>not created</code></td>

									<?php elseif(in_array($column, ['column8', 'column9', 'column10', 'column11'])): ?>
										<td uniquid="<?=$column ?>">&nbsp;</td>

									<?php elseif($column == 'column12'): ?>
										<td uniquid="<?=$column ?>"><code>no products</code></td>
										
									<?php elseif($column == 'column13'): ?>
										<td uniquid="<?=$column ?>"><code>no PL container</code></td>

									<?php elseif(in_array($column, ['column14', 'column15'])): ?>
										<td uniquid="<?=$column ?>">&nbsp;</td>
										
									<?php elseif($column == 'column16'): ?>
										<td uniquid="<?=$column ?>" class="tar"><code>0</code></td>
										
									<?php elseif(in_array($column, ['column17', 'column18', 'column19', 'column20', 'column21', 'column22', 'column23', 'column24', 'column25', 'column26', 'column27', 'column28', 'column29', 'column30', 'column31'])): ?>
										<td uniquid="<?=$column ?>" class="tar">&nbsp;</td>
									
									<?php endif ?>
								<?php endforeach ?>
							</tr>
							<?php $index++ ?>
						<?php endif ?>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>
		<div id="Abcd"></div>
	</div>
</div>
<?php 
	$save_url = Url::to(['save']);
	$this->registerJs('
		var url = "'.$save_url.'"
		$("input.save_this").blur(function(){
			var inp = $(this)
			RJS.post(inp, url).then(res => {
				if("data" in res && "value" in res.data){
					if(inp.attr("sum_format")){
						inp.val(TBL_TV.formatPrice(res.data.value, 0, true))
					} else {
						inp.val(res.data.value)
					}
				}
			})
		})
		$("input.save_this[nkey]").keyup(function(){
			var val = $(this).val();
			var key = $(this).attr("nkey")
			if(key){
				$("input.save_this[nkey=\'"+key+"\']").val(val)
				console.log(val);
			}
		})
		$(".ui.checkbox.mine").checkbox();
		$(".ui.checkbox.mine input[type=\'checkbox\']").trigger("change")
		
		MyTable.id = "#BigTable"
		MyTable.loadFilters()
	');
?>