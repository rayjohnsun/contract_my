<?php 

use app\components\Currencies;
use app\components\Helper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
	
$this->title = 'База данных (Экспорт)';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="full-height">
	<div class="itog_table full-height">
		<div style="position: fixed;right: 15px;top: 60px;z-index: 2;">
			<a href="#" onclick="RJS.slideHideShow('#Filter');return false;">Фильтр</a>
		</div>
		<div id="Filter" style="display: none;position: fixed;top: 85px;left: 0;right: 0;z-index: 1;height: 115px;">
			<div style="background:#adeadd;padding: 0 15px;">
				<?php $form = ActiveForm::begin();?>
					<div style="overflow: auto;">
						<div style="white-space: nowrap;">
							<div class="inline field" id="CheckList">
						    <?php if (!empty($headers)): ?>
						    	<?php foreach ($headers as $column => $row): ?>
						    		<?php 
						    			$visible = $row['visible'];
						    			if ($user_table) {
						    				if (isset($user_table->$column) && $user_table->$column == 1) {
						    					$visible = true;
						    				} else {
						    					$visible = false;
						    				}
						    			}
						    		?>
						    		<div class="ui toggle checkbox mine" style="text-align: center;">
						    			<span style="cursor: default;font-size: 11px;font-weight: bold;"><?=$row['title'] ?>&nbsp;&nbsp;&nbsp;</span>
						    			<input type="hidden" name="MyTables[<?=$column ?>]" value="0">
								      <input type="checkbox" uniquid="<?=$column ?>" name="MyTables[<?=$column ?>]" tabindex="0" class="hidden" onchange="RJS.visibleTableItems(this, '#BigTable')" <?=$visible == true?'checked':'' ?> value="1">
								      <label style="width: 63px;margin: 0 auto;">&nbsp;</label>
								    </div>
						    	<?php endforeach ?>
						    <?php endif ?>
						  </div>
						</div>
					</div>
				  <div class="tar" style="padding: 5px;">
				  	<a href="#" class="btn btn-info" onclick="RJS.selectAll('#CheckList');return false;">Select all</a>&nbsp;&nbsp;&nbsp;
				    <button type="submit" class="btn btn-success">Save</button>
				  </div>
		    <?php ActiveForm::end();?>
			</div>
		</div>
		<br>
		<br>
		<table id="BigTable" class="ui table structured celled striped selectable form compact definition">
			<thead class="full-width">
				<tr>
					<th></th>
					<th colspan="2">ИНВОЙС</th>
					<th colspan="3">Дата отгрузки</th>
					<th colspan="29"></th>
				</tr>
				<tr>
					<th><div>№</div><div>п/п</div></th>
					<?php foreach ($headers as $column => $row): ?>
						<th uniquid="<?=$column ?>"><?=$row['title'] ?></th>
					<?php endforeach ?>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($data)): ?>
					<?php $index = 1; ?>
					<?php foreach ($data as $key => $value): ?>
						<?php if (!empty($value->invoices)): ?>
							<?php foreach ($value->invoices as $key2 => $value2): ?>
								<tr>
									<td class="tac"><?=$index ?></td>
									<?php foreach ($headers as $column => $row): ?>

										<?php if ($column == 'column1'): ?>
											<td uniquid="<?=$column ?>" class="tac">
												<a href="<?=Url::to([$value2->modelName=='ODInvoice'?'org_diller/o-d-invoice/update':'org_buyer/o-b-invoice/update', 'id' => $value2->id]) ?>" target="_blank"><?=$value2->number->number ?></a>
											</td>

										<?php elseif($column == 'column2'): ?>
											<td uniquid="<?=$column ?>" class="tac"><?=$value2->new_ndate ?></td>

										<?php elseif($column == 'column3'): ?>
											<td uniquid="<?=$column ?>" class="pa-3">
												<input type="text" name="<?=$value2->modelName ?>[<?=$value2->id ?>][den]" class="tac save_this" style="width: 50px;" value="<?=$value2->den ?>">
											</td>
											
										<?php elseif($column == 'column4'): ?>
											<td uniquid="<?=$column ?>" class="pa-3">
												<input type="text" name="<?=$value2->modelName ?>[<?=$value2->id ?>][mesyats]" class="tac save_this" style="width: 100px;" value="<?=$value2->mesyats ?>">
											</td>

										<?php elseif($column == 'column5'): ?>
											<td uniquid="<?=$column ?>" class="pa-3">
												<input type="text" name="<?=$value2->modelName ?>[<?=$value2->id ?>][god]" class="tac save_this" style="width: 70px;" value="<?=$value2->god ?>">
											</td>

										<?php elseif($column == 'column6'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->title_ru ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td"><code>no poducts</code></div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column7'): ?>
											<td uniquid="<?=$column ?>" class="px-0">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->container ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td"><code>no PL container</code></div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column8'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tac">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<div role="td"><?=$value5->poddon ?></div>
													<?php endforeach ?>
												<?php else: ?>
												<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column9'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tac">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->unit ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column10'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tac">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->quantity ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column11'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tar">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=number_format($value6->price, 2, '.', ' ') ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column12'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tar">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=number_format($value6->total_price, 2, '.', ' ') ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column13'): ?>
											<td uniquid="<?=$column ?>" class="tac"><?=Currencies::get($value->currency) ?></td>

										<?php elseif($column == 'column14'): ?>
											<td uniquid="<?=$column ?>" class="px-0 tac">
												<?php if (!empty($value2->packinglists)): ?>
													<?php foreach ($value2->packinglists as $key5 => $value5): ?>
														<?php if (!empty($value5->products)): ?>
															<?php foreach ($value5->products as $key6 => $value6): ?>
																<div role="td"><?=$value6->country_ru ?></div>
															<?php endforeach ?>
														<?php else: ?>
															<div role="td">&nbsp;</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td">&nbsp;</div>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column15'): ?>
											<td uniquid="<?=$column ?>"><?=$value2->consignee->title_ru ?></td>

										<?php elseif($column == 'column16'): ?>
											<td uniquid="<?=$column ?>" class="tac">
												<a href="<?=Url::to([$value->modelName=='ODContract'?'org_diller/o-d-contract/update':'org_buyer/o-b-contract/update', 'id' => $value->id]) ?>" target="_blank"><?=$value->number->number ?></a>
											</td>

										<?php elseif($column == 'column17'): ?>
											<td uniquid="<?=$column ?>"><?=($value->modelName == 'ODContract')?$value->diller->title_ru:$value->buyer->title_ru ?></td>

										<?php elseif($column == 'column18'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && $value11->dBContract->buyer): ?>
																<?=Helper::brs($value11->dBContract->invoices) ?>
																<div role="td"><?=$value11->dBContract->buyer->title_ru ?></div>
															<?php endif ?>
														<?php endforeach ?>
													<?php else: ?>
														<div role="td"><code>no contract(D<small>&</small>B)</code></div>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column19'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract): ?>
																<?=Helper::brs($value11->dBContract->invoices) ?>
																<div role="td">
																	<a href="<?=Url::to(['dil_buyer/d-b-contract/update', 'id' => $value11->id]) ?>" target="_blank"><?=$value11->dBContract->number?$value11->dBContract->number->number:'NONE' ?></a>
																</div>
															<?php endif ?>
														<?php endforeach ?>
													<?php else: ?>
														<code>No D&B Contract</code>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column20'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract): ?>
																<?=Helper::brs($value11->dBContract->invoices) ?>
																<div role="td"><?=$value11->dBContract->new_ndate ?></div>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column21'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<?php if ($value12->number): ?>
																		<div role="td">
																			<a href="<?=Url::to(['dil_buyer/d-b-invoice/update', 'id' => $value12->id]) ?>" target="_blank"><small><?=$value12->number->number ?></small></a>
																		</div>
																	<?php else: ?>
																		<div role="td"><code>empty</code></div>
																	<?php endif ?>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column22'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<div role="td"><small><?=$value12->new_ndate ?></small></div>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column23'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?php if (!empty($value12->products)): ?>
																		<?php foreach ($value12->products as $key13 => $value13): ?>
																			<div role="td"><small><?=$value13->title_ru ?></small></div>
																		<?php endforeach ?>
																	<?php endif ?>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column24'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?php if (!empty($value12->products)): ?>
																		<?php foreach ($value12->products as $key13 => $value13): ?>
																			<div role="td"><small><?=$value13->unit ?></small></div>
																		<?php endforeach ?>
																	<?php endif ?>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column25'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat tac">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?php if (!empty($value12->products)): ?>
																		<?php foreach ($value12->products as $key13 => $value13): ?>
																			<div role="td"><small><?=$value13->quantity ?></small></div>
																		<?php endforeach ?>
																	<?php endif ?>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column26'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat tar">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?php if (!empty($value12->products)): ?>
																		<?php foreach ($value12->products as $key13 => $value13): ?>
																			<div role="td"><small><?=number_format($value13->price, 2, '.', ' ') ?></small></div>
																		<?php endforeach ?>
																	<?php endif ?>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column27'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat tar">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?php if (!empty($value12->products)): ?>
																		<?php foreach ($value12->products as $key13 => $value13): ?>
																			<div role="td"><small><?=number_format($value13->total_price, 2, '.', ' ') ?></small></div>
																		<?php endforeach ?>
																	<?php endif ?>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column28'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<div role="td">
																		<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_ligistic]" class="tac save_this inp" value="<?=$value12->do_ligistic ?>" nkey="do_ligistic_<?=$value12->id ?>">
																	</div>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column29'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<div role="td">
																		<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_price]" class="tac save_this inp" value="<?=$value12->do_price ?>" nkey="do_price_<?=$value12->id ?>">
																	</div>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column30'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<div role="td">
																		<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_blno]" class="tac save_this inp" value="<?=$value12->do_blno ?>" nkey="do_blno_<?=$value12->id ?>">
																	</div>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column31'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<div role="td">
																		<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_date]" class="tac save_this inp" value="<?=$value12->do_date ?>" nkey="do_date_<?=$value12->id ?>">
																	</div>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column32'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<div role="td">
																		<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_pay]" class="tac save_this inp" value="<?=$value12->do_pay ?>" nkey="do_pay_<?=$value12->id ?>">
																	</div>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column33'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<div role="td">
																		<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_delivery_date]" class="tac save_this inp" value="<?=$value12->do_delivery_date ?>" nkey="do_delivery_date_<?=$value12->id ?>">
																	</div>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php elseif($column == 'column34'): ?>
											<td uniquid="<?=$column ?>" class="px-0 vat">
												<?php if ($value->modelName == 'ODContract'): ?>
													<?php if (!empty($value->od_db)): ?>
														<?php foreach ($value->od_db as $key11 => $value11): ?>
															<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
																<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																	<?=Helper::brs($value12->products, false) ?>
																	<div role="td">
																		<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_return_date]" class="tac save_this inp" value="<?=$value12->do_return_date ?>" nkey="do_return_date_<?=$value12->id ?>">
																	</div>
																<?php endforeach ?>
															<?php endif ?>
														<?php endforeach ?>
													<?php endif ?>
												<?php else: ?>
													<span>&nbsp;</span>
												<?php endif ?>
											</td>

										<?php endif ?>										
									<?php endforeach ?>
								</tr>
								<?php $index++ ?>
							<?php endforeach ?>
						<?php else: ?>
							<tr>
								<td class="tac"><?=$index ?></td>
								<?php foreach ($headers as $column => $row): ?>

									<?php if (in_array($column, ['column1'])): ?>
										<td uniquid="<?=$column ?>" class="tac"><code>not created</code></td>

									<?php elseif(in_array($column, ['column2', 'column3', 'column4', 'column5'])): ?>
										<td uniquid="<?=$column ?>">&nbsp;</td>

									<?php elseif(in_array($column, ['column6'])): ?>
										<td uniquid="<?=$column ?>"><code>no poducts</code></td>

									<?php elseif(in_array($column, ['column7'])): ?>
										<td uniquid="<?=$column ?>"><code>no PL container</code></td>

									<?php elseif(in_array($column, ['column8', 'column9', 'column10', 'column11', 'column12', 'column13', 'column14', 'column15'])): ?>
										<td uniquid="<?=$column ?>">&nbsp;</td>

									<?php elseif(in_array($column, ['column16'])): ?>
										<td uniquid="<?=$column ?>" class="tac">
											<a href="<?=Url::to([$value->modelName=='ODContract'?'org_diller/o-d-contract/update':'org_buyer/o-b-contract/update', 'id' => $value->id]) ?>" target="_blank"><?=$value->number->number ?></a>
										</td>

									<?php elseif(in_array($column, ['column17'])): ?>
										<td uniquid="<?=$column ?>"><?=($value->modelName == 'ODContract')?$value->diller->title_ru:$value->buyer->title_ru ?></td>

									<?php elseif(in_array($column, ['column18'])): ?>
										<td uniquid="<?=$column ?>" class="px-0">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && $value11->dBContract->buyer): ?>
															<?=Helper::brs($value11->dBContract->invoices) ?>
															<div role="td"><?=$value11->dBContract->buyer->title_ru ?></div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<div role="td"><code>no contract(D<small>&</small>B)</code></div>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column19'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract): ?>
															<?=Helper::brs($value11->dBContract->invoices) ?>
															<div role="td">
																<a href="<?=Url::to(['dil_buyer/d-b-contract/update', 'id' => $value11->id]) ?>" target="_blank"><?=$value11->dBContract->number?$value11->dBContract->number->number:'NONE' ?></a>
															</div>
														<?php endif ?>
													<?php endforeach ?>
												<?php else: ?>
													<code>No D&B Contract</code>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column20'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract): ?>
															<?=Helper::brs($value11->dBContract->invoices) ?>
															<div role="td"><?=$value11->dBContract->new_ndate ?></div>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column21'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<?php if ($value12->number): ?>
																	<div role="td">
																		<a href="<?=Url::to(['dil_buyer/d-b-invoice/update', 'id' => $value12->id]) ?>" target="_blank"><small><?=$value12->number->number ?></small></a>
																	</div>
																<?php else: ?>
																	<div role="td"><code>empty</code></div>
																<?php endif ?>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column22'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<div role="td"><small><?=$value12->new_ndate ?></small></div>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column23'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?php if (!empty($value12->products)): ?>
																	<?php foreach ($value12->products as $key13 => $value13): ?>
																		<div role="td"><small><?=$value13->title_ru ?></small></div>
																	<?php endforeach ?>
																<?php endif ?>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column24'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?php if (!empty($value12->products)): ?>
																	<?php foreach ($value12->products as $key13 => $value13): ?>
																		<div role="td"><small><?=$value13->unit ?></small></div>
																	<?php endforeach ?>
																<?php endif ?>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column25'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat tac">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?php if (!empty($value12->products)): ?>
																	<?php foreach ($value12->products as $key13 => $value13): ?>
																		<div role="td"><small><?=$value13->quantity ?></small></div>
																	<?php endforeach ?>
																<?php endif ?>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column26'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat tar">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?php if (!empty($value12->products)): ?>
																	<?php foreach ($value12->products as $key13 => $value13): ?>
																		<div role="td"><small><?=number_format($value13->price, 2, '.', ' ') ?></small></div>
																	<?php endforeach ?>
																<?php endif ?>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column27'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat tar">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?php if (!empty($value12->products)): ?>
																	<?php foreach ($value12->products as $key13 => $value13): ?>
																		<div role="td"><small><?=number_format($value13->total_price, 2, '.', ' ') ?></small></div>
																	<?php endforeach ?>
																<?php endif ?>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column28'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<div role="td">
																	<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_ligistic]" class="tac save_this inp" value="<?=$value12->do_ligistic ?>" nkey="do_ligistic_<?=$value12->id ?>">
																</div>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column29'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<div role="td">
																	<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_price]" class="tac save_this inp" value="<?=$value12->do_price ?>" nkey="do_price_<?=$value12->id ?>">
																</div>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column30'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<div role="td">
																	<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_blno]" class="tac save_this inp" value="<?=$value12->do_blno ?>" nkey="do_blno_<?=$value12->id ?>">
																</div>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column31'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<div role="td">
																	<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_date]" class="tac save_this inp" value="<?=$value12->do_date ?>" nkey="do_date_<?=$value12->id ?>">
																</div>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column32'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<div role="td">
																	<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_pay]" class="tac save_this inp" value="<?=$value12->do_pay ?>" nkey="do_pay_<?=$value12->id ?>">
																</div>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column33'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<div role="td">
																	<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_delivery_date]" class="tac save_this inp" value="<?=$value12->do_delivery_date ?>" nkey="do_delivery_date_<?=$value12->id ?>">
																</div>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php elseif($column == 'column34'): ?>
										<td uniquid="<?=$column ?>" class="px-0 vat">
											<?php if ($value->modelName == 'ODContract'): ?>
												<?php if (!empty($value->od_db)): ?>
													<?php foreach ($value->od_db as $key11 => $value11): ?>
														<?php if ($value11->dBContract && !empty($value11->dBContract->invoices)): ?>
															<?php foreach ($value11->dBContract->invoices as $key12 => $value12): ?>
																<?=Helper::brs($value12->products, false) ?>
																<div role="td">
																	<input type="text" name="<?=$value12->modelName ?>[<?=$value12->id ?>][do_return_date]" class="tac save_this inp" value="<?=$value12->do_return_date ?>" nkey="do_return_date_<?=$value12->id ?>">
																</div>
															<?php endforeach ?>
														<?php endif ?>
													<?php endforeach ?>
												<?php endif ?>
											<?php else: ?>
												<span>&nbsp;</span>
											<?php endif ?>
										</td>

									<?php endif ?>
								<?php endforeach ?>
							</tr>
							<?php $index++ ?>
						<?php endif ?>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>
	</div>
</div>
<?php 
	$save_url = Url::to(['save']);
	$this->registerJs('
		var url = "'.$save_url.'"
		$("input.save_this").blur(function(){
			var inp = $(this)
			RJS.post(inp, url)
		})
		$("input.save_this[nkey]").keyup(function(){
			var val = $(this).val();
			var key = $(this).attr("nkey")
			if(key){
				$("input.save_this[nkey=\'"+key+"\']").val(val)
				console.log(val);
			}
		})
		$(".ui.checkbox.mine").checkbox();
		$(".ui.checkbox.mine input[type=\'checkbox\']").trigger("change")
	');
?>