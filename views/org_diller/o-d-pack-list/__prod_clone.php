<table class="dtable" id="dtableClone">
	<tbody>
		<tr>
			<td class="numr">{{index}}</td>
			<td>
				<input type="text" onkeyup="TBL_TV.runCounter(this, true)" class="tac nred" name="Products[{{index}}][container]" placeholder="Container No." value="" from-attr="p_container" disabled>
			</td>
			<td class="descr">
				<div class="ul_dropdown" role="product">
					<div class="ul_head">Select</div>
					<ul class="ul_content"></ul>
					<input class="ul_input" type="hidden" name="Products[{{index}}][product_id]" value="">
				</div>
			</td>
			<td class="coder">
				<div from-attr="p_code">&nbsp;</div>
				<!-- <div from-attr="p_country">&nbsp;</div> -->
			</td>
			<td class="quanr nred">
				<input type="text" from-attr="p_qty" onkeyup="TBL_TV.runCounter(this)" name="Products[{{index}}][quantity]" placeholder="quantity" value="1" qty-block disabled>
			</td>
			<td class="netwr nred">
				<input type="text" onkeyup="TBL_TV.runCounter(this)" name="Products[{{index}}][net_weight]" placeholder="NET WEIGHT" value="0" from-attr="p_net_w" netw-block disabled>
			</td>
			<td class="growr nred">
				<input type="text" onkeyup="TBL_TV.runCounter(this)" name="Products[{{index}}][gross_weight]" placeholder="GROSS WEIGHT" value="0" from-attr="p_gross_w" grossw-block disabled>
				<div class="table-product">
            <div class="tr-minus" onclick="TBL_TV.removeTr($(this).parent().parent().parent())"></div>
        </div>
			</td>
		</tr>
	</tbody>
</table>