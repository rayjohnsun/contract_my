<?php 

use app\components\Currencies;
use yii\helpers\Url;

?>
<table class="btable">
	<thead>
		<tr>
			<th style="padding: 0 15px;" class="w50">
				<p class="tal">APPENDIX №1</p>
				<p class="tac">to CONTRACT № <?=@$model->number->number ?> dated <span class="editable_ndate_en"><?=$model->date_en ?></span></p>
			</th>
			<th style="padding: 0 15px;" class="w50">
				<p class="tar">ПРИЛОЖЕНИЕ №1</p>
				<p class="tac">к КОНТРАКТУ № <?=@$model->number->number ?> от <span class="editable_ndate_ru"><?=$model->date_ru ?></span></p>
			</th>
		</tr>
	</thead>
</table>
<br>
<br>
<div style="padding: 0 25px;">
	<table class="etable">
		<thead>
			<tr>
				<th class="tac">Specification of the Goods / Спецификация Товара</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="2">
					<br>
					<p class="tar fsi" style="padding-right: 15px;">Цены указаны в Евро, НДС 0% / Price indicate in USD, only VAT 0%</p>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="dtable" id="dtableContent">
		<thead>
			<tr>
				<th class="numr">№</th>
				<th class="descr">Наименование товара / Name of the product</th>
				<th class="unitr">Ед. из. / Unit</th>
				<th class="pricr"><?=Currencies::textPrice($model->currency) ?></th>
				<th class="quanr">Количество / Quantity</th>
				<th class="totar">
					<div><?=Currencies::textTotalAmount($model->currency) ?></div>
					<div class="table-product">
						<div class="tr-refresh" onclick="TBL_TV.getAll()" title="Обнавить список продуктов"></div>
					</div>
				</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		<tfoot>
      <tr>
    		<th colspan="4">
    			<p>Контрактная стоимость на условиях <input name="ODContract[region_en]" type="text" style="text-align: left;padding-left: 5px;" disabled placeholder="Region" value="<?=$model->region_en ?>"></p>
    			<p>The contract price under conditions on <input name="ODContract[region_ru]" type="text" style="text-align: left;padding-left: 5px;" disabled placeholder="Region" value="<?=$model->region_ru ?>"></p>
    		</th>
    		<th class="t-quanr"><span total-qty-block>0</span></th>
        <th class="t-pricr">
        	<input type="hidden" total-price-block value="0" id="number-input" currency="<?=$model->currency ?>">
          <span total-price-block>0</span>
          <div class="table-product">
              <div class="tr-plus" onclick="TBL_TV.addTr()" title="Добавить продукт"></div>
          </div>
        </th>
    	</tr>
    </tfoot>
	</table>
</div>
<br>
<br>
<?php 
	$get_url = Url::to(['get-products', 'id' => $model->id]);
	$add_url = Url::to(['add-products', 'id' => $model->id]);
	$save_url = null; //Url::to(['save-products', 'id' => $model->id]);
	$balance_url = Url::to(['balance-products', 'id' => $model->id]);

	$this->registerJs('

		TBL_TV.opt = {
			table: "#dtableContent",
			table_clone: "#dtableClone",
			getUrl: "'.$get_url.'",
			addUrl: "'.$add_url.'",
			saveUrl: "'.$save_url.'",
			balanceUrl: "'.$balance_url.'"
		};

		TBL_TV.getAll()

		$("#number-input").trigger("change");

	');
?>