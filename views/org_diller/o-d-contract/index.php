<?php

use app\components\Attribute;
use app\components\Currencies;
use app\components\Podtverjdeno;
use app\components\Zakryto;
use app\components\Zapolnen;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = '(VT<small>&</small>D) Контракт';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];

?>
<div>
  <h3><?=$this->title?></h3>
  <div class="row">
    <div class="col-sm-12">
      <button type="button" class="ui primary basic button" data-toggle="modal" data-target="#Modal1">Создать новый</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?=Html::a('Все <b>(VT<small>&</small>D)</b> Инвойс', ['org_diller/o-d-invoice'], ['class' => 'ui teal basic button'])?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?=Html::a('Все <b>(VT<small>&</small>D)</b> Упаковочный лист', ['org_diller/o-d-pack-list'], ['class' => 'ui teal basic button'])?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?=Html::a('Все <b>(D<small>&</small>B)</b> Контракты', ['dil_buyer/d-b-contract'], ['class' => 'ui pink basic button'])?>
    </div>
  </div>
  <br>
  <div>
  <?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'summary' => false,
  'tableOptions' => ['class' => 'ui celled striped selectable table form'],
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    [
      'attribute' => 'nnumber',
      'value' => function ($m) {return Attribute::number($m->number ? $m->number->number : '');},
      'format' => 'raw',
    ],
    'adres_ru',
    'date_ru',
    [
      'attribute' => 'total_lprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_total_lprice', Podtverjdeno::get(), ['class' => '', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::lprice($m->total_lprice, $m->zapolnen, $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'total_nprice',
      'value' => function ($m) {return Attribute::nprice($m->total_nprice, $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'receive_sum',
      'value' => function ($m) {return Attribute::receiveSum($m->receive_sum, null, $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'dbcon_prod_nprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_dbcon_prod_nprice', Zakryto::getForVT(), ['class' => 'form-control', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::conBalance(($m->dbcon_prod_nprice - $m->dbcon_prod_lprice), $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'inv_prod_nprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_inv_prod_nprice', Zakryto::getForVT(), ['class' => 'form-control', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::invBalance(($m->inv_prod_nprice - $m->inv_prod_lprice), $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'com_inv_prod_nprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_com_inv_prod_nprice', Zakryto::getForVT(), ['class' => 'form-control', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::invBalance(($m->com_inv_prod_nprice - $m->com_inv_prod_lprice), $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'created_at',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);},
      'format' => 'raw',
    ],

    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '<div class="b_style">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div><div class="b_style">{invoices}</div><div class="b_style">{cominvoices}</div><div class="b_style">{contracts}</div>',
      'buttons' => [
        'invoices' => function ($url, $model, $key) {
          $invoices = $model->invoices;
          $cnt = count($invoices);
          $calas = $cnt > 0 ? 'positive' : 'negative';
          $invoice_ids = ArrayHelper::map($invoices, 'id', 'id');
          if (empty($invoice_ids)) {
            $invoice_ids = [0];
          }
          return Html::a("Инвойс ({$cnt})", ['org_diller/o-d-invoice', 'ODInvoiceSearch[invoice_ids]' => $invoice_ids], ['class' => 'mini ui button ' . $calas]);
        },
        'cominvoices' => function ($url, $model, $key) {
          $cominvoices = $model->cominvoices;
          $cnt = count($cominvoices);
          $calas = $cnt > 0 ? 'positive' : 'negative';
          $invoice_ids = ArrayHelper::map($cominvoices, 'id', 'id');
          if (empty($invoice_ids)) {
            $invoice_ids = [0];
          }
          return Html::a("Ком. Инвойс ({$cnt})", ['org_diller/o-d-com-invoice', 'ODComInvoiceSearch[invoice_ids]' => $invoice_ids], ['class' => 'mini ui button ' . $calas]);
        },
        'contracts' => function ($url, $model, $key) {
          $cnt = count($model->od_db);
          $calas = $cnt > 0 ? 'positive' : 'negative';
          $db_ids = ArrayHelper::map($model->od_db, 'db_id', 'od_id');
          if (empty($db_ids)) {
            $db_ids = [0];
          }
          return Html::a("Контракт ({$cnt})", ['dil_buyer/d-b-contract', 'DBContractSearch[db_ids]' => $db_ids], ['class' => 'mini ui button ' . $calas]);
        },
      ],
    ],
  ],
]);?>
  </div>
</div>
<?php Modal::begin([
  'header' => '<h4>Выберите организацию и Диллер</h4>',
  'options' => [
    'id' => 'Modal1',
  ],
  'footer' => Html::a('Создать', ['create'], ['class' => 'btn btn-success', 'id' => 'CreateBtn']),
]);?>
<div>
  <?=Html::dropDownList('od_id', null, $organizations, ['class' => 'form-control', 'id' => 'OdId'])?>
  <br>
  <?=Html::dropDownList('db_id', null, $dillers, ['class' => 'form-control', 'id' => 'DbId'])?>
  <br>
  <?=Html::dropDownList('cy', null, Currencies::get(), ['class' => 'form-control', 'id' => 'Cy'])?>
</div>
<?php Modal::end();?>
<?php $this->registerJs('
  $("#CreateBtn").click(function(e){
    var href = $(this).attr("href");
    var od_id = $("#OdId").val();
    var db_id = $("#DbId").val();
    var cy = $("#Cy").val();
    var new_href = href + "?od_id=" + od_id + "&db_id=" + db_id + "&cy=" + cy;
    $(this).attr("href", new_href);
    return;
  });
')?>