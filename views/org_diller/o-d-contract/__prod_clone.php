<table class="dtable" id="dtableClone">
	<tbody>
		<tr>
			<td class="numr">{{index}}</td>
			<td class="descr">
				<div class="ul_dropdown" role="product">
					<div class="ul_head">Select</div>
					<ul class="ul_content"></ul>
					<input class="ul_input" type="hidden" name="Products[{{index}}][product_id]" value="">
				</div>
			</td>
			<td class="unitr"><span from-attr="p_unit">&nbsp;</span></td>
			<td class="pricr">
				<input type="text" from-attr="p_price" onkeyup="TBL_TV.runCounter(this)" name="Products[{{index}}][price]" placeholder="price" value="0" price-block disabled>
			</td>
			<td class="quanr">
				<input type="text" from-attr="p_qty" onkeyup="TBL_TV.runCounter(this)" name="Products[{{index}}][quantity]" placeholder="quantity" value="1" qty-block disabled>
			</td>
			<td class="totar">
				<span price-qty-block>&nbsp;</span>
				<div class="table-product">
            <div class="tr-minus" onclick="TBL_TV.removeTr($(this).parent().parent().parent())"></div>
        </div>
			</td>
		</tr>
	</tbody>
</table>