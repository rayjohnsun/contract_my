<?php

use app\components\Helper;
use app\components\Status;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = $title;
$this->params['breadcrumbs'] = $bread;

?>
<div class="<?=$clas?>">
  <?=Helper::showMessage()?>
	<h1><?=Html::encode($this->title)?></h1>
	<p>
		<?=Html::a('Create', ['create'], ['class' => 'btn btn-success'])?>
    <?=Html::a('Sections', ['org_diller/o-d-paragraph-sec'], ['class' => 'btn btn-info'])?>
	</p>
	<?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    'id',
    'title_ru',
    'norder',
    [
      'attribute' => 'status',
      'value' => function ($m) {return Status::label($m->status);},
      'format' => 'raw',
      'filter' => Html::activeDropDownList($searchModel, 'status', Status::get(), ['class' => 'form-control', 'prompt' => 'All']),
    ],
    [
      'class' => 'yii\grid\ActionColumn',
      'template' => Helper::template(),
    ],
  ],
]);?>
</div>
