<?php

use app\components\Attribute;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = '(VT<small>&</small>D) Ком. Инвойс';
$this->params['breadcrumbs'][] = ['label' => '(VT<small>&</small>D) Контракт', 'url' => ['org_diller/o-d-contract']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];

?>

<div>
	<h3><?=$this->title?></h3>
	<div class="row">
		<div class="col-sm-12">
			<button type="button" class="ui primary basic button" data-toggle="modal" data-target="#Modal1">Создать новый</button>
		</div>
	</div>
	<br>
	<div>
	<?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'summary' => false,
  'tableOptions' => ['class' => 'ui celled striped selectable table form'],
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    [
      'attribute' => 'nnumber',
      'value' => function ($m) {return Attribute::number(@$m->number->number);},
      'format' => 'raw',
    ],
    [
      'attribute' => 'contract_id',
      'filter' => Html::activeDropDownList($searchModel, 'contract_id', $contracts, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'contract.number.number',
    ],
    [
      'attribute' => 'ndate',
      'value' => function ($m) {return Attribute::ndate($m->ndate);},
      'format' => 'raw',
    ],
    [
      'attribute' => 'nprice',
      'value' => function ($m) {return Attribute::nprice($m->nprice, $m->currency);},
      'format' => 'raw',
    ],
    [
      'attribute' => 'receive_sum',
      'value' => function ($m) {return Attribute::receiveSum($m->receive_sum, $m->receive_date, $m->currency);},
      'format' => 'raw',
    ],
    [
      'attribute' => 'created_at',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);},
      'format' => 'raw',
    ],

    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '<div class="b_style">{update}&nbsp;&nbsp;&nbsp;{delete}</div><div class="b_style">{contract}</div>', //<div class="b_style">{packinglists}</div>
      'buttons' => [
        'contract' => function ($url, $model, $key) {
          return Html::a("Контракт", ['org_diller/o-d-contract/update', 'id' => $model->contract_id], ['class' => 'mini ui button blue']);
        },
        // 'packinglists' => function ($url, $model, $key) {
        //   if (!empty($model->packinglists)) {
        //     $packing = $model->packinglists[0];
        //     return Html::a("Редак. PL", ['org_diller/o-d-pack-list/update', 'id' => $packing->id], ['class' => 'mini ui button teal']);
        //   }
        //   return Html::a("Создать PL", ['org_diller/o-d-pack-list/create', 'invoice_id' => $model->id], ['class' => 'mini ui button red']);
        // },
      ],
    ],
  ],
]);?>
	</div>
</div>
<?php Modal::begin([
  'header' => '<h4>Выберите Контракт</h4>',
  'options' => [
    'id' => 'Modal1',
  ],
  'footer' => Html::a('Создать', ['create'], ['class' => 'btn btn-success', 'id' => 'CreateBtn']),
]);?>
<div>
  <?=Html::dropDownList('contract_id', null, $contracts2, ['class' => 'form-control', 'id' => 'OdId'])?>
  <br>
</div>
<?php Modal::end();?>
<?php $this->registerJs('
  $("#CreateBtn").click(function(e){
    var href = $(this).attr("href");
    var contract_id = $("#OdId").val();
    var new_href = href + "?contract_id=" + contract_id;
    $(this).attr("href", new_href);
    return;
  });
')?>
