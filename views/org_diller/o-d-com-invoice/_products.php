<?php 

use app\components\Currencies;
use yii\helpers\Url;

?>
<div style="padding: 0 25px;">
	<table class="dtable" id="dtableContent">
		<thead>
			<tr>
				<th class="numr">№</th>
				<th class="descr">Описание товара / Description</th>
				<th class="coder">Код ТН ВЭД / HS code</th>
				<th class="unitr">Единица измерения / Unit of measurement</th>
				<th class="pricr"><?=Currencies::textTsenaInvoice($model->contract->currency) ?></th>
				<th class="quanr">Количество / Quantity</th>
				<th class="totar">
					<div><?=Currencies::textObshayaInvoice($model->contract->currency) ?></div>
					<div class="table-product">
						<div class="tr-refresh" onclick="TBL_TV.getAll()" title="Обнавить список продуктов"></div>
					</div>
				</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="5" class="nred">ИТОГО / TOTAL</th>
				<th class="t-quanr nred"><span total-qty-block>0</span></th>
				<th class="t-pricr nred">
					<span total-price-block>0</span>
					<div class="table-product">
						<div class="tr-plus" onclick="TBL_TV.addTr()" title="Добавить продукт"></div>
					</div>
				</th>
			</tr>
		</tfoot>
	</table>
</div>
<br>
<br>
<?php 
	$get_url = Url::to(['get-products', 'id' => $model->id]);
	$add_url = Url::to(['add-products', 'id' => $model->id]);
	$save_url = null; //Url::to(['save-products', 'id' => $model->id]);
	$balance_url = Url::to(['balance-products', 'id' => $model->id]);

	$this->registerJs('

		TBL_TV.opt = {
			table: "#dtableContent",
			table_clone: "#dtableClone",
			getUrl: "'.$get_url.'",
			addUrl: "'.$add_url.'",
			saveUrl: "'.$save_url.'",
			balanceUrl: "'.$balance_url.'"
		};

		TBL_TV.getAll()

	');
?>