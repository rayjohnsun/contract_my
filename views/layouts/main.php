<?php

use app\assets\AppAsset;
use app\components\Status;
use app\models\Messages;
use app\rbac\Roles;
use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
<head>
  <meta charset="<?=Yii::$app->charset?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?=Html::csrfMetaTags()?>
  <title><?=strip_tags($this->title)?></title>
  <?php $this->head()?>
  <link rel="shortcut icon" type="image/png" href="/favicon.png" />
</head>
<body>
<?php $this->beginBody()?>

<?php
$user = Yii::$app->user->identity;
NavBar::begin([
  'brandLabel' => $user ? $user->fio : Yii::$app->name,
  'brandUrl' => Yii::$app->homeUrl,
  'innerContainerOptions' => ['class' => 'container-fluid'],
  'options' => [
    'class' => 'navbar-inverse navbar-fixed-top',
  ],
]);

$items = [];
if (Yii::$app->user->can(Roles::MANAGER)) {
  $cnt = '';
  $cntmsg = Messages::find()->where(['status' => Status::NOACTIVE])->count();
  if ($cntmsg > 0) {
    $cnt = '<span class="badge">'.$cntmsg.'</span>';
  }
  $items[] = ['label' => 'Сообщения '.$cnt, 'url' => '/messages'];
}
if (Yii::$app->user->can(Roles::ADMIN)) {
  $items[] = [
    'label' => 'Меню',
    'items' => [
      ['label' => 'Пользователи', 'url' => '/users'],
      // ['label' => 'Страна', 'url' => '/countries'],
      ['label' => 'Товары', 'url' => ['/products']],
      '<li><hr></li>',
      ['label' => 'Производители', 'url' => '/companies/organizations'],
      ['label' => 'Диллеры', 'url' => '/companies/dillers'],
      ['label' => 'Покупатели', 'url' => '/companies/buyers'],
      ['label' => 'Грузополучатели', 'url' => '/companies/consignees'],
    ],
  ];
}
if (Yii::$app->user->can(Roles::ORGANIZATION) || Yii::$app->user->can(Roles::DILLER)){
  $items[] = [
    'label' => 'Производитель(VT) <small>&</small> Диллер(D)',
    'items' => [
      ['label' => '(VT<small>&</small>D) КОНТРАКТ', 'url' => '/org_diller/o-d-contract'],
      ['label' => '(VT<small>&</small>D) ИНВОЙС', 'url' => '/org_diller/o-d-invoice'],
      ['label' => '(VT<small>&</small>D) PURCHASE ORDER', 'url' => '/org_diller/o-d-invoice/po'],
      ['label' => '(VT<small>&</small>D) УПАКОВОЧНЫЙ ЛИСТ', 'url' => '/org_diller/o-d-pack-list'],
      ['label' => '(VT<small>&</small>D) КОММЕРЧЕСКИЙ ИНВОЙС', 'url' => '/org_diller/o-d-com-invoice'],
      '<li><hr></li>',
      ['label' => '(VT<small>&</small>D) Параграфы', 'url' => '/org_diller/o-d-paragraph'],
      ['label' => '(VT<small>&</small>D) Подпункты параграфа', 'url' => '/org_diller/o-d-paragraph-sec'],
    ],
  ];
  $items[] = [
    'label' => 'Производитель(VT) <small>&</small> Покупатель(B)',
    'items' => [
      ['label' => '(VT<small>&</small>B) КОНТРАКТ', 'url' => '/org_buyer/o-b-contract'],
      ['label' => '(VT<small>&</small>B) ИНВОЙС', 'url' => '/org_buyer/o-b-invoice'],
      ['label' => '(VT<small>&</small>B) УПАКОВОЧНЫЙ ЛИСТ', 'url' => '/org_buyer/o-b-pack-list'],
      ['label' => '(VT<small>&</small>D) КОММЕРЧЕСКИЙ ИНВОЙС', 'url' => '/org_buyer/o-b-com-invoice'],
      '<li><hr></li>',
      ['label' => '(VT<small>&</small>B) Параграфы', 'url' => '/org_buyer/o-b-paragraph'],
      ['label' => '(VT<small>&</small>B) Подпункты параграфа', 'url' => '/org_buyer/o-b-paragraph-sec'],
    ],
  ];
}
if (Yii::$app->user->can(Roles::DILLER)){
  $items[] = [
    'label' => 'Диллер(D) <small>&</small> Покупатель(B)',
    'items' => [
      ['label' => '(D<small>&</small>B) КОНТРАКТ', 'url' => '/dil_buyer/d-b-contract'],
      ['label' => '(D<small>&</small>B) ИНВОЙС', 'url' => '/dil_buyer/d-b-invoice'],
      ['label' => '(D<small>&</small>B) УПАКОВОЧНЫЙ ЛИСТ', 'url' => '/dil_buyer/d-b-pack-list'],
      '<li><hr></li>',
      ['label' => '(D<small>&</small>B) Параграфы', 'url' => '/dil_buyer/d-b-paragraph'],
      ['label' => '(D<small>&</small>B) Подпункты параграфа', 'url' => '/dil_buyer/d-b-paragraph-sec'],
    ],
  ];
}
$items[] = ['label' => 'Таблица', 'url' => ['/table/list']];

if (!Yii::$app->user->isGuest) {
  $items[] = '<li>'
  . Html::beginForm(['/logout'], 'post')
  . Html::submitButton(
    '<i class="glyphicon glyphicon-log-out"></i>',
    ['class' => 'btn btn-link logout']
  )
  . Html::endForm()
    . '</li>';
}

echo Nav::widget([
  'options' => ['class' => 'navbar-nav navbar-right'],
  'encodeLabels' => false,
  'items' => $items,
]);

NavBar::end();
?>
<div id="NLoader">
  <div class="ui active loader big"></div>
</div>
<div id="NMessage">
  <div class="ui message">
    <i class="close icon" onclick="RJS.setHidden(this)"></i>
    <div class="cont">&nbsp;</div>
  </div>
</div>
<div class="wrap <?=Yii::$app->params['full_height']?'full-height':'' ?>" style="padding-top: 50px;padding-bottom: 45px;">
  <div class="container-fluid <?=Yii::$app->params['full_height']?'full-height':'' ?>">
    <div class="row">
      <?=Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'encodeLabels' => false,
      ])?>
    </div>
    <?=$content?>
  </div>
</div>

<!-- <footer class="footer">
  <div class="container">
    <p class="pull-left">&copy; My Company <?//=date('Y')?></p>

    <p class="pull-right"><?//=Yii::powered()?></p>
  </div>
</footer> -->
<?=$this->render('_messages') ?>
<?php $this->endBody()?>
</body>
</html>
<?php $this->endPage()?>
