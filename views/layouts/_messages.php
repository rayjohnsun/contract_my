<?php 

	$session = Yii::$app->session;
  $flashes = $session->getAllFlashes();
  $messsTypes = ['success', 'error'];
  foreach ($flashes as $type => $flash) {
  	if (in_array($type, $messsTypes)) {
  		foreach ((array) $flash as $i => $message) {
				$this->registerJs('
					console.log("'.$type.'")
					console.log("'.$message.'")
					RJS.message2("'.$message.'", "'.$type.'")
				');
  		}
  	}
  	$session->removeFlash($type);
  }
?>