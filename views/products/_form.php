<?php

use app\components\Units;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div>
	<?php $form = ActiveForm::begin();?>
	<div class="row">
		<div class="col-sm-6">
			<?=$form->field($model, 'title_en')->textInput(['maxlength' => true])?>
			<?=$form->field($model, 'title_ru')->textInput(['maxlength' => true])?>
			<?=$form->field($model, 'description_en')->textInput(['maxlength' => true])?>
			<?=$form->field($model, 'description_ru')->textInput(['maxlength' => true])?>
			<?=$form->field($model, 'status')->checkbox()?>
		</div>
		<div class="col-sm-6">
			<?=$form->field($model, 'code')->textInput(['maxlength' => true])?>
			<?=$form->field($model, 'country_id')->dropDownlist($countries, [
			  'prompt' => 'Select',
			])?>
			<?=$form->field($model, 'unit')->dropDownlist(Units::get())?>
		</div>
	</div>
	<div class="form-group">
			<?=Html::submitButton('Save', ['class' => 'btn btn-success'])?>
	</div>
	<?php ActiveForm::end();?>
</div>
