<?php

use app\components\Attribute;
use app\components\Helper;
use app\components\Status;
use app\components\Units;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = $title;
$this->params['breadcrumbs'] = $bread;

?>
<div class="<?=$clas?>">
	<?=Helper::showMessage()?>
	<h1><?=Html::encode($this->title)?></h1>
	<p>
		<?=Html::a('Create', ['create'], ['class' => 'btn btn-success'])?>
	</p>
	<?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    // 'id',
    'title_ru',
    'description_ru',
    'code',
    [
      'attribute' => 'country_id',
      'filter' => Html::activeDropDownList($searchModel, 'country_id', $countries, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'country.nTitle',
    ],
    [
      'attribute' => 'unit',
      'filter' => Html::activeDropDownList($searchModel, 'unit', Units::get(), ['class' => 'form-control', 'prompt' => 'All']),
    ],
    // 'price',
    [
      'attribute' => 'status',
      'value' => function ($m) {return Status::label($m->status);},
      'format' => 'raw',
      'filter' => Html::activeDropDownList($searchModel, 'status', Status::get(), ['class' => 'form-control', 'prompt' => 'All']),
    ],
    [
      'attribute' => 'created_at',
      'format' => 'raw',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);},
    ],
    [
      'class' => 'yii\grid\ActionColumn',
      'template' => Helper::template(),
    ],
  ],
]);?>
</div>
