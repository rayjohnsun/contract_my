<?php

use app\components\Helper;
use yii\helpers\Html;

$this->title = $title;
$this->params['breadcrumbs'] = $bread

?>
<div class="<?=$clas?>">
	<?=Helper::showMessage()?>
  <h1><?=Html::encode($this->title)?></h1>
  <?=$this->render('_form', [
	  'model' => $model,
	  'countries' => $countries
	])?>
</div>
