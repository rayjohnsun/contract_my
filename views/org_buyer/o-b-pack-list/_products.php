<?php 

use app\components\Currencies;
use yii\helpers\Url;

?>
<div style="padding: 0 25px;">
	<table class="dtable" id="dtableContent">
		<thead>
			<tr>
				<th class="numr">#</th>
				<th class="coder">НОМЕР КОНТЕЙНЕРА CONTAINER No.</th>
				<th class="descr">ОПИСАНИЕ ТОВАРА / DESCRIPTION</th>
				<th>HS Code / Код ТНВЭД</th>
				<th>КОЛ-ВО (ШТ./МЕШКИ) / QUANTITY (PCS/BAGS)</th>
				<th>ВЕС НЕТТО (КГ) / NET WEIGHT (KG)</th>
				<th>
					<div>ВЕС БРУТТО (КГ) / GROSS WEIGHT (KG)</div>
					<div class="table-product">
						<div class="tr-refresh" onclick="TBL_TV.getAll()" title="Обнавить список продуктов"></div>
					</div>
				</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="3" class="">ИТОГО / TOTAL:</th>
				<th class=""><span>&nbsp;</span></th>
				<th class="t-quanr nred"><span total-qty-block>0</span></th>
				<th class="t-netwr nred"><span total-netw-block>0</span></th>
				<th class="t-grosr nred">
					<span total-grossw-block>0</span>
					<div class="table-product">
						<div class="tr-plus" onclick="TBL_TV.addTr()" title="Добавить продукт"></div>
					</div>
				</th>
			</tr>
		</tfoot>
	</table>
</div>
<br>
<br>
<?php 
	$get_url = Url::to(['get-products', 'id' => $model->id]);
	$add_url = Url::to(['add-products', 'id' => $model->id]);

	$this->registerJs('

		TBL_TV.opt = {
			table: "#dtableContent",
			table_clone: "#dtableClone",
			getUrl: "'.$get_url.'",
			addUrl: "'.$add_url.'",
			saveUrl: "",
			balanceUrl: ""
		};

		TBL_TV.getAll()

	');
?>