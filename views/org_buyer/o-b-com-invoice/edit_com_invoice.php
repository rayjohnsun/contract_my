<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$contract_link = Url::to(['org_buyer/o-b-contract/update', 'id' => $model->contract_id]);
$this->title = '(VT<small>&</small>B) ';
$sp_title = '<a href="'.Url::to(['index']).'" class="fwb">&lsaquo;&lsaquo;&lsaquo; '.$this->title.' &nbsp;&nbsp;</a>';
$k = 'Редактировать ком. инвойс №'.$model->number->number;
$this->title .= $k;
$sp_title .= $k;
$this->params['breadcrumbs'][] = ['label' => '(VT<small>&</small>B) Контракт', 'url' => ['org_buyer/o-b-contract']];
$this->params['breadcrumbs'][] = ['label' => '(VT<small>&</small>B) Ком. Инвойс', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="n-page" id="NPage">
	<div class="no-print">
		<?=$this->render('__prod_clone') ?>
	</div>
	<?php $form = ActiveForm::begin(['id' => 'MyFormId', 'action' => ['edit', 'id' => $model->id]]); ?>
		<div class="ui raised segment">
			<div id="EditButtons" class="no-print">
				<div class="parent-block">
					<a href="<?=$contract_link ?>">Контракт №: <strong><?=$contract_num ?></strong></a>
				</div>
				<h3 class="ui green ribbon label large mb6"><?=$sp_title ?></h3>
				<?php if ($contract->res_type == 'cominv'): ?>
					<div class="dib vat" style="width: 100px;">
						<?=$form->field($model, 'receive_sum')->textInput(['maxlength' => true, 'placeholder' => 'Сумма поступление', 'disabled' => true])->label(false)?>
					</div>
					<div class="dib vat" style="width: 100px;">
						<?=$form->field($model, 'receive_date')->textInput(['maxlength' => true, 'placeholder' => 'Дата поступление', 'disabled' => true])->label(false)?>
					</div>
				<?php endif ?>
				<div class="dib vat ml5">
					<button type="button" class="btn btn-img bgn otn" id="BtnSaveForm" title="Сохранить" disabled="true">
						<img src="/images/save5.png" alt="" style="color: #fff;">
					</button>
				</div>
				<div class="dib vat ml3">
					<a href="/org_buyer/o-b-com-invoice/to-word?id=<?=$model->id ?>" class="btn btn-img" title="Экспорт на Word">
						<img src="/images/to_word2.svg" alt="">
					</a>
				</div>
				<div class="dib vat ml3">
					<a href="javascript:window.print();" class="btn btn-img" title="Печать">
						<img src="/images/print2.png" alt="">
					</a>
				</div>
				<div class="dib vat ml3 inline field">
			    <div class="ui toggle checkbox mt3">
			      <input type="checkbox" tabindex="0" class="hidden" onchange="RJS.toggleEdit(this, '#NPage')">
			      <label></label>
			    </div>
			  </div>
			</div>
			<div id="EditContent" style="padding-top: 60px;">
				<h3 align="center">
					<span>КОММЕРЧЕСКИЙ ИНВОЙС / COMMERSIAL INVOICE </span>
					<span><?=$model->number->number ?></span>
				</h3>
				<p align="center" class="fwb">
					Дата / Date: 
					<input type="text" id="OBComInvoiceNdate" name="OBInvoice[ndate]" value="<?=$model->new_ndate ?>" style="border: 0 none; width: 85px;" disabled class="nred">
				</p>
				<br>
				<table class="btable">
					<tbody>
						<tr>
							<td class="w50">
								<div contenteditable="false" onkeyup="RJS.toHidden(this)">
									<?=$model->new_org_text_en ?>
								</div>
								<input type="hidden" name="OBInvoice[org_text_en]" value="<?=$model->org_text_en ?>">
							</td>
							<td class="w50">
								<div contenteditable="false" onkeyup="RJS.toHidden(this)">
									<?=$model->new_org_text_ru ?>
								</div>
								<input type="hidden" name="OBInvoice[org_text_ru]" value="<?=$model->org_text_ru ?>">
							</td>
						</tr>
						<tr>
							<td class="w50">
								<div contenteditable="false" onkeyup="RJS.toHidden(this)">
									<?=$model->new_buy_text_en ?>
								</div>
								<input type="hidden" name="OBInvoice[buy_text_en]" value="<?=$model->buy_text_en ?>">
							</td>
							<td class="w50">
								<div contenteditable="false" onkeyup="RJS.toHidden(this)">
									<?=$model->new_buy_text_ru ?>
								</div>
								<input type="hidden" name="OBInvoice[buy_text_ru]" value="<?=$model->buy_text_ru ?>">
							</td>
						</tr>
					</tbody>
				</table>
				<br>
				<?=$this->render('_products', ['model' => $model]) ?>
				<div style="padding: 15px;">
					<div style="width: 100%;min-height: 50px;" contentEditable="false" onkeyup="RJS.toHidden(this)" onfocus="RJS.toHidden(this)" onblur="RJS.toHidden(this)" placeholder="Примечание / Notes:"><?=$model->new_notes ?></div>
					<input type="hidden" name="OBInvoice[notes]" value="<?=$model->notes ?>">
				</div>
				<br>
				<br>
				<br>
				<br>        
				<div style="padding: 15px;">
					<table>
						<tr>
							<td><strong>Подпись / Signature: &nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
							<td>
								_________________________
							</td>
						</tr>
						<tr>
							<td><strong>Должность / Position: &nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
							<td>
								<input type="text" name="OBInvoice[position]" style="border: 0 none;width: 400px;" maxlength="250" placeholder="Должность" value="<?=$model->position ?>" disabled>
							</td>
						</tr>
						<tr>
							<td><strong>Имя / Name: &nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
							<td>
								<input type="text" id="invoices-director_org_en" name="OBInvoice[director_org_en]" style="border: 0 none;width: 200px;" maxlength="250" placeholder="Имя" value="<?=$model->director_org_en ?>" disabled>
								<input type="text" id="invoices-director_org_ru" name="OBInvoice[director_org_ru]" style="border: 0 none;width: 200px;" maxlength="250" placeholder="Имя" value="<?=$model->director_org_ru ?>" disabled>
							</td>
						</tr>
						<tr>
							<td><strong>Место / Place: &nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
							<td>
								<input type="text" id="invoices-place" name="OBInvoice[place]" style="border: 0 none;width: 400px;" maxlength="250" placeholder="Место" value="<?=$model->place ?>" disabled>
							</td>
						</tr>
					</table>
				</div>
				<br>
				<br>
				<br>
			</div>
		</div>
	<?php ActiveForm::end(); ?>
</div>
<?php 
	$this->registerJs('

		$("#BtnSaveForm").click(() => {
			RJS.post("#MyFormId").then(res => {
				if(res && res.status){
					RJS.message(res)
					TBL_TV.getAll()
					if(res.data){
						if("receive_date" in res.data){
							$("#obcominvoice-receive_date").val(res.data.receive_date)
						}
					}
				}
			});
		})

		$("#OBComInvoiceNdate").datepicker({
				format: "dd.mm.yyyy",
				autoclose: true,
				language: "ru",
		});

		$("#obcominvoice-receive_date").datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				language: "ru",
		});

		$(".ui.checkbox").checkbox();

		$(document).keydown(function(e){
		  if( e.which === 90 && e.ctrlKey ){
		    RJS.primenitAll("#EditContent")
		  }          
		});

		$("#OBInvoiceConsignee").change(function(){
			var name_en = $(this).find("option:selected").attr("name_en")
			var name_ru = $(this).find("option:selected").attr("name_ru")
			var adres_en = $(this).find("option:selected").attr("adres_en")
			var adres_ru = $(this).find("option:selected").attr("adres_ru")
			var tel = $(this).find("option:selected").attr("tel")
			var email = $(this).find("option:selected").attr("email")
			name_en = name_en ? name_en : ""
			name_ru = name_ru ? name_ru : ""
			adres_en = adres_en ? adres_en : ""
			adres_ru = adres_ru ? adres_ru : ""
			tel = tel ? tel : ""
			email = email ? email : ""

			if($(".editable_cons_text_en").length > 0){
				$(".editable_cons_text_en").text(name_en)
			}
			if($(".editable_cons_text_ru").length > 0){
				$(".editable_cons_text_ru").text(name_ru)
			}
			if($(".editable_consignee_adr_en").length > 0){
				$(".editable_consignee_adr_en").text(adres_en)
			}
			if($(".editable_consignee_adr_ru").length > 0){
				$(".editable_consignee_adr_ru").text(adres_ru)
			}
			if($(".editable_consignee_tel").length > 0){
				$(".editable_consignee_tel").text(tel)
			}
			if($(".editable_consignee_email").length > 0){
				$(".editable_consignee_email").text(email)
			}
			RJS.primenitAll("#EditContent")
		})

	');
?>