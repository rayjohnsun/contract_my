<?php 
	//
?>
<table class="atable">
	<thead>
		<tr>
			<th class="w50" colspan="2">CONTRACT № <span class="nred"><?=@$model->number->number ?></span></th>
			<th class="w50" colspan="2">КОНТРАКТ № <span class="nred"><?=@$model->number->number ?></span></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="first w25">
				<input class="ninput nred italic" block type="text" name="OBContract[adres_en]" value="<?=$model->adres_en ?>" disabled>
			</td>
			<td class="second w25">
				<input id="ch_date_en" class="ninput nred italic" block type="text" name="OBContract[date_en]" value="<?=$model->date_en ?>" disabled>
			</td>
			<td class="first w25">
				<input class="ninput nred italic" block type="text" name="OBContract[adres_ru]" value="<?=$model->adres_ru ?>" disabled>
			</td>
			<td class="second w25">
				<input id="ch_date_ru" class="ninput nred italic" block type="text" name="OBContract[date_ru]" value="<?=$model->date_ru ?>" disabled>
			</td>
		</tr>
	</tbody>
</table>
<br><br>
<table class="btable">
	<tbody>
		<tr>
			<td class="w50">
				<div class="cont" contenteditable="false" onkeyup="RJS.toHidden(this)">
					<?=$model->new_text_en ?>
				</div>
				<input type="hidden" name="OBContract[text_en]" value="<?=$model->text_en ?>">
			</td>
			<td class="w50">
				<div class="cont" contenteditable="false" onkeyup="RJS.toHidden(this)">
					<?=$model->new_text_ru ?>
				</div>
				<input type="hidden" name="OBContract[text_ru]" value="<?=$model->text_ru ?>">
			</td>
		</tr>
	</tbody>
</table>
<div id="ctableContent">
	<?php if (!empty($paragraphs)): ?>
		<?php foreach ($paragraphs as $key => $paragraph): ?>
			<div class="table-paragraph">
				<div class="table-plus" onclick="TBL_PG.addBefore($(this).parent())"></div>
				<div class="table-plus-element"></div>
				<div class="table-minus" onclick="TBL_PG.remove($(this).parent())"></div>
				<table class="ctable">
					<thead>
						<tr>
							<th class="w50">
								<div class="posr">
									<span class="nnum posa"></span>
									<textarea name="xxx" class="ninput w100 tac px8 pt1" en block disabled><?=$paragraph->title_en?></textarea>
								</div>
							</th>
							<th class="w50">
								<div class="posr">
									<span class="nnum posa"></span>
									<textarea name="xxx" class="ninput w100 tac px8 pt1" ru block disabled><?=$paragraph->title_ru?></textarea>
								</div>
							</th>
						</tr>
					</thead>
	        <tbody>
	        	<?php if (!empty($paragraph->sections)): ?>
	        		<?php foreach ($paragraph->sections as $key2 => $section): ?>
	            	<tr class="table-section">
	            		<td class="w50">
	            			<div class="posr">
	            				<div class="tr-plus" onclick="TBL_PG.addBefore($(this).parent().parent().parent(), true)"></div>
	            				<div class="tr-minus" onclick="TBL_PG.remove($(this).parent().parent().parent())"></div>
	            				<span class="nnum posa nnum2"></span>
	            				<div contenteditable="false" onkeyup="RJS.primenit(this)"><?=$section->new_title_en?></div>
	            				<input type="hidden" class="ninput" en name="xxx" value="<?=$section->title_en?>">
	            			</div>
	            		</td>
	            		<td class="w50">
	            			<div class="posr">
	            				<span class="nnum posa nnum2"></span>
	            				<div contenteditable="false" onkeyup="RJS.primenit(this)"><?=$section->new_title_ru?></div>
	            				<input type="hidden" class="ninput" ru name="xxx" value="<?=$section->title_ru?>">
	            			</div>
	            		</td>
	            	</tr>
							<?php endforeach?>
						<?php endif?>
						<tr>
							<td>
								<div class="posr">
									<div class="tr-plus last" onclick="TBL_PG.addBefore($(this).parent().parent().parent(), true)"></div>
								</div>
							</td>
							<td>
								<div class="posr"></div>
							</td>
						</tr>
	        </tbody>
	      </table>
			</div>
		<?php endforeach?>
	<?php endif?>
	<div class="table-paragraph">
		<div class="table-plus" onclick="TBL_PG.addBefore($(this).parent())"></div>
		<div class="table-plus-element"></div>
	</div>
</div>
<table class="ctable">
	<thead>
		<tr>
			<th class="w50">
				<br>
				<div>Signatures of the Parties:</div>
				<br>
				<div>THE SELLER (Продавец):</div>
				<br>
				<br>
				<br>
				<br>
				<div>__________________ <input class="ninput nred tal" type="text" name="OBContract[director_org_en]" value="<?=$model->director_org_en ?>" disabled onkeyup="TBL_PG.applyDirector()" from-seller-d></div>
				<br>
			</th>
			<th class="w50">
				<br>
				<div>Подписи сторон:</div>
				<br>
				<div>THE BUYER (Покупатель):</div>
				<br>
				<br>
				<br>
				<br>
				<div>__________________ <input class="ninput nred tal" type="text" name="OBContract[director_buy_en]" value="<?=$model->director_buy_en ?>" disabled onkeyup="TBL_PG.applyDirector()" from-buyer-d></div></div>
				<br>
			</th>
		</tr>
	</thead>
</table>
<br>
<br>
<?php 
	$this->registerJs('
		TBL_PG.opt = {
			body: "#EditContent",
			paragraph: ".table-paragraph",
			paragraph_number: ".ctable thead .nnum",
			paragraph_input_en: ".ctable thead .ninput[en]",
			paragraph_input_ru: ".ctable thead .ninput[ru]",
			paragraph_clone: "#ctableClone .table-paragraph",
			section: ".table-section",
			section_number: ".nnum",
			section_input_en: ".ninput[en]",
			section_input_ru: ".ninput[ru]",
			director: {
				seller: {from: "from-seller-d", to: "to-seller-d"},
				buyer: {from: "from-buyer-d", to: "to-buyer-d"}
			}
		};
		TBL_PG.refresh();
		$("textarea.ninput").textareaAutoSize();
	');
?>