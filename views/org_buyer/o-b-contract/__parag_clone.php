<div id="ctableClone">
	<div class="table-paragraph">
		<div class="table-plus" onclick="TBL_PG.addBefore($(this).parent())"></div>
		<div class="table-plus-element"></div>
		<div class="table-minus" onclick="TBL_PG.remove($(this).parent())"></div>
		<table class="ctable">
			<thead>
				<tr>
					<th class="w50">
						<div class="posr">
							<span class="nnum posa"></span>
							<textarea name="xxx" class="ninput w100 tac px8 pt1" en block></textarea>
						</div>
					</th>
					<th class="w50">
						<div class="posr">
							<span class="nnum posa"></span>
							<textarea name="xxx" class="ninput w100 tac px8 pt1" ru block></textarea>
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr class="table-section">
	    		<td class="w50">
	    			<div class="posr">
	    				<div class="tr-plus" onclick="TBL_PG.addBefore($(this).parent().parent().parent(), true)"></div>
	    				<div class="tr-minus" onclick="TBL_PG.remove($(this).parent().parent().parent())"></div>
	    				<span class="nnum posa nnum2"></span>
	    				<div contenteditable="true" onkeyup="editCo(this)"></div>
	    				<input type="hidden" class="ninput" en name="xxx" value="">
	    			</div>
	    		</td>
	    		<td class="w50">
	    			<div class="posr">
	    				<span class="nnum posa nnum2"></span>
	    				<div contenteditable="true" onkeyup="editCo(this)"></div>
	    				<input type="hidden" class="ninput" ru name="xxx" value="">
	    			</div>
	    		</td>
	    	</tr>
	    	<tr>
					<td>
						<div class="posr">
							<div class="tr-plus last" onclick="TBL_PG.addBefore($(this).parent().parent().parent(), true)"></div>
						</div>
					</td>
					<td>
						<div class="posr"></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>