<?php

use app\components\Attribute;
use app\components\Currencies;
use app\components\Podtverjdeno;
use app\components\Zakryto;
use app\components\Zapolnen;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = '(VT<small>&</small>B) Контракт';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];

?>
<div>
  <h3><?=$this->title?></h3>
  <div class="row">
    <div class="col-sm-12">
      <button type="button" class="ui primary basic button" data-toggle="modal" data-target="#Modal1">Создать новый</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?=Html::a('Все <b>(VT<small>&</small>B)</b> Инвойс', ['org_buyer/o-b-invoice'], ['class' => 'ui teal basic button'])?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?=Html::a('Все <b>(VT<small>&</small>B)</b> Упаковочный лист', ['org_buyer/o-b-pack-list'], ['class' => 'ui teal basic button'])?>
    </div>
  </div>
  <br>
  <div>
  <?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'summary' => false,
  'tableOptions' => ['class' => 'ui celled striped selectable table form'],
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    [
      'attribute' => 'nnumber',
      'value' => function ($m) {return Attribute::number($m->number ? $m->number->number : '');},
      'format' => 'raw',
    ],
    'adres_ru',
    'date_ru',
    [
      'attribute' => 'total_lprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_total_lprice', Podtverjdeno::get(), ['class' => '', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::lprice($m->total_lprice, $m->zapolnen, $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'total_nprice',
      'value' => function ($m) {return Attribute::nprice($m->total_nprice, $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'receive_sum',
      'value' => function ($m) {return Attribute::receiveSum($m->receive_sum, null, $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'inv_prod_nprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_inv_prod_nprice', Zakryto::getForVT(), ['class' => 'form-control', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::invBalance(($m->inv_prod_nprice - $m->inv_prod_lprice), $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'com_inv_prod_nprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_com_inv_prod_nprice', Zakryto::getForVT(), ['class' => 'form-control', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::invBalance(($m->com_inv_prod_nprice - $m->com_inv_prod_lprice), $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    // [
    //   'attribute' => 'receive_sum',
    //   'value' => function ($m) {return Attribute::receiveSum($m->receive_sum, null, $m->currency);},
    //   'format' => 'raw',
    //   'contentOptions' => ['class' => 'aligned'],
    // ],
    [
      'attribute' => 'created_at',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);},
      'format' => 'raw',
    ],

    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '<div class="b_style">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div><div class="b_style">{invoices}</div><div class="b_style">{cominvoices}</div>',
      'buttons' => [
        'invoices' => function ($url, $model, $key) {
          $invoices = $model->invoices;
          $cnt = count($invoices);
          $calas = $cnt > 0 ? 'positive' : 'negative';
          $invoice_ids = ArrayHelper::map($invoices, 'id', 'id');
          if (empty($invoice_ids)) {
            $invoice_ids = [0];
          }
          return Html::a("Инвойс ({$cnt})", ['org_buyer/o-b-invoice', 'OBInvoiceSearch[invoice_ids]' => $invoice_ids], ['class' => 'mini ui button ' . $calas]);
        },
        'cominvoices' => function ($url, $model, $key) {
          $cominvoices = $model->cominvoices;
          $cnt = count($cominvoices);
          $calas = $cnt > 0 ? 'positive' : 'negative';
          $invoice_ids = ArrayHelper::map($cominvoices, 'id', 'id');
          if (empty($invoice_ids)) {
            $invoice_ids = [0];
          }
          return Html::a("Ком. Инвойс ({$cnt})", ['org_buyer/o-b-com-invoice', 'OBComInvoiceSearch[invoice_ids]' => $invoice_ids], ['class' => 'mini ui button ' . $calas]);
        },
      ],
    ],
  ],
]);?>
  </div>
</div>
<?php Modal::begin([
  'header' => '<h4>Выберите организацию и Покупатель</h4>',
  'options' => [
    'id' => 'Modal1',
  ],
  'footer' => Html::a('Создать', ['create'], ['class' => 'btn btn-success', 'id' => 'CreateBtn']),
]);?>
<div>
  <?=Html::dropDownList('o_id', null, $organizations, ['class' => 'form-control', 'id' => 'OId'])?>
  <br>
  <?=Html::dropDownList('b_id', null, $buyers, ['class' => 'form-control', 'id' => 'BId'])?>
  <br>
  <?=Html::dropDownList('cy', null, Currencies::get(), ['class' => 'form-control', 'id' => 'Cy'])?>
</div>
<?php Modal::end();?>
<?php $this->registerJs('
  $("#CreateBtn").click(function(e){
    var href = $(this).attr("href");
    var o_id = $("#OId").val();
    var b_id = $("#BId").val();
    var cy = $("#Cy").val();
    var new_href = href + "?o_id=" + o_id + "&b_id=" + b_id + "&cy=" + cy;
    $(this).attr("href", new_href);
    return;
  });
')?>