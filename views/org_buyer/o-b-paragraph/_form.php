<?php

use app\components\Helper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div>
	<?php $form = ActiveForm::begin();?>
	<div class="row">
		<div class="col-sm-6">
			<?=$form->field($model, 'title_en')->textInput(['maxlength' => true])?>
			<?=$form->field($model, 'norder')->dropDownlist(Helper::numbers())?>
		</div>
		<div class="col-sm-6">
			<?=$form->field($model, 'title_ru')->textInput(['maxlength' => true])?>
		</div>
	</div>
	<?=$form->field($model, 'status')->checkbox()?>
	<div class="form-group">
			<?=Html::submitButton('Save', ['class' => 'btn btn-success'])?>
	</div>
	<?php ActiveForm::end();?>
</div>
