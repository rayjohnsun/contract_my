<?php

use app\components\Helper;
use app\components\Status;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = $title;
$this->params['breadcrumbs'] = $bread;

?>
<div class="<?=$clas?>">
	<?=Helper::showMessage()?>
	<h1><?=Html::encode($this->title)?></h1>
	<p>
		<?=Html::a('Создать', ['create'], ['class' => 'btn btn-success'])?>
	</p>
	<?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    'id',
    [
      'attribute' => 'paragraph_id',
      'filter' => Html::activeDropDownList($searchModel, 'paragraph_id', $paragraphs, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'paragraph.nTitle',
    ],
    [
      'attribute' => 'title_ru',
      'format' => 'raw',
    ],
    'norder',
    [
      'attribute' => 'status',
      'value' => function ($m) {return Status::label($m->status);},
      'format' => 'raw',
      'filter' => Html::activeDropDownList($searchModel, 'status', Status::get(), ['class' => 'form-control', 'prompt' => '']),
    ],
		[
      'class' => 'yii\grid\ActionColumn',
      'template' => Helper::template(),
    ],
  ],
]);?>
</div>
