<?php

use app\components\Attribute;
use app\components\Helper;
use app\components\Status;
use app\rbac\Roles;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = $title;
$this->params['breadcrumbs'] = $bread;

?>
<div class="<?=$clas?>">
	<h3 style="margin-top: 10px;"><i class="icon mail"></i> <?=Html::encode($this->title)?></h3>
	<?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    // 'id',
    [
      'attribute' => 'user_id',
      'filter' => Html::activeDropDownList($searchModel, 'user_id', $users, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'user.fio',
    ],
    [
      'attribute' => 'txt',
      'format' => 'raw',
      'value' => function ($m) {return Attribute::link($m->txt, $m->link);}
    ],
    [
      'attribute' => 'created_at',
      'format' => 'raw',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);}
    ],
    [
      'attribute' => 'readed_at',
      'format' => 'raw',
      'value' => function ($m) {return Attribute::createdAt($m->readed_at);}
    ],
    [
      'attribute' => 'status',
      'value' => function ($m) {return Status::label2($m->status);},
      'format' => 'raw',
      'filter' => Html::activeDropDownList($searchModel, 'status', Status::get2(), ['class' => 'form-control', 'prompt' => 'All']),
    ],
    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '<div class="b_style">{watched}&nbsp;&nbsp;&nbsp;{delete}</div>',
      'buttons' => [
        'watched' => function ($url, $model, $key) {
          if ($model->status != Status::ACTIVE) {
            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['watched', 'id' => $model->id]);
          }
          return '';
        }
      ]
    ],
  ],
]);?>
</div>
