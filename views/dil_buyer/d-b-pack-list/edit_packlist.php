<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$contract_link = $model->po_id>0?Url::to(['org_diller/o-d-invoice/update', 'id' => $model->po_id]):Url::to(['dil_buyer/d-b-contract/update', 'id' => $model->contract_id]);
$invoice_link = Url::to(['dil_buyer/d-b-invoice/update', 'id' => $model->invoice_id]);
$number = $model->number;
$this->title = '(D<small>&</small>B) ';
$sp_title = '<a href="'.Url::to(['index']).'" class="fwb">&lsaquo;&lsaquo;&lsaquo; '.$this->title.' &nbsp;&nbsp;</a>';
$k = 'Редактировать PL № '.$number->pack_number;
$this->title .= $k;
$sp_title .= $k;
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Контракт', 'url' => ['dil_buyer/d-b-contract']];
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Инвойс', 'url' => ['dil_buyer/d-b-invoice']];
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Упаковочный лист', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$invoice = $model->invoice;

?>
<div class="n-page" id="NPage">
	<div class="no-print">
		<?=$this->render('__prod_clone') ?>
	</div>
	<?php $form = ActiveForm::begin(['id' => 'MyFormId', 'action' => ['edit', 'id' => $model->id]]); ?>
		<div class="ui raised segment">
			<div id="EditButtons" class="no-print">
				<div class="parent-block">
					<a href="<?=$contract_link ?>"><?=$model->po_id > 0 ? 'Purchase Order' : 'Контракт' ?> №: <?=$contract_num ?></a>
					<span style="position: relative;top: -2px;">&nbsp;|&nbsp;</span>
					<a href="<?=$invoice_link ?>">Инвойс №: <strong><?=$number->number ?></strong></a>
				</div>
				<h3 class="ui green ribbon label large mb6"><?=$sp_title ?></h3>
				<div class="dib vat" style="width: 100px;">
					<?=$form->field($model, 'poddon')->textInput(['maxlength' => true, 'placeholder' => 'Поддон', 'disabled' => true])->label(false)?>
				</div>
				<div class="dib vat ml5">
					<button type="button" class="btn btn-img bgn otn" id="BtnSaveForm" title="Сохранить" disabled="true">
						<img src="/images/save5.png" alt="" style="color: #fff;">
					</button>
				</div>
				<div class="dib vat ml3">
					<a href="/dil_buyer/d-b-pack-list/to-word?id=<?=$model->id ?>" class="btn btn-img" title="Экспорт на Word">
						<img src="/images/to_word2.svg" alt="">
					</a>
				</div>
				<div class="dib vat ml3">
					<a href="javascript:window.print();" class="btn btn-img" title="Печать">
						<img src="/images/print2.png" alt="">
					</a>
				</div>
				<div class="dib vat ml3 inline field">
			    <div class="ui toggle checkbox mt3">
			      <input type="checkbox" tabindex="0" class="hidden" onchange="RJS.toggleEdit(this, '#NPage')">
			      <label></label>
			    </div>
			  </div>
			  <div class="ui form" style="padding-left: 10px;">
					<div class="inline fields my_chbox">
						<label>Продавец:</label>
							<input type="hidden" name="DBPackList[show_siller]" value="0">
							<div class="ui toggle checkbox mt3">
				      <input type="checkbox" name="DBPackList[show_siller]" tabindex="0" value="1" class="hidden" <?=$model->show_siller > 0 ? 'checked' : '' ?> onchange="RJS.showHide(this, '#ComBlock1')" disabled>
				      <label></label>
				    </div>
						<label>Покупатель:</label>
							<input type="hidden" name="DBPackList[show_buyer]" value="0">
							<div class="ui toggle checkbox mt3">
				      <input type="checkbox" name="DBPackList[show_buyer]" tabindex="0" value="1" class="hidden" <?=$model->show_buyer > 0 ? 'checked' : '' ?> onchange="RJS.showHide(this, '#ComBlock2')" disabled>
				      <label></label>
				    </div>
						<label>Грузополучатель:</label>
							<input type="hidden" name="DBPackList[show_consignee]" value="0">
							<div class="ui toggle checkbox mt3">
				      <input type="checkbox" name="DBPackList[show_consignee]" tabindex="0" value="1" class="hidden" <?=$model->show_consignee > 0 ? 'checked' : '' ?> onchange="RJS.showHide(this, '#ComBlock3')" disabled>
				      <label></label>
				    </div>
					</div>
			  </div>
			</div>
			<div id="EditContent" style="padding-top: 60px;">
				<div style="height: 60px;" class="no-print">&nbsp;</div>
				<h3 align="center">
					<span>УПАКОВОЧНЫЙ ЛИСТ / PACKING LIST </span>
					<span class="nred"><?=$number->pack_number ?></span>
				</h3>
				<p align="center" class="nred fwb">
					Дата / Date: 
					<input type="text" id="DBPackListNdate" name="DBPackList[ndate]" value="<?=$model->new_ndate ?>" style="border: 0 none; width: 85px;" disabled>
				</p>
				<br>
				<div style="padding: 0 25px;">
					<table class="btable">
						<tbody>
							<tr class="<?=$model->show_siller > 0 ? '' : 'hidden' ?>" id="ComBlock1">
								<td class="w50">
									<div class="table_div">
										<?=$invoice->new_dil_text_en ?>
									</div>
								</td>
								<td class="w50">
									<div class="table_div">
										<?=$invoice->new_dil_text_ru ?>
									</div>
								</td>
							</tr>
							<tr class="<?=$model->show_buyer > 0 ? '' : 'hidden' ?>" id="ComBlock2">
								<td class="w50">
									<div class="table_div">
										<?=$invoice->new_buy_text_en ?>
									</div>
								</td>
								<td class="w50">
									<div class="table_div">
										<?=$invoice->new_buy_text_ru ?>
									</div>
								</td>
							</tr>
							<tr class="<?=$model->show_consignee > 0 ? '' : 'hidden' ?>" id="ComBlock3">
								<td class="w50">
									<div class="table_div">
										<?=$invoice->new_cons_text_en ?>
									</div>
								</td>
								<td class="w50">
									<div class="table_div">
										<?=$invoice->new_cons_text_ru ?>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<br>
				<?=$this->render('_products', ['model' => $model]) ?>
				<!-- <div style="padding: 15px;">
					<div style="width: 100%;min-height: 50px;" contentEditable="false" onkeyup="RJS.toHidden(this)" onfocus="RJS.toHidden(this)" onblur="RJS.toHidden(this)" placeholder="Примечание / Notes:"><?//=$model->new_notes ?></div>
					<input type="hidden" name="DBPackList[notes]" value="<?//=$model->notes ?>">
				</div> -->
				<br>
				<br>
				<br>      
				<div style="padding: 15px;">
					<!-- <table class="ktable">
						<tr>
							<td>Номер контейнера / Container No &nbsp;&nbsp;</td>
							<td>
								<input type="text" name="DBPackList[container_no]" value="<?//=$model->container_no ?>" maxlength="100" placeholder="номер1, номер2, номер3, ..." class="input1 nred" disabled>
							</td>
						</tr>
					</table> -->
					<div style="padding: 0 25px;">       
						<table class="ktable">
							<tr>
								<td><strong>Директор / Director: </strong></td>
								<!-- <td>
									<input type="text" id="packinglists-director_dil_en" name="DBPackList[director_dil_en]" style="border: 0 none;width: 200px;" maxlength="250" placeholder="Имя" value="<?//=$model->director_dil_en ?>" disabled>
									<input type="text" id="packinglists-director_dil_ru" name="DBPackList[director_dil_ru]" style="border: 0 none;width: 200px;" maxlength="250" placeholder="Имя" value="<?//=$model->director_dil_ru ?>" disabled>
								</td> -->
							</tr>
						</table>
					</div>
				</div>
				<div class="no-print" style="height: 60px;">&nbsp;</div>
			</div>
		</div>
	<?php ActiveForm::end(); ?>
</div>
<?php 
	$this->registerJs('

		$("#BtnSaveForm").click(() => {
			RJS.post("#MyFormId").then(res => {
				if(res && res.status){
					RJS.message(res)
					TBL_TV.getAll()
				}
			});
		})

		$("#DBPackListNdate").datepicker({
				format: "dd.mm.yyyy",
				autoclose: true,
				language: "ru",
		})

		$(".ui.checkbox").checkbox();

		$(document).keydown(function(e){
		  if( e.which === 90 && e.ctrlKey ){
		    RJS.primenitAll("#EditContent")
		  }          
		});

		$("#obpacklist-poddon").keyup(function(){
			var editable_poddon = $(".editable_poddon")
			if(editable_poddon.length > 0){
				editable_poddon.text($(this).val())
			}
		})

	');
?>