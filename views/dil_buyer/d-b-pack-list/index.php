<?php

use app\components\Attribute;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = '(D<small>&</small>B) Упаковочный лист';
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Контракт', 'url' => ['dil_buyer/d-b-contract']];
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Инвойс', 'url' => ['dil_buyer/d-b-invoice']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];

?>
<div>
	<h3><?=$this->title?></h3>
  <div class="row">
    <div class="col-sm-12">
      <button type="button" class="ui primary basic button" data-toggle="modal" data-target="#Modal1">Создать новый</button>
    </div>
  </div>
	<br>
	<div>
	<?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'summary' => false,
  'tableOptions' => ['class' => 'ui celled striped selectable table form'],
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    [
      'attribute' => 'nnumber',
      'value' => function ($m) {return Attribute::number($m->number->pack_number);},
      'format' => 'raw',
    ],
    [
      'attribute' => 'invoice_id',
      'filter' => Html::activeDropDownList($searchModel, 'invoice_id', $invoices, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'invoice.number.number',
    ],
    [
      'attribute' => 'contract_id',
      'filter' => Html::activeDropDownList($searchModel, 'contract_id', $contracts, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'contract.number.number',
    ],
    [
      'attribute' => 'po_id',
      'filter' => Html::activeDropDownList($searchModel, 'po_id', $purchorders, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'poInvoice.number.number',
    ],
    'container_no',
    'poddon',
    [
      'attribute' => 'ndate',
      'value' => function ($m) {return Attribute::ndate($m->ndate);},
      'format' => 'raw',
    ],
    [
      'attribute' => 'created_at',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);},
      'format' => 'raw',
    ],

    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '<div class="b_style">{update}&nbsp;&nbsp;&nbsp;{delete}</div><div class="b_style">{contract}</div><div class="b_style">{invoice}</div>',
      'buttons' => [
      	'contract' => function ($url, $model, $key) {
          $text = $model->po_id>0?'Purchase Order':'Контракт';
          $link = $model->po_id>0?['org_diller/o-d-invoice/update', 'id'  => $model->po_id]:['dil_buyer/d-b-contract/update', 'id' => $model->contract_id];
          return Html::a($text, $link, ['class' => 'mini ui button blue']);
        },
        'invoice' => function ($url, $model, $key) {
          return Html::a("Инвойс", ['dil_buyer/d-b-invoice/update', 'id' => $model->invoice_id], ['class' => 'mini ui button teal']);
        }
      ],
    ],
  ],
]);?>
	</div>
</div>
<?php Modal::begin([
  'header' => '<h4>Выберите Инвойс</h4>',
  'options' => [
    'id' => 'Modal1',
  ],
  'footer' => Html::a('Создать', ['create'], ['class' => 'btn btn-success', 'id' => 'CreateBtn']),
]);?>
<div>
  <?=Html::dropDownList('invoice_id', null, $invoices2, ['class' => 'form-control', 'id' => 'DbInId'])?>
</div>
<?php Modal::end();?>
<?php $this->registerJs('
  $("#CreateBtn").click(function(e){
    var href = $(this).attr("href");
    var invoice_id = $("#DbInId").val();
    var new_href = href + "?invoice_id=" + invoice_id;
    $(this).attr("href", new_href);
    return;
  });
')?>
