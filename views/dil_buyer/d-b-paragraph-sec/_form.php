<?php

use app\components\Helper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div>
	<?php $form = ActiveForm::begin();?>
	<div class="row">
		<div class="col-sm-6">
			<label for="Ps_En" class="control-label">Title En</label>
			<div contenteditable="true" onkeyup="editCo(this)" class="form-control2" id="Ps_En"><?=$model->new_Title_En?></div>
			<input type="hidden" name="DBParagraphSec[title_en]" value="<?=$model->title_en?>">
			<?=$form->field($model, 'paragraph_id')->dropDownlist($paragraphs)?>
		</div>
		<div class="col-sm-6">
			<label for="Ps_Ru" class="control-label">Title Ru</label>
			<div contenteditable="true" onkeyup="editCo(this)" class="form-control2" id="Ps_Ru"><?=$model->new_Title_Ru?></div>
			<input type="hidden" name="DBParagraphSec[title_ru]" value="<?=$model->title_ru?>">
			<?=$form->field($model, 'norder')->dropDownlist(Helper::numbers())?>
		</div>
	</div>
	<?=$form->field($model, 'status')->checkbox()?>
	<div class="form-group">
		<?=Html::submitButton('Save', ['class' => 'btn btn-success'])?>
	</div>
	<?php ActiveForm::end();?>
</div>
<?php
$this->registerJs('
	$("#Ps_En").trigger("keyup");
	$("#Ps_Ru").trigger("keyup");
');
?>
