<?php

use app\components\Attribute;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = '(D<small>&</small>B) Инвойс';
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Контракт', 'url' => ['dil_buyer/d-b-contract']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];

?>

<div>
	<h3><?=$this->title?></h3>
	<div class="row">
		<div class="col-sm-12">
			<button type="button" class="ui primary basic button" data-toggle="modal" data-target="#Modal1" id="Sozdat">Создать новый</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?=Html::a('Все <b>(D<small>&</small>B)</b> Упаковочный лист', ['dil_buyer/d-b-pack-list'], ['class' => 'ui teal basic button'])?>
		</div>
	</div>
	<br>
	<div>
	<?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'summary' => false,
  'tableOptions' => ['class' => 'ui celled striped selectable table form'],
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    [
      'attribute' => 'nnumber',
      'value' => function ($m) {return Attribute::number(@$m->number->number);},
      'format' => 'raw',
    ],
    [
      'attribute' => 'contract_id',
      'filter' => Html::activeDropDownList($searchModel, 'contract_id', $contracts, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'contract.number.number',
    ],
    [
      'attribute' => 'po_id',
      'filter' => Html::activeDropDownList($searchModel, 'po_id', $pur_orders, ['class' => 'form-control', 'prompt' => 'All']),
      'value' => 'poInvoice.number.number',
    ],
    [
      'attribute' => 'ndate',
      'value' => function ($m) {return Attribute::ndate($m->ndate);},
      'format' => 'raw',
    ],
    [
      'attribute' => 'nprice',
      'value' => function ($m) {return Attribute::nprice($m->nprice, $m->currency);},
      'format' => 'raw',
    ],
    // [
    //   'attribute' => 'receive_sum',
    //   'value' => function ($m) {return Attribute::receiveSum($m->receive_sum, $m->receive_date, $m->currency);},
    //   'format' => 'raw',
    // ],
    [
      'attribute' => 'created_at',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);},
      'format' => 'raw',
    ],

    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '<div class="b_style">{update}&nbsp;&nbsp;&nbsp;{delete}</div><div class="b_style">{contract}</div><div class="b_style">{packinglists}</div>',
      'buttons' => [
        'contract' => function ($url, $model, $key) {
          if ($model->po_id > 0) {
            return Html::a("Purchase Order", ['org_diller/o-d-invoice/update', 'id' => $model->po_id], ['class' => 'mini ui button blue']);
          } else {
            return Html::a("Контракт", ['dil_buyer/d-b-contract/update', 'id' => $model->contract_id], ['class' => 'mini ui button blue']);
          }
        },
        'packinglists' => function ($url, $model, $key) {
          if (!empty($model->packinglists)) {
            $packing = $model->packinglists[0];
            return Html::a("Редак. PL", ['dil_buyer/d-b-pack-list/update', 'id' => $packing->id], ['class' => 'mini ui button teal']);
          }
          return Html::a("Создать PL", ['dil_buyer/d-b-pack-list/create', 'invoice_id' => $model->id], ['class' => 'mini ui button red']);
        },
      ],
    ],
  ],
]);?>
	</div>
</div>
<?php Modal::begin([
  'header' => '<h4>Выберите Контракт (или Purchase Order) и Грузополучатель</h4>',
  'options' => [
    'id' => 'Modal1',
  ],
  'footer' => Html::a('Создать', ['create'], ['class' => 'btn btn-success', 'id' => 'CreateBtn']),
]);?>
<div>
  <div class="ui toggle checkbox mine">
    <div style="cursor: default;font-size: 11px;font-weight: bold;display: inline-block;">Контракт&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    <input type="checkbox" id="PO" name="is_contract" tabindex="0" class="hidden" style="display: inline-block;">
    <label style="width: 60px;margin: 0 auto;display: inline-block;">&nbsp;</label>
    <div style="cursor: default;font-size: 11px;font-weight: bold;display: inline-block;">Purchase Order&nbsp;</div>
  </div>
  <br>
  <br>
  <div id="chbContract">
    <div><strong><small>Contract</small></strong></div>
    <?=Html::dropDownList('contract_id', null, $contracts2, ['class' => 'form-control', 'id' => 'OdId'])?>
  </div>
  <div id="chbPO">
    <div><strong><small>Purchase Order</small></strong></div>
    <?=Html::dropDownList('po_id', null, $pur_orders2, ['class' => 'form-control', 'id' => 'PoId'])?>
    <br>
    <div><strong><small>Buyer</small></strong></div>
    <?=Html::dropDownList('buyer_id', null, $buyers, ['class' => 'form-control', 'id' => 'BId'])?>
  </div>
  <br>
  <div><strong><small>Consignee</small></strong></div>
  <?=Html::dropDownList('consignee_id', null, $consignees, ['class' => 'form-control', 'id' => 'DbId'])?>
</div>
<?php Modal::end();?>
<?php $this->registerJs('
  var po_id = parseInt("'.$po_id.'");
  if(isNaN(po_id)){
    po_id = 0;
  }
  $("#CreateBtn").click(function(e){
    var href = $(this).attr("href");
    var contract_id = $("#OdId").val();
    var consignee_id = $("#DbId").val();
    var po_d = $("#PoId").val();
    var b_id = $("#BId").val();
    var po = $("#PO").is(":checked")?1:0;
    var new_href = href+"?contract_id="+contract_id+"&po_id="+po_d+"&consignee_id="+consignee_id+"&po="+po+"&buyer_id="+b_id;
    $(this).attr("href", new_href);
    return;
  });
  $(".ui.checkbox.mine").checkbox();
  $("#PO").change(function(){
    if($(this).is(":checked")){
      $("#chbContract").hide()
      $("#chbPO").show()
    } else {
      $("#chbPO").hide()
      $("#chbContract").show()
    }
  })
  $("#PO").trigger("change");
  console.log(po_id)
  if(po_id > 0){
    $("#Sozdat").trigger("click")
    $("#PO").prop("checked", true)
    $("#PO").trigger("change")
    $("#PoId").val(po_id)
  }
')?>
