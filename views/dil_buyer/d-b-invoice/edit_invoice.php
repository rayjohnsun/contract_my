<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$contract_link = $model->po_id>0?Url::to(['org_diller/o-d-invoice/update', 'id' => $model->po_id]):Url::to(['dil_buyer/d-b-contract/update', 'id' => $model->contract_id]);
$this->title = '(D<small>&</small>B) ';
$sp_title = '<a href="'.Url::to(['index']).'" class="fwb">&lsaquo;&lsaquo;&lsaquo; '.$this->title.' &nbsp;&nbsp;</a>';
$k = 'Редактировать инвойс №'.$model->number->number;
$this->title .= $k;
$sp_title .= $k;
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Контракт', 'url' => ['dil_buyer/d-b-contract']];
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Инвойс', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="n-page" id="NPage">
	<div class="no-print">
		<?=$this->render('__prod_clone') ?>
	</div>
	<?php $form = ActiveForm::begin(['id' => 'MyFormId', 'action' => ['edit', 'id' => $model->id]]); ?>
		<div class="ui raised segment">
			<div id="EditButtons" class="no-print">
				<div class="parent-block">
					<a href="<?=$contract_link ?>"><?=$model->po_id>0?'Purchase Order':'Контракт' ?> №: <strong><?=$contract_num ?></strong></a>
				</div>
				<h3 class="ui green ribbon label large mb6"><?=$sp_title ?></h3>
				<div class="dib vat" style="width: 100px;">
					<select name="DBInvoice[consignee_id]" class="form-control" id="DBInvoiceConsignee" disabled>
						<?php if (!empty($consignees)): ?>
							<?php foreach ($consignees as $key => $val): ?>
								<option <?=($val->id==$model->consignee_id)?'selected':'' ?> value="<?=$val->id ?>"
									name_en="<?=$val->title_en ?>" name_ru="<?=$val->title_ru ?>" 
									adres_en="<?=$val->adres_en ?>" adres_ru="<?=$val->adres_ru ?>" 
									tel="<?=$val->phone ?>" email="<?=$val->email ?>">
									<?=$val->title_ru ?>
				 				</option>
							<?php endforeach ?>
						<?php else: ?>
							<option value="0">No data</option>
						<?php endif ?>
					</select>
				</div>
				<div class="dib vat" style="width: 100px;">
					<?=$form->field($model, 'receive_sum')->textInput(['maxlength' => true, 'placeholder' => 'Сумма поступление', 'disabled' => true])->label(false)?>
				</div>
				<div class="dib vat" style="width: 100px;">
					<?=$form->field($model, 'receive_date')->textInput(['maxlength' => true, 'placeholder' => 'Дата поступление', 'disabled' => true])->label(false)?>
				</div>
				<div class="dib vat ml5">
					<button type="button" class="btn btn-img bgn otn" id="BtnSaveForm" title="Сохранить" disabled="true">
						<img src="/images/save5.png" alt="" style="color: #fff;">
					</button>
				</div>
				<div class="dib vat ml3">
					<a href="/dil_buyer/d-b-invoice/to-word?id=<?=$model->id ?>" class="btn btn-img" title="Экспорт на Word">
						<img src="/images/to_word2.svg" alt="">
					</a>
				</div>
				<div class="dib vat ml3">
					<a href="javascript:window.print();" class="btn btn-img" title="Печать">
						<img src="/images/print2.png" alt="">
					</a>
				</div>
				<div class="dib vat ml3 inline field">
			    <div class="ui toggle checkbox mt3">
			      <input type="checkbox" tabindex="0" class="hidden" onchange="RJS.toggleEdit(this, '#NPage')">
			      <label></label>
			    </div>
			  </div>
			</div>
			<div id="EditContent" style="padding-top: 60px;">
				<br>
				<br>
				<br>
				<table class="etable">
					<tbody>
						<tr>
							<td class="w50">
								<div contenteditable="false" onkeyup="RJS.toHidden(this)">
									<?=$model->new_dil_text_en ?>
								</div>
								<input type="hidden" name="DBInvoice[dil_text_en]" value="<?=$model->dil_text_en ?>">
							</td>
							<td class="w50">
								<h1 class="inv" style="padding-right: 10px;">INVOICE</h1>
							</td>
						</tr>
					</tbody>
				</table>
				<br>
				<p align="right" style="font-weight: bold;line-height: 16px;margin-bottom: 0;">INVOICE: <?=$model->number->number ?></p>
				<p align="right" style="font-weight: bold;line-height: 16px;" class="nred">DATE: <input type="text" id="DBInvoiceNdate" name="DBInvoice[ndate]" value="<?=$model->new_ndate ?>" style="border: 0 none; width: 85px;text-align: right;" disabled class="nred"></p>
				<br>
				<table class="btable">
					<tbody>
						<tr>
							<td class="w50" colspan="2">
								<div contenteditable="false" onkeyup="RJS.toHidden(this)">
									<?=$model->new_buy_text_en ?>
								</div>
								<input type="hidden" name="DBInvoice[buy_text_en]" value="<?=$model->buy_text_en ?>">
							</td>
						</tr>
					</tbody>
				</table>
				<br>
				<br>
				<br>
				<?=$this->render('_products', ['model' => $model]) ?>
				<div style="padding: 15px;">
					<div style="width: 100%;min-height: 50px;" contentEditable="false" onkeyup="RJS.toHidden(this)" onfocus="RJS.toHidden(this)" onblur="RJS.toHidden(this)" placeholder="Примечание / Notes:"><?=$model->new_notes ?></div>
					<input type="hidden" name="DBInvoice[notes]" value="<?=$model->notes ?>">
				</div>
				<br>
				<br>
				<div style="padding: 15px;">
					<table style="width: 100%;">
						<tbody><tr>
							<td style="width: 30%;text-align: right;"><strong>Signature:</strong></td>
							<td style="width: 30%;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td style="width: 40%;">
								<input type="text" id="invoices-director_dil_en" name="DBInvoice[director_dil_en]" style="border: 0 none;width: 200px;text-align: center;" maxlength="250" placeholder="Имя" value="<?=$model->director_dil_en ?>" disabled>
							</td>
						</tr>
					</tbody></table>
				</div>
				<br>
				<br>
				<br>
			</div>
		</div>
	<?php ActiveForm::end(); ?>
</div>
<?php 
	$this->registerJs('

		$("#BtnSaveForm").click(() => {
			RJS.post("#MyFormId").then(res => {
				if(res && res.status){
					RJS.message(res)
					TBL_TV.getAll()
					if(res.data){
						if("receive_date" in res.data){
							$("#dbinvoice-receive_date").val(res.data.receive_date)
						}
					}
				}
			});
		})

		$("#DBInvoiceNdate").datepicker({
				format: "dd.mm.yyyy",
				autoclose: true,
				language: "ru",
		});

		$("#dbinvoice-receive_date").datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				language: "ru",
		});

		$(".ui.checkbox").checkbox();

		$(document).keydown(function(e){
		  if( e.which === 90 && e.ctrlKey ){
		    RJS.primenitAll("#EditContent")
		  }          
		});

		$("#DBInvoiceConsignee").change(function(){
			var name_en = $(this).find("option:selected").attr("name_en")
			var name_ru = $(this).find("option:selected").attr("name_ru")
			var adres_en = $(this).find("option:selected").attr("adres_en")
			var adres_ru = $(this).find("option:selected").attr("adres_ru")
			var tel = $(this).find("option:selected").attr("tel")
			var email = $(this).find("option:selected").attr("email")
			name_en = name_en ? name_en : ""
			name_ru = name_ru ? name_ru : ""
			adres_en = adres_en ? adres_en : ""
			adres_ru = adres_ru ? adres_ru : ""
			tel = tel ? tel : ""
			email = email ? email : ""

			if($(".editable_cons_text_en").length > 0){
				$(".editable_cons_text_en").text(name_en)
			}
			if($(".editable_cons_text_ru").length > 0){
				$(".editable_cons_text_ru").text(name_ru)
			}
			if($(".editable_consignee_adr_en").length > 0){
				$(".editable_consignee_adr_en").text(adres_en)
			}
			if($(".editable_consignee_adr_ru").length > 0){
				$(".editable_consignee_adr_ru").text(adres_ru)
			}
			if($(".editable_consignee_tel").length > 0){
				$(".editable_consignee_tel").text(tel)
			}
			if($(".editable_consignee_email").length > 0){
				$(".editable_consignee_email").text(email)
			}
			RJS.primenitAll("#EditContent")
		})

	');
?>