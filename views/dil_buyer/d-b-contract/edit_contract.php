<?php

use app\components\Currencies;
use app\components\Zapolnen;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = '(D<small>&</small>B) ';
$sp_title = '<a href="'.Url::to(['index']).'" class="fwb">&lsaquo;&lsaquo;&lsaquo; '.$this->title.' &nbsp;&nbsp;</a>';
$k = $model->zapolnen == Zapolnen::YES ? 'Редактировать' : 'Продолжит создать';
$k .= ' контракт №'.@$model->number->number;
$this->title .= $k;
$sp_title .= $k;
$this->params['breadcrumbs'][] = ['label' => '(D<small>&</small>B) Контракт', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="n-page" id="NPage">
	<div class="no-print">
		<?=$this->render('__parag_clone') ?>
		<?=$this->render('__prod_clone') ?>
	</div>
	<?php $form = ActiveForm::begin(['id' => 'MyFormId', 'action' => ['edit', 'id' => $model->id]]); ?>
		<div class="ui raised segment">
			<div id="EditButtons" class="no-print">
				<div class="parent-block">&nbsp;</div>
				<h3 class="ui green ribbon label large mb6"><?=$sp_title ?></h3>
				<div class="dib vat" style="width: 100px;">
					<?=$form->field($model, 'ndate')->textInput(['maxlength' => true, 'disabled' => true])->label(false) ?>
				</div>
				<div class="dib vat ml5">
					<button type="button" class="btn btn-img bgn otn" id="BtnSaveForm" title="Сохранить" disabled="true">
						<img src="/images/save5.png" alt="" style="color: #fff;">
					</button>
				</div>
				<div class="dib vat ml3">
					<button id="podtverButton" type="button" class="btn btn-img otn bgn" data-toggle="modal" data-target="#PODVModal" title="Подтвердить" onclick="TBL_TV.refreshModal(this)" <?=($model->total_lprice <= 0)?'disabled':'' ?>>
						<img src="/images/apply3.png" alt="">
					</button>
				</div>
				<div class="dib vat ml3">
					<a href="/dil_buyer/d-b-contract/to-word?id=<?=$model->id ?>" class="btn btn-img" title="Экспорт на Word" <?=$model->zapolnen == Zapolnen::NO ? 'disabled' : '' ?>>
						<img src="/images/to_word2.svg" alt="">
					</a>
				</div>
				<div class="dib vat ml3">
					<a href="javascript:window.print();" class="btn btn-img" title="Печать">
						<img src="/images/print2.png" alt="">
					</a>
				</div>
				<div class="dib vat ml3 inline field">
			    <div class="ui toggle checkbox mt3">
			      <input type="checkbox" tabindex="0" class="hidden" onchange="RJS.toggleEdit(this, '#NPage')">
			      <label></label>
			    </div>
			  </div>
			</div>
			<div id="EditContent" style="padding-top: 60px;">
				<?=$this->render('_paragraphs', ['model' => $model, 'paragraphs' => $paragraphs]) ?>
				<?=$this->render('_products', ['model' => $model]) ?>
				<div style="padding: 0 15px;">
          <div style="width: 100%;min-height: 50px;" contentEditable="false" onkeyup="RJS.toHidden(this)" onfocus="RJS.toHidden(this)" onblur="RJS.toHidden(this)" placeholder="Примечание:"><?=$model->new_note_ru ?></div>
          <input type="hidden" name="DBContract[note_ru]" value="<?=$model->note_ru ?>">
        </div>
				<br>
				<div style="padding: 0 15px;">
          <div style="width: 100%;min-height: 50px;" contentEditable="false" onkeyup="RJS.toHidden(this)" onfocus="RJS.toHidden(this)" onblur="RJS.toHidden(this)" placeholder="Note:"><?=$model->new_note_en ?></div>
          <input type="hidden" name="DBContract[note_en]" value="<?=$model->note_en ?>">
        </div>
				<br>
				<br>
				<table class="etable">
					<thead>
						<tr>
							<th colspan="2">
								<p class="tac">Signatures of the Parties: / Подписи сторон:</p>
								<br>
								<br>
								<br>
								<br>
							</th>
						</tr>
						<tr>
							<th>
								<p class="tac">THE SELLER  (Продавец):</p>
								<br>
								<br>
								<br>
								<br>
								<br>
								<p class="tac">
									<span>__________________ </span>
									<span to-seller-d><?=$model->director_dil_en ?></span>
								</p>
							</th>
							<th>
								<p class="tac">THE BUYER (Покупатель):</p>
								<br>
								<br>
								<br>
								<br>
								<br>
								<p class="tac">
									<span>__________________ </span>
									<span td-buyer-d><?=$model->director_buy_en ?></span>
								</p>
							</th>
						</tr>
					</thead>
				</table>
				<br>
				<br>
				<br>
			</div>
		</div>
	<?php ActiveForm::end(); ?>
</div>
<div id="PODVModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Подтверждение контракта</h4>
			</div>
			<div class="modal-body">
				<p>Заключается контракт на сумму: <strong total-price></strong> <?=Currencies::get($model->currency) ?></p>
				<p>Подтвердить для обновление баланса?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal" id="Podverdit">Да</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
			</div>
		</div>
	</div>
</div>
<?php 
	$podv_url = Url::to(['podtverdit', 'id' => $model->id]);
	$this->registerJs('

		function podvBtnOpt(status){
			if(status == true){
				$("#podtverButton").attr("disabled", "true")
			} else {
				$("#podtverButton").removeAttr("disabled")
			}
		}

		$("#BtnSaveForm").click(() => {
			RJS.post("#MyFormId").then(res => {
				if(res && res.status){
					RJS.message(res)
					if("applayed" in res.data){
						podvBtnOpt(res.data.applayed)
					}
					TBL_TV.getAll()
				}
			});
		})

		$("#Podverdit").click(function (){
			RJS.get("'.$podv_url.'").then(res => {
				if(res && res.status){
					RJS.message(res)
					podvBtnOpt(true)
				}
			})
		})

		$("#dbcontract-ndate").datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				language: "ru",
		});

		$("#dbcontract-ndate").change(function(){
			var d_en = $("#ch_date_en");
			var d_ru = $("#ch_date_ru");
			var editable_ndate_en = $(".editable_ndate_en");
			var editable_ndate_ru = $(".editable_ndate_ru");
			var res = $(this).val().split("-");
			if(res.length == 3){
				var day = res[2];
				var mon = res[1];
				var yea = res[0];
				var m_en = en_moths[mon];
				var txt_en = day;
				if(m_en){
					txt_en += " " + m_en + ", " + yea;
				}
				if(d_en.length > 0){
					d_en.val(txt_en);
				}
				if(editable_ndate_en.length > 0){
					editable_ndate_en.text(txt_en);
				}
				var m_ru = ru_moths[mon];
				var txt_ru = day;
				if(m_ru){
					txt_ru += " " + m_ru + ", " + yea;
				}
				if(d_ru.length > 0){
					d_ru.val(txt_ru);
				}
				if(editable_ndate_ru.length > 0){
					editable_ndate_ru.text(txt_ru);
				}
			}
		});

		$(".ui.checkbox").checkbox();

		$(document).keydown(function(e){
		  if( e.which === 90 && e.ctrlKey ){
		    RJS.primenitAll("#EditContent")
		  }          
		});

	');
?>