<?php

use app\components\Attribute;
use app\components\Currencies;
use app\components\Podtverjdeno;
use app\components\Zakryto;
use app\components\Zapolnen;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = '(D<small>&</small>B) Контракт';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$get_contract_link = Url::to(['dil_buyer/d-b-contract/get-contracts']);

?>
<div>
  <h3><?=$this->title?></h3>
  <div class="row">
    <div class="col-sm-12">
      <button id="CreateNew" type="button" class="ui primary basic button" data-toggle="modal" data-target="#Modal1">Создать новый</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?=Html::a('Все <b>(D<small>&</small>B)</b> Инвойс', ['dil_buyer/d-b-invoice'], ['class' => 'ui teal basic button'])?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?=Html::a('Все <b>(D<small>&</small>B)</b> Упаковочный лист', ['dil_buyer/d-b-pack-list'], ['class' => 'ui teal basic button'])?>
    </div>
  </div>
  <br>
  <div>
  <?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'summary' => false,
  'tableOptions' => ['class' => 'ui celled striped selectable table form'],
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    [
      'attribute' => 'nnumber',
      'value' => function ($m) {return Attribute::number($m->number ? $m->number->number : '');},
      'format' => 'raw',
    ],
    'adres_ru',
    'date_ru',
    [
      'attribute' => 'total_lprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_total_lprice', Podtverjdeno::get(), ['class' => '', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::lprice($m->total_lprice, $m->zapolnen, $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'total_nprice',
      'value' => function ($m) {return Attribute::nprice($m->total_nprice, $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    // [
    //   'attribute' => 'receive_sum',
    //   'value' => function ($m) {return Attribute::receiveSum($m->receive_sum, null, $m->currency);},
    //   'format' => 'raw',
    //   'contentOptions' => ['class' => 'aligned'],
    // ],
    [
      'attribute' => 'inv_prod_nprice',
      'filter' => Html::activeDropDownList($searchModel, 'check_inv_prod_nprice', Zakryto::getForD(), ['class' => 'form-control', 'prompt' => 'All']),
      'value' => function ($m) {return Attribute::invBalance(($m->inv_prod_nprice - $m->inv_prod_lprice), $m->currency);},
      'format' => 'raw',
      'contentOptions' => ['class' => 'aligned'],
    ],
    [
      'attribute' => 'created_at',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);},
      'format' => 'raw',
    ],

    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '<div class="b_style">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div><div class="b_style">{invoices}</div>',
      'buttons' => [
        'invoices' => function ($url, $model, $key) {
          $invoices = $model->invoices;
          $cnt = count($invoices);
          $calas = $cnt > 0 ? 'positive' : 'negative';
          $invoice_ids = ArrayHelper::map($invoices, 'id', 'id');
          if (empty($invoice_ids)) {
            $invoice_ids = [0];
          }
          return Html::a("Инвойс ({$cnt})", ['dil_buyer/d-b-invoice', 'DBInvoiceSearch[invoice_ids]' => $invoice_ids], ['class' => 'mini ui button ' . $calas]);
        },
      ],
    ],
  ],
]);?>
  </div>
</div>
<?php Modal::begin([
  'header' => '<h4>Выберите организацию и Покупатель</h4>',
  'options' => [
    'id' => 'Modal1',
  ],
  'footer' => Html::a('Создать', ['create'], ['class' => 'btn btn-success', 'id' => 'CreateBtn']),
]);?>
<div>
  <?=Html::dropDownList('d_id', null, $dillers, ['class' => 'form-control', 'id' => 'DId'])?>
  <br>
  <?=Html::dropDownList('od_ids', null, [], ['class' => 'form-control', 'id' => 'ODId', 'multiple' => true])?>
  <br>
  <?=Html::dropDownList('b_id', null, $buyers, ['class' => 'form-control', 'id' => 'BId'])?>
  <br>
  <?=Html::dropDownList('cy', null, Currencies::get(), ['class' => 'form-control', 'id' => 'Cy'])?>
</div>
<?php Modal::end();?>
<?php $this->registerJs('
  $("#DId").change(function(){
    var val_id = $(this).val()
    var link = "'.$get_contract_link.'?id="+val_id
    RJS.get(link).then(res => {
      $("#ODId").html("")
      if(res.status == true){
        if(res.data.length > 0){
          var opts = ""
          for(var i = 0; i < res.data.length; i++){
            var opt_data = res.data[i]
            opts += "<option value=\'"+opt_data.c_id+"\'>"+opt_data.c_title+"</option>"
          }
          $("#ODId").html(opts)
        } else {
          RJS.message(res)
        }
      }
    })
  })
  $("#CreateNew").click(function(){
    $("#DId").trigger("change")
  })
  $("#CreateBtn").click(function(e){
    var href = $(this).attr("href");
    var d_id = $("#DId").val();
    var b_id = $("#BId").val();
    var od_ids = $("#ODId").val();
    var cy = $("#Cy").val();
    var new_href = href + "?d_id=" + d_id + "&od_ids=" + od_ids + "&b_id=" + b_id + "&cy=" + cy;
    $(this).attr("href", new_href);
    return;
  });
')?>