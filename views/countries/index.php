<?php

use app\components\Attribute;
use app\components\Helper;
use app\components\Status;
use yii\bootstrap\Html;
use yii\grid\GridView;

$this->title = $title;
$this->params['breadcrumbs'] = $bread;

?>
<div class="<?=$clas?>">
	<?=Helper::showMessage()?>
	<h1><?=Html::encode($this->title)?></h1>
	<p>
		<?=Html::a('Create Countries', ['create'], ['class' => 'btn btn-success'])?>
	</p>
	<?=GridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],

    'id',
    'title_ru',
    [
      'attribute' => 'status',
      'value' => function ($m) {return Status::label($m->status);},
      'format' => 'raw',
      'filter' => Html::activeDropDownList($searchModel, 'status', Status::get(), ['class' => 'form-control', 'prompt' => 'All']),
    ],
    [
      'attribute' => 'created_at',
      'format' => 'raw',
      'value' => function ($m) {return Attribute::createdAt($m->created_at);},
    ],
    [
      'class' => 'yii\grid\ActionColumn',
      'template' => Helper::template(),
    ],
  ],
]);?>
</div>
