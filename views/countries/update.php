<?php

use yii\helpers\Html;

$this->title = $title;
$this->params['breadcrumbs'] = $bread

?>
<div class="<?=$clas?>">
	<h1><?=Html::encode($this->title)?></h1>
	<?=$this->render('_form', ['model' => $model])?>
</div>
