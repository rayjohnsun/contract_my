<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/bootstrap-datepicker.css',
        'css/bootstrap-datepicker3.css',
        'css/site.css',
        'css/style.css?v=003',
        'css/contract.css?v=001',
        'css/nstyle.css?v=004',
    ];
    public $js = [
        'js/bootstrap-datepicker.js',
        'js/bootstrap-datepicker.ru.min.js',
        'js/script.js',
        'js/textarea_autosize.js',
        'js/TrControl.js',
        'js/NObjects.js',
        'js/Contract.js',
        'js/checkbox.js',
        'js/RJS.js',
        'js/TBL_PG.js',
        'js/TBL_TV.js',
        'js/MyTable.js',
        'js/nload.js',
        'js/bundle.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'Zelenin\yii\SemanticUI\assets\SemanticUICSSAsset'
    ];
}
