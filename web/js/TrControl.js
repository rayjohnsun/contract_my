var TrControl = {
	number: 0,
	tr: null,
	tbody: null,
	init: function (table1, table2) {
		this.tbody 	= $(table1).find('tbody');
		this.tr 	= $(table2).find('tr');
		this.number = $(table1).find('tbody tr').length;
	},
	refreshNumber: function () {
		var trs = this.tbody.find('tr');
		if (trs.length > 0) {
			trs.each(function () {
				var index = ($(this).index() + 1);
				if ($(this).find('td.tnumber > span').length > 0) {
					$(this).find('td.tnumber > span').text(index);
				} else {
					$(this).find('td.tnumber').text(index);
				}
			});
		}
		raschet2();
	},
	plus: function () {
		if (this.tbody.length > 0 && this.tr.length > 0) {
			this.number++;
			var clone = this.tr.clone();
			var names = clone.find('input');
			var n = this.number;
			names.each(function () {
				var t = $(this);
				var new_name = t.attr('name').replace(/{key}/g, n);
				t.attr('name', new_name);
			});
	        this.tbody.append(clone);
	        this.refreshNumber();
		}
	},
	minus: function (t) {
		var tt = $(t);
		tt.parent().parent().parent().remove();
		this.refreshNumber();
	}
};