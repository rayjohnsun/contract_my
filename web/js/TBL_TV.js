'use strict'

const TBL_TV = {
	opt: {
		table: '',
		table_clone: '',
		getUrl: null,
		addUrl: null,
		saveUrl: null,
		balanceUrl: null
	},
	totalPrice: 0,
	refreshWord(){
		$("#number-input").trigger("change")
	},
	getAll(){
		var cont = $(this.opt.table)
		var _this = this
		if (cont.length > 0) {
			var url = this.opt.getUrl
			if (url) {
				RJS.get(url).then(res => {
					var data = []
					if (res.status == true) {
						data = res.data
					}
					var cont_body = cont.find('tbody')
					cont_body.html('')
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							this.addTr(data[i])
						}
					}
					_this.refreshWord()
				})
			}
		}
	},
	removeTr(el) {
		if (el.prop('tagName') == 'TR') {
			el.remove()
		}
		this.refresh()
		this.refreshWord()
	},
	addTr(prod){
		var cont = $(this.opt.table)
		var cl = $(this.opt.table_clone)
		if (cont.length > 0 && cl.length > 0) {
			var cont_body = cont.find('tbody')
			var clone = cl.find('tbody tr').clone()
			if (cont_body.length > 0 && clone.length > 0) {
				if (prod) {
					var ul = clone.find('.descr .ul_content')
					if (ul.length > 0) {
						var li = '<li'
						for (var prop in prod) {
							li += ' '+prop+'="'+prod[prop]+'"'
						}
						li += '>'+prod.p_title+'</li>'
						ul.html(li)
					}
					cont_body.append(clone)
					var tttr = cont_body.find('tr')
					if(tttr.length > 0){
						tttr.each(function () {
							$(this).find('.ul_content li:eq(0)').trigger('click')
						})
					}
					this.refresh()
					this.refreshWord()
				} else {
					var url = this.opt.addUrl
					if (url) {
						RJS.get(url).then(res => {
							if (res.status == true) {
								var ul = clone.find('.descr .ul_content')
								if (ul.length > 0 && res.data.length > 0) {
									var li = ''
									for (var i = 0; i < res.data.length; i++) {
										var p = res.data[i]
										li += '<li'
										for (var prop in p) {
											li += ' '+prop+'="'+p[prop]+'"'
										}
										li += '>'+p.p_title+'</li>'
									}
									ul.html(li)
								}
								cont_body.append(clone)
								this.refresh()
								this.refreshWord()
							}
						})
					}
				}
			}
		}
	},
	runCounter(t, no_price){
		if (t) {
			var tt = $(t)
			var tt_val = tt.val()
			if (no_price != true) {
				tt_val = this.formatPrice(tt_val, 11)
			}
			tt.attr('value', tt_val)
			tt.val(tt_val)
		}
		var _this = this
		var cont = $(this.opt.table)
		if (cont.length > 0) {
			var tbody_tr = cont.find('tbody tr')
			var total_qty = 0
			var total_price = 0
			var total_net_w = 0
			var total_gross_w = 0
			if (tbody_tr.length > 0) {
				tbody_tr.each(function () {
					
					var tr = $(this)

					var qty = tr.find('[qty-block]')
					var nqty = 0
					if (qty.length > 0) {
						nqty = _this.getIntVal(qty, 10)
						total_qty += nqty
					}

					var price = tr.find('[price-block]')
					var nprice = 0
					if (price.length > 0) {
						nprice = _this.getFloatVal(price, 12)
						var tot = (nqty * nprice)
						total_price += tot
						var priceQty = tr.find('[price-qty-block]')
						if (priceQty.length > 0) {
							priceQty.text(_this.formatPrice(tot))
						}
					}

					var netw = tr.find('[netw-block]')
					if (netw.length > 0) {
						total_net_w += _this.getIntVal(netw, 10)
					}

					var grossw = tr.find('[grossw-block]')
					if (grossw.length > 0) {
						total_gross_w += _this.getIntVal(grossw, 10)
					}

				})
			}
			_this.totalPrice = total_price
			_this.setTagVal(cont.find('[total-qty-block]'), _this.formatPrice(total_qty))
			_this.setTagVal(cont.find('[total-price-block]'), _this.formatPrice(total_price))
			_this.setTagVal(cont.find('[total-netw-block]'), _this.formatPrice(total_net_w))
			_this.setTagVal(cont.find('[total-grossw-block]'), _this.formatPrice(total_gross_w))
			_this.refreshWord()
		}
	},
	refresh(){
		var cont = $(this.opt.table)
		if (cont.length > 0) {
			var tbody_tr = cont.find('tbody tr')
			if (tbody_tr.length > 0) {
				tbody_tr.each(function () {
					var index = $(this).index()+1
					var tr = $(this)
					var tr2 = tr.html().replace(/{{index}}/g, index)
					tr.html(tr2)
				})
			}
			this.runCounter()
		}
	},
	getFloatVal(tag, length){
		var val = this.getTagVal(tag)
		var val1 = this.formatSumm(val, length)
		var val2 = parseFloat(val1)
		if (!isNaN(val2)) {
			return val2
		}
		return 0
	},
	getIntVal(tag, length){
		var val = this.getTagVal(tag)
		var val1 = this.formatSumm(val, length)
		var val2 = parseInt(val1)
		if (!isNaN(val2)) {
			return val2
		}
		return 0
	},
	setTagVal(tag, val){
		if (tag.length > 0) {
			for (var i = 0; i < tag.length; i++) {
				var tag2 = $(tag[i])
				var tn = tag2.prop('tagName')
				if (tn == 'INPUT' || tn == 'SELECT') {
					tag2.val(val)
				} else {
					tag2.text(val)
				}
			}
		}
	},
	getTagVal(tag){
		if (tag.length > 0) {
			var tag2 = $(tag[0])
			var tn = tag2.prop('tagName')
			if (tn == 'INPUT' || tn == 'SELECT') {
				return tag2.val()
			} else {
				return tag2.text()
			}
		}
	},
	refreshModal(el){
		var btn = $(el)
		var modal = btn.attr('data-target')
		var _this = this
		if (modal) {
			var mod = $(modal)
			if (mod.length > 0) {
				var tp = mod.find('[total-price]')
				var url = this.opt.balanceUrl
				if (url && tp.length > 0) {
					RJS.get(url).then(res => {
						if (res.status == true) {
							if ('balance' in res.data) {
								tp.text(_this.formatPrice(res.data.balance))
							}
						}
					})
				}
			}
		}
	},
	formatInt(val, def){
		var dif2 = !isNaN(parseInt(def)) ? parseInt(def) : 0
		if (val) {
			val = parseInt(val)
			if (!isNaN(val)) {
				if (val > 0) {
					return val
				}
			}
		}
		return dif2
	},
	formatFloat(val, def){
		if (val) {
			val = parseFloat(val)
			if (!isNaN(val)) {
				if (val > 0) {
					return val
				}
			}
		}
		return def
	},
	formatSumm(value, length){
		var nlength = this.formatInt(length, 15)
		if (value) {
			var val = value.toString().replace(/\s+/g, "")
			var val2 = val.split('.')
			var val3 = val2[0]
			if (val3.length > nlength) {
				val3 = val3.substr(0,nlength)
			}
			if (val2[1]) {
				val3 += '.' + val2[1]
			}
			val3 = parseFloat(val3)
			if (!isNaN(val3)) {
				var val4 = parseFloat(val3.toFixed(2))
				if (!isNaN(val4)) {
					return val4
				} else {
					return 0
				}
			} else {
				return 0
			}
		}
		return 0
	},
	formatPrice(value, length, zero){
		var res = '0'
		if (value) {
			var val = this.formatSumm(value, length)
			if (zero == true) {
				val = val.toFixed(2)
			}
			var last = value.toString().substr(-1, 1)
			res = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
			if (last == '.') {
				res += '.'
			}
		}
		return res
	}
}