var NCont = {
	body: '',
	new_rec: 0,
	shablone: '',
	id: 0,
	// nshablone: '',
	number: '',
	ndate: '',
	seller_en: '',
	seller_ru: '',
	poddon: '',
	initBody: function (body) {

		this.body 	= body;

	},
	init: function (body, shablone, id) {

		this.body 	= body;
		this.shablone = shablone;
		this.id = id;

	},
	applyPoddon: function (t) {
		this.poddon = $(t).val();
		this.refreshBody();
		refreshEditables(this.body);
	},
	apply: function (t) {
		var tt = $(t);
		var selected_option = tt.find('option:selected');
		if (selected_option.length < 1) {
			selected_option = tt;
		}
		// var repl_id = selected_option.attr('value2') ? selected_option.attr('value2') : tt.val();
		// this.replaceShablone(repl_id);
		this.setFromAttr(selected_option);
		this.refreshBody();
		refreshEditables(this.body);

	},
	setFromAttr: function (elem) {
		if (elem.length > 0) {
			this.id 		= elem.val();
			this.number 	= (elem.attr('number')) ? elem.attr('number') : '';
			this.ndate 		= (elem.attr('ndate')) ? elem.attr('ndate') : '';
			this.seller_en 	= (elem.attr('seller_en')) ? elem.attr('seller_en') : '';
			this.seller_ru 	= (elem.attr('seller_ru')) ? elem.attr('seller_ru') : '';
		}
	},
	replaceShablone: function (contract_id) {

		// var shablone 	= this.shablone;
		// var nid 		= this.zerofilTere(this.id);
		// var cid 		= this.zerofilTere(contract_id);
		// this.nshablone 	= shablone.replace(/{id}/g, nid).replace(/{contract}/g, cid);

	},
	zerofilTere: function (id) {

		var number = parseInt(id);
		if (number > 0) {
			return number < 10 ? '0'+number : number;
		}
		return '__';

	},
	getShablone: function () {

		// return this.nshablone;

		var shablone 	= this.shablone;
		var nid 		= this.zerofilTere(this.id);
		return shablone.replace(/{id}/g, nid);

	},
	refreshBody: function () {

		this.refreshElement('.editable_number', this.getShablone());
		this.refreshElement('.editable_contract_number', this.number);
		this.refreshElement('.editable_contract_ndate', this.ndate);
		this.refreshElement('.editable_contract_number_ndate', this.ndate);
		this.refreshElement('.editable_contract_seller_en', this.seller_en);
		this.refreshElement('.editable_contract_seller_ru', this.seller_ru);
		this.refreshElement('.editable_poddon', this.poddon);

	},
	refreshElement: function (clas, val) {

		var body = $(this.body);
		if (body.length > 0) {
			var elem = body.find(clas);
			if (elem.length > 0) {
				elem.text(val);
			}
		}

	}

};

var NSell = {
	body: '',
	new_rec: 0,
	name_en: '',
	name_ru: '',
	adres_en: '',
	adres_ru: '',
	tel: '',
	email: '',
	init: function (body) {

		this.body 	= body;

	},
	apply: function (t) {
		var tt = $(t);
		var selected_option = tt.find('option:selected');
		if (selected_option.length < 1) {
			selected_option = tt;
		}
		this.setFromAttr(selected_option);
		this.refreshBody();
		refreshEditables(this.body);
		
	},
	setFromAttr: function (elem) {
		if (elem.length > 0) {
			this.name_en 	= (elem.attr('name_en')) ? elem.attr('name_en') : '';
			this.name_ru 	= (elem.attr('name_ru')) ? elem.attr('name_ru') : '';
			this.adres_en 	= (elem.attr('adres_en')) ? elem.attr('adres_en') : '';
			this.adres_ru 	= (elem.attr('adres_ru')) ? elem.attr('adres_ru') : '';
			this.tel 		= (elem.attr('tel')) ? elem.attr('tel') : '';
			this.email 		= (elem.attr('email')) ? elem.attr('email') : '';
		}
	},
	refreshBody: function () {

		this.refreshElement('.editable_seller_en', 		this.name_en);
		this.refreshElement('.editable_seller_ru', 		this.name_ru);
		this.refreshElement('.editable_seller_adr_en', 	this.adres_en);
		this.refreshElement('.editable_seller_adr_ru', 	this.adres_ru);
		this.refreshElement('.editable_seller_tel', 	this.tel);
		this.refreshElement('.editable_seller_email', 	this.email);

	},
	refreshElement: function (clas, val) {
		
		var body = $(this.body);
		if (body.length > 0) {
			var elem = body.find(clas);
			if (elem.length > 0) {
				elem.text(val);
			}
		}

	}
};

var NBuyr = {
	body: '',
	new_rec: 0,
	name_en: '',
	name_ru: '',
	adres_en: '',
	adres_ru: '',
	tel: '',
	email: '',
	init: function (body) {

		this.body 	= body;

	},
	apply: function (t) {
		var tt = $(t);
		var selected_option = tt.find('option:selected');
		if (selected_option.length < 1) {
			selected_option = tt;
		}
		this.setFromAttr(selected_option);
		this.refreshBody();
		refreshEditables(this.body);
		
	},
	setFromAttr: function (elem) {
		if (elem.length > 0) {
			this.name_en 	= (elem.attr('name_en')) ? elem.attr('name_en') : '';
			this.name_ru 	= (elem.attr('name_ru')) ? elem.attr('name_ru') : '';
			this.adres_en 	= (elem.attr('adres_en')) ? elem.attr('adres_en') : '';
			this.adres_ru 	= (elem.attr('adres_ru')) ? elem.attr('adres_ru') : '';
			this.tel 		= (elem.attr('tel')) ? elem.attr('tel') : '';
			this.email 		= (elem.attr('email')) ? elem.attr('email') : '';
		}
	},
	refreshBody: function () {

		this.refreshElement('.editable_buyer_en', 		this.name_en);
		this.refreshElement('.editable_buyer_ru', 		this.name_ru);
		this.refreshElement('.editable_buyer_adr_en', 	this.adres_en);
		this.refreshElement('.editable_buyer_adr_ru', 	this.adres_ru);
		this.refreshElement('.editable_buyer_tel', 		this.tel);
		this.refreshElement('.editable_buyer_email', 	this.email);

	},
	refreshElement: function (clas, val) {
		
		var body = $(this.body);
		if (body.length > 0) {
			var elem = body.find(clas);
			if (elem.length > 0) {
				elem.text(val);
			}
		}

	}
};

var NCons = {
	body: '',
	new_rec: 0,
	name_en: '',
	name_ru: '',
	adres_en: '',
	adres_ru: '',
	tel: '',
	email: '',
	init: function (body) {

		this.body 	= body;

	},
	apply: function (t) {

		this.setFromAttr($(t).find('option:selected'));
		this.refreshBody();
		refreshEditables(this.body);
		
	},
	setFromAttr: function (elem) {
		if (elem.length > 0) {
			this.name_en 	= (elem.attr('name_en')) ? elem.attr('name_en') : '';
			this.name_ru 	= (elem.attr('name_ru')) ? elem.attr('name_ru') : '';
			this.adres_en 	= (elem.attr('adres_en')) ? elem.attr('adres_en') : '';
			this.adres_ru 	= (elem.attr('adres_ru')) ? elem.attr('adres_ru') : '';
			this.tel 		= (elem.attr('tel')) ? elem.attr('tel') : '';
			this.email 		= (elem.attr('email')) ? elem.attr('email') : '';
		}
	},
	refreshBody: function () {

		this.refreshElement('.editable_cons_text_en', 		this.name_en);
		this.refreshElement('.editable_cons_text_ru', 		this.name_ru);
		this.refreshElement('.editable_consignee_adr_en', 	this.adres_en);
		this.refreshElement('.editable_consignee_adr_ru', 	this.adres_ru);
		this.refreshElement('.editable_consignee_tel', 		this.tel);
		this.refreshElement('.editable_consignee_email', 	this.email);

	},
	refreshElement: function (clas, val) {
		
		var body = $(this.body);
		if (body.length > 0) {
			var elem = body.find(clas);
			if (elem.length > 0) {
				elem.text(val);
			}
		}

	}
};