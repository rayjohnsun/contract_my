
var CCT = {
	body: '',
	init: function (id) {
		this.body = id;
	},
	apply: function (input, text) {
		var inp = $(input);
		if (inp.length > 0) {
			inp.on('keyup', function () {
				var parent = $(this).parent();
				var txt = parent.find(text);
				if (txt.length < 1) {
					txt = parent.parent().find(text);
				}
				if (txt.length == 1) {
					txt.text($(this).val());
				}
			});
		}
	},
	toggleEdit: function (btn) {
		var button = $(btn);
		var body = $(this.body);
		if (body.length > 0) {
			body.toggleClass('neditable');
			var editables = body.find('div[contenteditable]');
			if (editables.length > 0) {
				if (body.hasClass('neditable')) {
					editables.attr('contenteditable', 'true');
					button.text('Cancel');
				} else {
					editables.attr('contenteditable', 'false');
					button.text('Edit');
				}
			}
			button.toggleClass('active');
		}
	},
	table: {
		t_id: '',
		t_clone: '',
		init: function (id, clone) {
			this.t_id = id;
			this.t_clone = $(clone);
			this.numerovat();
		},
		plus: function (t) {
			var tt = $(t);
			var clone = this.t_clone.clone();
			clone.removeClass('clone-paragraph');
			tt.parent().before(clone);
			this.numerovat();
			CCT.apply(".ninput", ".ntext");
		},
		minus: function (t) {
			var tt = $(t);
			tt.parent().remove();
			this.numerovat();
			CCT.apply(".ninput", ".ntext");
		},
		setText: function (elelment, text) {
			if (elelment.length > 0) {
				elelment.text(text);
			}
		},
		setName: function (input, name) {
			if (input.length > 0) {
				input.attr('name', name);
			}
		},
		numerovat: function () {

			var table_cont = $(this.t_id);
			var t_this = this;

			if (table_cont.length > 0) {

				var paragraphs = table_cont.find('.table-paragraph');

				if (paragraphs.length > 0) {

					paragraphs.each(function () {

						var paragraph = $(this);
						var num 	= (paragraph.index() + 1);
						var name 	= 'Paragraphs['+num+']';

						var number 	= paragraph.find('table thead tr th .nnum');
						var index 	= num + '.';
						t_this.setText(number, index);

						var input_en= paragraph.find('table thead tr th .ninput[en]');
						var name_en	= name + '[title_en]';
						t_this.setName(input_en, name_en);

						var input_ru= paragraph.find('table thead tr th .ninput[ru]');
						var name_ru	= name + '[title_ru]';
						t_this.setName(input_ru, name_ru);

						var sections = paragraph.find('table tbody tr');

						if (sections.length > 0) {

							sections.each(function () {

								var section = $(this);
								var num2 	= (section.index() + 1);
								var name2 	= '[items]['+num2+']';

								var number2 = section.find('td .nnum');
								var index2 	= index + num2 + '.';
								t_this.setText(number2, index2);

								var input2_en 	= section.find('td .ninput[en]');
								var name2_en	= name + name2 + '[title_en]';
								t_this.setName(input2_en, name2_en);

								var input2_ru 	= section.find('td .ninput[ru]');
								var name2_ru	= name + name2 + '[title_ru]';
								t_this.setName(input2_ru, name2_ru);

							});

						}
					});

				}

			}

		},
		removeTr: function (t) {
			var tt = $(t);
			tt.parent().parent().parent().remove();
			this.numerovat();
			CCT.apply(".ninput", ".ntext");
		},
		addTr: function (t) {
			var tt = $(t);
			var clone = this.t_clone.find('tbody tr:first').clone();
			tt.parent().parent().parent().before(clone);
			this.numerovat();
			CCT.apply(".ninput", ".ntext");
		}
	},
}