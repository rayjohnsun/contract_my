'use strict'

const TBL_PG = {
	opt: {
		body: '',
		paragraph: '',
		paragraph_number: '',
		paragraph_input_en: '',
		paragraph_input_ru: '',
		paragraph_clone: '',
		section: '',
		section_number: '',
		section_input_en: '',
		section_input_ru: '',
		director: null
	},
	addBefore(el, section){
		var cl = $(this.opt.paragraph_clone)
		if (cl.length > 0) {
			if (section == true) {
				var clone = cl.find('table .table-section').clone()
				el.before(clone)
			} else {
				var clone = cl.clone()
				el.before(clone)
			}
		}
		this.refresh()
	},
	remove(el){
		el.remove();
		this.refresh();
	},
	applyDirector(){
		var body = $(this.opt.body)
		if (this.opt.director && body.length > 0) {
			var director = this.opt.director
			var seller_director = $('['+director.seller.from+']')
			var buyer_director = $('['+director.buyer.from+']')
			if (seller_director.length > 0) {
				var sd = seller_director.val()
				var sd_to = $('['+director.seller.to+']')
				if (sd_to.length > 0) {
					sd_to.text(sd)
				}
			}
			if (buyer_director.length > 0) {
				var bd = buyer_director.val()
				var bd_to = $('['+director.buyer.to+']')
				if (bd_to.length > 0) {
					bd_to.text(bd)
				}
			}
			RJS.primenitAll(this.opt.body)
		}
	},
	refresh(){
		var _this = this
		var paragraph = $(_this.opt.paragraph);
		if (paragraph.length > 0) {
			paragraph.each(function(){
				var p_f = $(this)
				var num = p_f.index() + 1
				var index = num + '.'

				_this.setText(p_f.find(_this.opt.paragraph_number), index)

				var p_title_en = _this.getParagraphName(num, 'title_en')
				_this.setName(p_f.find(_this.opt.paragraph_input_en), p_title_en)

				var p_title_ru = _this.getParagraphName(num, 'title_ru')
				_this.setName(p_f.find(_this.opt.paragraph_input_ru), p_title_ru)

				var sections = p_f.find(_this.opt.section)
				if (sections.length > 0) {
					sections.each(function() {
						var sec = $(this)
						var num2= sec.index() + 1
						var index2 = index + num2 + '.'

						_this.setText(sec.find(_this.opt.section_number), index2)

						var s_title_en = _this.getSectionName(num, num2, 'title_en')
						_this.setName(sec.find(_this.opt.section_input_en), s_title_en)

						var s_title_ru = _this.getSectionName(num, num2, 'title_ru')
						_this.setName(sec.find(_this.opt.section_input_ru), s_title_ru)
					})
				}

			})
		}
	},
	setText(cont, num){
		if (cont.length > 0) {
			cont.text(num)
		}
	},
	setName(cont, name){
		if (cont.length > 0) {
			$(cont[0]).attr('name', name)
		}
	},
	getParagraphName(number, name) {
		var text= 'Paragraphs['+number+']['+name+']'
		return text
	},
	getSectionName(number1, number2, name) {
		var text= 'Paragraphs['+number1+'][items]['+number2+']['+name+']'
		return text
	},
	scrolled(){
		var header = document.getElementById("EditButtons");
		var content = document.getElementById("EditContent");
		if (header && content) {
			var off_top = content.offsetTop + 50
			var pg_top = Math.round(window.pageYOffset)
			if (pg_top >= off_top) {
				header.classList.add("fix");
			} else {
				header.classList.remove("fix");
			}
		}
	}
}
TBL_PG.scrolled()
window.onscroll = function() {TBL_PG.scrolled()};