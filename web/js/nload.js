$(document).ready(function () {

	$(document).on("change","[ch_all]",function() {
		var parent = $(this)
    var list = parent.parents('[ch_content]').find('[ch_one]')
    if (list.length > 0) {
  		list.each(function () {
  			$(this).prop('checked', parent.is(':checked'))
  		})
      $(list[0]).trigger('change')
    }
	});

	$(document).on("change","[ch_one]",function() {
		var child = $(this)
    var parent = child.parents('[ch_content]').find('[ch_all]')
    var list = child.parents('[ch_content]').find('[ch_one]')
    if (parent.length > 0 && list.length > 0) {
    	var all = true
  		list.each(function () {
  			if (!$(this).is(':checked')) {
  				all = false
  			}
  		})
  		parent.prop('checked', all)
    }
	});

  $('.chbox_data .chbox_caret').click(function () {
    var parent = $(this).parents('.chbox_data')
    var ch = !parent.hasClass('active')
    $('.chbox_data').removeClass('active')
    if (ch) {
      parent.addClass('active')
    }
  })

  $(document).on('click', function (e) {
    if ($(e.target).closest('.chbox_data').length == 0) {
      $('.chbox_data').removeClass('active')
    }
  })
	
})