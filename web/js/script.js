var en_moths = {
	'01': 'January',
	'02': 'February',
	'03': 'March',
	'04': 'April',
	'05': 'May',
	'06': 'June',
	'07': 'July',
	'08': 'August',
	'09': 'September',
	'10': 'October',
	'11': 'November',
	'12': 'December',
}
var ru_moths = {
	'01': 'Январь',
	'02': 'Февраль',
	'03': 'Март',
	'04': 'Априль',
	'05': 'Май',
	'06': 'Июнь',
	'07': 'Июль',
	'08': 'Августь',
	'09': 'Сентябрь',
	'10': 'Октябрь',
	'11': 'Ноябрь',
	'12': 'Декабрь',
}
$(document).ready(function () {

	function applyChangesUlDropDown(ul_tr, el){
		var from_attrs = ul_tr.find('[from-attr]')
		if (from_attrs.length > 0) {
			for (var i = 0; i < from_attrs.length; i++) {
				var tt = $(from_attrs[i])
					var atttr = tt.attr('from-attr')
					if (atttr) {
						var attr_val = el.attr(atttr)
						if (attr_val) {
							if(atttr == 'p_price' || atttr == 'p_qty'){
								attr_val = TBL_TV.formatPrice(attr_val)
							}
							var this_tag = tt.prop('tagName')
							if (this_tag == 'INPUT') {
								tt.attr('value', attr_val)
								tt.trigger('keyup')
							} else if(this_tag == 'SELECT'){
								tt.val(attr_val)
							} else {
								tt.text(attr_val)
							}
							tt.trigger("change")
						}
					}
			}
		}
	}

	$(document).click(function(event) { 
		var el = $(event.target);
		if (el.is('.ul_dropdown[role="product"] .ul_content li')) {

			var ul_tr 	= el.parent().parent().parent().parent();

			applyChangesUlDropDown(ul_tr, el)

		}
		if (el.is('.ul_dropdown[role="product"] .ul_content li span') || el.is('.ul_dropdown[role="product"] .ul_content li strong')) {

			var ul_tr 	= el.parent().parent().parent().parent().parent();

			applyChangesUlDropDown(ul_tr, el)

		}
		if (el.is('.ul_dropdown .ul_head')) {
			if($('body .ul_dropdown').length > 0){
				$('body .ul_dropdown').each(function () {
					if (!$(this).is(el.parent())) {
						$(this).find('.ul_content').hide();
					}
				});
			}
			el.parent().find('.ul_content').toggle();
		}
		if (el.is('.ul_dropdown .ul_head span') || el.is('.ul_dropdown .ul_head strong')) {
			if($('body .ul_dropdown').length > 0){
				$('body .ul_dropdown').each(function () {
					if (!$(this).is(el.parent().parent())) {
						$(this).find('.ul_content').hide();
					}
				});
			}
			el.parent().parent().find('.ul_content').toggle();
		}
		if (el.is('.ul_dropdown .ul_content li')) {
			var par = el.parent();
			var val = el.attr('p_id');
			par.parent().find('.ul_input').val(val);
			par.find('li').removeClass('ul_selected');
			el.addClass('ul_selected');
			par.parent().find('.ul_head').html(el.html());
			par.hide();
		}
		if (el.is('.ul_dropdown .ul_content li span') || el.is('.ul_dropdown .ul_content li strong')) {
			var par = el.parent().parent();
			var val = el.parent().attr('p_id');
			par.parent().find('.ul_input').val(val);
			par.find('li').removeClass('ul_selected');
			el.parent().addClass('ul_selected');
			par.parent().find('.ul_head').html(el.parent().html());
			par.hide();
		}
	    if(!el.closest('.ul_dropdown').length) {
	        if($('.ul_dropdown .ul_content').is(":visible")) {
	            $('.ul_dropdown .ul_content').hide();
	        }
	    }        
	});

});

function PlusTovar() {
	console.log('plus');
}

function raschet(t) {
	var tt = $(t);
	var par = tt.parent().parent();
	var pricr = par.find('.pricr input[type="text"]');
	var f_price = 0;
	if (pricr.length > 0) {
		f_price = pricr.val();
	} else {
		pricr = par.find('.pricr span');
		f_price = pricr.text();
	}
	var quanr = par.find('.quanr input[type="text"]');
	var totar = par.find('.totar span');
	var num1 = !isNaN(parseInt(f_price)) ? parseInt(f_price) : 0;
	var num2 = !isNaN(parseInt(quanr.val())) ? parseInt(quanr.val()) : 0;
	var res = (num1 * num2);
	totar.text(res);

	raschet2();
}

function raschet_packinglist(t) {

	var tt 		= $(t);
	var par 	= tt.parent().parent();
	var parpar 	= par.parent().parent();
	var tr 		= parpar.find('tr');

	var n1 		= 0;
	var n2 		= 0;
	var n3 		= 0;

	if (tr.length > 0) {

		tr.each(function () {
			
			var quanr 	= $(this).find('.quanr input[type="text"]');
			var netwr 	= $(this).find('.netwr input[type="text"]');
			var growr 	= $(this).find('.growr input[type="text"]');

			var num1 	= !isNaN(parseInt(quanr.val())) ? parseInt(quanr.val()) : 0;
			var num2 	= !isNaN(parseInt(netwr.val())) ? parseInt(netwr.val()) : 0;
			var num3 	= !isNaN(parseInt(growr.val())) ? parseInt(growr.val()) : 0;

			n1 += num1;
			n2 += num2;
			n3 += num3;

		});

	}
	
	var t_quanr = parpar.find('.t-quanr span')
	var t_netwr = parpar.find('.t-netwr span')
	var t_grosr = parpar.find('.t-grosr span')

	t_quanr.text(n1);
	t_netwr.text(n2);
	t_grosr.text(n3);

	raschet2();
}
function summa(inputs, tag) {
	var summa = 0;
	if (inputs.length > 0) {
		inputs.each(function () {
			var input = $(this).find(tag);
			if (input.length > 0) {
				var val = '';
				if (tag == 'span') {
					val = input.text();
				} else {
					val = input.val();
				}
				var value = parseInt(val);
				if (!isNaN(value)) {
					summa += value;
				}
			}
		});
	}
	return summa;
}
function raschet2() {
	var table = $('.dtable');
	if (table.length > 0) {
		var quanrs = table.find('tbody td.quanr');
		var t_quan = table.find('tfoot th.t-quanr span');
		// console.log(t_quan);
		if (t_quan.length > 0) {
			var s_quanr = summa(quanrs, 'input[type="text"]');
			t_quan.text(s_quanr);
		}
		var pricrs = table.find('tbody td.totar');
		var t_pric = table.find('tfoot th.t-pricr span');
		var i_pric = $('#number-input');
		if (t_pric.length > 0) {
			var s_pricr = summa(pricrs, 'span');
			t_pric.text(s_pricr);
			if (i_pric.length > 0) {
				i_pric.val(s_pricr);
				i_pric.trigger('change');
			}
		}
	}
}

function CoInvoice(index, contract = 0) {
	var ind = parseInt(index);

	var SellerValue 	= "#Company_company_seller_id";
	var BuyerValue 		= "#Company_company_buyer_id";
	var ConsigneeValue 	= "#Company_consignee_id";

	var SContentRu = "#SContentRu";
	var SContentEn = "#SContentEn";
	var BContentRu = "#BContentRu";
	var BContentEn = "#BContentEn";
	var CContentRu = "#CContentRu";
	var CContentEn = "#CContentEn";

	var selle_ru = {name_ru:'ПРОДАВЕЦ:',adres_ru:'',tel:'Тел.:',email:'Адрес эл. почты:'};
	var selle_en = {name_en:'THE SELLER:',adres_en:'',tel:'Phone:',email:'E-mail address:'};
	var buyer_ru = {name_ru:'ПОКУПАТЕЛЬ:',adres_ru:'',tel:'Тел.:',email:'Адрес эл. почты:'};
	var buyer_en = {name_en:'THE BUYER:',adres_en:'',tel:'Phone:',email:'E-mail address:'};
	var consi_ru = {name_ru:'ГРУЗОПОЛУЧАТЕЛЬ:',adres_ru:'',tel:'Тел.:',email:'Адрес эл. почты:'};
	var consi_en = {name_en:'THE CONSIGNEE:',adres_en:'',tel:'Phone:',email:'E-mail address:'};

	if (ind == 1) {
		goSelect(SellerValue, SContentRu, selle_ru, contract);
		goSelect(SellerValue, SContentEn, selle_en, contract);
	} else if (ind == 2) {
		goSelect(BuyerValue, BContentRu, buyer_ru, contract);
		goSelect(BuyerValue, BContentEn, buyer_en, contract);
	} else if(ind == 3){
		goSelect(ConsigneeValue, CContentRu, consi_ru, contract);
		goSelect(ConsigneeValue, CContentEn, consi_en, contract);
	} else {
		goSelect(SellerValue, SContentRu, selle_ru, contract);
		goSelect(SellerValue, SContentEn, selle_en, contract);
		goSelect(BuyerValue, BContentRu, buyer_ru, contract);
		goSelect(BuyerValue, BContentEn, buyer_en, contract);
		goSelect(ConsigneeValue, CContentRu, consi_ru, contract);
		goSelect(ConsigneeValue, CContentEn, consi_en, contract);
	}
}

function editDate(t) {
	var tt = $(t);
	var cont = $('.editable_ndate');
	if (cont.length > 0) {
		cont.text(tt.val());
	}
}

function refreshEditables(t) {
	var body = $(t);
	if (body.length > 0) {

		var editables = body.find('div[contenteditable]');
		if (editables.length > 0) {
			editables.each(function () {
				editCo(this);
			});
		}
		
	}
}

function editCo(t) {
	var tt = $(t);
	if (tt.length > 0) {
		var par = tt.parent();

		var inp = par.find('input');
		if (inp.length > 0) {
			var dd = tt.html();
			var ddd = ekranirovat(dd);
			inp.val(ddd);
		}
	}
}

function goSelect(id1, id2, attributes, contract) {
	var id1C = $(id1);
	if (id1C.length > 0) {
		var getContent = id1C.find('option:selected');
		if (contract < 1) {
			var context = "";
			$.each(attributes, function (key, value) {
				var atr_val = getContent.attr(key);
				if (atr_val) {
					if (key == 'name_ru' || key == 'name_en') {
						context +="<h5>"+value+" "+atr_val+"</h5>";
					} else {
						context +="<p>"+value+" "+atr_val+"</p>";
					}
				}
			});
			if ($(id2).length > 0) {
				$(id2).html(context);
				editCo(id2);
			}
		}
		if (id1 == '#Company_company_seller_id') {
			var con1 = $('.editable_seller_en');
			var con2 = $('.editable_seller_ru');
			// console.log(con1);
			var val1 = getContent.attr('name_en');
			var val2 = getContent.attr('name_ru');
			if (con1.length > 0) {
				if (val1) {
					con1.text(val1);
					editCo("#InvoiceNoteValue");
					if (contract > 0) {
						editCo(id2);
					}
				}
			}
			if (con2.length > 0) {
				if (val2) {
					con2.text(val2);
					editCo("#InvoiceNoteValue");
					if (contract > 0) {
						editCo(id2);
					}
				}
			}
		}
		if (id1 == '#Company_company_buyer_id') {
			var con1 = $('.editable_buyer_en');
			var con2 = $('.editable_buyer_ru');
			// console.log(con1);
			var val1 = getContent.attr('name_en');
			var val2 = getContent.attr('name_ru');
			if (con1.length > 0) {
				if (val1) {
					con1.text(val1);
					editCo("#InvoiceNoteValue");
					if (contract > 0) {
						editCo(id2);
					}
				}
			}
			if (con2.length > 0) {
				if (val2) {
					con2.text(val2);
					editCo("#InvoiceNoteValue");
					if (contract > 0) {
						editCo(id2);
					}
				}
			}
		}
	}
}

function ekranirovat(string) {
    return string.replace(/\\/g, '\\\\').
        replace(/"/g, '&quot;');
}

function addslashes(string) {
    return string.replace(/\\/g, '\\\\').
        replace(/\u0008/g, '\\b').
        replace(/\t/g, '\\t').
        replace(/\n/g, '\\n').
        replace(/\f/g, '\\f').
        replace(/\r/g, '\\r').
        replace(/'/g, '\\\'').
        replace(/"/g, '\\"');
}

function closeThis(t) {
	var tt = $(t);
	tt.parent().parent().hide();
}