'use strict'

const RJS = {
	cntMess: 0,
	loaderId: '#NLoader',
	messageId: '#NMessage',
	setHidden(elem){
		var el = $(elem)
		if (el.length > 0) {
			el.parent().parent().removeClass('nactive')
		}
	},
	loader(show){
		var loader = $(this.loaderId)
		if (loader.length > 0) {
			if (show == true) {
				loader.addClass('active')
			} else {
				loader.removeClass('active')
			}
		}
	},
	serialize(elem){
		if (elem && elem.length > 0) {
			if (elem.prop('tagName') == 'FORM') {
				return elem.serialize()
			}
			else if (elem.prop('tagName') == 'INPUT' || elem.prop('tagName') == 'SELECT' || elem.prop('tagName') == 'TEXTAREA'){
				return elem.serialize()
			} else {
				return elem.find('input[name],select[name],textarea[name]').serialize()
			}
		}
		return ''
	},
	message2(message, status){
		var status2 = false
		if (status == true || status == 'success') {
			status2 = true
		}
		this.message({status: status2, message: message})
	},
	message(res){
		var _this = this
		var message = $(this.messageId)
		if (message.length > 0) {
			if ('status' in res && 'message' in res) {
				var contMess = message.find('.message');
				if (contMess.length > 0) {
					if (message.hasClass('nactive')) {
						message.removeClass('nactive')
						setTimeout(function () {
							_this.showMess(message, res)
						}, 500)
					} else {
						_this.showMess(message, res)
					}
				}
			}
		}
	},
	showMess(message, res){
		var _this = this
		var contMess = message.find('.message');
		contMess.removeClass('success greeen error red')
		var clas = res.status == true ? 'success greeen' : 'error red'
		contMess.addClass(clas);
		var cont = contMess.find('.cont')
		if (cont.length > 0) {
			var htm = ''
			if (res.message) {
				htm += '<div class="header mb2">'+res.message+'</div>'
			}
			if (res.messages) {
				var new_mess = (res.messages instanceof Array) ? res.messages : Object.values(res.messages)
				if (new_mess.length > 0) {
					htm += '<ul>'
					for (var i = 0; i < new_mess.length; i++) {
						var hval = (typeof new_mess[i] == 'object') ? new_mess[i].text : new_mess[i]
						htm += '<li>'+hval+'</li>'
					}
					htm += '</ul>'
				}
			}
			if (res.links && res.links.length > 0) {
				for (var j = 0; j < res.links.length; j++) {
					var lval = res.links[j]
					if ('text' in lval && 'link' in lval) {
						htm += '<div><a href="'+lval.link+'" target="_blank" title="Откроется на новый вкладке">'+lval.text+'</a></div>'
					}
				}
			}
			cont.html(htm)
			var cnt = _this.cntMess + 1
			_this.cntMess++
			message.addClass('nactive')
			setTimeout(function () {
				if (cnt == _this.cntMess) {
					message.removeClass('nactive')
				}
			}, 15000)
		}
	},
	get(from_url){
		return new Promise((resolve, reject) => {
			this.loader(true)
			$.ajax({
				url: from_url,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					if (data && 'status' in data && 'data' in data && 'message' in data && 'code' in data) {
            resolve(data)
					} else {
						resolve({data: {}, status: false, message: 'Retun unavailable data', code: 103})
					}
        },
        error: function (err) {
        	console.log(err.responseText)
        	var err_txt = "Somethign went wrong"
        	if (err.status == 404) {
        		err_txt = "Request page is not found (404) "+from_url
        	} else if (err.status == 403){
        		err_txt = "You have no access to the request page "+from_url
        	}
          resolve({data: {}, status: false, message: err_txt, code: 102})
        }
			});
		}).then(res => {
			this.loader(false)
			if (!res.status) {
				this.message(res)
			}
			return res
		});
	},
	post(form_id, to_url, encrypt = false){
		return new Promise((resolve, reject) => {
			this.loader(true)
			var form = (typeof form_id == "string") ? $(form_id) : form_id
			if (form.length > 0) {
				var formData = this.serialize(form);
				var url = to_url ? to_url : form.attr("action")
				$.ajax({
					url: url,
					type: 'POST',
					data: formData,
					dataType: 'json',
					success: function (data) {
						if (data && 'status' in data && 'data' in data && 'message' in data && 'code' in data) {
	            resolve(data)
						} else {
							resolve({data: {}, status: false, message: 'Retun unavailable data', code: 103})
						}
          },
          error: function (err) {
          	console.log(err.responseText)
          	var err_txt = "Somethign went wrong"
          	if (err.status == 404) {
          		err_txt = "Request page is not found (404) " + url
          	} else if (err.status == 403){
          		err_txt = "You have no access to the request page " + url
          	}
            resolve({data: {}, status: false, message: err_txt, code: 102})
          }
				});
			} else {
				resolve({data: {}, status: false, message: 'Can not find form', code: 101});
			}
		}).then(res => {
			this.loader(false)
			if (!res.status) {
				this.message(res)
			}
			return res
		});
	},
	toHidden(elem) {
		var j_elem = $(elem)
		if (j_elem.length > 0) {
			var input = j_elem.parent().find('input[type="hidden"]')
			if (input.length > 0) {
				var e_text = this.ekranirovat(j_elem.html())
				input.val(e_text)
			}
		}
	},
	primenitAll(id){
		setTimeout(() => {
			if (id && $(id).length > 0) {
				var _this = this
				var editables = $(id).find('div[contenteditable]')
				if (editables.length > 0) {
					editables.each(function () {
						_this.primenit(this)
					})
				}
			}
		}, 500)
	},
	primenit(t) {
		var tt = $(t);
		if (tt.length > 0) {
			var par = tt.parent();

			var inp = par.find('input');
			if (inp.length > 0) {
				var dd = tt.html();
				var ddd = this.ekranirovat(dd);
				inp.val(ddd);
			}
		}
	},
	ekranirovat(string) {
		if (string) {
	    return string.replace(/\\/g, '\\\\').replace(/"/g, '&quot;')
		}
		return ''
	},
	toggleEdit(checkbox, content) {
		var ch = $(checkbox)
		var cont = $(content)
		if (ch.length > 0 && cont.length > 0) {

			if (ch.is(':checked')) {
				cont.addClass('active')
			} else {
				cont.removeClass('active')
			}

			var sbtn = cont.find('#BtnSaveForm')
			if (sbtn.length > 0) {
				if (ch.is(':checked')) {
					sbtn.removeAttr('disabled')
				} else {
					sbtn.attr('disabled', 'true')
				}
			}

			var divs = cont.find('div[contenteditable]')
			if (divs.length > 0) {
				if (ch.is(':checked')) {
					divs.attr('contenteditable', 'true')
				} else {
					divs.attr('contenteditable', 'false')
				}
			}

			var text_inputs = cont.find('input[type="text"]')
			if (text_inputs.length > 0) {
				if (ch.is(':checked')) {
					text_inputs.removeAttr('disabled')
				} else {
					text_inputs.attr('disabled', 'true')
				}
			}

			var textareas = cont.find('textarea')
			if (textareas.length > 0) {
				if (ch.is(':checked')) {
					textareas.removeAttr('disabled')
				} else {
					textareas.attr('disabled', 'true')
				}
			}

			var selects = cont.find('select')
			if (selects.length > 0) {
				if (ch.is(':checked')) {
					selects.removeAttr('disabled')
				} else {
					selects.attr('disabled', 'true')
				}
			}

			var checkboxes = cont.find('.my_chbox input[type="checkbox"]')
			if (checkboxes.length > 0) {
				if (ch.is(':checked')) {
					checkboxes.removeAttr('disabled')
				} else {
					checkboxes.attr('disabled', 'true')
				}
			}
			
		}
	},
	showHide(checkbox, id){
		var ch = $(checkbox)
		var b_id = $(id)
		if (b_id.length > 0) {
			if (ch.is(':checked')) {
				b_id.removeClass('hidden')
			} else {
				b_id.addClass('hidden')
			}
		}
	},
	visibleTableItems(tt, table){
		var chb = $(tt)
		var table = $(table)
		if (table.length > 0) {
			var uniq = chb.attr('uniquid')
			var th = table.find('th[uniquid="'+uniq+'"]')
			var td = table.find('td[uniquid="'+uniq+'"]')
			if (th.length > 0) {
				if (chb.is(':checked')) {
					th.show()
				} else {
					th.hide()
				}
			}
			if (td.length > 0) {
				if (chb.is(':checked')) {
					td.show()
				} else {
					td.hide()
				}
			}
		}
	},
	slideHideShow(id, e){
		var idd = $(id)
		if (idd.length > 0) {
			idd.slideToggle();
		}
	},
	selectAll(id){
		var cont = $(id)
		if (cont.length > 0) {
			var check_list = cont.find('.checkbox input[type="checkbox"]');
			if (check_list.length > 0) {
				for (var i = 0; i < check_list.length; i++) {
					$(check_list[i]).prop('checked', true)
					$(check_list[i]).trigger('change')
				}
			}
		}
	}
}