'use strict'

const MyTable = {
	id: null,
	objects: {},
	filters: [],
	filtering: false,
	refresh(){
		var _this = this
		var table = $(_this.id)
		if (table.length > 0) {
			var trs = table.find('tbody tr')
			if (trs.length > 0) {
				trs.each(function () {
					if (_this.filtering == true) {
						var index = $(this).index().toString()
						if (_this.filters.indexOf(index) >= 0) {
							$(this).removeClass('inactive')
						} else {
							$(this).addClass('inactive')
						}
					} else {
						$(this).removeClass('inactive')
					}
				})
			}
		}
	},
	applyChBox(chbox){
		var _this = this
		var table = $(_this.id)
		if (table.length > 0) {
			var theads = table.find('thead tr.ikkinchi th[uniquid]')
			if (theads.length > 0) {
				//***********************************************//
				var th = $(chbox).parents('th[uniquid]')
				if (th.length > 0) {
					var uniquid = th.attr('uniquid')
					var chboxes = th.find('.checkbox_body input[type="checkbox"]:checked')
					var caret = th.find('.chbox_caret')
					if (caret.length > 0) {
						if (chboxes.length > 0) {
							caret.addClass('active')
						} else {
							caret.removeClass('active')
						}
					}
				}
				//***********************************************//
				var larr = []
				theads.each(function () {
					var ll = $(this).find('.checkbox_body input[type="checkbox"]:checked')
					if (ll.length > 0) {
						var arr = []
						ll.each(function () {
							arr.push($(this).attr('value'))
						})
						var arr2 = arr.join(',').split(',').filter(function (a, b, c) {
							return c.indexOf(a) === b
						}).sort()
						if (arr2.length > 0) {
							larr.push(arr2)
						}
					}
				})
				if (larr.length > 0) {
					var sinov = larr[0]
					for (var i = 0; i < larr.length; i++) {
						sinov = sinov.filter(function (item) {
							return larr[i].includes(item)
						})
					}
					_this.filters = sinov
					_this.filtering = true
				} else {
					_this.filters = []
					_this.filtering = false
				}
				//***********************************************//
			}
		}
		_this.refresh()
	},
	loadFilters(){
		var _this = this
		if (_this.id) {
			var table = $(_this.id)
			if (table.length > 0) {
				var theads = table.find('thead tr.ikkinchi th[uniquid]')
				if (theads.length > 0) {
					var objects = []
					theads.each(function () {
						var uniquid = $(this).attr('uniquid')
						var tds = table.find('tbody tr td[uniquid="'+uniquid+'"]')
						var list = {}
						if (tds.length > 0) {
							tds.each(function () {
								var tr_index = $(this).parent().index()
								var tds2 = $(this).find('[role="td"]')
								if (tds2.length > 0) {
									tds2.each(function () {
										var nayden = $.trim($(this).text().toString())
										if(nayden != "" && nayden != 'not created' && nayden != 'no products' && nayden != 'no PL container'){
											if (nayden in list) {
												list[nayden].push(tr_index)
											} else {
												list[nayden] = [tr_index]
											}
										} else {
											if ('Пустой' in list) {
												list['Пустой'].push(tr_index)
											} else {
												list['Пустой'] = [tr_index]
											}
										}
									})
								} else {
									var nayden = $.trim($(this).text().toString())
									if(nayden != "" && nayden != 'not created' && nayden != 'no products' && nayden != 'no PL container'){
										if (nayden in list) {
											list[nayden].push(tr_index)
										} else {
											list[nayden] = [tr_index]
										}
									} else {
										if ('Пустой' in list) {
											list['Пустой'].push(tr_index)
										} else {
											list['Пустой'] = [tr_index]
										}
									}
								}
							})
						}
						objects[uniquid] = {filter: false, list: list}
						var checkbox_body = $(this).find('.checkbox_body')
						if (checkbox_body.length > 0) {
							var li  = ''
							for(var ob in list){
								li += '<li><label><input type="checkbox" value="'+list[ob].join(',')+'" ch_one onchange="MyTable.applyChBox(this)">&nbsp;'+ob+'</label></li>'
							}
							checkbox_body.html(li)
						}
					})
					_this.objects = objects
					_this.filters = []
					_this.filtering = false
				}
			}
		}
	}
}