<?php

namespace app\components;

/**
 *
 */
class Attribute {

  public static function number($value) {
    $txt = '<div class="tac"><strong>' . $value . '</strong></div>';
    return $txt;
  }

  public static function ndate($value) {
    $txt = '<div><strong><i>';
    $txt .= date("d.m.Y", strtotime($value));
    $txt .= '</i></strong></div>';
    return $txt;
  }

  public static function createdAt($value) {
    if (!empty($value)) {
      $txt = '<div><strong>';
      $txt .= date("d.m.Y", strtotime($value));
      $txt .= '</strong></div>';
      $txt .= '<div><code>';
      $txt .= date("H:i", strtotime($value));
      $txt .= '</code></div>';
      return $txt;
    }
    return '';
  }

  public function receiveSum($value, $date = null, $currency = 0) {
    $ddate = !empty($date) ? date("d.m.Y", strtotime($date)) : '';
    $txt = '<div class="tac">';
    if ($value > 0) {
      $title = !empty($ddate) ? "Поступлено в: {$ddate}" : "Поступлено";
      $txt .= '<span class="ui teal basic label" title="' . $title . '">' . number_format($value, 2, '.', ' ') .' '.Currencies::getShort($currency).'</span>';
    } else {
      $txt .= '<span class="ui orange basic label" title="Еще не поступлено">' . number_format($value, 2, '.', ' ') .' '.Currencies::getShort($currency).'</span>';
    }
    $txt .= '</div>';
    return $txt;
  }

  public function lprice($value, $zapolnen = null, $currency = 0) {
  	if ($value > 0) {
      $text = Podtverjdeno::label(Podtverjdeno::NET);
      $text .= '<div><span class="ui pointing red basic label small">Сумма: '.number_format($value, 2, '.', ' ').' '.Currencies::getShort($currency).'</span></div>';
      return $text;
    } elseif ($zapolnen == Zapolnen::YES) {
      return Podtverjdeno::label(Podtverjdeno::DA);
    } else {
      return Zapolnen::label(Zapolnen::NO);
    }
  }

  public function nprice($value, $currency = 0) {
    $txt = '<div class="tac">';
    if ($value > 0) {
      $txt .= '<span class="ui blue basic label" title="Объщая сумма продуктов">' . number_format($value, 2, '.', ' ') . ' '.Currencies::getShort($currency).'</span>';
    } else {
      $txt .= '<span class="ui red basic label" title="Продуктов не добавлены">' . number_format($value, 2, '.', ' ') . ' '.Currencies::getShort($currency).'</span>';
    }
    $txt .= '</div>';
    return $txt;
  }

  public function nbalance($value, $currency=0) {
    $txt = '<div class="tac">';
    if ($value > 0) {
      $txt .= '<span class="ui orange tag label" title="Требуется создать контракт (D&B)">' . number_format($value, 2, '.', ' ') .' '.Currencies::getShort($currency).'</span>';
    } else {
      $txt .= '<span class="ui teal tag label" title="Не требуется создать контракт (D&B)">' . number_format($value, 2, '.', ' ') .' '.Currencies::getShort($currency).'</span>';
    }
    $txt .= '</div>';
    return $txt;
  }

  public function conBalance($value, $currency=0) {
    $txt = '<div class="tac">';
    if ($value > 0) {
      $txt .= '<span class="ui orange tag label" title="Требуется создать контракт (D&B)">' . number_format($value, 2, '.', ' ') .' '.Currencies::getShort($currency).'</span>';
    } else {
      $txt .= '<span class="ui teal tag label" title="Не требуется создать контракт (D&B)">' . number_format($value, 2, '.', ' ') .' '.Currencies::getShort($currency).'</span>';
    }
    $txt .= '</div>';
    return $txt;
  }

  public function invBalance($value, $currency=0) {
    $txt = '<div class="tac">';
    if ($value > 0) {
      $txt .= '<span class="ui orange tag label" title="Требуется создать нивойс">' . number_format($value, 2, '.', ' ') .' '.Currencies::getShort($currency).'</span>';
    } else {
      $txt .= '<span class="ui teal tag label" title="Не требуется создать нивойс">' . number_format($value, 2, '.', ' ') .' '.Currencies::getShort($currency).'</span>';
    }
    $txt .= '</div>';
    return $txt;
  }

  public function ntannarx($value) {
    $txt = '<div class="tac">';
    if ($value > 0) {
      $txt .= '<span class="label label-success size-1" title="Объщые себестоимости продуктов">' . number_format($value, 2, '.', ' ') . '</span>';
    } else {
      $txt .= '<span class="label label-danger size-1" title="Продуктов не добавлены">' . number_format($value, 2, '.', ' ') . '</span>';
    }
    $txt .= '</div>';
    return $txt;
  }

  public function link($txt, $value='') {
    if ($value) {
      return '<a href="'.$value.'">'.$txt.'</a>';
    }
    return $txt;
  }

}
