<?php 

namespace app\components;

/**
 * Sign
 */
class Sign
{

	public static function sellerContractLTZ()
	{
		return '________________ S. Abdumalikov';
	}

	public static function buyerContractLTZ()
	{
		return '________________   Chris Kim';
	}

	public static function invoiceVT($value='')
	{
		switch ($value) {
			case 'signature':
				$result = '________________';
				break;
			case 'position':
				$result = 'Директор / Director';
				break;
			case 'name':
				$result = 'Миркамилов М.Т. / Mr. Mirkamilov M.T.';
				break;
			case 'place':
				$result = 'г. Ташкент, Узбекистан / Tashkent, Uzbekistan ';
				break;			
			default:
				$result = '';
				break;
		}
		return $result;
	}

	public static function invoiceLTZ($value='')
	{
		switch ($value) {
			case 'name':
				$result = 'Abdumalikov Sobidjon';
				break;	
			default:
				$result = '';
				break;
		}
		return $result;
	}

	public static function cominvoiceVT($value='')
	{
		return self::invoiceVT($value);
	}

	public static function cominvoiceLTZ($value='')
	{
		return self::invoiceLTZ($value);
	}

	public static function packinglistVT()
	{
		return 'M.T. MIRKAMILOV';
	}

	public static function packinglistLTZ($value='')
	{
		return '_____________________ M.T. MIRKAMILOV';
	}

}