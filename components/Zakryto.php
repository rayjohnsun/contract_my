<?php

namespace app\components;

/**
 * Zakryto
 */
class Zakryto {

  const NET = 1;
  const DA = 2;
  const MINUS = 3;

  public static function get($val = null) {

    $arr = [
      static::NET => 'Не закрыто',
      static::DA => 'Закрыто',
      static::MINUS => 'Не совпадение',
    ];

    if (!is_null($val)) {

      if (array_key_exists($val, $arr)) {

        return $arr[$val];

      }

      return null;

    }

    return $arr;

  }

  public static function getForVT($val = null) {

    $arr = [
      static::NET => 'Не закрыто',
      static::DA => 'Закрыто',
    ];

    if (!is_null($val)) {

      if (array_key_exists($val, $arr)) {

        return $arr[$val];

      }

      return null;

    }

    return $arr;

  }

  public static function getForD($val = null) {

    $arr = [
      static::NET => 'Не закрыто',
      static::DA => 'Закрыто',
    ];

    if (!is_null($val)) {

      if (array_key_exists($val, $arr)) {

        return $arr[$val];

      }

      return null;

    }

    return $arr;

  }

  public static function label($val, $title = '') {

    $arr = [
      static::NET => '<span class="label label-danger" title="' . $title . '">' . static::get(static::NET) . '</span>',
      static::DA => '<span class="label label-success" title="' . $title . '">' . static::get(static::DA) . '</span>',
      static::MINUS => '<span class="label label-danger" title="' . $title . '">' . static::get(static::MINUS) . '</span>',
    ];

    if (array_key_exists($val, $arr)) {

      return $arr[$val];

    }

    return '';

  }

  public static function list() {
    return [static::NET, static::DA, static::MINUS];
  }

  public static function textOfCLTZ($balance) {
    if ($balance > 0) {
      return '<span class="label label-warning2">' . static::get(static::NET) . '</span><br><span class="label label-warning2">Остаток: ' . $balance . '</span><br><span class="label label-warning2">Создайте еще инвойс LTZ</span>';
    } elseif ($balance == 0) {
      return '<span class="label label-success2">' . static::get(static::DA) . '</span>';
    } else {
      return '<span class="label label-danger">Не совподает количество продуктов</span><br><span class="label label-danger">пересохраните всех связанных инвойсов</span>';
    }
  }

  public static function textOf($balance) {
    if ($balance > 0) {
      return '<span class="label label-warning2">' . static::get(static::NET) . '</span><br><span class="label label-warning2">Остаток: ' . $balance . '</span><br><span class="label label-warning2">Создайте инвойс VT</span>';
    } elseif ($balance == 0) {
      return '<span class="label label-success2">' . static::get(static::DA) . '</span>';
    } else {
      return '<span class="label label-danger">Не совподает количество продуктов</span><br><span class="label label-danger">пересохраните всех связанных инвойсов VT</span>';
    }
  }

  public static function textOf2($balance) {
    if ($balance > 0) {
      return '<span class="label label-warning2">' . static::get(static::NET) . '</span><br><span class="label label-warning2">Остаток: ' . $balance . '</span><br><span class="label label-warning2">Создайте Packing List</span>';
    } elseif ($balance == 0) {
      return '<span class="label label-success2">' . static::get(static::DA) . '</span>';
    } else {
      return '<span class="label label-danger">Не совподает количество продуктов</span><br><span class="label label-danger">пересохраните все связанные Packing List</span>';
    }
  }

}