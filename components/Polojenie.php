<?php 

namespace app\components;

/**
 * Polojenie
 */
class Polojenie
{

	const HOLD 		= 0;
	const RECALL 	= 1;
	const APPROVE	= 2;
	const INACTIVE	= 3;

	public static function get($val = null)
	{

		$arr = [
			static::HOLD 	=> 'В ожидании', 
			static::RECALL 	=> 'Не полный',
			static::APPROVE	=> 'Готово',
			static::INACTIVE=> 'Не активный',
		];

		if (!is_null($val)) {

			if (array_key_exists($val, $arr)) {

				return $arr[$val];

			}

			return null;

		}

		return $arr;
		
	}

	public static function label($val)
	{

		$arr = [
			static::HOLD 	=> '<span class="label label-danger">Продуктов: '.static::get(static::HOLD).'</span>',
			static::RECALL 	=> '<span class="label label-warning">Продуктов: '.static::get(static::RECALL).'</span>',
			static::APPROVE => '<span class="label label-success">Продуктов: '.static::get(static::APPROVE).'</span>',
			static::INACTIVE=> '<span class="label label-danger">Контракт: '.static::get(static::INACTIVE).'</span>',
		];

		if (array_key_exists($val, $arr)) {

			return $arr[$val];

		}

		return '';
		
	}

	public static function list()
	{
		return [static::HOLD, static::RECALL, static::APPROVE, static::INACTIVE];
	}

}