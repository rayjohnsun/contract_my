<?php

namespace app\components;

/**
 * POInvoice
 */
class POInvoice {

  const YES = 1;
  const NO = 0;

  public static function get($val = null) {
    $arr = [
      static::YES => 'Purchase Order',
      static::NO => 'Обычный'
    ];
    if (!is_null($val)) {
      if (array_key_exists($val, $arr)) {
        return $arr[$val];
      }
      return null;
    }
    return $arr;
  }

  public static function label($val) {
    $arr = [
      static::YES => '<div><span class="ui tag teal label">' . static::get(static::YES) . '</span></div>',
      static::NO => '<div><span class="ui tag red label">' . static::get(static::NO) . '</span></div>'
    ];
    if (array_key_exists($val, $arr)) {
      return $arr[$val];
    }
    return '';
  }
  public static function list() {
    return [static::YES, static::NO];
  }
}