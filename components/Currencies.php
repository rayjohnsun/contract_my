<?php

namespace app\components;

/**
 * Currencies
 */
class Currencies {

  const USD = 0;
  const EUR = 1;

  public static function get($val = null) {

    $arr = [
      static::USD => 'Доллар США',
      static::EUR => 'Евро',
    ];

    if (!is_null($val)) {

      return isset($arr[$val]) ? $arr[$val] : null;

    }

    return $arr;

  }

  public static function getShort($val) {
    $arr = [
      static::USD => 'USD',
      static::EUR => 'EUR',
    ];
    if (isset($arr[$val])) {
      return $arr[$val];
    }
    return '';
  }

  public static function label($val) {

    $arr = [
      static::USD => '<span class="label label-default">' . static::get(static::USD) . '</span>',
      static::EUR => '<span class="label label-info">' . static::get(static::EUR) . '</span>',
    ];

    return isset($arr[$val]) ? $arr[$val] : '';

  }

  public static function list() {
    return [static::USD, static::EUR];
  }

  public static function textPrice($val = null) {
    $arr = [
      static::USD => 'Цена за ед. изм. в долларах США./Price per Unit in USD',
      static::EUR => 'Цена за ед. изм. в Евро./Price per Unit in EUR',
    ];

    return isset($arr[$val]) ? $arr[$val] : $arr;
  }

  public static function textTotalAmount($val = null) {
    $arr = [
      static::USD => 'Сумма в Долларах США Total amount in USD',
      static::EUR => 'Сумма в Евро Total amount in EUR',
    ];

    return isset($arr[$val]) ? $arr[$val] : $arr;
  }

  public static function textPriceInvoice($val = null) {
    $arr = [
      static::USD => 'UNIT PRICE IN USD',
      static::EUR => 'UNIT PRICE IN EUR',
    ];

    return isset($arr[$val]) ? $arr[$val] : '';
  }

  public static function textTotalPriceInvoice($val = null) {
    $arr = [
      static::USD => 'TOTAL PRICE IN USD',
      static::EUR => 'TOTAL PRICE IN EUR',
    ];

    return isset($arr[$val]) ? $arr[$val] : '';
  }

  public static function textTotalAmountInvoice($val = null) {
    $arr = [
      static::USD => 'TOTAL AMOUNT IN USD',
      static::EUR => 'TOTAL AMOUNT IN EUR',
    ];

    return isset($arr[$val]) ? $arr[$val] : '';
  }

  public static function textTsenaInvoice($val = null) {
    $arr = [
      static::USD => 'Цена за единицу (доллары США) / Unit price (USD)',
      static::EUR => 'Цена за единицу (Евро) / Unit price (EUR)',
    ];

    return isset($arr[$val]) ? $arr[$val] : '';
  }

  public static function textObshayaInvoice($val = null) {
    $arr = [
      static::USD => 'Общая сумма (доллары США) / Total price (USD)',
      static::EUR => 'Общая сумма (Евро) / Total price (EUR)',
    ];

    return isset($arr[$val]) ? $arr[$val] : '';
  }

}