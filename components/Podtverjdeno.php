<?php

namespace app\components;

/**
 * Podtverjdeno
 */
class Podtverjdeno {

  const DA = 1;
  const NET = 0;
  const EMPTY = -1;

  public static function get($val = null) {
    $arr = [
      static::DA => 'Подтвержден',
      static::NET => 'Не подтвержден',
      static::EMPTY => 'Не заполнен',
    ];
    if (!is_null($val)) {
      if (array_key_exists($val, $arr)) {
        return $arr[$val];
      }
      return null;
    }
    return $arr;
  }

  public static function label($val) {
    $arr = [
      static::DA => '<div><span class="ui tag teal label">' . static::get(static::DA) . '</span></div>',
      static::NET => '<div><span class="ui tag red label">' . static::get(static::NET) . '</span></div>',
      static::EMPTY => '<div><span class="ui tag orange label">' . static::get(static::EMPTY) . '</span></div>',
    ];
    if (array_key_exists($val, $arr)) {
      return $arr[$val];
    }
    return '';
  }
  public static function list() {
    return [static::DA, static::NET];
  }
}