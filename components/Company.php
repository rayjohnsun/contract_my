<?php 

namespace app\components;

/**
 * Company
 */
class Company
{

	const SELLER 	= 1;
	const BUYER 	= 2;
	const CONSIGNEE = 3;

	public static function get($val = null)
	{

		$arr = [
			self::SELLER 	=> 'Продавец', 
			self::BUYER 	=> 'Покупатель',
			self::CONSIGNEE => 'Грузополучатель',
		];

		if (!is_null($val)) {

			if (array_key_exists($val, $arr)) {

				return $arr[$val];

			}

			return null;

		}

		return $arr;
		
	}

}