<?php 

namespace app\components\export;

use app\components\Helper;

/**
 * EXW
 */
class EXW
{
	
	public static function text($html)
	{
		$html = Helper::decodeQuote($html);
		$text = strip_tags($html);
		$text = htmlspecialchars($text);
		return $text;
	}

	public static function normalize($html, $number = null)
	{	
		$html = trim($html);
		$html = Helper::decodeQuote($html);
		$html = str_replace(['</br>', '<br>', "\r", "\n"], '', $html);
		$html = str_replace('<p', '<div', $html);
		$html = str_replace('</p>', '</div>', $html);
		$html = str_replace('<div', '</div><div', $html);
		$html = '<div>'.$html.'</div>';
		$html = str_replace('<div></div>', '', $html);
		for ($i=0; $i < 10; $i++) { 
			$html = str_replace('</div></div>', '</div>', $html);
		}
		$html = trim($html);
		if (!empty($html)) {
			$html = explode('>', $html);
			foreach ($html as $k => $htm) {
				if ($k == 1) {
					$html[$k] = '<strong>'.$number.'</strong> '.$htm;
				}
			}
			$html = implode('>', $html);
			$html = str_replace('<div', '<p', $html);
			$html = str_replace('</div>', '</p>', $html);
		} else {
			$html = '<p><strong>'.$number.'</strong>&nbsp;&nbsp;</p>';
		}

		return $html;
	}

	public static function html($html)
	{	
		$html = trim($html);
		$html = Helper::decodeQuote($html);
		$html = str_replace('<div><br></div>', '<div>&nbsp;</div>', $html);
		$html = str_replace(['</br>', '<br>', "\r", "\n", "\t"], '', $html);
		$html = str_replace('<p', '<div', $html);
		$html = str_replace('</p>', '</div>', $html);
		$html = str_replace('<div', '</div><div', $html);
		$html = '<div>'.$html.'</div>';
		$html = str_replace('<div></div>', '', $html);
		for ($i=0; $i < 10; $i++) { 
			$html = str_replace('</div></div>', '</div>', $html);
		}
		$html = trim($html);
		if (!empty($html)) {
			$html = str_replace('<div>&nbsp;</div>', '<div></div>', $html);
			$html = str_replace('<div', '<p', $html);
			$html = str_replace('</div>', '</p>', $html);
		}

		return $html;
	}

}