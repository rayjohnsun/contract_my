<?php
namespace app\components;

class Zapolnen {

	const YES = 1;
	const NO = 0;

	public static function get($val = null) {
		$arr = [
			static::YES => 'ОК',
			static::NO => 'Не заполнен',
		];
		if (!is_null($val)) {
			if (array_key_exists($val, $arr)) {
				return $arr[$val];
			}
			return null;
		}
		return $arr;
	}

	public static function label($val) {
		$arr = [
			static::YES => '<span class="ui tag green label">' . static::get(static::YES) . '</span>',
			static::NO => '<span class="ui tag red label" title="Нужно либо заполнить либо удалить">' . static::get(static::NO) . '</span>',
		];
		if (array_key_exists($val, $arr)) {
			return $arr[$val];
		}
		return '';
	}

	public static function list() {
		return [static::YES, static::NO];
	}

}