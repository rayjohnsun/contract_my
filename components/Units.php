<?php 

namespace app\components;

/**
 * Units
 */
class Units
{

	const T 	= 'тн / t';
	const KG 	= 'кг / kg';
	const SHT 	= 'шт / PC';
	const ED 	= 'ед. / unit';

	public static function get()
	{

		$arr = [
			self::T 	=> self::T, 
			self::KG 	=> self::KG,
			self::SHT 	=> self::SHT,
			self::ED 	=> self::ED,
		];

		return $arr;
		
	}

	public static function first()
	{
		return self::T;
	}

}