<?php

namespace app\components\text;

use app\components\Helper;

/**
 * OBTxt
 */
class OBTxt {

  public static function noteEn($replaces = []) {
    $txt = '<div style="line-height: 17px;font-size: 15px;">
			<strong style="line-height: 17px;font-size: 15px;">Note: </strong>
			<span style="line-height: 17px;font-size: 15px;">&nbsp; </span>
		</div>
		<div style="line-height: 17px;font-size: 15px;">
			<strong style="line-height: 17px;font-size: 15px;">The manufacture: </strong>
			<strong class="editable_seller_en" style="line-height: 17px;font-size: 15px;">{{seller_en}} </strong>
			<span style="line-height: 17px;font-size: 15px;">&nbsp; </span>
		</div>
		<div style="line-height: 17px;font-size: 15px;">
			<strong style="line-height: 17px;font-size: 15px;">Country origin  оf the goods: Republic of Uzbekistan </strong>
			<span style="line-height: 17px;font-size: 15px;">&nbsp; </span>
		</div>
		<div style="line-height: 17px;font-size: 15px;">
			<strong style="line-height: 17px;font-size: 15px;">H.S. Code of Republic of Uzbekistan: 3404900009 </strong>
			<span style="line-height: 17px;font-size: 15px;">&nbsp; </span>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function noteRu($replaces = []) {
    $txt = '<div style="line-height: 17px;font-size: 15px;">
			<strong style="line-height: 17px;font-size: 15px;">Примечание: </strong>
			<span style="line-height: 17px;font-size: 15px;">&nbsp; </span>
		</div>
		<div style="line-height: 17px;font-size: 15px;">
			<strong style="line-height: 17px;font-size: 15px;">Производитель: </strong>
			<strong class="editable_seller_en" style="line-height: 17px;font-size: 15px;">{{seller_ru}} </strong>
			<span style="line-height: 17px;font-size: 15px;">&nbsp; </span>
		</div>
		<div style="line-height: 17px;font-size: 15px;">
			<strong style="line-height: 17px;font-size: 15px;">Страна происхождения товара: Республика Узбекистан </strong>
			<span style="line-height: 17px;font-size: 15px;">&nbsp; </span>
		</div>
		<div style="line-height: 17px;font-size: 15px;">
			<strong style="line-height: 17px;font-size: 15px;">Код ТН ВЭД РУЗ: 3404900009 </strong>
			<span style="line-height: 17px;font-size: 15px;">&nbsp; </span>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invOrgEn($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">
			<strong style="line-height: 20px;font-size:14px;">THE SELLER: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_seller_en">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Address: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_adr_en">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Phone: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">E-mail address: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invOrgRu($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">
			<strong style="line-height: 20px;font-size:14px;">ПРОДАВЕЦ: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_seller_ru">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Адрес: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_adr_ru">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Тел.: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Адрес эл. почты: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invBuyEn($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">
			<strong style="line-height: 20px;font-size:14px;">THE BUYER: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_buyer_en">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Address: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_adr_en">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Phone: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">E-mail address: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invBuyRu($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">
			<strong style="line-height: 20px;font-size:14px;">ПОКУПАТЕЛЬ: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_buyer_ru">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Адрес: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_adr_ru">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Тел.: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Адрес эл. почты: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invConsEn($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;" class="nred">
			<strong style="line-height: 20px;font-size:14px;">THE CONSIGNEE: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_cons_text_en">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Address: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_adr_en">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Phone: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">E-mail address: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;" class="nred">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invConsRu($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;" class="nred">
			<strong style="line-height: 20px;font-size:14px;">ГРУЗОПОЛУЧАТЕЛЬ: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_cons_text_ru">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Адрес: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_adr_ru">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Тел.: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Адрес эл. почты: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;" class="nred">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invNotes($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">Примечание / Notes:&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Условие оплаты: 100% предоплата прямым банковским переводом / Payment terms: 100% prepayment  Diret Bank Transfer&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Цена указана на условиях: {{region_ru}}/ Price is given on terms of: {{region_en}}&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size: 14px;">Номер и дата Контракта: </span>
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number nred">{{C_NUM}}</span>
			<span style="line-height: 20px;font-size: 14px;"> от </span>
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number_ndate nred">{{C_DATE}}</span>
			<span style="line-height: 20px;font-size: 14px;"> / Number and date of a contract: </span>
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number nred">{{C_NUM}}</span>
			<span style="line-height: 20px;font-size: 14px;"> dated on </span>
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number_ndate nred">{{C_DATE}}</span>
			<span style="line-height: 20px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">Место происхождения товара: Республика Узбекистан / Place of origin of the goods: Republic of  Uzbekistan&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">
			<br>
		</div>
		<div style="line-height: 20px;font-size: 14px;">Подтверждаю, что все указанное верно / I declare that the above information is true and correct.&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">
			<br>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function comInvNotes($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">Примечание / Notes:&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Условие оплаты: 100% предоплата прямым банковским переводом / Payment terms: 100% prepayment  Diret Bank Transfer&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Цена указана на условиях: {{region_ru}}/ Price is given on terms of: {{region_en}}&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size: 14px;">Номер и дата Контракта: </span>
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number nred">{{C_NUM}}</span>
			<span style="line-height: 20px;font-size: 14px;"> от </span>
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number_ndate nred">{{C_DATE}}</span>
			<span style="line-height: 20px;font-size: 14px;"> / Number and date of a contract: </span>
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number nred">{{C_NUM}}</span>
			<span style="line-height: 20px;font-size: 14px;"> dated on </span>
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number_ndate nred">{{C_DATE}}</span>
			<span style="line-height: 20px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">Место происхождения товара: Республика Узбекистан / Place of origin of the goods: Republic of  Uzbekistan&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">
			<br>
		</div>
		<div style="line-height: 20px;font-size: 14px;">Детали банка/Bank details:&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">{{company_en}}&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Sergeli branch JSB «KAPITALBANK»&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Bank address: Republic of Uzbekistan, Tashkent city, Sergeli district, Yangi Sergeli str., 25&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Account (USD): 20208840104705888004&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Acc. number (EURO): 20208978504705888004&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">TIN: 300836747&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">SWIFT: KACHUZ22&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Beneficiary: {{company_ru}}&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">Подтверждаю, что все указанное верно / I declare that the above information is true and correct.&nbsp; </div>
		<div style="line-height: 20px;font-size: 14px;">
			<br>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function packNotes($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size: 14px;">Номер и дата Контракта / Number and date of a contract:</span> 
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number nred">{{C_NUM}}</span> 
			<span style="line-height: 20px;font-size: 14px;"> dated on </span> 
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number_ndate nred">{{C_DATE}}</span>
		</div>
		<div>
			<span style="line-height: 20px;font-size: 14px;">Количество поддонов (шт.) / Quantity of pallets (piece) – </span> 
			<span style="line-height: 20px;font-size: 14px;" class="editable_poddon nred">&nbsp;</span> 
			<span style="line-height: 20px;font-size: 14px;"> (Wooden pallets) </span> 
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<br>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

}