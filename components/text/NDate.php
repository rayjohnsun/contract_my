<?php

namespace app\components\text;

/**
 * NDate
 */
class NDate {

  protected static $en_moths = [
    1 => 'January',
    2 => 'February',
    3 => 'March',
    4 => 'April',
    5 => 'May',
    6 => 'June',
    7 => 'July',
    8 => 'August',
    9 => 'September',
    10 => 'October',
    11 => 'November',
    12 => 'December',
  ];

  protected static $ru_moths = [
    1 => 'Январь',
    2 => 'Февраль',
    3 => 'Март',
    4 => 'Апрель',
    5 => 'Май',
    6 => 'Июнь',
    7 => 'Июль',
    8 => 'Августь',
    9 => 'Сентябрь',
    10 => 'Октябрь',
    11 => 'Ноябрь',
    12 => 'Декабрь',
  ];

  public static function en($value = 'now') {
    $day = date('d', strtotime($value));
    $month = (int) date('m', strtotime($value));
    $mword = isset(self::$en_moths[$month]) ? self::$en_moths[$month] : '';
    $year = date('Y', strtotime($value));
    return $day . ' ' . $mword . ', ' . $year;
  }

  public static function ru($value = 'now') {
    $day = date('d', strtotime($value));
    $month = (int) date('m', strtotime($value));
    $mword = isset(self::$ru_moths[$month]) ? self::$ru_moths[$month] : '';
    $year = date('Y', strtotime($value));
    return $day . ' ' . $mword . ', ' . $year;
  }

  public static function get($value = 'now') {
    $date = date('d.m.Y', strtotime($value));
    return $date;
  }

  public function getDay($value = null) {
    if (!empty($value)) {
      $day = date("d", strtotime($value));
      return $day;
    }
    return '';
  }

  public function getMonth($value = null) {
    if (!empty($value)) {
      $month = (int) date("m", strtotime($value));
      $months = self::$ru_moths;
      return isset($months[$month]) ? $months[$month] : '';
    }
    return '';
  }

  public function getYear($value = null) {
    if (!empty($value)) {
      $year = (int) date("Y", strtotime($value));
      return $year . ' г.';
    }
    return '';
  }

}