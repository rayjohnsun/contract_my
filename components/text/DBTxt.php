<?php

namespace app\components\text;

use app\components\Helper;

/**
 * DBTxt
 */
class DBTxt {

  public static function noteEn($replaces = []) {
    $txt = '<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">Note: </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">The manufacture: </strong>
			<strong class="editable_seller_en" style="line-height: 15px;font-size: 14px;">{{seller_en}} </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">Country origin  оf the goods: Uzbekistan </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">HS Code: 3404900000 </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">Prices are given in USD </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function noteRu($replaces = []) {
    $txt = '<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">Примечание: </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">Производитель: </strong>
			<strong class="editable_seller_en" style="line-height: 15px;font-size: 14px;">{{seller_ru}} </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">Страна происхождения товара: Узбекистан </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">Код ТН ВЭД: 3404900000 </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size: 14px;">
			<strong style="line-height: 15px;font-size: 14px;">Цены указаны в долларах США </strong>
			<span style="line-height: 15px;font-size: 14px;">&nbsp; </span>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invDilEn($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 18px;">
			<strong style="line-height: 20px;font-size:18px;">Seller: </strong>
		</div>
		<div style="line-height: 20px;font-size: 18px;">
			<strong style="line-height: 20px;font-size:18px;" class="editable_seller_en">{{company}} </strong>
			<span style="line-height: 20px;font-size:18px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size:13px;">
			<span style="line-height: 20px;font-size:13px;">Address: </span>
			<span style="line-height: 20px;font-size:13px;" class="editable_seller_adr_en">{{address}} </span>
			<span style="line-height: 20px;font-size:13px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size:13px;">
			<span style="line-height: 20px;font-size:13px;">Phone: </span>
			<span style="line-height: 20px;font-size:13px;" class="editable_seller_adr_en">{{phone}} </span>
			<span style="line-height: 20px;font-size:13px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size:13px;">
			<span style="line-height: 20px;font-size:13px;">E-mail address: </span>
			<span style="line-height: 20px;font-size:13px;" class="editable_seller_adr_en">{{email}} </span>
			<span style="line-height: 20px;font-size:13px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size:13px;">
			<br>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invDilRu($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">
			<strong style="line-height: 20px;font-size:14px;">ПРОДАВЕЦ: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_seller_ru">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Адрес: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_adr_ru">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Тел.: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Адрес эл. почты: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_seller_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size:13px;">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invBuyEn($replaces = []) {
    $txt = '<div class="nred" style="line-height: 20px;font-size: 14px;">
			<strong style="line-height: 20px;font-size: 14px;">BUYER: </strong>
			<span style="line-height: 20px;font-size: 14px;">&nbsp; </span>
		</div>
		<div class="nred" style="line-height: 20px;font-size: 14px;">
			<strong class="editable_buyer_en" style="line-height: 20px;font-size: 14px;">{{company}} </strong>
			<span style="line-height: 20px;font-size: 14px;">&nbsp; </span>
		</div>
		<div class="nred" style="line-height: 20px;font-size: 13px;">
			<span style="line-height: 20px;font-size: 13px;">Address: </span>
			<span class="editable_buyer_adr_en" style="line-height: 20px;font-size: 13px;">{{address}} </span>
			<span style="line-height: 20px;font-size: 13px;">&nbsp; </span>
		</div>
		<div class="nred" style="line-height: 20px;font-size: 13px;">
			<span style="line-height: 20px;font-size: 13px;">Phone: </span>
			<span class="editable_buyer_tel" style="line-height: 20px;font-size: 13px;">{{phone}} </span>
			<span style="line-height: 20px;font-size: 13px;">&nbsp; </span>
		</div>
		<div class="nred" style="line-height: 20px;font-size: 13px;">
			<span style="line-height: 20px;font-size: 13px;">E-mail address: </span>
			<span class="editable_buyer_email" style="line-height: 20px;font-size: 13px;">{{email}} </span>
			<span style="line-height: 20px;font-size: 13px;">&nbsp; </span>
		</div>
		<div class="nred" style="line-height: 20px;font-size: 13px;">
			<br>
		</div>
		<div class="nred" style="line-height: 20px;font-size: 13px;">
			<br>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invBuyRu($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">
			<strong style="line-height: 20px;font-size:14px;">ПОКУПАТЕЛЬ: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_buyer_ru">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Адрес: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_adr_ru">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Тел.: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size:14px;">Адрес эл. почты: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_buyer_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invConsEn($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;" class="nred">
			<strong style="line-height: 20px;font-size:14px;">THE CONSIGNEE: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_cons_text_en">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Address: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_adr_en">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Phone: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">E-mail address: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;" class="nred">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function invConsRu($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;" class="nred">
			<strong style="line-height: 20px;font-size:14px;">ГРУЗОПОЛУЧАТЕЛЬ: </strong>
			<strong style="line-height: 20px;font-size:14px;" class="editable_cons_text_ru">{{company}} </strong>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Адрес: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_adr_ru">{{address}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Тел.: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_tel">{{phone}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 20px;font-size: 14px;" class="nred">
			<span style="line-height: 20px;font-size:14px;">Адрес эл. почты: </span>
			<span style="line-height: 20px;font-size:14px;" class="editable_consignee_email">{{email}} </span>
			<span style="line-height: 20px;font-size:14px;">&nbsp; </span>
		</div>
		<div style="line-height: 15px;font-size:13px;" class="nred">
			<br/>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  // public static function invNotes($replaces = []) {
  //   $txt = '<div style="line-height: 20px;font-size: 14px;">Примечание / Notes:&nbsp; </div>
		// <div style="line-height: 20px;font-size: 14px;">Условие оплаты: 100% предоплата прямым банковским переводом / Payment terms: 100% prepayment  Diret Bank Transfer&nbsp; </div>
		// <div style="line-height: 20px;font-size: 14px;">Цена указана на условиях: {{region_ru}}/ Price is given on terms of: {{region_en}}&nbsp; </div>
		// <div style="line-height: 20px;font-size: 14px;">
		// 	<span style="line-height: 20px;font-size: 14px;">Номер и дата Контракта: </span>
		// 	<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number nred">{{C_NUM}}</span>
		// 	<span style="line-height: 20px;font-size: 14px;"> от </span>
		// 	<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number_ndate nred">{{C_DATE}}</span>
		// 	<span style="line-height: 20px;font-size: 14px;"> / Number and date of a contract: </span>
		// 	<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number nred">{{C_NUM}}</span>
		// 	<span style="line-height: 20px;font-size: 14px;"> dated on </span>
		// 	<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number_ndate nred">{{C_DATE}}</span>
		// 	<span style="line-height: 20px;font-size: 14px;">&nbsp; </span>
		// </div>
		// <div style="line-height: 20px;font-size: 14px;">Место происхождения товара: Республика Узбекистан / Place of origin of the goods: Republic of  Uzbekistan&nbsp; </div>
		// <div style="line-height: 20px;font-size: 14px;">
		// 	<br>
		// </div>
		// <div style="line-height: 20px;font-size: 14px;">Подтверждаю, что все указанное верно / I declare that the above information is true and correct.&nbsp; </div>
		// <div style="line-height: 20px;font-size: 14px;">
		// 	<br>
		// </div>';
		// if (!empty($replaces)) {
		// 	$txt = str_replace(array_keys($replaces), $replaces, $txt);
		// }
  //   return Helper::convert($txt);
  // }

  public static function invNotes($replaces = []) {
    $txt = '<div style="line-height: 16px;font-size: 13px;">Notes:</div>
		<div style="line-height: 16px;font-size: 13px;">HS code: 3404900000</div>
		<div style="line-height: 16px;font-size: 13px;">Prices are given in {{CURRENCY}} </div>
		<div style="line-height: 16px;font-size: 13px;">Payment terms 100% payment within 10 banking days after invoice date</div>
		<div style="line-height: 16px;font-size: 13px;">Prices are given on terms of CFR Qingdao port</div>
		<div style="line-height: 16px;font-size: 13px;">Place of origin of the goods: {{REGION_EN}} </div>
		<div style="line-height: 16px;font-size: 13px;">
			<br>
		</div>
		<div style="line-height: 16px;font-size: 13px;">I declare that the above information is true and correct to the best of my knowledge. </div>
		<div style="line-height: 16px;font-size: 13px;">
			<br>
		</div>
		<div style="line-height: 16px;font-size: 13px;">Bank details:</div>
		<div style="line-height: 16px;font-size: 13px;">
			<span style="line-height: 16px;font-size: 13px;" class="editable_seller_en">{{SELLER}} </span>
		</div>
		<div style="line-height: 16px;font-size: 13px;">Corporate ID 3774</div>
		<div style="line-height: 16px;font-size: 13px;">NBAD (NBAD is a trademark owned by First Abu Dhabi Bank PJSC)</div>
		<div style="line-height: 16px;font-size: 13px;">DEIRA City center branch</div>
		<div style="line-height: 16px;font-size: 13px;">Address: Abu Baker Al Siddique str.</div>
		<div style="line-height: 16px;font-size: 13px;">SWIFT CODE: NBADAEAA</div>
		<div style="line-height: 16px;font-size: 13px;">IBAN: AE6003 5000 0006 2077 32492 ({{CURRENCY}})</div>
		<div style="line-height: 16px;font-size: 13px;">CIF 8036821569</div>
		<div style="line-height: 16px;font-size: 13px;">ACCOUNT NO: 6207732492 ({{CURRENCY}})</div>
		<div style="line-height: 16px;font-size: 13px;">Bank correspondent: Deutsche Bank Trust Company Americas</div>
		<div style="line-height: 16px;font-size: 13px;">SWIFT: BKTRUS33</div>
		<div style="line-height: 16px;font-size: 13px;">
			<br>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

  public static function packNotes($replaces = []) {
    $txt = '<div style="line-height: 20px;font-size: 14px;">
			<span style="line-height: 20px;font-size: 14px;">Номер и дата Контракта / Number and date of a contract:</span> 
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number nred">{{C_NUM}}</span> 
			<span style="line-height: 20px;font-size: 14px;"> dated on </span> 
			<span style="line-height: 20px;font-size: 14px;" class="editable_contract_number_ndate nred">{{C_DATE}}</span>
		</div>
		<div>
			<span style="line-height: 20px;font-size: 14px;">Количество поддонов (шт.) / Quantity of pallets (piece) – </span> 
			<span style="line-height: 20px;font-size: 14px;" class="editable_poddon nred">&nbsp;</span> 
			<span style="line-height: 20px;font-size: 14px;"> (Wooden pallets) </span> 
		</div>
		<div style="line-height: 20px;font-size: 14px;">
			<br>
		</div>';
		if (!empty($replaces)) {
			$txt = str_replace(array_keys($replaces), $replaces, $txt);
		}
    return Helper::convert($txt);
  }

}