<?php 

namespace app\components\text;

/**
 * Adres
 */
class Adres
{

	const TASHKENT_EN 	= 'Tashkent';
	const TASHKENT_RU 	= 'Ташкент';
	const DUBAI_EN 		= 'Dubai';
	const DUBAI_RU 		= 'Дубаи';

}