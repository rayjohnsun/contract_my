<?php 

namespace app\components;

/**
 * NNumber
 */
class Shablon
{

	const VT 	= 'VT';
	const LTZ 	= 'LTZ';
	const LI 	= 'LI';
	const LPL 	= 'LPL';

	const CODE1 = '{code}-{id}-{year}';
	const CODE2 = '{code}-{contract}-{id}-{year}';

	public static function get($val = null)
	{

		$arr = [
			self::VT => self::VT,
			self::LTZ=> self::LTZ,
			self::LI => self::LI,
			self::LPL=> self::LPL,
		];

		if (!is_null($val)) {

			if (array_key_exists($val, $arr)) {

				return $arr[$val];

			}

			return null;

		}

		return $arr;

	}

	public static function createNumber($shablone, $params = [])
	{
		$code 		= isset($params['code']) ? $params['code'] : static::VT;
		$id 		= isset($params['id']) ? static::numFormat($params['id']) : '__';
		$contract 	= isset($params['contract']) ? static::numFormat($params['contract']) : '__';
		$year 		= date('Y');

		$search 	= ['{code}', '{id}', '{contract}', '{year}'];
		$replace 	= [$code, $id, $contract, $year];

		return str_replace($search, $replace, $shablone);
	}

	public static function numFormat($num)
	{
		$num = (int)$num;
		if ($num < 10) {
			return '0'.$num;
		}
		return $num;
	}

	public static function getNumber($code, $id, $contract='')
	{

		$shablon 	= ($code == self::VT OR $code == self::LTZ) ? self::CODE1 : self::CODE2;
		$num 		= $id < 10 ? '0'.$id : $id;
		if (!empty($contract) AND $contract > 0) {
			if ($contract < 10) {
				$contract = '0'.$contract;
			}
		} else {
			$contract = '(NONE)';
		}
		$year 		= date('Y');

		$search 	= ['{code}', '{id}', '{contract}', '{year}'];
		$replace 	= [$code, $num, $contract, $year];

		return str_replace($search, $replace, $shablon);

	}

	public static function getMask($code, $replace_tere = true)
	{
		
		$shablon 	= ($code == self::VT OR $code == self::LTZ) ? self::CODE1 : self::CODE2;
		$num 		= '__';
		$contract 	= '__';
		$year 		= date('Y');

		if ($replace_tere === true) {
			$search 	= ['{code}', '{id}', '{contract}', '{year}'];
			$replace 	= [$code, $num, $contract, $year];
		} elseif($replace_tere === false) {
			$search 	= ['{code}', '{year}'];
			$replace 	= [$code, $year];
		} elseif (is_numeric($replace_tere) AND $replace_tere > 0) {
			$search 	= ['{code}', '{id}', '{contract}', '{year}'];
			$replace 	= [$code, $num, $replace_tere, $year];
		} else {
			$search 	= ['{code}', '{year}'];
			$replace 	= [$code, $year];
		}

		return str_replace($search, $replace, $shablon);

	}

}
