<?php 

namespace app\components;

class NumberShablones {

	public static function year() {
		$year = date('Y');
		return $year;
	}
	
	public static function get($num, $type) {
		if ($num < 10) {
			$num = '0'.$num;
		}
		$val = $type.'-{num}-{year}';
		return str_replace(['{num}', '{year}'], [$num, self::year()], $val);
	}

}