<?php
namespace app\components;

use Yii;

class Helper {
  public static function numbers($from = 0, $to = 30) {
    $numbers = [];
    for ($i = $from; $i <= $to; $i++) {
      $numbers[$i] = $i;
    }
    return $numbers;
  }

  public static function dt($dt) {
    if (!empty($dt)) {
      $dt1 = explode(' ', $dt);
      if (isset($dt1[1]) AND strlen($dt1[0]) != 10) {
        unset($dt1[1]);
      }
      $dt2 = explode('.', $dt1[0]);
      if (count($dt2) > 1) {
        $dt3 = array_reverse($dt2);
        $dt = implode('-', $dt3);
        if (isset($dt1[1])) {
          $dt .= ' ' . $dt1[1];
        }
      }
      return $dt;
    }
    return '';
  }

  public static function message($model) {
    $txt = '<ul>';
    foreach ($model->errors as $key => $value) {
      if (!empty($value)) {
        foreach ($value as $key2 => $value2) {
          $val3 = explode('<br>', $value2);
          foreach ($val3 as $kk => $vv) {
            $txt .= '<li>' . $vv . '</li>';
          }
        }
      }
    }
    $txt .= '</ul>';
    return $txt;
  }

  public static function encodeQuote($value = '') {
    $value = trim($value);
    return str_replace('"', '&quot;', $value);
  }

  public static function decodeQuote($value = '') {
    $value = trim($value);
    return str_replace('&quot;', '"', $value);
  }

  public static function trimEnters($value = "") {
    $value = str_replace(PHP_EOL, "", $value);
    $value = str_replace(["\r\n", "\r", "\n", "\t"], "", $value);
    return $value;
  }

  public static function convert($value = '') {
    $value = self::trimEnters($value);
    $value = self::encodeQuote($value);
    return $value;
  }

  public static function showMessage() {
    $txt = '';
    if (Yii::$app->session->hasFlash('message')) {
      $txt .= '<div class="alert alert-danger">';
      $txt .= Yii::$app->session->getFlash('message');
      $txt .= '</div>';
    }
    return $txt;
  }

  public static function template() {
    return '<div class="b_style">{update}&nbsp;&nbsp;&nbsp;{delete}</div>';
  }

  public static function brs($arr = [], $product = true) {
    if (!empty($arr)) {
      $brs = -1;
      foreach ($arr as $key => $value) {
        if ($product == true) {
          $cnt = count($value->products);
          if ($cnt > 0) {
            $brs+= $cnt;
          } else {
            $brs++;
          }
        } else {
          $brs++;
        }
      }
      if ($brs > 0) {
        $br = '';
        for ($i=0; $i < $brs; $i++) { 
          $br .= '<div role="td" style="border-color:transparent;">&nbsp;&nbsp;&nbsp;</div>';
        }
        return $br;
      }
    }
    return '';
  }

  public static function replacer($txt, $replaces) {
    if (!empty($txt) && is_array($replaces) && !empty($replaces)) {
      return str_replace(array_keys($replaces), $replaces, $txt);
    }
    return $txt;
  }
}